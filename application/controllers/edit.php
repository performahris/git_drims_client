<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Edit extends CI_Controller {
    
    function __construct()
    {
        parent::__construct();

		$this->load->config('rest');
		$this->load->spark('restclient/2.0.0');		
		$this->rest->initialize(array('server' => 'http://localhost:81/drims_server/hr_edit/'));
    }
    
    function _view( $template = '', $param = '')
	{
        $this->load->view('core/header');
        $this->load->view('core/menu');
        $this->load->view($template, $param);
        $this->load->view('core/footer');
    }

    //EMPLOYEEMENT
    function employee_edit()
	{
		$this->_view('edit/employeement/employee_edit');
	}
	function warning_report_edit()
	{
		$this->_view('edit/employeement/warning_report_edit');
	}

	//PERSONNEL SCHEDULE
	function adjustment_edit()
	{
		$this->_view('edit/personnel_schedule/adjustment_edit');
	}
    
    //LOAN
	function loan_requested_edit()
	{
		$this->_view('edit/loan/loan_requested_edit');
	}

    //EXIT CLEARANCE
	function interview_edit()
	{
		$this->_view('edit/exit_clearance/interview_edit');
	}
	
	function clearance_checklist_edit()
	{
		$this->_view('edit/exit_clearance/clearance_checklist_edit');
	}

	//SPPD
    function sppd_requested_edit()
	{
		$this->_view('edit/sppd/sppd_edit');
	}

	//LEAVE REQUEST
	function leave_request_edit(){
		$this->_view('edit/leave_request/leave_request_edit');	
	}

	//IZIN
	function ijin_requested_edit()
	{
		$this->_view('edit/ijin/ijin_requested_edit');
	}
	
	//BOOKING ROOM
	function booking_room_requested_edit()
	{
		$this->_view('edit/booking_room/booking_room_requested_edit');
	}

	//BENEFIT MANAGEMENT
	function benefit_sheet_edit()
	{
		$this->_view('edit/benefit_management/benefit_sheet_edit');
	}

	//ANNOUNCEMENT
	function announcement_submision_edit()
   	{
		$this->_view('edit/announcement/announcement_submision_edit');
	}
	
	//TUNJANGAN 
	function tunjangan_requested_edit()
	{
		$this->_view('edit/tunjangan/tunjangan_requested_edit');	
	}

	//MAN POWER REQUESTED
	function edit_man_power_requested()
	{
		$this->_view('edit/man_power_requested/man_power_edit');
	}
	function edit_man_power_requested_open()
	{
			$this->_view('edit/man_power_requested/man_power_edit_open');
	}
	function edit_man_power_requested_closed()
	{
		$this->_view('edit/man_power_requested/man_power_edit_closed');
	}
	
    //HOLIDAY
	function holiday_edit()
	{
		$this->_view('edit/holiday_management/holiday');
	}

	//RECRUITMENT
	function vacancy_edit()
	{
		$this->_view('edit/recruitment/vacancy_edit');
	}
	function vacancy_trainer_edit_1()
	{
		$this->_view('edit/recruitment/applicant/vacancy_trainer_edit_1');
	}

	function vacancy_trainer_edit_2()
	{
		$this->_view('edit/recruitment/applicant/vacancy_trainer_edit_2');
	}

	function vacancy_trainer_edit_3()
	{
		$this->_view('edit/recruitment/applicant/vacancy_trainer_edit_3');
	}

	function vacancy_trainer_edit_4()
	{
		$this->_view('edit/recruitment/applicant/vacancy_trainer_edit_4');
	}

	function vacancy_trainer_edit_5()
	{
		$this->_view('edit/recruitment/applicant/vacancy_trainer_edit_5');
	}
	/*
	function departement_edit($id)
    {
		$data['data'] = $this->rest->get('edit_departement/id/'.$id);
		$this->_view('edit/departement_edit',$data);
	}
	function departement_edit()
    {
		$this->_view('edit/departement_edit');
	}

	function tunjangan()
	{
		$this->_view('edit/tunjangan_edit');
	}

	function sppd_edit()
	{
		$this->_view('edit/sppd_edit');
	}

	function leave_type()
	{
		$this->_view('edit/leave_type');
	}

	function ijin_pulang_cepat()
	{
		$this->_view('edit/ijin_pulang_cepat');
	}
	
	function employee_edit_karyawan()
	{
		$this->_view('edit/employee_edit_menu/employee_edit_karyawan');
	}

	function employee_edit_keluarga()
	{
		$this->_view('edit/employee_edit_menu/employee_edit_keluarga');
	}

	/*public function departement_edit_action(){
		$id = $this->input->post('id');
		$data = array(
			'departement_id' => $this->input->post('id'), 'departement_name' => $this->input->post('name')
		);
		$query = $this->rest->post('edit_departement/id/'.$id.'/format/php', $data);
		if($query){
			redirect('dashboard/departement_list');
		} else {
			echo "<script>alert('Gagal coy'); window.close ();</script>";
		}
	}

	function depatement_edit()
	{
		$this->_view('edit/departement_edit');
	}

	function ijin_pulang_cepat2()
	{
		$this->_view('edit/ijin_pulang_cepat2');
	}

	function bonus_sheet_edit()
	{
		$this->_view('edit/bonus_sheet');
	}
	*/
}