<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Add extends CI_Controller {
    
    function __construct()
    {
        parent::__construct();

		$this->load->config('rest');
		$this->load->spark('restclient/2.0.0');		
		$this->rest->initialize(array('server' => 'http://localhost:81/drims_server/hr_add/'));
    }
    
    function _view( $template = '', $param = '')
	{
        $this->load->view('core/header');
        $this->load->view('core/menu');
        $this->load->view($template, $param);
        $this->load->view('core/footer');
    }
	
	//EMPLOYEE
	function employee_new()
	{
		$this->_view('add/employeement/employee_register');
	}
	function warning_report_add()
	{
		$this->_view('add/employeement/warning_report_add');
	}
	function surat_keterangan_kerja_register()
    {
       $this->_view('add/employeement/surat_keterangan_kerja_register');
    }
	function surat_keterangan_pengalaman_kerja_register()
    {
       $this->_view('add/employeement/surat_keterangan_pengalaman_kerja_register');
    }

	//LOAN
	function loan_requested_add()
	{
		$this->_view('add/loan/loan_requested_add');
	}

	//EXIT CLEARANCE
	function interview_add()
	{
		$this->_view('add/exit_clearance/interview_add');
	}
	function clearance_checklist_add()
	{
		$this->_view('add/exit_clearance/clearance_checklist_add');
	}

	//SPPD
    function sppd_requested_add()
	{
		$this->_view('add/sppd/sppd_creation');
	}
	 function sppd_realisasi_creation()
	{
		$this->_view('add/sppd/realisasi_creation');
	}
	
	
	//IJIN
	function ijin_requested_add()
	{
		$this->_view('add/ijin/ijin_requested_add');
	}
	
	//LEAVE REQUEST
	function leave_requested_add(){
		$this->_view('add/leave_request/leave_requested_add');	
	}

	//TRAINING
	function training_approval()
	{
		$this->_view('add/training/training_approval');
	}
	//BOOKING ROOM
	function booking_room_requested_add()
	{
		$this->_view('add/booking_room/booking_room_requested_add');
	}

	//ANNOUNCEMENT
	function announcement_submision_add()
    {
		$this->_view('add/announcement/announcement_submision_add');
	}

	//TUNJANGAN
	function tunjangan_requested_add()
	{
		$this->_view('add/tunjangan/tunjangan_requested_add');	
	}
	
	//HOLIDAY MANAGEMENT
    	function holiday_management()
	{
		$this->_view('add/holiday_management/holiday');
	}

	//BENEFIT MANAGEMENT
    	function payroll_add()
	{
		$this->_view('add/benefit_management/payroll_add');
	}
	function benefit_sheet_create()
    {
    	$this->_view('add/benefit_management/benefit_sheet_create');
    }

	//MAN POWER REQUEST
	function add_man_power_requested()
	{
		$this->_view('add/man_power_request/add_man_power_requested');
	}

	//RECRUITMENT
	function vacancy_add()
	{
		$this->_view('add/recruitment/vacancy_add');
	}

	function vacancy_trainer_add_1()
	{
		$this->_view('add/recruitment/applicant/vacancy_trainer_add_1');
	}
	function vacancy_trainer_add_2()
	{
		$this->_view('add/recruitment/applicant/vacancy_trainer_add_2');
	}
	function vacancy_trainer_add_3()
	{
		$this->_view('add/recruitment/applicant/vacancy_trainer_add_3');
	}
	function vacancy_trainer_add_4()
	{
		$this->_view('add/recruitment/applicant/vacancy_trainer_add_4');
	}
	function vacancy_trainer_add_5()
	{
		$this->_view('add/recruitment/applicant/vacancy_trainer_add_5');
	}


	//LEAVE
	/*
	function leave_type()
	{
		$this->_view('add/leave/leave_type');
	}
	function leave_request()
	{
		$this->_view('add/leave/leave_request');
	}
	*/


	/*
	function ijin_approval () {
		$this->_view('add/ijin_approval');
	}
	
	function departement_register()
	{
		$this->_view('add/departement_register');
	}
	
	public function departement_register_add(){
		$data = array(
			'departement_id' => $this->input->post('departement_id'),
			'departement_name' => $this->input->post('departement_name')
		);
		
		$query = $this->rest->post('departement_register_add/svaha/1/format/php', $data);
		if($query) {
			redirect('dashboard/departement_list');
		} else {
			echo "<script>alert('Terjadi Error Saat Query')</script>";
		}
	}
	
	function ijin_pulang_cepat()
	{
		$this->_view('add/ijin_pulang_cepat');
	}
	
	function leave_type()
	{
		$this->_view('add/leave_type');
	}
	
	function leave_request()
	{
		$this->_view('add/leave_request');
	}
	
	function sppd_creation()
	{
		$this->_view('add/sppd_creation');
	}
	
	function tunjangan_creation()
	{
		$this->_view('add/tunjangan_creation');
	}

	function employee_register_karyawan()
	{
		$this->_view('add/add_employe_register_menu/employee_register_karyawan');
	}

	function employee_register_keluarga()
	{
		$this->_view('add/add_employe_register_menu/employee_register_keluarga');
	}

	*/

}