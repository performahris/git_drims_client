<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class View extends CI_Controller {
    
    function __construct()
    {
        parent::__construct();

		$this->load->config('rest');
		$this->load->spark('restclient/2.0.0');		
		$this->rest->initialize(array('server' => 'http://localhost:81/drims_server/hr_add/'));
    }
    
    function _view( $template = '', $param = '')
	{
        $this->load->view('core/header');
        $this->load->view('core/menu');
        $this->load->view($template, $param);
        $this->load->view('core/footer');
    }

    //EMPLOYEMENT
    function employee_view()
	{
		$this->_view('view/employeement/employee_view');
	}
	function warning_report_view()
	{
		$this->_view('view/employeement/warning_report_view');
	}
	function surat_keterangan_kerja_view()
    {
       $this->_view('view/employeement/surat_keterangan_kerja_view');
    }
	function surat_keterangan_pengalaman_kerja_view()
    {
       $this->_view('view/employeement/surat_keterangan_pengalaman_kerja_view');
    }
	function surat_keterangan_print()
    {
       $this->_view('view/employeement/surat');
    }
	function surat_pengalaman_print()
    {
       $this->_view('view/employeement/surat_pengalaman');
    }

	//PERSONNEL SCHEDULE
    function schedule_view()
	{
		$this->_view('view/personnel_schedule/schedule_view');
	}
	function adjustment_view()
	{
		$this->_view('view/personnel_schedule/adjustment_view');
	}
	function actualization_view()
	{
		$this->_view('view/personnel_schedule/actualization_view');
	}

	//ANNOUNCEMENT
	function announcement_submision_view()
    	{
		$this->_view('view/announcement/announcement_submision_view');
	}
	function released_announcement_view()
    	{
		$this->_view('view/announcement/released_announcement_view');
	}

	//LOAN
    function loan_requested_view()
	{
		$this->_view('view/loan/loan_requested_view');
	}
	function loan_released_view()
	{
		$this->_view('view/loan/loan_released_view');
	}
	
	//TUNJANGAN
	function tunjangan_requested_view()
	{
		$this->_view('view/tunjangan/tunjangan_requested_view');	
	}
	function tunjangan_released_view()
	{
		$this->_view('view/tunjangan/tunjangan_released_view');	
	}

	//EXIT CLEARANCE
	function interview_view()
	{
		$this->_view('view/exit_clearance/interview_view');
	}
	function clearance_checklist_view_needapproval()
	{
		$this->_view('view/exit_clearance/clearance_checklist_view_needapproval');
	}
	function clearance_checklist_view()
	{
		$this->_view('view/exit_clearance/clearance_checklist_view');
	}

	//BOOKING ROOM
	function booking_room_requested_view_needapproval()
	{
		$this->_view('view/booking_room/booking_room_requested_view_needapproval');
	}
	function booking_room_released_view()
	{
		$this->_view('view/booking_room/booking_room_released_view');
	}

	//IJIN
	function ijin_requested_view()
	{
		$this->_view('view/ijin/ijin_requested_view');
	}
	function ijin_released_view()
	{
		$this->_view('view/ijin/ijin_released_view');
	}

	//SPPD
    function sppd_requested_view()
	{
		$this->_view('view/sppd/sppd_view');
	}

	//LEAVE REQUEST
	function leave_request_view(){
		$this->_view('view/leave_request/leave_request_view');	
	}
	function leave_released_view(){
		$this->_view('view/leave_request/leave_released_view');	
	}

	//REPORT
	function report_employee_view()
    {
		$this->_view('view/report/report_employee_view');
	}
	function report_leave_view()
    {
		$this->_view('view/report/report_leave_view');
	}
	function report_loan_view()
    {
		$this->_view('view/report/report_loan_view');
	}
	
	//BENEFIT MANAGEMENT
	function benefit_sheet_view()
	{
		$this->_view('view/benefit_management/benefit_sheet_view');
	}

	//HOLIDAY
    function holiday_view()
	{
		$this->_view('view/holiday_management/holiday');
	}

	//MAN POWER REQUEST
	function man_power_request_closed()
	{
		$this->_view('view/man_power_request/man_power_request_closed');
	}
	function man_power_request_needaproval()
	{
		$this->_view('view/man_power_request/man_power_request_needaproval');
	}
	function man_power_request_open()
	{
		$this->_view('view/man_power_request/man_power_request_open');
	}
	function list_employee_on_mpr_open()
	{
		$this->_view('view/man_power_request/list_employe_on_mpr_open');
	}

	function list_employee_on_mpr_close()
	{
		$this->_view('view/man_power_request/list_employe_on_mpr_close');
	}

	//RECRUITMENT
	function vacancy_view()
	{
		$this->_view('view/recruitment/vacancy_view');
	}
	function vacancy_trainer()
	{
		$this->_view('view/recruitment/vacancy_trainer');
	}

	function vacancy_trainer_view_1()
	{
		$this->_view('view/recruitment/applicant/vacancy_trainer_view_1');
	}

	function vacancy_trainer_view_2()
	{
		$this->_view('view/recruitment/applicant/vacancy_trainer_view_2');
	}

	function vacancy_trainer_view_3()
	{
		$this->_view('view/recruitment/applicant/vacancy_trainer_view_3');
	}

	function vacancy_trainer_view_4()
	{
		$this->_view('view/recruitment/applicant/vacancy_trainer_view_4');
	}

	function vacancy_trainer_view_5()
	{
		$this->_view('view/recruitment/applicant/vacancy_trainer_view_5');
	}


	/*
	function employee_view_keluarga()
	{
		$this->_view('view/employe_view_menu/employee_view_keluarga');
	}
	function employee_view_karyawan()
	{
		$this->_view('view/employe_view_menu/employee_view_karyawan');
	}
\

	function vacancy_trainer()
	{
		$this->_view('view/vacancy_trainer');
	}
	*/
}