<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// Created on Aug 26, 2011 by Damiano Venturin @ Squadra Informatica

class Dashboard extends CI_Controller {
	public function __construct() 
	{
		parent::__construct();
		
		$this->load->config('rest');
		$this->load->spark('restclient/2.0.0');		
		$this->rest->initialize(array('server' => 'http://localhost:81/drims_server/index.php/hr_dashboard/'));
		$this->load->helper('url');
	}
	
	function _view( $template = '', $param = '')
	{
        $this->load->view('core/header');
        $this->load->view('core/menu');
        $this->load->view($template, $param);
        $this->load->view('core/footer');
    }

	/*public function get_all_employee(){
		$data['employee'] =json_encode($this->rest->get('all_employee'));
		$this->_view('timesheet/list_all_employee', $data);
	}
	
	public function departement_list(){
		$data['departement'] =json_encode($this->rest->get('departement_list'));
		$this->_view('timesheet/departement_list', $data);
	}
	
	
		public function index()
	{
		$this->departement_list();
	}*/
	
	function index()
	{
		$this->_view('index1');
	}
	
	 function index2()
    {
        $this->_view('dashboard');
    }
    
    


    
    //EMPLOYEEMENT
    function list_all_employee()
    {
       $this->_view('timesheet/employeement/list_all_employee');
    }
    function list_all_warning_report()
    {
       $this->_view('timesheet/employeement/list_all_warning_report');
    }
	function list_surat_keterangan_pengalaman_kerja()
    {
       $this->_view('timesheet/employeement/list_surat_keterangan_pengalaman_kerja');
    }
	function list_surat_keterangan_kerja()
    {
       $this->_view('timesheet/employeement/list_surat_keterangan_kerja');
    }	
	
    //PERSONNEL SCHEDULE
    function schedule_list()
	{
		$this->_view('timesheet/personnel_schedule/schedule_list');
	}
	function adjustment_list()
	{
		$this->_view('timesheet/personnel_schedule/adjustment_list');
	}
	function actualization_list()
	{
		$this->_view('timesheet/personnel_schedule/actualization_list');
	}

	//LOAN
	function loan_requested_list()
	{
		$this->_view('timesheet/loan/loan_requested_list');
	}
	function loan_released_list()
	{
		$this->_view('timesheet/loan/loan_released_list');
	}

	//EXIT CLEARANCE
	function interview_list()
	{
		$this->_view('timesheet/exit_clearance/interview_list');
	}
	function clearance_checklist_list()
	{
		$this->_view('timesheet/exit_clearance/clearance_checklist_list');
	}

	//SPPD
    function sppd_requested()
	{
		$this->_view('timesheet/sppd/sppd_list');
	}

	function sppd_released()
	{
		$this->_view('timesheet/sppd/sppd_list');
	}


    //MAN POWER
	function man_power_requested()
        {
		$this->_view('timesheet/man_power_request/list_man_power_request');
	}


    //LEAVE REQUEST
	function leave_requested()
        {
		$this->_view('/timesheet/leave_request/leave_requested_list');
	}

	function leave_released()
	{
		$this->_view('/timesheet/leave_request/leave_released_list');
	}
	
	//TUNJANGAN
	function tunjangan_requested()
	{
		$this->_view('timesheet/tunjangan/list_tunjangan');
	}
	function tunjangan_released()
	{
		$this->_view('timesheet/tunjangan/list_tunjangan_released');
	}
	
	
	//RECRUITMENT
	function vacancy()
	{
		$this->_view('timesheet/recruitment/list_vacancy');
	}
	function applicant()
	{
		$this->_view('timesheet/recruitment/list_applicant');
	}
	function Interview_Record()
	{
		$this->_view('timesheet/recruitment/Interview_Record');
	}
	function Penarikan_Karyawan()
	{
		$this->_view('timesheet/recruitment/list_penarikan_karyawan');
	}
	function form_penarikan_Karyawan()
	{
		$this->_view('timesheet/recruitment/form_penarikan_karyawan');
	}

	//BENEFIT MANGEMENT
	function benefit_sheet()
	{
		$this->_view('timesheet/benefit_management/benefit_sheet');
	}
	function employee_benefit()
	{
		$this->_view('timesheet/benefit_management/employee_benefit');
	}

	function payroll_list()
	{
		$this->_view('timesheet/benefit_management/payroll_list');
	}

    //IJIN
	
	function ijin_requested()
    {
		$this->_view('timesheet/ijin/ijin_requested');
	}
	
	function ijin_released()
    {
		$this->_view('timesheet/ijin/ijin_released');
	}

	//TRAINING
	function training_requested()
    {
		$this->_view('timesheet/training/training_list');
	}
	
	function training_released()
    {
		$this->_view('timesheet/training/training_approval');
	}

	//BOOKING ROOM
	function booking_room_requested_list()
    {
		$this->_view('timesheet/booking_room/booking_room_requested_list');
	}
	function booking_room_released_list()
    {
		$this->_view('timesheet/booking_room/booking_room_released_list');
	}

	//REPORT
	function report_employee()
    {
		$this->_view('timesheet/report/report_employee');
	}
	function report_leave_request()
    {
		$this->_view('timesheet/report/report_leave_request');
	}
	function report_loan()
    {
		$this->_view('timesheet/report/report_loan');
	}

	//ANNOUNCEMENT
	function announcement_submision_list()
    {
		$this->_view('timesheet/announcement/announcement_submision_list');
	}
	function released_announcement_list()
    {
		$this->_view('timesheet/announcement/released_announcement_list');
	}

	//HOLIDAY
	function holiday_management()
	{
		$this->_view('timesheet/holiday_management/holiday');
	}
	
	
	/*

	function leave_request_summary()
    {
        $this->_view('timesheet/personel_schedule/leave_request_sumary');
    }
	function sppd_list()
    {
		$this->_view('timesheet/sppd_list');
	}
	
	function list_employee_request()
    {
		$this->_view('timesheet/list_employee_request');
	}

	function list_applicant()
    {
		$this->_view('timesheet/list_applicant');
	}
	
	function ijin_list()
    {
		$this->_view('timesheet/ijin_pulang_cepat_list');
	}
	
	function ijin_pulang_cepat_approval()
    {
		$this->_view('timesheet/ijin_pulang_cepat_approval');
	}
	
	function list_man_power_request()
    {
		$this->_view('timesheet/list_man_power_request');
	}
	
	function list_employee_on_mpr()
    {
		$this->_view('timesheet/list_employee_on_mpr');
	}
	
	function man_power_request_release()
    {
		$this->_view('timesheet/man_power_request_release');
	}
	
	function department_list()
    {
		$this->_view('timesheet/departement_list');
	}
	
	function list_of_employee_request()
    {
		$this->_view('timesheet/list_of_employee_request');
	}
	
	function leave_type_list()
	{
		$this->_view('timesheet/leave_type_list');
	}
	
	function leave_list()
	{
		$this->_view('timesheet/leave_list');
	}
	
	function leave_list_per_employee()
	{
		$this->_view('timesheet/leave_list_per_employee');
	}
	
	function list_tunjangan()
	{
		$this->_view('timesheet/list_tunjangan');
	}

	function list_tunjangan_luar_kota()
	{
		$this->_view('timesheet/list_tunjangan_luar_kota');
	}
	
	function list_vacancy()
	{
		$this->_view('timesheet/list_vacancy');
	}

	function data_pengalaman_kerja()
	{
		$this->_view('timesheet/data_pengalaman_kerja');
	}
	
	function list_penarikan_karyawan()
	{
		$this->_view('timesheet/list_penarikan_karyawan');
	}
	

	function list_ijin_bae()
	{
		$this->_view('timesheet/list_ijin_bae');
	}

	function hai()
	{
		$this->_view('timesheet/hai');
	}

	function Bonus_Sheet()
	{
		$this->_view('timesheet/Bonus_Sheet');
	}

	function employee_grade()
	{
		$this->_view('timesheet/employee_grade');
	}


	function project_schedule()
	{
		$this->_view('timesheet/project_schedule');
	}


	function list_karyawan()
	{
		$this->_view('timesheet/list_karyawan');
	}*/
}
