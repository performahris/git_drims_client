<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" >
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>dashboard/department_list">HR</a></li>
            <li class="active">Department</li>
            <li class="active">New</li>
        </ol>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Department | New</h3>
                        <hr>
                    </div>
                    <div class="box-body table-responsive">
                        <?php echo form_open('add/departement_register_add');?>
                            <div class="col-md-3 col-xs-12 col-sm-12">
                                <label>Department Code</label>
                            </div>
                            <div class="col-md-9 col-xs-12 col-sm-12">
                                <label><input type="text" name="departement_id" class="form-control" required /></label>
                            </div>
                            <div class="col-md-3 col-xs-12 col-sm-12">
                                <label>Department Name</label>
                            </div>
                            <div class="col-md-9 col-xs-12 col-sm-12">
                                <label><input type="text" name="departement_name" class="form-control" required/></label>
                            </div>
                            <div class="col-md-5 col-xs-12 col-sm-12" style="padding-top:10px;">
                                <div class="col-md-4 col-xs-12 col-sm-12">
                                    <a href="<?php echo base_url();?>dashboard/department_list"><input type="submit" class="btn btn-block btn-success" value="Save"></a>
                                </div>
                                <div class="col-md-4 col-xs-12 col-sm-12">
                                    <a href="<?php echo base_url();?>dashboard/department_list"><input type="button" class="btn btn-block" style="background-color:#32cd32; color:white;" value="Save As Draft"></a>
                                </div>
                                <div class="col-md-4 col-xs-12 col-sm-12">
                                    <a href="<?php echo base_url();?>dashboard/department_list"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
                                </div>
                            </div>
                        <?php echo form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

