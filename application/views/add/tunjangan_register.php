	<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" >
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#">DEPARTMENT</a></li>
            <li class="active">Department Creation</li>
        </ol>
    </section>
	
    <section class="content">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Tunjangan | Register</h3>
                    </div>
                    <div class="box-body table-responsive">
						<?php echo form_open('add/departement_register_add');?>
							<div class="col-md-3 col-xs-12 col-sm-12">
								<label>Category</label>
							</div>
							<div class="col-md-9 col-xs-12 col-sm-12">
							    <select class="form-control select2" style="width: 25%;">
					                  <option selected="selected">Pilih Tunjangan</option>
					                  <option>Tunjangan Perkawinan</option>
					                  <option>Tunjangan Kematian (Suami/Istri)</option>
					                  <option>Tunjangan Kematian (Anak / Ayah / Ibu Kandung)</option>
                				</select>	
								<!--<label><input type="text" name="departement_id" class="form-control" required /></label>-->
							</div>
							<div class="col-md-3 col-xs-12 col-sm-12">
								<label>Position</label>
							</div>
							<div class="col-md-9 col-xs-12 col-sm-12">
								<select class="form-control select2" style="width: 25%;">
					                  <option selected="selected">Pilih Position</option>
					                  <option>Non Staff</option>
					                  <option>Senior Staff/Supervisor</option>
					                  <option>Manager</option>
					                  <option>Director</option>
					                  <option>Staff</option>
                				</select>
								<!--<label><input type="text" name="departement_name" class="form-control" required/></label>-->
							</div>
							<div class="col-md-3 col-xs-12 col-sm-12">
								<label>Value</label>
							</div>
							<div class="col-md-9 col-xs-12 col-sm-12" >
								<label style="width: 25%;"><input type="text" name="value" class="form-control" required/></label>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12" style="padding-top:10px;">
								<div class="col-md-4 col-xs-12 col-sm-12">
									<a href="#"><input type="submit" class="btn btn-block btn-success" value="Save"></a>
								</div>
								<div class="col-md-4 col-xs-12 col-sm-12">
									<a href="#"><input type="submit" class="btn btn-block btn-warning" value="Save As Draft"></a>
								</div>
								<div class="col-md-4 col-xs-12 col-sm-12">
									<a href="#"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
								</div>
							</div>
						<?php echo form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

