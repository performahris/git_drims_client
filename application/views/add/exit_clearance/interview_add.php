<link href="../../asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<div class="content-wrapper">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Exit Clearance</li>
            <li class="active">Interview</li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Exit Clearance | Interview | Add</h3>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                <div class="col-md-3 col-xs-12 col-sm-4" >
                                    <label for="comment">Division Office/Project</label>
                                </div> 
                                <div class="col-md-9 col-xs-12 col-sm-5">
                                    <input type="text" class="form-control" id="" placeholder=""> 
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-6 col-xs-12 col-sm-4">
                                        <label for="comment">Employee Code</label>
                                    </div> 
                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                        <input type="text" class="form-control" id="" placeholder="Employee Code"> 
                                    </div>
                                </div>
                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-6 col-xs-12 col-sm-4">
                                        <label for="comment">Employee Name</label>
                                    </div> 
                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                        <input type="text" class="form-control" id="" disabled> 
                                    </div>
                                </div>
                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-6 col-xs-12 col-sm-4">
                                        <label for="comment">Department</label>
                                    </div> 
                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                        <input type="text" class="form-control" id="" disabled> 
                                    </div>
                                </div>
                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-6 col-xs-12 col-sm-4">
                                        <label for="comment">Length of Time with Company</label>
                                    </div> 
                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                        <input type="text" class="form-control" id="" disabled> 
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-2 col-xs-12 col-sm-4">
                                        <label for="comment">Date</label>
                                    </div> 
                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                        <?php echo date("d F Y"); ?>
                                    </div>
                                </div>
                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-2 col-xs-12 col-sm-4">
                                        <label for="comment">Position</label>
                                    </div> 
                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                        <input type="text" class="form-control" id="" disabled> 
                                    </div>
                                </div>
                            </div>

                            <hr>
                        </div>
                        <div class="box-body  table-responsive">
                            <table id="example1" class="table table-bordered">
                                <tr>
                                    <td><center>1</center></td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="comment">Why Are you Leaving the company ? Mengapa anda keluar dari perusahaan ?</label>
                                        </div> 
                                        <div class="col-md-12">
                                            <textarea class="form-control" rows="3" id="comment"></textarea>
                                        </div>
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <td><center>2</center></td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="comment">What were the positive thinks about working at this company ?</label><br>
                                            <label for="comment">Apa hal positif yang anda dapatkan selama bekerja di perusahaan ini ?</label>
                                        </div> 
                                        <div class="col-md-12">
                                            <textarea class="form-control" rows="3" id="comment"></textarea>
                                        </div>
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <td><center>3</center></td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="comment">Do you feel that you salary was adequate ? if no, please explain it</label><br>
                                            <label for="comment">Apakah anda merasa upah anda sudah Cukup ? jika tidak, tolong Jelaskan</label>
                                        </div> 
                                        <div class="col-md-12">
                                            <textarea class="form-control" rows="3" id="comment"></textarea>
                                        </div>
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <td><center>4</center></td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="comment">Do you feel salary increases you recieved were adequate ? if no, please explain it.</label><br>
                                            <label for="comment">Apakah anda merasa kenaikan upah yang anda terima sudah Cukup ? jika tidak, tolong Jelaskan</label>
                                        </div> 
                                        <div class="col-md-12">
                                            <textarea class="form-control" rows="3" id="comment"></textarea>
                                        </div>
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <td><center>5</center></td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="comment">Were You given the training necessary to perform professionally in your job ? if no, why not ?</label><br>
                                            <label for="comment">Apakah anda telah diberikan pelatihan yang dibutuhkan untuk melakukan pekerjaan anda ? Jika tidak, kenapa ?</label>
                                        </div> 
                                        <div class="col-md-12">
                                            <textarea class="form-control" rows="3" id="comment"></textarea>
                                        </div>
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <td><center>6</center></td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="comment">Did you manager Set reasonable performance standards and communicate them cleary ?</label><br>
                                            <label for="comment">Apakah manager anda telah menetapkan Standard Kineja yang Layak dan mengkomunikasikannya secara jelas ?</label>
                                        </div> 
                                        <div class="col-md-12">
                                            <textarea class="form-control" rows="3" id="comment"></textarea>
                                        </div>
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <td><center>7</center></td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="comment">Were you given feedback on the quality of you work ?</label><br>
                                            <label for="comment">Apakah Anda telah diberikan informasi balik mengenai kualitas pekerjaan anda ?</label>
                                        </div> 
                                        <div class="col-md-12">
                                            <textarea class="form-control" rows="3" id="comment"></textarea>
                                        </div>
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <td><center>8</center></td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="comment">Were there any major areas of discontent with your role ?</label><br>
                                            <label for="comment">Diarea mana yang membuat ketidakpuasan anda yang terbesar dengan peran anda ?</label>
                                        </div> 
                                        <div class="col-md-12">
                                            <textarea class="form-control" rows="3" id="comment"></textarea>
                                        </div>
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <td><center>9</center></td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="comment">Were there any major areas of discontent with this company ?</label><br>
                                            <label for="comment">Diarea mana yang membuat ketidakpuasan anda yang terbesar dengan perusahaan ini ?</label>
                                        </div> 
                                        <div class="col-md-12">
                                            <textarea class="form-control" rows="3" id="comment"></textarea>
                                        </div>
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <td><center>10</center></td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="comment">Any other problem areas ?</label><br>
                                            <label for="comment">Apakah ada area lainnya yang bermasalah?</label>
                                        </div> 
                                        <div class="col-md-12">
                                            <textarea class="form-control" rows="3" id="comment"></textarea>
                                        </div>
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <td><center>11</center></td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="comment">Under what circumstances whould you have stayed at this company ?</label><br>
                                            <label for="comment">keadaan yang seperti apa yang memungkinkan anda tetap berada di perusahaan ini ?</label>
                                        </div> 
                                        <div class="col-md-12">
                                            <textarea class="form-control" rows="3" id="comment"></textarea>
                                        </div>
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <td><center>12</center></td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="comment">What Changes would you suggest to improve the religion/company ?</label><br>
                                            <label for="comment">peruabahan apa yang anda sarankan untuk memperbaiki Perusahaan ini secara khusus dan secara umum ?</label>
                                        </div> 
                                        <div class="col-md-12">
                                            <textarea class="form-control" rows="3" id="comment"></textarea>
                                        </div>
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <td><center>13</center></td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="comment">Do you Have any other comments ?</label><br>
                                            <label for="comment">Apakah anda masih punya komentar yang lain ?</label>
                                        </div> 
                                        <div class="col-md-12">
                                            <textarea class="form-control" rows="3" id="comment"></textarea>
                                        </div>
                                    </div>
                                </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-8 col-xs-12 col-sm-12" style="padding-top:10px; padding-bottom:10px;">
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                <a href="<?php echo base_url('dashboard/interview_list'); ?>"><input type="button" class="btn btn-block btn-success" value="Submit"></a>
                            </div>
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                <a href="<?php echo base_url('dashboard/interview_list'); ?>"><input type="button" class="btn btn-block btn-save-as" value="Save As Draft"></a>
                            </div>
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                <a href="<?php echo base_url('dashboard/interview_list'); ?>"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>
</div>




<script type="text/javascript">
    $(document).ready(function() {
        $(".departemen").select2();
    });

    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
        $scope.sortType = 'no'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order
        $scope.search = '';
        $scope.limit = 25;
        $scope.dari = "";
        $scope.to = "";
        $scope.depart = "";
        $scope.jumlah = 0;


        $scope.employee = <?php echo $employee ?>;


        $scope.departemen = [
            {nama: "Purchasing"},
            {nama: "Research"},
            {nama: "HRD"}
        ];

        $scope.reset = function() {
            $scope.depart = "";
        }


        $scope.currentPage = 1;
        $scope.totalItems = $scope.employee.length;
        $scope.numPerPage = $scope.limit;

        $scope.limitPage = function() {
            $scope.numPerPage = $scope.limit;
            if ($scope.currentPage * $scope.numPerPage > $scope.employee.length) {
                $scope.currentPage = 1;
            }
        };

        $scope.lastPage = function() {
            $scope.currentPage = $scope.pageCount();
        };

        $scope.firstPage = function() {
            $scope.currentPage = 1;
        };

        $scope.nextPage = function() {

            if ($scope.currentPage < $scope.pageCount()) {
                $scope.currentPage++;
            }
        };

        $scope.jumlahPerpage = function(value) {
            $scope.numPerPage = value;
        }

        $scope.prevPage = function() {
            if ($scope.currentPage > 1) {
                $scope.currentPage--;
            }
        };

        $scope.pageCount = function() {
            return Math.ceil($scope.jumlah / $scope.numPerPage);
        };

        $scope.checkAll = function() {
            angular.forEach($scope.employee, function(item) {
                item.Selected = $scope.selectAll;
            });
        };

    });

    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }


    app.filter("dateRange", function() {
        return function(items, dari, to) {
            if (dari.length == 0) {
                var dari = +new Date("1980-01-01");
            } else {
                var dari = +new Date(dari);
            }

            if (to.length == 0) {
                var to = +new Date();
            } else {
                var to = +new Date(to);
            }
            var df = dari;
            var dt = to;
            var arrayToReturn = [];
            for (var i = 0; i < items.length; i++) {
                var tf = +new Date(items[i].join);
                if ((tf > df && tf < dt) || (tf == dt)) {
                    arrayToReturn.push(items[i]);
                }
            }

            return arrayToReturn;
        };

    });


    app.filter("paging", function() {
        return function(items, limit, currentPage) {
            if (typeof limit === 'string' || limit instanceof String) {
                limit = parseInt(limit);
            }

            var begin, end, index;
            begin = (currentPage - 1) * limit;
            end = begin + limit;
            var arrayToReturn = [];
            for (var i = 0; i < items.length; i++) {
                if (begin <= i && i < end) {
                    arrayToReturn.push(items[i]);
                }
            }
            return arrayToReturn;
        };
    });

    app.filter("departemen", function() {
        return function(items, depart) {
            if (depart.length == 0) {
                return items;
            }
            var arrayToReturn = [];
            for (var i = 0; i < items.length; i++) {
                if (items[i].dept == depart) {
                    arrayToReturn.push(items[i]);
                }
            }
            return arrayToReturn;
        };
    });

    app.filter("nama", function() {
        return function(items, search) {
            if (search.length == 0) {
                return items;
            }
            var arrayToReturn = [];
            for (var i = 0; i < items.length; i++) {
                if (items[i].name.toUpperCase().indexOf(search.toUpperCase()) != -1) {
                    arrayToReturn.push(items[i]);
                }
            }
            return arrayToReturn;
        };
    });


</script>