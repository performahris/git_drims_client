<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Recruitment</li>
            <li class="active">Vacancy</li>
            <li class="active">Vacancy Trainer</li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
		    <div class="box-header">
                        <h3 class="box-title">Recruitment | Vacancy | Vacancy Trainer | Add</h3>
                    <hr>
		    </div>
			<div class="col-md- col-xs-12 col-sm-12" >
			      <div class="col-md-8">
				   <!--<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Search</div>-->
				   <!--<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" placeholder="Enter ..." ng-model="search"/></div>-->
			      </div>
			 </div>
                    <div class="box-body  table-responsive">
                        <table border="1" id="example1" class="table table-bordered table-striped">
                                <tr class="info">
                                    <th colspan="6">
					     <center>RIWAYAT PENDIDIKAN FORMAL/INFORMAL</center>
				    </th>
                                </tr>
			      
		        <!--DISINI-->
			
			        <tr>
				   <th>Tingkat</th>
				   <th>Nama Sekolah</th>
				   <th>Tempat/Kota</th>
				   <th>Jurusan</th>
				   <th>Tahun Lulus</th>
				   <th>Keterangan</th>
				</tr>
				<tr>
				   <td>SD</td>
				   <td><input type="varchar" class="form-control" id="NamaSD" placeholder="Nama Sekolah"></td>
				   <td><input type="varchar" class="form-control" id="TempatSD" placeholder="Tempat/Kota"</td>
				   <td><input type="varchar" class="form-control" id="JurusanSD" placeholder="Jurusan"</td>
				   <td><input type="varchar" class="form-control" id="TahunSD" placeholder="Tahun Lulus"</td>
				   <td><input type="varchar" class="form-control" id="KeteranganSD" placeholder="Keterangan"</td>
				</tr>
				<tr>
				   <td>SMP</td>
				   <td><input type="varchar" class="form-control" id="NamaSMP" placeholder="Nama Sekolah"></td>
				   <td><input type="varchar" class="form-control" id="TempatSMP" placeholder="Tempat/Kota"</td>
				   <td><input type="varchar" class="form-control" id="JurusanSMP" placeholder="Jurusan"</td>
				   <td><input type="varchar" class="form-control" id="TahunSMP" placeholder="Tahun Lulus"</td>
				   <td><input type="varchar" class="form-control" id="KeteranganSMP" placeholder="Keterangan"</td>
				</tr>
				<tr>
				   <td>SMA</td>
				   <td><input type="varchar" class="form-control" id="NamaSMA" placeholder="Nama Sekolah"></td>
				   <td><input type="varchar" class="form-control" id="TempatSMA" placeholder="Tempat/Kota"</td>
				   <td><input type="varchar" class="form-control" id="JurusanSMA" placeholder="Jurusan"</td>
				   <td><input type="varchar" class="form-control" id="TahunSMA" placeholder="Tahun Lulus"</td>
				   <td><input type="varchar" class="form-control" id="KeteranganSMA" placeholder="Keterangan"</td>
				</tr>
				<tr>
				   <td>Akademi</td>
				   <td><input type="varchar" class="form-control" id="NamaAkademi" placeholder="Nama Akademi"></td>
				   <td><input type="varchar" class="form-control" id="TempatAkademi" placeholder="Tempat/Kota"</td>
				   <td><input type="varchar" class="form-control" id="JurusanAkademi" placeholder="Jurusan"</td>
				   <td><input type="varchar" class="form-control" id="TahunAkademi" placeholder="Tahun Lulus"</td>
				   <td><input type="varchar" class="form-control" id="KeteranganAkademi" placeholder="Keterangan"</td>
				</tr>
				<tr>
				   <td>Universitas</td>
				     <td><input type="varchar" class="form-control" id="NamaUniversitas" placeholder="Nama Universitas"></td>
				   <td><input type="varchar" class="form-control" id="TempatUniversitas" placeholder="Tempat/Kota"</td>
				   <td><input type="varchar" class="form-control" id="JurusanUniversitas" placeholder="Jurusan"</td>
				   <td><input type="varchar" class="form-control" id="TahunUniversitas" placeholder="Tahun Lulus"</td>
				   <td><input type="varchar" class="form-control" id="KeteranganUniversitas" placeholder="Keterangan"</td>
				</tr>
				<tr>
				   <td><input type="varchar" class="form-control" id="Degree" placeholder="Nama "></td>
				   <td><input type="varchar" class="form-control" id="NamaUniversitas" placeholder="Nama Sekolah"></td>
				   <td><input type="varchar" class="form-control" id="TempatUniversitas" placeholder="Tempat/Kota"</td>
				   <td><input type="varchar" class="form-control" id="JurusanUniversitas" placeholder="Jurusan"</td>
				   <td><input type="varchar" class="form-control" id="TahunUniversitas" placeholder="Tahun Lulus"</td>
				   <td><input type="varchar" class="form-control" id="KeteranganUniversitas" placeholder="Keterangan"</td>
				</tr>
				
				</table>
			<br>
			<table class="table table-bordered table-striped">
			       <tr >
                                    <td colspan="6">
					   Kursus/ Training 
				    </td>
                                </tr>
			        <tr>
				   <th>Bidang/Jenis</th>
				   <th>Penyelenggara</th>
				   <th>Lama Kursus</th>
				   <th>Tahun</th>
				   <th>Tahun Lulus</th>
				   <th>Dibiayai Oleh</th>
				</tr>
				<tr>
				   <td><input type="varchar" class="form-control" id="Bidang1" placeholder="Bidang/Jenis"></td>
				   <td><input type="varchar" class="form-control" id="Penyelenggara1" placeholder="Penyelenggara"></td>
				   <td><input type="varchar" class="form-control" id="Lama Kursus1" placeholder="Lama Kursus"</td>
				   <td><input type="varchar" class="form-control" id="Tahun1" placeholder="Tahun"</td>
				   <td><input type="varchar" class="form-control" id="TahunLulus1" placeholder="Tahun Lulus"</td>
				   <td><input type="varchar" class="form-control" id="DibiayaiOleh1" placeholder="Dibiayai Oleh"</td>
				</tr>
				<tr>
				   <td><input type="varchar" class="form-control" id="Bidang2" placeholder="Bidang/Jenis"></td>
				   <td><input type="varchar" class="form-control" id="Penyelenggara2" placeholder="Penyelenggara"></td>
				   <td><input type="varchar" class="form-control" id="Lama Kursus2" placeholder="Lama Kursus"</td>
				   <td><input type="varchar" class="form-control" id="Tahun2" placeholder="Tahun"</td>
				   <td><input type="varchar" class="form-control" id="TahunLulus2" placeholder="Tahun Lulus"</td>
				   <td><input type="varchar" class="form-control" id="DibiayaiOleh2" placeholder="Dibiayai Oleh"</td>
				</tr>
			</table>
			<br>
			<table border="1" id="example1" class="table table-bordered table-striped">
				 <tr>
                                    <td colspan="5">
					   Pengetahuan Bahasa : (diisi dengan baik sekali, baik cukup )
				    </td>
                                </tr>
			      
			        <tr>
				   <th>Macam Bahasa</th>
				   <th>Mendengar</th>
				   <th>Berbicara</th>
				   <th>Membaca</th>
				   <th>Menulis</th>
				  
				</tr>
				<tr>
				   <td><input type="varchar" class="form-control" id="Bahasa1" placeholder="Macam Bahasa"></td>
				   <td><input type="varchar" class="form-control" id="Mendengar1" placeholder="Mendengar"></td>
				   <td><input type="varchar" class="form-control" id="Berbicara1" placeholder="Berbicara"</td>
				   <td><input type="varchar" class="form-control" id="Membaca1" placeholder="Membaca"</td>
				   <td><input type="varchar" class="form-control" id="Menulis1" placeholder="Menulis"</td>
				</tr>
				<tr>
				   <td><input type="varchar" class="form-control" id="Bahasa2" placeholder="Macam Bahasa"></td>
				   <td><input type="varchar" class="form-control" id="Mendengar2" placeholder="Mendengar"></td>
				   <td><input type="varchar" class="form-control" id="Berbicara2" placeholder="Berbicara"</td>
				   <td><input type="varchar" class="form-control" id="Membaca2" placeholder="Membaca"</td>
				   <td><input type="varchar" class="form-control" id="Menulis2" placeholder="Menulis"</td>
				</tr>
				
                        </table>
		<br>
			<table border="1" id="example1" class="table table-bordered table-striped">
			       <tr>
                                    <td colspan="4">
					  Kegiatan Sosial
				    </td>
                                </tr>
			        <tr>
				   <th>Organisasi</th>
				   <th>Macam Kegiatan</th>
				   <th>Jabatan</th>
				   <th>Tahun</th>
				</tr>
				<tr>
				   <td><input type="varchar" class="form-control" id="Organisasi1" placeholder="Organisasi"></td>
				   <td><input type="varchar" class="form-control" id="Kegiatan1" placeholder="Macam Kegiatan"></td>
				   <td><input type="varchar" class="form-control" id="Jabatan1" placeholder="Jabatan"</td>
				   <td><input type="varchar" class="form-control" id="Tahun1" placeholder="Tahun"</td>
				</tr>
				<tr>
				   <td><input type="varchar" class="form-control" id="Organisasi2" placeholder="Organisasi"></td>
				   <td><input type="varchar" class="form-control" id="Kegiatan2" placeholder="Macam Kegiatan"></td>
				   <td><input type="varchar" class="form-control" id="Jabatan2" placeholder="Jabatan"</td>
				   <td><input type="varchar" class="form-control" id="Tahun2" placeholder="Tahun"</td>
				</tr>
			      </table>
			
			<br>
			<table border="1" id="example1" class="table table-bordered table-striped">
			       <tr>
                                    <td colspan="2">
					 Hobi dan kegiatan di waktu luang 
				    </td>
                                </tr>
			        
				<tr>
				   <td colspan="2"><textarea name="Hobi" class="form-control"  rows="5"  >Hobi dan kegiatan saya... </textarea></td>
				  
				</tr>
				<tr>
				   <td style="width:50%;">Anda Membaca:
				   <br>
				   <div style="margin-left:80px;">
				   <br><input type="radio" name="baca1" class="minimal" checked> Banyak
				   <br><input type="radio" name="baca2" class="minimal" > Sedang
				   <br><input type="radio" name="baca3" class="minimal" > Sedikit
				   <div>
				   </td>
				   <td>
					Pokok - pokok yang dibaca:
					
					<textarea name="Hobi" class="form-control"  rows="5"  >Saya membaca tentang... </textarea>
				   </td>	
					
				</tr>
			        <tr>
                                    <td colspan="2" >
					<div class="col-sm-12">
					     Anda biasa membaca surat kabar/majalah apa saja? <br/> <br/>
					</div>
					<div class="col-sm-2">
					     Surat Kabar :
					</div>
					<div class="col-sm-10">
					     <input type="varchar" class="form-control" id="SuratKabar">
					     <div style="height:10px;">&nbsp; </div>
					</div>
					<div class="col-sm-2">
					     Majalah :
					</div>
					<div class="col-sm-10">
					     <input type="varchar" class="form-control" id="Majalah">
					</div>
					
				    </td>
                                </tr>
			      </table>
		        
			<br/>
			<div class="col-sm-12" style="text-align:right;">
			 Page 2 of 5
			<div style="height:10px;">&nbsp; </div>
			</div>

		    <div class="box-footer" style="border-top:none;">
			 	<a href="<?php echo base_url('view/vacancy_trainer'); ?>">"><button type="submit" class="btn btn-danger">Cancel</button></a> 
			  	<a href="vacancy_trainer_add_3" class="btn btn-primary pull-right" >Next</a>
			  	<a href="vacancy_trainer_add_1" class="btn btn-primary pull-right" style="margin-right:5px;">Back</a>&nbsp;
			 	<a href="<?php echo base_url('view/vacancy_trainer'); ?>"><button type="submit" class="btn" style="background-color:#32cd32; color:white;">Save As Draft</button></a>
				<a href="<?php echo base_url('view/vacancy_trainer'); ?>"><button type="submit" class="btn btn-success">Submit</button></a> 
		    </div>
		
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
