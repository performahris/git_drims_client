<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Recruitment</li>
            <li class="active">Vacancy</li>
            <li class="active">Vacancy Trainer</li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class=" col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
		    <div class="box-header">
                        <h3 class="box-title">Recruitment | Vacancy | Vacancy Trainer | Add</h3>
                    <hr>
		    </div>
			<div class="col-md- col-xs-12 col-sm-12" >
			      <div class="col-md-8">
				   <!--<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Search</div>-->
				   <!--<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" placeholder="Enter ..." ng-model="search"/></div>-->
			      </div>
			 </div>
                    <div class="box-body  table-responsive">
                        <table border="1" id="example1" class="table table-bordered table-striped ">
                                <tr class="info">
                                    <th colspan="6">
									<center>DATA PRIBADI PELAMAR</center>
									</th>
                                </tr>
					
	<!--DISINI-->
	<tr>
	<td colspan="4" style="width:80%; margin-bottom:0px; padding-bottom:0px;"><form class="form-horizontal">
              
                <div class="form-group">
                  <border><label for="inputPekerjaan" class="col-sm-4 control-label">Pekerjaan yang dilamar</label></border>

                  <div class="col-sm-8">
                    <input type="varchar" class="form-control" id="inputPekerjaan" placeholder="Pekerjaan yang di inginkan">
                  </div>
                </div>
				
                <div class="form-group">
                  <label for="inputNama" class="col-sm-4 control-label">Nama Lengkap / Nama Kecil</label>

                  <div class="col-sm-8">
                    <input type="varchar" class="form-control" id="inputNama" placeholder="Nama Anda">
                  </div>
                </div>
             
			  
	         <div class="form-group">
                  <label for="inputAlamat" class="col-sm-4 control-label">Alamat di dalam Kota</label>

                  <div class="col-sm-8">
                    <textarea class="form-control" id="inputAlamat" placeholder="Alamat Anda Saat Ini"></textarea>
		    <div style="height:10px;">&nbsp; </div>
                  </div>
				  
				  <label for="inputTelp" class="col-sm-8 control-label">Telp :</label>

                  <div class="col-sm-4">
                    <input type="varchar" class="form-control" id="inputTelp" placeholder="Nomor Telp Anda">
                  </div>
				  
                </div>
              </div>
			  
	       <div class="form-group">
                  <label for="inputAlamat" class="col-sm-4 control-label">Alamat di luar kota</label>
					
                  <div class="col-sm-8">
                    <textarea class="form-control" id="inputAlamat" placeholder="Alamat Tinggal Anda"></textarea>
		    <div style="height:10px;">&nbsp; </div>
                  </div>
				  <label for="inputTelp" class="col-sm-8 control-label">Telp :</label>

                  <div class="col-sm-4">
                    <input type="varchar" class="form-control" id="Telp" placeholder="Nomor Telp Anda">
                  </div>
				  
                </div>
              </div>
              <!-- /.box-body -->
			  <!--<div class="col-md-6 col-xs-12 col-sm-12" style="padding-top:10px;">
                        <div class="col-md-4 col-xs-12 col-sm-12">
                        <a href="#"><input type="submit" class="btn btn-info btn-success" value="Save"></a>
                      </div>
                      <div class="col-md-4 col-xs-12 col-sm-12">
                        <a href="#"><input type="button" class="btn btn-block btn-warning" value="Save As Draft"></a>
                      </div>
                      <div class="col-md-4 col-xs-12 col-sm-12">
                        <a href="#"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
                      </div>
                  </div>-->
              <!-- /.box-footer -->
			  
			  <div class="">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
            <td class="col-sm-12" style="width:12%">
					<label>
					<input type="radio" name="r3" class="minimal" checked> Pria
					</label>
					<br>
					<label>
					<input type="radio" name="r3" class="minimal" > Wanita
					</label>
			</td>
			
            <td>
				  
              <div class="">
                <center><div class="col-sm-12">
                  <label for="tinggi1">Tinggi</label>
                  <input type="email" class="form-control" id="tinggi1">
				</div></center>
			  </div>
			  
			</td>
				  
            <td>
			
              <div class="">
                <center>
	        <div class="col-sm-12">
                  <label for="beratbadan1">Berat Badan</label>
                  <input type="integer" class="form-control" id="beratbadan1">
	        </div>
		</center>
	       </div>
	      
	      </td>
			
            <td>
			
			<div class="">
                <center><div class="col-sm-12">
                  <label for="agama1">Agama</label>
                  <input type="text" class="form-control" id="agama1">
				</div></center>
			  </div>
			
			</td>
			
            <td>
			
			<div class="">
                <center><div class="col-sm-12">
                  <label for="bangsa1">Kebangsaan</label>
                  <input type="text" class="form-control" id="bangsa1">
				</div></center>
			  </div>
			
			</td>
                </tr>
                </thead>
                </table>
            </div>
			  
            </form>
			<!--BATAS-->
		</td>
	<!--PHOTO-->	
	<td colspan="2" >
        <div class="">
	       <center>	
		       <!--<img src="views/core/1.jpeg">-->
		       <div style="border:grey solid; width:200px; height:250px;">
		       </div>
		       <div style="height:10px;">&nbsp;</div>
		       <input type="file" name="photo" class="form-control">
		       <div style="height:10px;">&nbsp;</div>
		       <button type="submit" class="btn btn-info">Upload Image</button>
	   		</center>	
	    </div>
	</td>
	
	</tr>
	<tr>
	<td colspan="2" style="width:30%">
	
			<div class="">
                <div class="col-sm-12">
                  <center><label for="bangsa1">LAHIR</label></center>
				</div>
			</div>
	
	</td>
	
	<td colspan="2" style="width:30%">
	
			<div class="">
                <div class="col-sm-10">
                  <center><label for="bangsa1">STATUS PERKAWINAN</label></center>
				</div>
			</div>
	
	</td>
	
	<td colspan="1" style="width:8%">
	  <div>
                <div class="col-sm-10">
                  <center><label for="bangsa1">No.KTP</label></center>
	        </div>
	  </div>
	</td>
	<td colspan="1" style="width:20%">
	
	<div class="">
                <center><div class="col-sm-12">
                 <input type="text" class="form-control" id="bangsa1">
				</div></center>
			  </div>
	
	</td>
	<!--BATAS BAWAH-->
	
	</tr>
	<tr>
	<td colspan="1" style="width:10%">
	
			<div class="">
                <center><div class="col-sm-12">
                  <label for="bangsa1">Tempat</label>
                  <input type="text" class="form-control" id="bangsa1">
				</div></center>
			</div>
	
	</td>
	
	<td colspan="1" style="width:12%">
	
	<div class="">
                <center>
	        <div class="col-sm-11">
                  <label for="bangsa1">Tgl/Bln/Thn</label>
                      <div>
                         <div class="input-group">
                              <input type="text" class="form-control" data-provide="datepicker"/>
                              <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                          </div>
		</div>
		</center>
	</div>
	
	</td>
	<td colspan="1" style="width:15%">

		<label>
			<input type="radio" name="r1" class="minimal" checked>Kawin
		</label>
				<br>
		<label>
			<input type="radio" name="r1" class="minimal" >Belum Kawin
		</label>
	
	</td>

	<td class="col-sm-12"  style="width:15%" >
	
		<label>
			<input type="radio" name="r1" class="minimal" >Janda/Duda
		</label>
				<br>
		<label>
			<input type="radio" name="r1" class="minimal" >Cerai
		</label>
	
	</td>

	
	<td  style="width:8%"> 
	
			<div class="">
                <center><div class="col-sm-10">
                  <label for="bangsa1">No.SIM</label>
				</div></center>
			</div>
	
	</td>
	<td colspan="1" style="width:10%">
	
			<div class="form-group">
              

                  <div class="col-md-12">
                    <input type="email" class="form-control" id="inputEmail3" placeholder="Sim A">
			 <div style="height:5px;">&nbsp; </div>
                  </div>
            </div>
			
			<div class="form-group">
                

                  <div class="col-md-12">
                    <input type="email" class="form-control" id="inputEmail3" placeholder="Sim B">
                  </div>
            </div>
	
	</td>
	</tr>
	
	<tr>
	<td colspan="2" style="width:50%">
	  <div class="">
                <div class="col-sm-"><center>
                  <label for="bangsa1">RUMAH TEMPAT TINGGAL</label></center>
	        </div>
	  </div>
	</td>
	
        <td colspan="4" style="width:50%">
	  <div class="">
                <div class="col-sm-12">
                  <center>
		    <label for="bangsa1">KENDARAAN</label>
		  </center>
	        </div>
	  </div>
	</td>
	</tr>
	
	<tr>
		<td rowspan="2" style="width:25%">
			<label>
				<input type="radio" name="r4" class="minimal" checked> Milik Sendiri
			</label>
				<br>
			<label>
				<input type="radio" name="r4" class="minimal"  > Milik Orang Tua
			</label>
			<br>
			<label>
				<input type="radio" name="r4" class="minimal" > Lain-lain
			</label>
		</td>
	
		<td rowspan="2" style="width:25%">
			
			<label>
				<input type="radio" name="r4" class="minimal" checked> Sewa/Kontrak
			</label>
				<br>
			<label>
				<input type="radio" name="r4" class="minimal" checked> Indekost
			</label>
			
		</td>
			
		<td colspan="4" style="width:50%">
			<label for="inputkendaraan3" class="col-sm-4 control-label">Jenis/Merk/Tahun:</label>
            <div class="col-sm-8">
            <input type="varchar" class="form-control" id="inputkendaraan3" placeholder="Jenis/Merk/Tahun">
			</div>
			
				
		</td>
			
	</tr>
	<tr style="background:none;">
	  <td style="vertical-align:middle; text-align:center;"><b>Milik</b></td>
	  <td>
	      
			<label>
				<input type="radio" name="r5" class="minimal" checked> Sendiri
			</label>
				<br>
			<label>
				<input type="radio" name="r5" class="minimal"  > Orang Tua
			</label>
			      <br>
		       
			
	       
	  </td>
	   <td colspan="2">
	      
		
		        <label>
				<input type="radio" name="r5" class="minimal" checked> Kantor
			</label>
				<br>
			<label>
				<input type="radio" name="r5" class="minimal"  > Lain-lain
			</label>
			
	       
	  </td>
	</tr>
	
	
	<!--DISINI-->
                        </table>
			<br/>
		 <table id="example2" class="table table-bordered table-striped">
		 <tr>
		    <th colspan="7" class="info"><center>Susunan Keluarga (termasuk diri saudara sendiri)</center></th>
		 </tr>
		 <tr>
		    <th rowspan="2" style="vertical-align:middle"><center>Hubungan Keluarga</center></th>
		    <th rowspan="2" style="vertical-align:middle"><center>Nama</center></th>
		    <th rowspan="2" style="vertical-align:middle"><center>L/P</center></th>
		    <th rowspan="2" style="vertical-align:middle"><center>Usia</center></th>
		    <th rowspan="2" style="vertical-align:middle"><center>Pendidikan Terakhir</center></th>
		    <th colspan="2" style="vertical-align:middle"><center>Pekerjaan Terakhir</center></th>
		   
		 </tr>
		 <tr style="background:none !important; ">
		    <th><center>Jabatan</center></th>
		    <th><center>Perusahaan</center></th>
		 </tr>
		 
		 <tr>
		    <td width="9%">Ayah</td>
		    <td width="20%"><input type="varchar" class="form-control" id="NamaAyah" placeholder="Nama Ayah"></td>
		    <td width="15%">  	<select class="form-control" id="GenderAyah">
			      					<option>Laki-laki</option>
                              		<option>Perempuan</option>
                          		</select>
		    </td>
		    <td width="6%"><input type="Integer" class="form-control" id="UsiaAyah"</td>
		    <td width="15%"><input type="varchar" class="form-control" id="PendidikanAyah" placeholder="Pendidikan Ayah"></td>
		    <td width="15%"><input type="varchar" class="form-control" id="JabatanAyah" placeholder="Jabatan"</td>
		    <td width="20%"><input type="varchar" class="form-control" id="PerusahaanAyah" placeholder="Perusahaan"</td>
		 </tr>
		  <tr>
		    <td>Ibu</td>
		    <td><input type="varchar" class="form-control" id="NamaIbu" placeholder="Nama Ibu"></td>
		    <td>  <select class="form-control" id="GenderIbu">
			      <option>Laki-laki</option>
                              <option selected>Perempuan</option>
                          </select>
		    </td>
		    <td><input type="Integer" class="form-control" id="UsiaIbu"</td>
		    <td><input type="varchar" class="form-control" id="PendidikanIbu" placeholder="Pendidikan Ibu"></td>
		    <td><input type="varchar" class="form-control" id="JabatanIbu" placeholder="Jabatan"</td>
		    <td><input type="varchar" class="form-control" id="PerusahaanIbu" placeholder="Perusahaan"</td>
		
		 </tr>
		   <tr>
		    <td>Ayah Mertua</td>
		      <td><input type="varchar" class="form-control" id="NamaAyahMertua" placeholder="Nama Ayah"></td>
		    <td>  <select class="form-control" id="GenderAyahMertua">
			      <option>Laki-laki</option>
                              <option>Perempuan</option>
                          </select>
		    </td>
		    <td><input type="Integer" class="form-control" id="UsiaAyahMertua"</td>
		    <td><input type="varchar" class="form-control" id="PendidikanAyahMertua" placeholder="Pendidikan Ayah"></td>
		    <td><input type="varchar" class="form-control" id="JabatanAyahMertua" placeholder="Jabatan"</td>
		    <td><input type="varchar" class="form-control" id="PerusahaanAyahMertua" placeholder="Perusahaan"</td>
		
		 </tr>
		    <tr>
		    <td>Ibu Mertua</td>
		    <td><input type="varchar" class="form-control" id="NamaIbuMertua" placeholder="Nama Ayah"></td>
		    <td>  <select class="form-control" id="GenderIbuMertua">
			      <option>Laki-laki</option>
                              <option selected>Perempuan</option>
                          </select>
		    </td>
		    <td><input type="Integer" class="form-control" id="UsiaIbuMertua"</td>
		    <td><input type="varchar" class="form-control" id="PendidikanIbuMertua" placeholder="Pendidikan Ibu"></td>
		    <td><input type="varchar" class="form-control" id="JabatanIbuMertua" placeholder="Jabatan"</td>
		    <td><input type="varchar" class="form-control" id="PerusahaanIbuMertua" placeholder="Perusahaan"</td>
		
		 </tr>
		    <tr>
		    <td>Saudara 1</td>
		     <td><input type="varchar" class="form-control" id="NamaSaudara1" placeholder="Nama Saudara 1"></td>
		    <td>  <select class="form-control" id="GenderSaudara1">
			      <option>Laki-laki</option>
                              <option>Perempuan</option>
                          </select>
		    </td>
		    <td><input type="Integer" class="form-control" id="UsiaSaudara1"</td>
		    <td><input type="varchar" class="form-control" id="PendidikanSaudara1" placeholder="Pendidikan Saudara 1"></td>
		    <td><input type="varchar" class="form-control" id="JabatanSaudara1" placeholder="Jabatan"</td>
		    <td><input type="varchar" class="form-control" id="PerusahaanSaudara1" placeholder="Perusahaan"</td>
		
		 </tr>
		    <tr>
		    <td>Saudara 2</td>
		   <td><input type="varchar" class="form-control" id="NamaSaudara2" placeholder="Nama Saudara 2"></td>
		    <td>  <select class="form-control" id="GenderSaudara2">
			      <option>Laki-laki</option>
                              <option>Perempuan</option>
                          </select>
		    </td>
		    <td><input type="Integer" class="form-control" id="UsiaSaudara2"</td>
		    <td><input type="varchar" class="form-control" id="PendidikanSaudara2" placeholder="Pendidikan Saudara 2"></td>
		    <td><input type="varchar" class="form-control" id="JabatanSaudara2" placeholder="Jabatan"</td>
		    <td><input type="varchar" class="form-control" id="PerusahaanSaudara2" placeholder="Perusahaan"</td>
		 
		 </tr>
		    
		    <tr style="background:none;">
			 <td colspan="7" style="text-align:right;"><a href="#" class="glyphicon glyphicon-plus"></a></td>
		    </tr>
		    <tr>
		    <tr>
			  <td>Istri/Suami</td>
		      <td><input type="varchar" class="form-control" id="NamaPartner" placeholder="Nama Pasangan"></td>
		    <td>  <select class="form-control" id="GenderPartner">
			      <option>Laki-laki</option>
                              <option>Perempuan</option>
                          </select>
		    </td>
		 <td><input type="Integer" class="form-control" id="UsiaPartner"</td>
		    <td><input type="varchar" class="form-control" id="PendidikanPartner" placeholder="Pendidikan Pasangan"></td>
		    <td><input type="varchar" class="form-control" id="JabatanPartner" placeholder="Jabatan"</td>
		    <td><input type="varchar" class="form-control" id="PerusahaanPartner" placeholder="Perusahaan"</td>
		    </tr>
		    
		   
		    <td>Anak 1</td>
		    <td><input type="varchar" class="form-control" id="NamaAnak1" placeholder="Nama Anak 1"</td>
		    <td>  <select class="form-control" id="GenderAnak1">
			      <option>Laki-laki</option>
                              <option>Perempuan</option>
                          </select>
		    </td>
		    
		    <td><input type="Integer" class="form-control" id="UsiaAnak1"</td>
		    <td><input type="varchar" class="form-control" id="PendidikanAnak1" placeholder="Pendidikan Anak 1"></td>
		    <td><input type="varchar" class="form-control" id="JabatanAnak1" placeholder="Jabatan"</td>
		    <td><input type="varchar" class="form-control" id="PerusahaanAnak1" placeholder="Perusahaan"</td>
		 </tr>
		    <tr>
		    <td>Anak 2</td>
		    
		    <td><input type="varchar" class="form-control" id="NamaAnak2" placeholder="Nama Anak 2"</td>
		    <td>  <select class="form-control" id="GenderAnak2">
			      <option>Laki-laki</option>
                              <option>Perempuan</option>
                          </select>
		    </td>
		    <td><input type="Integer" class="form-control" id="UsiaAnak2"</td>
		    <td><input type="varchar" class="form-control" id="PendidikanAnak2" placeholder="Pendidikan Anak 2"></td>
		    <td><input type="varchar" class="form-control" id="JabatanAnak2" placeholder="Jabatan"</td>
		    <td><input type="varchar" class="form-control" id="PerusahaanAnak2" placeholder="Perusahaan"</td>
		 </tr>
		      <tr style="background:none;">
			 <td colspan="7" style="text-align:right;"><a href="#" class="glyphicon glyphicon-plus"></a></td>
		    </tr>
		 </table>
		 
		
			<br/>
			<div class="col-sm-12" style="text-align:right;">
			 Page 1 of 5
			<div style="height:10px;">&nbsp; </div>
			</div>

		    <div class="box-footer" style="border-top:none;">
                <a href="<?php echo base_url('view/vacancy_trainer'); ?>"><button type="submit" class="btn btn-danger">Cancel</button></a>
                <a href="vacancy_trainer_add_2" class="btn btn-primary pull-right">Next</a>
				<a href="<?php echo base_url('view/vacancy_trainer'); ?>"><button type="submit" class="btn" style="background-color:#32cd32; color:white;">Save As Draft</button></a>
				<a href="<?php echo base_url('view/vacancy_trainer'); ?>"><button type="submit" class="btn btn-success">Submit</button></a>
         </div>
		
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
