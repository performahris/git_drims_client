<script>
    $(function() {
        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
</script>

<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Recruitment</li>
            <li class="active">Vacancy</li>
            <li class="active">Vacancy Trainer</li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class=" col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Recruitment | Vacancy | Vdcancy Trainer | Add</h3>
                        <hr>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <center>
                            <b><h4>RIWAYAT PEKERJAAN</h4></b>
                            <H5>Dimulai Dengan Pekerjaan Sekarang Dan Diisi Lengkap</H5>
                        </center>
                    </div>
                    <div class="box-body  table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td style="width:50%;"> 
                                        <div class="col-md-6 col-xs-12 col-sm-12">
                                            <h5><label>Masuk : tgl.</label></h5> 
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
                                            <div class="input-group" style="width:100%;">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control" name="date" id="datepicker1"/>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-md-6 col-xs-12 col-sm-12">
                                            <h5><label>Nama/Alamat/Telepon Perusahaan :</label></h5>
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-sm-12">
                                            <input type="text" name="keahlian" class="form-control"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td> 
                                        <div class="col-md-6 col-xs-12 col-sm-12">
                                            <h5><label>Keluar : tgl.</label></h5> 
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
                                            <div class="input-group" style="width:100%;">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control" name="date" id="datepicker2"/>
                                            </div>
                                        </div>
                                    </td>
                                    <td rowspan="3">
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <h5><label>Jenis USaha :</label></h5>
                                            <textarea class="form-control" rows="5" id="comment"></textarea>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td> 
                                        <div class="col-md-6 col-xs-12 col-sm-12">
                                            <h5><label>Jabatan Awal : </label></h5> 
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-sm-12">
                                            <input type="text" class="form-control"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td> 
                                        <div class="col-md-6 col-xs-12 col-sm-12">
                                            <h5><label>Jabatan Akhir : </label></h5> 
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-sm-12">
                                            <input type="text" class="form-control"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td> 
                                        <div class="col-md-6 col-xs-12 col-sm-12">
                                            <h5><label>Nama Atasan Langsung : </label></h5> 
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-sm-12">
                                            <input type="text" class="form-control"/>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-md-6 col-xs-12 col-sm-12">
                                            <h5><label>Nama Direktur</label></h5> 
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-sm-12">
                                            <input type="text" class="form-control"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td > 
                                        <div class="col-md-6 col-xs-12 col-sm-12">
                                            <h5><label>Alasan Berhenti : </label></h5> 
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-sm-12">
                                            <input type="text" class="form-control"/>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-md-6 col-xs-12 col-sm-12">
                                            <h5><label>Gaji Terakhir :</label></h5> 
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-sm-12">
                                            <input type="text" class="form-control"/>
                                        </div>
                                    </td>
                                </tr>
                                <!--
                                <tr style="background:none;">
                                    <td colspan="2" style="text-align:right;"><a href="#" class="glyphicon glyphicon-plus"></a></td>
                                </tr>
                                -->
                            </tbody>
                        </table>
                    </div>

                    <div class="box-body  table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="info">
                                    <td colspan="5"> 
                                        <label for="comment">Referensi (Kepada siapa Kami menanyakan diri anda lebih lengkap)</label>	
                                    </td>
                                </tr>
                            </thead>
                            <thead>
                                <tr>
                                    <th><center>NAMA</center></th>
                                    <th><center>AlAMAT</center></th>
                                    <th><center>TELP</center></th>
                                    <th><center>HUBUNGAN</center></th>
                                    <th><center>PEKERJAAN</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="text" class="form-control"/></td>
                                    <td><input type="text" class="form-control"/></td>
                                    <td><input type="text" class="form-control"/></td>
                                    <td><input type="text" class="form-control"/></td>
                                    <td><input type="text" class="form-control"/></td>
                                </tr>
                                <tr>
                                    <td><input type="text" class="form-control"/></td>
                                    <td><input type="text" class="form-control"/></td>
                                    <td><input type="text" class="form-control"/></td>
                                    <td><input type="text" class="form-control"/></td>
                                    <td><input type="text" class="form-control"/></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="box-body  table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="info">
                                    <td colspan="5"> 
                                        <label for="comment">Orang yang Dihubungi Segera dalam keadaan Mendesak/Darurat</label>	
                                    </td>
                                </tr>
                            </thead>
                            <thead>
                                <tr>
                                    <th><center>NAMA</center></th>
                                    <th><center>AlAMAT</center></th>
                                    <th><center>TELP</center></th>
                                    <th><center>HUBUNGAN</center></th>
                                    <th><center>PEKERJAAN</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="text" class="form-control"/></td>
                                    <td><input type="text" class="form-control"/></td>
                                    <td><input type="text" class="form-control"/></td>
                                    <td><input type="text" class="form-control"/></td>
                                    <td><input type="text" class="form-control"/></td>
                                </tr>
                                <tr>
                                    <td><input type="text" class="form-control"/></td>
                                    <td><input type="text" class="form-control"/></td>
                                    <td><input type="text" class="form-control"/></td>
                                    <td><input type="text" class="form-control"/></td>
                                    <td><input type="text" class="form-control"/></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="box-body  table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th><center>NO NPWP</center></th>
                                    <th><center>NO BPJS KETENAGAKERJAAN</center></th>
                                    <th><center>NO REKENING</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="text" class="form-control"/></td>
                                    <td><input type="text" class="form-control"/></td>
                                    <td><input type="text" class="form-control"/></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="box-body  table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td> 
                                        <label for="comment">Tanggal MCU Terakhir :</label>	
                                    </td>
                                    <td> 
                                        <input type="text" class="form-control"/>	
                                    </td>
                                </tr>
                            </thead>
                        </table>
                    </div>

                    <div class="col-sm-12" style="text-align:right;">
                        Page 3 of 5
                        <div style="height:10px;">&nbsp; </div>
                    </div>

                    <div class="box-footer" style="border-top:none;">
                        <a href="<?php echo base_url('view/vacancy_trainer'); ?>"><button type="submit" class="btn btn-danger">Cancel</button></a>
                        <a href="vacancy_trainer_add_4" class="btn btn-primary pull-right" >Next</a>
                        <a href="vacancy_trainer_add_2" class="btn btn-primary pull-right" style="margin-right:5px;">Back</a>&nbsp;
                        <a href="<?php echo base_url('view/vacancy_trainer'); ?>"><button type="submit" class="btn" style="background-color:#32cd32; color:white;">Save As Draft</button></a>
                        <a href="<?php echo base_url('view/vacancy_trainer'); ?>"><button type="submit" class="btn btn-success">Submit</button></a>
                    </div>

                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    $(function() {
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Timepicker
        $(".timepicker").timepicker({
            showInputs: false
        });
    });
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {


    });

    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
</script>
