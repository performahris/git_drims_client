<script>
    $(function() {
        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Recruitment</li>
            <li class="active">Vacancy</li>
            <li class="active">Add</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Recruitment | Vacancy | Add</h3>
                        <hr>
                    </div>
                    <div class="box-body table-responsive">
                        <div class="col-md-3 col-xs-12 col-sm-12">
                            <label>
                                <div class="col-md-12 col-xs-12 col-sm-12">Vacancy ID</div>
                                <div class="col-md-12 col-xs-12 col-sm-12" style="padding-top:12px;">Requested By</div>
                            </label>
                        </div>
                        <div class="col-md-9 col-xs-12 col-sm-12">
                            <label>
                                <div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
                                    <div style="min-width:330px;">Vacancy 11</div>
                                    <div style="min-width:330px; padding-top:12px;">E_11</div>
                                </div>
                                <div class="col-md-6 col-xs-12 col-sm-12">
                                    <div class="col-md-5 col-xs-12 col-sm-12">Initiate Date</div>
                                    <div class="col-md-7 col-xs-12 col-sm-12" style="font-weight:normal">
                                        <?php echo date("d F Y") ?>
                                    </div>
                                    
                                    <div class="col-md-5 col-xs-12 col-sm-12" style="padding-top:12px;">Due Date</div>
                                    <div class="col-md-7 col-xs-12 col-sm-12" style="padding-top:12px;">
                                        <div class="input-group" style="font-weight:normal">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control" name="date" id="datepicker1"/>
                                        </div>
                                    </div>
                                    
                                </div>
                            </label>
                        </div>
                        
                        <div class="col-md-3 col-xs-12 col-sm-12">
                            <label ><div class="col-md-12 col-xs-12 col-sm-12">Vacancy Name</div></label>
                        </div>
                        <div class="col-md-9 col-xs-12 col-sm-12">
                            <label style="width:100%">
                                <div class="col-md-3 col-xs-12 col-sm-12" style="font-weight:normal">
                                    <input type="text" class="form-control" name="vacancyname" />
                                </div>
                            </label>		
                        </div>
                        <div class="col-md-3 col-xs-12 col-sm-12">
                            <label ><div class="col-md-12 col-xs-12 col-sm-12">Department</div></label>
                        </div>
                        <div class="col-md-9 col-xs-12 col-sm-12">
                            <label style="width:100%">
                                <div class="col-md-3 col-xs-12 col-sm-12" style="font-weight:normal">
                                    <select name="purpose" class="form-control">
                                        <option>Produksi</option>
                                        <option>Pemasaran</option>
                                        <option>Keuangan</option>
                                        <option>Layanan Pelanggan</option>
                                        <option>Purchasing</option>
                                        <option>Logistic</option>
                                        <option>Project</option>
                                        <option>HRD</option>
                                        <option>Supervisor</option>
                                        <option>Personalia</option>
                                    </select>
                                </div>
                            </label>		
                        </div>
                        <div class="col-md-3 col-xs-12 col-sm-12">
                            <label ><div class="col-md-12 col-xs-12 col-sm-12">Spesification</div></label>
                        </div>
                        <div class="col-md-9 col-xs-12 col-sm-12">
                            <label style="width:100%">
                                <div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
                                    <textarea name="spesificaion" class="form-control" rows=5></textarea>
                                </div>
                            </label>		
                        </div>
                        <div class="col-md-3 col-xs-12 col-sm-12">
                            <label ><div class="col-md-12 col-xs-12 col-sm-12">Notes</div></label>
                        </div>
                        <div class="col-md-9 col-xs-12 col-sm-12">
                            <label style="width:100%">
                                <div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
                                    <textarea name="notes" class="form-control" rows=5></textarea>
                                </div>
                            </label>		
                        </div>

                        <!--
<table id="example1" class="table table-striped	 ">
                        
                                <tr>
                                        <td style="width:200px">Employee ID </td>
                                        <td>
                                                <div class="col-md-4 col-xs-12 col-sm-12 pull-left">E_1 </div>
                                                <div class="col-md-8 col-xs-12 col-sm-12 pull-right">Date  : <?php echo date("d F Y") ?></div>
                                        </td>
                                </tr>
                                <tr>
                                        <td>Empoyee Name</td>
                                        <td >Dery</td>
                                </tr>
                                <tr>
                                        <td>Avaliable Leave <?php echo date('Y'); ?></td>
                                        <td >10</td>
                                </tr>
                                <tr>
                                        <td style="vertical-align: top;padding-top:5px;" ng-if='purpose=="1,0"'>
                                                <div>Purpose</div>
                                        </td>
                                        <td ng-if='purpose!="1,0"'>
                                                Purpose
                                        </td>
                                        <td><select name="purpose" ng-model="purpose" ng-change="changePurpose()">
                                                        <option value="0,0">Choose Purpose</option>
                                                        <option value="1,0">Other</option>
                                                        <option value="2,3">Menikah</option>
                                                        <option value="3,2">Sunat</option>
                                                </select>
                                                <div style="padding-top:5px;" ng-if='purpose=="1,0"'><input type="text" name="addPurpose" ng-model="addPurpose" placeholder ="Other"/></div>
                                        </td>
                                </tr>
                                <tr>
                                        <td>Date</td>
                                        <td>
                                                <input type="text" name="dateStart" ng-model="dateStart" ng-change="dateFunctionStart(dateStart)" id="datepicker1" style="width:100px"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                To &nbsp;<input type="text" name="dateEnd" ng-model="dateEnd" ng-change="dateFunctionEnd(dateStart,dateEnd)" id="datepicker2" style="width:100px"/>&nbsp;&nbsp;&nbsp; Total {{diffDays}} Days
                                        </td>
                                </tr>
                                <tr>
                                        <td>Free Leave</td>
                                        <td>{{purposeValue}} Days</td>
                                </tr>
                                <tr>
                                        <td>Leave Request</td>
                                        <td>
                                                {{leaveRequest}} Days
                                        </td>
                                </tr>
                                <tr>
                                        <td>Remaining Leave <?php echo date('Y') ?> </td>
                                        <td>
                                                <label id="cuti"></label> Days
                                                <div style="color:red" ng-if="sisaCuti<0">
                                                        <i class="fa fa-info-circle"></i> Sisa cuti anda sudah habis !! Bila anda tetap mengambil cuti tersebut gaji anda akan terpotong
                                                </div>
                                        </td>
                                </tr>
                                <tr valign ="top">
                                        <td>Emergency Contact <br> Number/Adress</td>
                                        <td ><textarea name="alamat" rows="5" cols="50"></textarea></td>
                                </tr>
</table>-->
                        <div class="col-md-8 col-xs-12 col-sm-12">
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                <a href="<?php echo base_url('dashboard/vacancy'); ?>"><input type="button" class="btn btn-block btn-success" value="Submit"></a>
                            </div>
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                <a href="<?php echo base_url('dashboard/vacancy'); ?>"><input type="button" class="btn btn-block btn-save-as" value="Save As Draft"></a>
                            </div>
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                <a href="<?php echo base_url('dashboard/vacancy'); ?>"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    $(function () {
         $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });
    });
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }

</script>
