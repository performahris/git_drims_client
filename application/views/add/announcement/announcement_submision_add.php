<link href="../../asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

<div class="content-wrapper">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Announcement</li>
            <li class="active">Submission</li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Announcement | Submission | Add</h3>
                        <hr>
                    </div>
                    		<div class="panel-body">
                    			<div class="col-md-12">
				                	  <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Title</label>
				                        </div> 
				                        <div class="col-md-5 col-xs-12 col-sm-5">
				                            <input type="text" class="form-control" id="" placeholder="Title"> 
				                        </div>
				                    </div>
				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Category</label>
				                        </div> 
				                        <div class="col-md-6 col-xs-12 col-sm-5">
				                            <label class="radio-inline">
                                      <input type="radio" name="optradio" id="hide" checked>All
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="optradio" id="show1">Selected User
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="optradio" id="show2">Selected Department
                                    </label>
				                        </div>
				                    </div>
				                	<PA1><div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment" >Selected User</label>
				                        </div> 
				                        <div class="col-md-4 col-xs-12 col-sm-5">
                                  <table class="table table-striped table-bordered" id="table_employee">
                                    <tr class="success">
                                        <th style="width:5%">No</th>
                                        <th style="width:90%"><center>Employee Name</center></th>
                                        <th style="width:5%">Action</th>
                                    </tr>   
                                    <tr>
                                        <td class="text-center" style="vertical-align:middle;">1</td>
                                        <td>
                                        <?php
                                            $employee=array("SUDARMONO","ARBONDES","ISKANDAR","ANTON PRIADI","I KETUT ADI PUJA ASTAWA","
                                            SUBARI.M","SUNARDI","BAMBANG IRAWAN","CHAIRUL AZWAN","AHMAD SARURI");

                                            if ($_SERVER['REQUEST_METHOD']==="POST") {
                                              if (isset($_POST['name_employee'])) {
                                                  if (in_array($_POST['name_employee'],$employee)) {
                                                      echo "You selected ".$_POST['name_employee']."!";
                                                      exit;
                                                  }
                                              }
                                            }
                                        ?>
                                            <div class="input-group" id="comboemployee" style="width:100%">
                                                <select class="form-control" name="name_employee">
                                                <?php
                                                foreach ($employee as $name_employee) {
                                                    echo '<option value="'.$name_employee.'">'.$name_employee.'</option>';
                                                }
                                                ?>
                                                </select>
                                            </div>
                                          </td>
                                          <td>
                                              <a><i class="fa fa-plus" onclick="add_employee()"></i></a>
                                          </td>
                                    </tr>
                                  </table>
				                        </div>
				                    </div>

                          </PA1>
                          <PA2>
                            <div id="tampil2" class="row" style="padding-top:5px; padding-bottom:5px;">
                                <div class="col-md-3 col-xs-12 col-sm-4">
                                    <label for="comment" >Selected Department</label>
                                </div> 
                                <div class="col-md-4 col-xs-12 col-sm-5">
                                  <table class="table table-striped table-bordered" id="table_department">
                                    <tr class="success">
                                        <th style="width:5%">No</th>
                                        <th style="width:90%"><center>Department Name</center></th>
                                        <th style="width:5%">Action</th>
                                    </tr>   
                                    <tr>
                                        <td class="text-center" style="vertical-align:middle;">1</td>
                                        <td>
                                        <?php
                                            $department=array("Produksi","Pemasaran","Keuangan","Layanan Pelanggan",
                                            "Purchasing","Logistic","Project","HRD","Supervisor","Personalia");

                                            if ($_SERVER['REQUEST_METHOD']==="POST") {
                                              if (isset($_POST['name_department'])) {
                                                  if (in_array($_POST['name_department'],$department)) {
                                                      echo "You selected ".$_POST['name_department']."!";
                                                      exit;
                                                  }
                                              }
                                            }
                                        ?>
                                            <div class="input-group" id="combodepartment" style="width:100%">
                                              <select class="form-control" name="name_department">
                                                <?php
                                                foreach ($department as $name_department) {
                                                    echo '<option value="'.$name_department.'">'.$name_department.'</option>';
                                                }
                                                ?>
                                              </select>
                                            </div>
                                        </td>
                                        <td>
                                            <a><i class="fa fa-plus" onclick="add_department()"></i></a>
                                        </td>
                                    </tr>
                                  </table>
                                </div>
                            </div>
                          </PA2>
				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-12">
                                      <div class='box-body pad'>
                                        <form>
                                          <textarea id="editor1" name="editor1" rows="10" cols="80"></textarea>
                                        </form>
                                      </div>
				                        </div>
				                    </div>
				                </div>
                      <div class="col-md-8 col-xs-12 col-sm-12" style="padding-top:10px; padding-bottom:10px;">
                         <div class="col-md-4 col-xs-4 col-sm-4">
                              <a href="<?php echo base_url('dashboard/announcement_submision_list'); ?>"><input type="button" class="btn btn-block btn-success" value="Submit"></a>
                          </div>
                          <div class="col-md-4 col-xs-4 col-sm-4">
                              <a href="<?php echo base_url('dashboard/announcement_submision_list'); ?>"><input type="button" class="btn btn-block btn-save-as" value="Save As Draft"></a>
                          </div>
                          <div class="col-md-4 col-xs-4 col-sm-4">
                              <a href="<?php echo base_url('dashboard/announcement_submision_list'); ?>"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
                          </div>  
                      </div>
				            </div>
				        </div>
				    </div>
				</div>
			</div>
		</div>
    </section>
</div>
<!--autonumber-
<script>
    var tables = document.getElementsByTagName('table');
    var table = tables[tables.length -1];
    var rows = table.rows;
    for(var i = 0, td; i < rows.length; i++){
        td = document.createElement('td');
        td.appendChild(document.createTextNode(i + 1));
        rows[i].insertBefore(td, rows[i].firstChild);
    }
</script>
<!--autonumber-->
<!--table tambah employee-->
<script>
function add_employee() {
    var table = document.getElementById("table_employee");

    var lastRow = table.getElementsByTagName("tr")[table.getElementsByTagName("tr").length-1];
    var lastSecondCell = lastRow.getElementsByTagName("td")[1];
    var lastSecondCellAction = lastRow.getElementsByTagName("td")[2];

    var element2 = document.createElement("select");
        element2.className = 'form-control';

    var div = document.getElementById("comboemployee");
    var options = div.getElementsByTagName("option");

    for (var i = 0; i < options.length; i++) {
      var option = options[i];
      var newOption = document.createElement("option");
          newOption.value = option.value;
          newOption.innerHTML = option.value;

          element2.appendChild(newOption);
    }

    lastSecondCell.appendChild(element2);
    lastSecondCellAction.innerHTML = "<a><i class='fa fa-remove' onclick='remove_employee(this)'></i></a>";

    // lastSecondCellAction.getElementsByTagName("i")[0].onclick = function () {
    //     lastRow.parentNode.removeChild(lastRow);
    // }

    var row = table.insertRow(table.rows.length);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    cell1.innerHTML = table.rows.length -1;
    cell2.appendChild(element2);
    cell3.innerHTML = "<a><i class='fa fa-plus' onclick='add_employee()'></i></a>";
}

function remove_employee(lastRow) {
    var index = lastRow.parentNode.parentNode.parentNode.rowIndex;

    document.getElementById('table_employee').deleteRow(index);
}
</script>
<!--table tambah employe-->

<!--table tambah department-->
<script>
function add_department() {
    var table = document.getElementById("table_department");

    var lastRow = table.getElementsByTagName("tr")[table.getElementsByTagName("tr").length-1];
    var lastSecondCell = lastRow.getElementsByTagName("td")[1];
    var lastSecondCellAction = lastRow.getElementsByTagName("td")[2];

    var element2 = document.createElement("select");
        element2.className = 'form-control';

    var div = document.getElementById("combodepartment");
    var options = div.getElementsByTagName("option");

    for (var i = 0; i < options.length; i++) {
      var option = options[i];
      var newOption = document.createElement("option");
          newOption.value = option.value;
          newOption.innerHTML = option.value;

          element2.appendChild(newOption);
    }

    lastSecondCell.appendChild(element2);
    lastSecondCellAction.innerHTML = "<a><i class='fa fa-remove' onclick='remove_department(this)'></i></a>";

    // lastSecondCellAction.getElementsByTagName("i")[0].onclick = function () {
    //     lastRow.parentNode.removeChild(lastRow);
    // }

    var row = table.insertRow(table.rows.length);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    cell1.innerHTML = table.rows.length -1;
    cell2.appendChild(element2);
    cell3.innerHTML = "<a><i class='fa fa-plus' onclick='add_department()'></i></a>";
}

function remove_department(lastRow) {
    var index = lastRow.parentNode.parentNode.parentNode.rowIndex;
    document.getElementById('table_department').deleteRow(index);
}
</script>
<!--table tambah department-->


<!--Radio Button Hide-->
<script>
$(document).ready(function(){
    $("#hide").click(function(){
        $("PA1").hide();
        $("PA2").hide();
    });
    $("#show1").click(function(){
        $("PA1").show(1000);
        $("PA2").hide(1000);
    });
    $("#show2").click(function(){
        $("PA1").hide(1000);
        $("PA2").show(1000);
    });
    $("#hide").trigger("click");
});
</script>
<!--Radio Button Hide-->

<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
<script type="text/javascript">
      $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1');
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();
      });
    </script>
<!-- CK Editor -->


<!--INPUT TAGS-->
<script>
  $(function() {
    var availableTags = [
      "ActionScript",
      "AppleScript",
      "Asp",
      "BASIC",
      "C",
      "C++",
      "Clojure",
      "COBOL",
      "ColdFusion",
      "Erlang",
      "Fortran",
      "Groovy",
      "Haskell",
      "Java",
      "JavaScript",
      "Lisp",
      "Perl",
      "PHP",
      "Python",
      "Ruby",
      "Scala",
      "Scheme"
    ];
    $( "#tags" ).autocomplete({
      source: availableTags
    });
  });
  </script>
<!--INPUT TAGS-->




<!--
<script type="text/javascript">

    $(function() {
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Timepicker
        $(".timepicker").timepicker({
            showInputs: false
        });
    });
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {


    });

    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    $(function() {
        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker3").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker4").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker5").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
</script>


<script type="text/javascript">
    $(document).ready(function() {
      $(".departemen").select2();
    });
    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      $scope.sortType     = 'no'; // set the default sort type
      $scope.sortReverse  = false;  // set the default sort order
      $scope.search   = ''; 
      $scope.limit =25;
      $scope.dari="";
      $scope.to=""; 
      $scope.depart="";  
      $scope.jumlah = 0;
                   
      
      $scope.employee = <?php echo $employee ?>;
      
      
      $scope.departemen = [
        {nama : "Purchasing"},
        {nama : "Research"},
        {nama : "HRD"}
        ];
        
        $scope.reset = function () {
            $scope.depart =  "";     
        } 
        
        
       $scope.currentPage = 1;  
       $scope.totalItems = $scope.employee.length;  
       $scope.numPerPage = $scope.limit;    
       
       $scope.limitPage = function() {
         $scope.numPerPage = $scope.limit;
         if($scope.currentPage * $scope.numPerPage > $scope.employee.length){
            $scope.currentPage = 1;   
         }
      };
       
       $scope.lastPage = function() {      
         $scope.currentPage=$scope.pageCount();
      };
      
      $scope.firstPage = function() {
         $scope.currentPage=1;
      };
       
       $scope.nextPage = function() {
        
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
      };
      
      $scope.jumlahPerpage = function (value) {
        $scope.numPerPage = value;
      } 
      
      $scope.prevPage = function() {
        if ($scope.currentPage > 1) {
          $scope.currentPage--;
        }
      };
      
       $scope.pageCount = function() {
        return Math.ceil($scope.jumlah/$scope.numPerPage);
      };  
      
      $scope.checkAll = function () {
        angular.forEach($scope.employee, function (item) {
            item.Selected = $scope.selectAll;
        });
      };
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    app.filter("dateRange", function() {      
      return function(items, dari, to) {
        if(dari.length==0){
            var dari = +new Date("1980-01-01");
        }else{
            var dari = +new Date(dari);
        }
        
        if(to.length==0){
            var to = +new Date();
        }else{
            var to = +new Date(to);
        }
        var df = dari;
        var dt = to ;
        var arrayToReturn = [];        
        for (var i=0; i<items.length; i++){
            var tf = +new Date(items[i].join);
            if ((tf > df && tf < dt) || (tf==dt) )  {
                arrayToReturn.push(items[i]);
            }
        }
        
        return arrayToReturn;
      };
        
    });
    
    
    app.filter("paging", function() {      
      return function(items, limit, currentPage) {
        if (typeof limit === 'string' || limit instanceof String){
           limit = parseInt(limit);
         }
         
         var begin, end, index;  
         begin = (currentPage - 1) * limit;  
         end = begin + limit;
         var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (begin <= i && i < end )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    
    app.filter("departemen", function() {      
      return function(items, depart) {
      if(depart.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].dept == depart )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    
    app.filter("nama", function() {      
      return function(items, search) {
      if(search.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].name.toUpperCase().indexOf(search.toUpperCase()) != -1 )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    

</script>
-->
