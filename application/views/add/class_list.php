<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" >
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#">Class</a></li>
            <li class="active">Class Creation</li>
        </ol>
    </section>
	
    <section class="content">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Class | Class Creation</h3>
                    </div>
                    <div class="box-body table-responsive">
						<?php echo form_open('add/class_list_add');?>
						<form action = "<?php echo base_url('add/class_list_add'); ?>" method = "post">
							<div class="col-md-3 col-xs-12 col-sm-12">
								<label>ID</label>
							</div>
							<div class="col-md-9 col-xs-12 col-sm-12">
								<label><input type="int" name="id" class="form-control" required /></label>
							</div>
							<div class="col-md-3 col-xs-12 col-sm-12">
								<label>Nama</label>
							</div>
							<div class="col-md-9 col-xs-12 col-sm-12">
								<label><input type="varchar" name="nama" class="form-control" required/></label>
							</div>
							<div class="col-md-3 col-xs-12 col-sm-12">
								<label>Kelas</label>
							</div>
							<div class="col-md-9 col-xs-12 col-sm-12">
								<label>
								<select name="kelas" class="form-control">
								  <option value="TKJ1" selected>TKJ1</option>
								  <option value="TKJ2">TKJ2</option>
								  <option value="TKJ3">TKJ3</option>
								</select>
								</label>
							</div>
							<div class="col-md-3 col-xs-12 col-sm-12">
								<label>Alamat</label>
							</div>
							<div class="col-md-9 col-xs-12 col-sm-12">
								<label><input type="varchar" name="alamat" class="form-control" required/></label>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12" style="padding-top:10px;">
								<div class="col-md-4 col-xs-12 col-sm-12">
									<a href="#"><input type="submit" class="btn btn-block btn-success" value="Save"></a>
								</div>
								<div class="col-md-4 col-xs-12 col-sm-12">
									<a href="#"><input type="button" class="btn btn-block btn-warning" value="Save As Draft"></a>
								</div>
								<div class="col-md-4 col-xs-12 col-sm-12">
									<a href="#"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
								</div>
							</div>
						<?php echo form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

