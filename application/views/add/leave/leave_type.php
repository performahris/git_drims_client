<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"></i> Leave</a></li>
            <li class="active">Register</li>
            <li class="active">Add</li>
        </ol>
    </section>
	
	 <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
					<div class="box-header">
                        <h3 class="box-title">LEAVE | Register</h3>
                        <hr>
                    </div>
                    <div class="box-body table-responsive">
						<div class="col-md-2 col-xs-12 col-sm-12">
							<label>Name</label>
						</div>
						<div class="col-md-10 col-xs-12 col-sm-12">
							<label style="width:50%;" ><input type="text" name="name" class="form-control"  /></label>
						</div>
						<div class="col-md-2 col-xs-12 col-sm-12">
							<label>Allocation</label>
						</div>
						<div class="col-md-10 col-xs-12 col-sm-12">
							<label style="width:12%;"><input type="text" name="lama" class="form-control" /></label>
                            <label>Days</label>
						</div>
						<!--
                        <table id="example1" class="table table-striped	 ">
							<tr>
								<td >Name </td>
								<td ><input type="text" name="nama" class="required" readonly="readonly"  /></td>
							</tr>
							<tr>
								<td >Lama Cuti</td>
								<td ><input type="text" name="lama" class="required" /></td>
							</tr>
                        </table>-->
						<div class="col-md-6 col-xs-12 col-sm-12" style="padding-top:10px;">
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="<?php echo base_url()."dashboard/leave_type_list" ?>"><input type="button" class="btn btn-block btn-success" value="Submit"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="<?php echo base_url()."dashboard/leave_type_list" ?>"><input type="button" class="btn btn-block btn-danger" value="back"></a>
							</div>
							
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
					<div class="box-header">
                        <h3 class="box-title">LEAVE | Leave Type Creation</h3>
                    </div>
                    <div class="box-body table-responsive">
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label>Nam</label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label><input type="text" name="nama" class="required"  /></label>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label>Lama Cuti</label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label><input type="text" name="lama" class="required" /></label>
						</div>
						<!--
                        <table id="example1" class="table table-striped	 ">
							<tr>
								<td >Name </td>
								<td ><input type="text" name="nama" class="required" readonly="readonly"  /></td>
							</tr>
							<tr>
								<td >Lama Cuti</td>
								<td ><input type="text" name="lama" class="required" /></td>
							</tr>
                        </table>-->
						<!--<div class="col-md-4 col-xs-12 col-sm-12" style="padding-top:10px;">
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-success" value="Save"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-warning" value="Save As Draft"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>-->
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
