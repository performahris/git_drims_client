		<script>
		 $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
		</script>
<div class="content-wrapper" >
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"></i> LEAVE</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
	
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
					<div class="box-header">
                        <h3 class="box-title">LEAVE | Add</h3>
                    </div>
                    <div class="box-body table-responsive">
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label>
								<div class="col-md-12 col-xs-12 col-sm-12">Employee ID</div>
							</label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label>
								<div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="employeeId" class="form-control"  placeholder ="Employee ID"/>
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<div class="col-md-3 col-xs-12 col-sm-12">Date</div>
									<div class="col-md-9 col-xs-12 col-sm-12" style="font-weight:normal">
										<?php echo date("d F Y")?>
									</div>
								</div>
							</label>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Employee Name</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
									Dery
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Position / Grade</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
									Marketing Manager
								</div>
							</label>		
						</div>
							<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Location</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
									Bali
								</div>
							</label>		
						</div>
						
						
						
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Avaliable Leave</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
									10 Days
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Purpose</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-3 col-xs-12 col-sm-12" style="font-weight:normal">
									<select name="purpose" class="form-control">
										<option value="0">Choose Purpose</option>
										<option value="1">Other</option>
										<option value="2">Menikah</option>
										<option value="3">Sunat</option>
									</select>
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Date</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%;margin-left:-15px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
										<div class="input-group">
										  <div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										  </div>
										  <input type="text" class="form-control" name="dateStart" id="datepicker1"/>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 col-sm-12">To</div> 
									<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
										<div class="input-group">
										  <div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										  </div>
										  <input type="text" class="form-control" name="dateEnd" id="datepicker2"/>
										</div>
									</div> 
									<div class="col-md-2 col-xs-12 col-sm-12">Total - Hari</div>
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Free Leave</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
									0 Days
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Leave Request</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
									5 Days
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Emergency Contact Number/Adress</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<textarea name="alamat" rows="5" class="form-control"></textarea>
								</div>
							</label>		
						</div>
						<!--
                        <table id="example1" class="table table-striped	 ">
						
							<tr>
								<td style="width:200px">Employee ID </td>
								<td>
									<div class="col-md-4 col-xs-12 col-sm-12 pull-left">E_1 </div>
									<div class="col-md-8 col-xs-12 col-sm-12 pull-right">Date  : <?php echo date("d F Y")?></div>
								</td>
							</tr>
							<tr>
								<td>Empoyee Name</td>
								<td >Dery</td>
							</tr>
							<tr>
								<td>Avaliable Leave <?php echo date('Y'); ?></td>
								<td >10</td>
							</tr>
							<tr>
								<td style="vertical-align: top;padding-top:5px;" ng-if='purpose=="1,0"'>
									<div>Purpose</div>
								</td>
								<td ng-if='purpose!="1,0"'>
									Purpose
								</td>
								<td><select name="purpose" ng-model="purpose" ng-change="changePurpose()">
										<option value="0,0">Choose Purpose</option>
										<option value="1,0">Other</option>
										<option value="2,3">Menikah</option>
										<option value="3,2">Sunat</option>
									</select>
									<div style="padding-top:5px;" ng-if='purpose=="1,0"'><input type="text" name="addPurpose" ng-model="addPurpose" placeholder ="Other"/></div>
								</td>
							</tr>
							<tr>
								<td>Date</td>
								<td>
									<input type="text" name="dateStart" ng-model="dateStart" ng-change="dateFunctionStart(dateStart)" id="datepicker1" style="width:100px"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									To &nbsp;<input type="text" name="dateEnd" ng-model="dateEnd" ng-change="dateFunctionEnd(dateStart,dateEnd)" id="datepicker2" style="width:100px"/>&nbsp;&nbsp;&nbsp; Total {{diffDays}} Days
								</td>
							</tr>
							<tr>
								<td>Free Leave</td>
								<td>{{purposeValue}} Days</td>
							</tr>
							<tr>
								<td>Leave Request</td>
								<td>
									{{leaveRequest}} Days
								</td>
							</tr>
							<tr>
								<td>Remaining Leave <?php echo date('Y')?> </td>
								<td>
									<label id="cuti"></label> Days
									<div style="color:red" ng-if="sisaCuti<0">
										<i class="fa fa-info-circle"></i> Sisa cuti anda sudah habis !! Bila anda tetap mengambil cuti tersebut gaji anda akan terpotong
									</div>
								</td>
							</tr>
							<tr valign ="top">
								<td>Emergency Contact <br> Number/Adress</td>
								<td ><textarea name="alamat" rows="5" cols="50"></textarea></td>
							</tr>
                        </table>-->
						<div class="col-md-4 col-xs-12 col-sm-12">
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-success" value="Approve"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-danger" value="Reject"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-warning" value="Revew Later"></a>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
            var app = angular.module('levaveRequestAdd', []);
            app.controller('levaveRequestAddCtr', function($scope) {
                $scope.sisaCuti= 10;
                var editDate2 = false;
                var editDate1 = false;
                var cutiTersedia = $scope.sisaCuti;
                $scope.purpose = '0,0';
                var purposeValue = $scope.purpose.split(",");
                $scope.purposeValue = parseInt(purposeValue[1]);  
                var tgl = new Date();
                var bulan = '';
                var tanggal = '';
                if(tgl.getMonth()+1<10){
                    bulan = bulan+'0'+(tgl.getMonth()+1).toString(); 
                }else{
                    bulan = bulan+'0'+(tgl.getMonth()+1).toString();
                }
                
                if(tgl.getDate()<10){
                    tanggal = tanggal+'0'+tgl.getDate(); 
                }else{
                    tanggal = tanggal+tgl.getDate(); 
                }

                $scope.dateStart= tgl.getFullYear()+'-'+bulan+'-'+tanggal;
                $scope.dateEnd= tgl.getFullYear()+'-'+bulan+'-'+tanggal;
                $scope.diffDays = 0; 
                $scope.leaveRequest=0;
                
                var CetakSisaCuti = $scope.sisaCuti;
                if(CetakSisaCuti<=0){
                    CetakSisaCuti=0;
                }
                document.getElementById("cuti").innerHTML=CetakSisaCuti;

                $scope.dateFunctionEnd = function(){
                    editDate2 = true;
                    var date1 = new Date($scope.dateStart);
                    var date2 = new Date($scope.dateEnd);
                    var timeDiff = Math.abs((date2.getTime() - date1.getTime())+1);
                    $scope.diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
                    $scope.leaveRequest = $scope.diffDays - $scope.purposeValue;
                    if($scope.leaveRequest<=0){
                        $scope.leaveRequest=0;
                    }
                    $scope.sisaCuti= $scope.sisaCuti-$scope.leaveRequest;
                    var CetakSisaCuti = $scope.sisaCuti;
                    if(CetakSisaCuti<=0){
                        CetakSisaCuti=0;
                    }
                    document.getElementById("cuti").innerHTML=CetakSisaCuti;
                }

                $scope.dateFunctionStart = function(dateStart){

                    var purposeValue = $scope.purpose.split(",");
                    $scope.purposeValue = parseInt(purposeValue[1]);
                    if(parseInt(purposeValue[1])!=0){
                        purposeValue[1]= parseInt(purposeValue[1]) -1;
                    }
                    var dateStart = new Date($scope.dateStart);
                    dateStart.setTime(dateStart.getTime() +  ((purposeValue[1]) * 24 * 60 * 60 * 1000));
                    var bulan = '';
                    var tanggal = '';
                    if(dateStart.getMonth()+1<10){
                        bulan = bulan+'0'+(dateStart.getMonth()+1).toString(); 
                    }else{
                        bulan = bulan+'0'+(dateStart.getMonth()+1).toString();
                    }
                    
                    if(dateStart.getDate()<10){
                        tanggal = tanggal+'0'+dateStart.getDate(); 
                    }else{
                        tanggal = tanggal+dateStart.getDate(); 
                    }
                    
                    $scope.dateEnd = dateStart.getFullYear()+'-'+bulan+'-'+tanggal;
                    
                    
                }

                $scope.changePurpose = function(){
                    if($scope.dateStart != $scope.dateEnd){ 
                        var purposeValue = $scope.purpose.split(",");
                        $scope.purposeValue = parseInt(purposeValue[1]);
                        if($scope.leaveRequest > 0){
                            $scope.leaveRequest= $scope.leaveRequest  - parseInt(purposeValue[1]);    
                        }
                        
                        if($scope.sisaCuti!=cutiTersedia && $scope.sisaCuti<=cutiTersedia){
                            $scope.sisaCuti= $scope.sisaCuti+parseInt(purposeValue[1]);    
                        }
                        

                        var CetakSisaCuti = $scope.sisaCuti;
                        if(CetakSisaCuti<=0){
                            CetakSisaCuti=0;
                        }
                        document.getElementById("cuti").innerHTML=CetakSisaCuti;
                        if(editDate2!=true){
                            var result = new Date();
                            var dateStart = new Date($scope.dateStart);
                            result.setTime(dateStart.getTime() +  ((purposeValue[1]) * 24 * 60 * 60 * 1000));
                            var bulan = '';
                            var tanggal = '';
                            if(result.getMonth()+1<10){
                                bulan = bulan+'0'+(result.getMonth()+1).toString(); 
                            }else{
                                bulan = bulan+'0'+(result.getMonth()+1).toString();
                            }

                            if(result.getDate()<10){
                                tanggal = tanggal+'0'+result.getDate(); 
                            }else{
                                tanggal = tanggal+result.getDate(); 
                            }
                            $scope.dateEnd = result.getFullYear()+'-'+bulan+'-'+tanggal;

                            var date1 = new Date($scope.dateStart);
                            var date2 = new Date($scope.dateEnd);
                            var timeDiff = Math.abs((date2.getTime() - date1.getTime())+1);
                            $scope.diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
                        }
                    }else{
                        var purposeValue = $scope.purpose.split(",");
                        $scope.purposeValue = parseInt(purposeValue[1]);
                        if(parseInt(purposeValue[1])!=0){
                            purposeValue[1]= parseInt(purposeValue[1]) -1;
                        }
                        var result = new Date();
                        var dateStart = new Date($scope.dateStart);
                        result.setTime(dateStart.getTime() +  ((purposeValue[1]) * 24 * 60 * 60 * 1000));
                        var bulan = '';
                        var tanggal = '';
                        if(result.getMonth()+1<10){
                            bulan = bulan+'0'+(result.getMonth()+1).toString(); 
                        }else{
                            bulan = bulan+'0'+(result.getMonth()+1).toString();
                        }

                        if(result.getDate()<10){
                            tanggal = tanggal+'0'+result.getDate(); 
                        }else{
                            tanggal = tanggal+result.getDate(); 
                        }
                        $scope.dateEnd = result.getFullYear()+'-'+bulan+'-'+tanggal;

                        var date1 = new Date($scope.dateStart);
                        var date2 = new Date($scope.dateEnd);
                        var timeDiff = Math.abs((date2.getTime() - date1.getTime())+1);
                        $scope.diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
                    }
                }                
            });
        </script>
