<script src="<?php echo base_url(); ?>webroot/js/fancybox/jquery.fancybox-1.3.4.js" language="javascript" type="text/javascript"></script>
<link href="<?php echo base_url(); ?>webroot/js/fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css" media="screen" />
<div id="content">


<div class="main-content">
<form method="post">
<table cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3" class="titlepage"><strong>Ijin | Approval</strong></td>
		
	</tr>
	<tr>
		<td colspan="3" class="breadcrumbadd">IJIN PULANG CEPAT | APPROVAL</td>
	</td>
	<tr>
		<td class="titleform">Employee ID</td>
		<td valign="bottom" >E_1</td>
	</tr>
	<tr>
		<td class="titleform">Request Number</td>
		<td valign="bottom">1</td>
	</tr>
	<tr>
		<td class="titleform">Employee Name</td>
		<td valign="bottom">Dery</td>
	</tr>
	<tr>
		<td class="titleform">Date</td>
		<td valign="bottom">16-Feb-2016</td>
	</tr>
	<tr>
		<td class="titleform">Category</td>
		<td valign="bottom">Pulang Cepat</td>
	</tr>
	<tr>
		<td lass="titleform">From</td>
		<td valign="bottom">14.00</td>
	</tr>
	<tr>
		<td class="titleform">To</td>
		<td valign="bottom" >-</td>
	</tr>
	<tr>
		<td class="titleform">Note</td>
		<td valign="bottom"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</td>
	</tr>
	<tr>
		<td valign="top" class="titleform">Remarks</td>
		<td style="padding-top:10px;" colspan = 2><textarea style="min-width:400px;max-width:60%;" rows = 15></textarea>
	</tr>
</table>
</form>
</div>
</div>
<script language="javascript" type="text/javascript">
$(document).ready(function() {
	$("a.button").click(function(){
		$.fancybox(
		'<div style="height:350px;width:450px;overflow:hidden;"><iframe src="<?php echo base_url(); ?>fix_asset/ijin_pulang_cepat/<?php echo $this->uri->segment(4); ?>/<?php echo $data[0]["equipment_type_id"]; ?>" frameborder="0" scrolling="no" style="width:100%;height:800px;margin-top:-200px;"></iframe></div>',
			{
				'autoDimensions'	: false,
				'width'         	: 'auto',
				'height'        	: 'auto',
				'transitionIn'		: 'none',
				'transitionOut'		: 'none'
			}
		);
	});
});
</script>
