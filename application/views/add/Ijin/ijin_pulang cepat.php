<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Ijin</li>
            <li class="active">Requested</li>
            <li class="active">Add</li>
        </ol>
    </section>
	
    <section class="content">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
					<div class="box-header">
                        <h3 class="box-title">Ijin | Requested | Add</h3>
                        <hr>
                    </div>
                    <div class="box-body table-responsive">
                        <table class="table table-striped">
                            <tr>
								<td class="titleform">Employee ID</td>
								<td valign="bottom" >E_1</td>
							</tr>
							<tr>
								<td class="titleform">Request Number</td>
								<td valign="bottom">1</td>
							</tr>
							<tr>
								<td class="titleform">Employee Name</td>
								<td valign="bottom">Dery</td>
							</tr>
							<tr>
								<td class="titleform">Date</td>
								<td valign="bottom">16-Feb-2016</td>
							</tr>
							<tr>
								<td class="titleform">Category</td>
								<td valign="bottom">Pulang Cepat</td>
							</tr>
							<tr>
								<td lass="titleform">From</td>
								<td valign="bottom">14.00</td>
							</tr>
							<tr>
								<td class="titleform">To</td>
								<td valign="bottom" >-</td>
							</tr>
							<tr>
								<td class="titleform">Note</td>
								<td valign="bottom"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</td>
							</tr>
							<tr>
								<td class="titleform">Remaks</td>
								<td valign="bottom"> <textarea name="remaks" class="form-control" rows=10 style="width:80%"></textarea></td>
							</tr>
                        </table>
						<div class="col-md-4 col-xs-12 col-sm-12">
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-success" value="Approve"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-danger" value="Reject"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-warning" value="Revew Later"></a>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
