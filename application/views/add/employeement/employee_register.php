<script type="text/javascript">

    $(function() {
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Timepicker
        $(".timepicker").timepicker({
            showInputs: false
        });
    });
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {


    });

    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    $(function() {
        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker3").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker4").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker5").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
</script>

<style type="text/css">
    .scroll{
        width:auto;
        height:auto;
        overflow:auto;
    }
</style>

<div class="content-wrapper" >
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li><a href="#"></i>Employeement</a></li>
            <li><a href="#"></i>Employee</a></li>
            <li class="active">Add</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Employeement | Employee | Add</h3>
                        <hr>
                    </div>
                    <div class="box-body table-responsive">    	
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#biodata">Biodata</a></li>
                                <li><a data-toggle="tab" href="#keluarga">Family</a></li>
                                <li><a data-toggle="tab" href="#karyawan">Employee</a></li>
                            </ul>
                            <br>
                            <div class="tab-content">
                                <div id="biodata" class="tab-pane fade in active">
                                    <div class="chart tab-pane active">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <!--Form Biodata-->
                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Employee Code</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        E_11
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Identity Number</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Employee Name</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Address (Identity Card)</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-7">
                                                        <textarea class="form-control" rows="3" id="comment"></textarea>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Address (Domicile)</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <textarea class="form-control" rows="3" id="comment"></textarea>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Postal Code</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">City</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Province</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Photo</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="file" name="photo" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Gender</label>
                                                    </div>
                                                    <div class="col-md-6 col-xs-6 col-sm-6">
                                                        <h4>
                                                            <span><input type="radio" name="gender" value="point1">Male</span>
                                                            <span style="padding-left:20px"><input type="radio" name="gender" value="point2">Female</span>
                                                        </h4>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Place off Birth</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Birth Date</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" id="datepicker1" class="form-control pull-rigth active"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Personel Nasionality</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <select class="form-control">
                                                            <option fisible="false">List of Nationality</option>
                                                            <option>Indonesia</option>
                                                            <option>Malaysia</option>
                                                            <option>Singapore</option>
                                                            <option>France</option>
                                                            <option>Italy</option>                                                            
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Race (for visa purposes)</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Personel Religion</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <select class="form-control">
                                                            <option fisible="false">List Of Religion</option>
                                                            <option>Christians</option>
                                                            <option>Muslims</option>
                                                            <option>Unaffiliated</option>
                                                            <option>Hindus</option>
                                                            <option>Budhists</option>
                                                            <option>Jews</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Phone 1</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Phone 2</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Tax ID</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Email</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">	
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Personal Email</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Emergency Contact Name</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Emergency Contact Number</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>
                                                <!--batas-->
                                            </div>
                                            <div class="col-md-8 col-xs-12 col-sm-12" style="padding-top:10px; padding-bottom:10px;">
                                                <div class="col-md-4 col-xs-4 col-sm-4">
                                                    <a href="<?php echo base_url('dashboard/list_all_employee'); ?>"><input type="button" class="btn btn-block btn-success" value="Submit"></a>
                                                </div>
                                                <div class="col-md-4 col-xs-4 col-sm-4">
                                                    <a href="<?php echo base_url('dashboard/list_all_employee'); ?>"><input type="button" class="btn btn-block btn-save-as" value="Save As Draft"></a>
                                                </div>
                                                <div class="col-md-4 col-xs-4 col-sm-4">
                                                    <a href="<?php echo base_url('dashboard/list_all_employee'); ?>"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="keluarga" class="tab-pane fade">
                                    <div class="chart tab-pane">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <!--Form Keluaarga-->
                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Family Card No</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder=""> 
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Father's Name</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Mother's Name</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Number Of Siblings</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Marital Status</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        <select class="form-control">
                                                            <option fisible="false">List Off Asigment Type</option>
                                                            <option>Married</option>
                                                            <option>single</option>
                                                            <option>divorced</option>
                                                            <option>widowed</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Husband / Wife Name</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>

                                                <!--anak-->
                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-10 col-xs-12 col-sm-10 ">
                                                        <div class="box box-default collapsed-box">
                                                            <div class="box-header with-border">
                                                                <h3 class="box-title">1st Child</h3>
                                                                <div class="box-tools pull-right">
                                                                    <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo"><i class="fa fa-plus"></i></button>
                                                                    <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo"><i class="fa fa-times"></i></button>
                                                                </div>
                                                                <div class="row" style="padding-top:5px;">
                                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                                        <label for="comment">Name</label>
                                                                    </div> 
                                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                                        <input type="text" class="form-control" id="" placeholder="">
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-top:5px;">
                                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                                        <label for="comment">Date Off Birth</label>
                                                                    </div> 
                                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                                        <div class="input-group">
                                                                            <div class="input-group-addon">
                                                                                <i class="fa fa-calendar"></i>
                                                                            </div>
                                                                            <input type="text" id="datepicker2" class="form-control pull-rigth active"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                                        <label for="comment">Gender</label>
                                                                    </div> 
                                                                    <div class="col-md-6 col-xs-6 col-sm-6">
                                                                        <h4>
                                                                            <span><input type="radio" name="gender1" value="point1">Male</span>
                                                                            <span style="padding-left:20px"><input type="radio" name="gender1" value="point2">Female</span>
                                                                        </h4>
                                                                    </div>
                                                                    <div class="col-md-2 col-xs-4 col-sm-2">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- /.box-header -->
                                                            <div id="demo" class="collapse">
                                                                <div class="box box-default collapsed-box">
                                                                    <div class="box-header with-border">
                                                                        <h3 class="box-title">2nd Child</h3>
                                                                        <div class="box-tools pull-right">
                                                                            <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#dem"><i class="fa fa-plus"></i></button>
                                                                        </div>
                                                                        <div class="row" style="padding-top:5px;">
                                                                            <div class="col-md-4 col-xs-12 col-sm-4">
                                                                                <label for="comment">Name</label>
                                                                            </div> 
                                                                            <div class="col-md-4 col-xs-12 col-sm-4">
                                                                                <input type="text" class="form-control" id="" placeholder="" value="">
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top:5px;">
                                                                            <div class="col-md-4 col-xs-12 col-sm-4">
                                                                                <label for="comment">Date Off Birth</label>
                                                                            </div> 
                                                                            <div class="col-md-4 col-xs-12 col-sm-4">
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">
                                                                                        <i class="fa fa-calendar"></i>
                                                                                    </div>
                                                                                    <input type="text" id="datepicker3" class="form-control pull-rigth active"/>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                                            <div class="col-md-4 col-xs-12 col-sm-4">
                                                                                <label for="comment">Gender</label>
                                                                            </div> 
                                                                            <div class="col-md-6 col-xs-6 col-sm-6">
                                                                                <h4>
                                                                                    <span><input type="radio" name="gender2" value="point1">Male</span>
                                                                                    <span style="padding-left:20px"><input type="radio" name="gender2" value="point2">Female</span>
                                                                                </h4>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="dem" class="collapse">
                                                                <div class="box box-default collapsed-box">
                                                                    <div class="box-header with-border">
                                                                        <h3 class="box-title">3rd Child</h3>
                                                                        <div class="box-tools pull-right">
                                                                            <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#dem"><i class="fa fa-plus"></i></button>
                                                                        </div>
                                                                        <div class="row" style="padding-top:5px;">
                                                                            <div class="col-md-4 col-xs-12 col-sm-4">
                                                                                <label for="comment">Name</label>
                                                                            </div> 
                                                                            <div class="col-md-4 col-xs-12 col-sm-4">
                                                                                <input type="text" class="form-control" id="" placeholder="">
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top:5px;">
                                                                            <div class="col-md-4 col-xs-12 col-sm-4">
                                                                                <label for="comment">Date Off Birth</label>
                                                                            </div> 
                                                                            <div class="col-md-4 col-xs-12 col-sm-4">
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">
                                                                                        <i class="fa fa-calendar"></i>
                                                                                    </div>
                                                                                    <input type="text" id="datepicker4" class="form-control pull-rigth active"/>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                                            <div class="col-md-4 col-xs-12 col-sm-4">
                                                                                <label for="comment">Gender</label>
                                                                            </div> 
                                                                            <div class="col-md-6 col-xs-6 col-sm-6">
                                                                                <h4>
                                                                                    <span><input type="radio" name="gender3" value="point1">Male</span>
                                                                                    <span style="padding-left:20px"><input type="radio" name="gender3" value="point2">Female</span>
                                                                                </h4>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- /.box -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8 col-xs-12 col-sm-12" style="padding-top:10px; padding-bottom:10px;">
                                                <div class="col-md-4 col-xs-4 col-sm-4">
                                                    <a href="<?php echo base_url('dashboard/list_all_employee'); ?>"><input type="button" class="btn btn-block btn-success" value="Submit"></a>
                                                </div>
                                                <div class="col-md-4 col-xs-4 col-sm-4">
                                                    <a href="<?php echo base_url('dashboard/list_all_employee'); ?>"><input type="button" class="btn btn-block btn-save-as" value="Save As Draft"></a>
                                                </div>
                                                <div class="col-md-4 col-xs-4 col-sm-4">
                                                    <a href="<?php echo base_url('dashboard/list_all_employee'); ?>"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="karyawan" class="tab-pane fade">
                                    <div class="chart tab-pane">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <!--Form karyawan-->
                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Join Date</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" id="datepicker5" class="form-control pull-rigth active"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Personnel Assignment Type</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        <select class="form-control">
                                                            <option fisible="false">List Off Asigment Type</option>

                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Personal Schedule Type</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        <select class="form-control">
                                                            <option fisible="false">List Off Schedule Type</option>

                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Rational Work Days</label>
                                                    </div> 
                                                    <div class="col-md-2 col-xs-6 col-sm-3">
                                                        <select class="form-control">
                                                            <option fisible="false">Days</option>
                                                            <option>28:28</option>
                                                            <option>28:14</option>
                                                            <option>6:1</option>               
                                                        </select>
                                                    </div>
                                                    <h5><b>Days</b></h5>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Annual Leave Entitlement</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Direct Supervisor Name</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        <select class="form-control">
                                                            <option fisible="false">List Off Supervisor</option>
                                                            <option>SUDARMONO</option>    
							                                <option>ARBONDES</option>
                                                            <option>ISKANDAR</option>
                                                            <option>ANTON PRIADI</option>
                                                            <option>I KETUT ADI PUJA ASTAWA</option>
                                                            <option>SUBARI.M</option>
                                                            <option>SUNARDI </option>
                                                            <option>BAMBANG IRAWAN  </option>
                                                            <option>CHAIRUL AZWAN</option>
                                                            <option>AHMAD SARURI</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Department</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        <select class="form-control">
                                                            <option fisible="false">List Off Department</option>
                                                            <option>HRD</option>
                                                            <option>Keuangan</option>
                                                            <option>Layanan Pelanggan</option>
                                                            <option>Logistic</option>
                                                            <option>Pemasaran</option>
                                                            <option>Produksi</option>
                                                            <option>Project</option>
                                                            <option>Purchasing</option>
                                                            <option>Personalia</option>
                                                            <option>Supervisor</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Position</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        <select class="form-control">
                                                            <option fisible="false">List Off Position</option>
                                                            <option>Asisten Admin II</option>
                                                            <option>Pembantu Teknisi ICT</option>
                                                            <option>Ops Crane</option>
                                                            <option>Petugas Admin III</option>
                                                            <option>Pemuka Lindungan Lingkungan</option>
                                                            <option>Ops Alat Berat</option>
                                                            <option>Petugas Admin 1</option>
                                                            <option>Pemuka Inspeksi</option>
                                                            <option>Petugas Admin II</option>
                                                            <option>Pengemudi Truck</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Employee Grade</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        <select class="form-control">
                                                            <option fisible="false">List Off Employee Grade</option>
                                                            <option>A</option>
                                                            <option>B</option>
                                                            <option>C</option>
                                                            <option>D</option>
                                                            <option>E</option>
                                                            <option>F</option>
                                                            <option>G</option>
                                                            <option>H</option>
                                                            <option>I</option>
                                                            <option>J</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Company Name</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        <select class="form-control">
                                                            <option fisible="false">List Off Company</option>

                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Point Off Hire</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Currency Base</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Personnel Salary Type</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        <select class="form-control">
                                                            <option fisible="false">List Off Salary Type</option>

                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Salary Currency</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        <select class="form-control">
                                                            <option fisible="false">List Off Currency</option>
                                                            <option>Dollar</option>
                                                            <option>Rupiah</option>
                                                            <option>Euro</option>
                                                            <option>Peso</option>
                                                            <option>Dinar</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Basic Salary</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Payment Method</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        <select class="form-control">
                                                            <option fisible="false">List Payment Method</option>

                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Bank Name</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>


                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Bank Code</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>


                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Branch Code & Bank Account No</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>






                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Status Pajak</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        <select class="form-control">
                                                            <option fisible="false">List Off Status Pajak</option>
                                                            <option>K/0</option>
                                                            <option>K/1</option>
                                                            <option>K/2</option>
                                                            <option>K/3</option>
                                                            <option>TK/0</option>
                                                            <option>TK/1</option>
                                                            <option>TK/2</option>
                                                            <option>TK/3</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">BPJS pribadi</label>
                                                    </div>
                                                    <div class="col-md-2 col-xs-12 col-sm-4">
                                                        <h4>
                                                            <span><input type="radio" name="stats1" id="yes" value="point1">Ada</span>
                                                            <span style="padding-left:20px"><input type="radio" name="stats1" id="no" value="point2">Tidak Ada</span>
                                                        </h4>
                                                    </div>
                                                     <div class="col-md-2 col-xs-8 col-sm-4">
                                                        <PA1> 
                                                            <input type="text" class="form-control" placeholder=""> 
                                                        </PA1>
                                                    </div>
                                                    
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">NPWP</label>
                                                    </div> 
                                                    <div class="col-md-2 col-xs-12 col-sm-4">
                                                        <h4>
                                                            <span><input type="radio" name="stats2" id="yes1" value="point1">Ada</span>
                                                            <span style="padding-left:20px"><input type="radio" name="stats2" id="no1" value="point2">Tidak Ada</span>
                                                        </h4>

                                                    </div> 
                                                    <div class="col-md-2 col-xs-8 col-sm-4">
                                                        <PA2>
                                                            <input type="text" class="form-control" id="" placeholder="" >
                                                        </PA2   >
                                                    </div>
                                                </div>
                                                <!--batas-->
                                            </div>
                                            <div class="col-md-8 col-xs-12 col-sm-12" style="padding-top:10px; padding-bottom:10px;">
                                                <div class="col-md-4 col-xs-4 col-sm-4">
                                                    <a href="<?php echo base_url('dashboard/list_all_employee'); ?>"><input type="button" class="btn btn-block btn-success" value="Submit"></a>
                                                </div>
                                                <div class="col-md-4 col-xs-4 col-sm-4">
                                                    <a href="<?php echo base_url('dashboard/list_all_employee'); ?>"><input type="button" class="btn btn-block btn-save-as" value="Save As Draft"></a>
                                                </div>
                                                <div class="col-md-4 col-xs-4 col-sm-4">
                                                    <a href="<?php echo base_url('dashboard/list_all_employee'); ?>"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close() ?>
                </div>
            </div>
        </div>
    </section>
</div>



<script>
$(document).ready(function(){
    $("#yes").click(function(){
        $("PA1").show(1000);
    });
    $("#no").click(function(){
        $("PA1").hide(1000);
    });

    $("#yes1").click(function(){
        $("PA2").show(1000);
    });
    $("#no1").click(function(){
        $("PA2").hide(1000);
    });
});
window.onload = function(){
    $("PA1").hide();
    $("PA2").hide();
}
</script>