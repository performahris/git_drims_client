<script type="text/javascript">

    $(function() {
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Timepicker
        $(".timepicker").timepicker({
            showInputs: false
        });
    });
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {


    });

    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    $(function() {
        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker3").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker4").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker5").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
</script>

<style type="text/css">
    .scroll{
        width:auto;
        height:auto;
        overflow:auto;
    }
</style>

<div class="content-wrapper" >
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"></i>HR</a></li>
            <li><a href="#"></i>Employeement</a></li>
            <li><a href="#"></i>Surat Keterangan Pengalaman Kerja</a></li>
            <li class="active">Add</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Employeement | Surat Keterangan Pengalaman Kerja | Add</h3>
                        <hr>
                    </div>

                    <div class="box-body table-responsive">    	
                        <?php echo form_open('add/surat_keterangan_kerja_register'); ?>
                        <div class="nav-tabs-custom">
                            <div class="tab-content">
                                <div id="biodata" class="tab-pane fade in active">
                                    <div class="chart tab-pane active">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <!--Form Biodata-->
                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
													<div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Department</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <select class="form-control">
                                                            <option fisible="false">List of Department</option>
                                                            <option>--</option>
                                                            <option>--</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Division</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <select class="form-control">
                                                            <option fisible="false">List of Division</option>
                                                            <option>--</option>
                                                            <option>--</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Employee Name</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>
												
												<div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Last Position</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="">
                                                    </div>
                                                </div>
												
												<div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Period of Employeement</label>
                                                    </div> 
                                                    <div class="col-md-1 col-xs-12 col-sm-7">
                                                        <label for="comment">From :</label>
                                                    </div>
													<div class="col-md-2 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="" value="26 - 05 - 2014" disabled>
                                                    </div>
													<div class="col-md-1 col-xs-12 col-sm-7">
                                                        <label for="comment">To :</label>
                                                    </div>
													<div class="col-md-2 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="" value="01 - 03 - 2017" disabled>
                                                    </div>
                                                </div>
												
                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Identity Card Number</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="" disabled>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Identity Card Address</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <input type="text" class="form-control" id="" placeholder="" disabled>
                                                    </div>
                                                </div>

                                                <!--batas-->
                                            </div>
                                            <div class="col-md-8 col-xs-12 col-sm-12" style="padding-top:10px; padding-bottom:10px;">
                                                <div class="col-md-4 col-xs-4 col-sm-4">
                                                    <a href="<?php echo base_url('dashboard/list_surat_keterangan_pengalaman_kerja'); ?>"><input type="button" class="btn btn-block btn-success" value="Submit"></a>
                                                </div>
                                                <div class="col-md-4 col-xs-4 col-sm-4">
                                                    <a href="<?php echo base_url('dashboard/list_surat_keterangan_pengalaman_kerja'); ?>"><input type="button" class="btn btn-block btn-save-as" value="Save As Draft"></a>
                                                </div>
                                                <div class="col-md-4 col-xs-4 col-sm-4">
                                                    <a href="<?php echo base_url('dashboard/list_surat_keterangan_pengalaman_kerja'); ?>"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close() ?>
                </div>
            </div>
        </div>
    </section>
</div>
