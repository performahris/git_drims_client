<link href="../../asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<div class="content-wrapper">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li><a href="#"></i>Benefit Management</a></li>
            <li><a href="#"></i>Payroll</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box table-responsive">
                    <div class="box-header">
                        <h3 class="box-title">Benefit Management | Payroll | Add</h3>
                        <hr>
                    </div>
                    <div class="box-body">
                      <div class="col-md-12 col-sm-12 xs-12">
                    <center><h3>PAYROLL</h3></center>
                    <hr>
                  </div>
                  <div class="panel panel-default">
                        <div class="panel-body">
                          <div class="col-md-12">
                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                <div class="col-md-2 col-xs-12 col-sm-4">
                                    <label for="comment">Employee Code</label>
                                </div> 
                                <div class="col-md-3 col-xs-12 col-sm-5">
                                    <input type="text" class="form-control" id="" placeholder="Employee Code"> 
                                </div>
                                <div class="col-md-2 col-xs-12 col-sm-4">
                                    <label for="comment">Grade</label>
                                </div> 
                                <div class="col-md-1 col-xs-12 col-sm-5">
                                    <input type="text" class="form-control" id="" disabled> 
                                </div>
                            </div>
                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                <div class="col-md-2 col-xs-12 col-sm-4">
                                    <label for="comment">Employee Name</label>
                                </div> 
                                <div class="col-md-3 col-xs-12 col-sm-5">
                                    <input type="text" class="form-control" id="" disabled> 
                                </div>
                                <div class="col-md-2 col-xs-12 col-sm-4">
                                    <label for="comment">Working Days</label>
                                </div> 
                                <div class="col-md-1 col-xs-12 col-sm-5">
                                    <input type="text" class="form-control" id="" disabled> 
                                </div>
                            </div>
                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                <div class="col-md-2 col-xs-12 col-sm-4">
                                    <label for="comment">Position</label>
                                </div> 
                                <div class="col-md-3 col-xs-12 col-sm-5">
                                    <input type="text" class="form-control" id="" disabled> 
                                </div>
                                <div class="col-md-2 col-xs-12 col-sm-4">
                                    <label for="comment">Attendance</label>
                                </div> 
                                <div class="col-md-1 col-xs-12 col-sm-5">
                                    <input type="text" class="form-control" id="" disabled> 
                                </div>
                            </div>
                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                <div class="col-md-2 col-xs-12 col-sm-4">
                                    <label for="comment">Department</label>
                                </div> 
                                <div class="col-md-3 col-xs-12 col-sm-5">
                                    <input type="text" class="form-control" id="" disabled> 
                                </div>
                                <div class="col-md-2 col-xs-12 col-sm-4">
                                    <label for="comment">Absense</label>
                                </div> 
                                <div class="col-md-1 col-xs-12 col-sm-5">
                                    <input type="text" class="form-control" id="" disabled> 
                                </div>
                            </div>
                <hr>
                          <div class="row" style="padding-top:5px; padding-bottom:5px;">
                            <table class="table table-striped table-bordered">
                              <tr class="success">
                                <th ><center>ALLOWANCE</center></th>
                                <th ><center>DECUTION</center></th>
                              </tr>
                              <tr>
                                <td >
                                  <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-5 col-xs-12 col-sm-4">
                                        <label for="comment">Gaji Pokok</label>
                                    </div> 
                                    <div class="col-md-7 col-xs-12 col-sm-5">
                                        <input type="text" class="form-control" id="" disabled> 
                                    </div>
                                  </div>
                                  <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-5 col-xs-12 col-sm-4">
                                        <label for="comment">Jabatan</label>
                                    </div> 
                                    <div class="col-md-7 col-xs-12 col-sm-5">
                                        <input type="text" class="form-control" id=""> 
                                    </div>
                                  </div>
                                  <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-5 col-xs-12 col-sm-4">
                                        <label for="comment">Transpot</label>
                                    </div> 
                                    <div class="col-md-7 col-xs-12 col-sm-5">
                                        <input type="text" class="form-control" id=""> 
                                    </div>
                                  </div>
                                  <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-5 col-xs-12 col-sm-4">
                                        <label for="comment">Uang Makan</label>
                                    </div> 
                                    <div class="col-md-7 col-xs-12 col-sm-5">
                                        <input type="text" class="form-control" id=""> 
                                    </div>
                                  </div>
                                  <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-5 col-xs-12 col-sm-4">
                                        <label for="comment">Bonus</label>
                                    </div> 
                                    <div class="col-md-7 col-xs-12 col-sm-5">
                                        <input type="text" class="form-control" id=""> 
                                    </div>
                                  </div>
                                  <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-5 col-xs-12 col-sm-4">
                                        <label for="comment">Uang Lembur</label>
                                    </div> 
                                    <div class="col-md-7 col-xs-12 col-sm-5">
                                        <input type="text" class="form-control" id=""> 
                                    </div>
                                  </div>
      <div class="row" style="padding-top:5px; padding-bottom:5px;" id="div_allowance">
        <div class="col-md-5 col-xs-12 col-sm-4">
            <input type="text" class="form-control" id="" placeholder="Lain-lainya"> 
        </div> 
        <div class="col-md-7 col-xs-12 col-sm-4">
            <input type="text" class="form-control">
        </div>  
      </div>
      <div class="row" style="padding-top:1px; padding-bottom:1px;" id="div_add_allowance">
        <div class="col-md-12 col-xs-12 col-sm-4" style="text-align:right;">
            <a class="glyphicon glyphicon-plus" style="padding-top:10px;" onclick="add_allowance()"></a>
            <a class="glyphicon glyphicon-trash" style="padding-top:10px;" onclick="remove_allowance()"></a>
        </div>  
      </div>
                                </td>
                                <td >
                                  <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-5 col-xs-12 col-sm-4">
                                        <label for="comment">Potongan PPH</label>
                                    </div> 
                                    <div class="col-md-7 col-xs-12 col-sm-5">
                                        <input type="text" class="form-control" id="" disabled> 
                                    </div>
                                  </div>
                                  <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-5 col-xs-12 col-sm-4">
                                        <label for="comment">Iuran Pensiun</label>
                                    </div> 
                                    <div class="col-md-7 col-xs-12 col-sm-5">
                                        <input type="text" class="form-control" id=""> 
                                    </div>
                                  </div>
                                  <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-5 col-xs-12 col-sm-4">
                                        <label for="comment">Pinjaman</label>
                                    </div> 
                                    <div class="col-md-7 col-xs-12 col-sm-5">
                                        <input type="text" class="form-control" id=""> 
                                    </div>
                                  </div>
                                  <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-5 col-xs-12 col-sm-4">
                                        <label for="comment">Terlambat</label>
                                    </div> 
                                    <div class="col-md-7 col-xs-12 col-sm-5">
                                        <input type="text" class="form-control" id=""> 
                                    </div>
                                  </div>
        <div class="row" style="padding-top:5px; padding-bottom:5px;" id="div_decution">
          <div class="col-md-5 col-xs-12 col-sm-4">
            <input type="text" class="form-control" id="" placeholder="Lain-lainya"> 
          </div> 
          <div class="col-md-7 col-xs-12 col-sm-4">
            <input type="text" class="form-control">
          </div>  
        </div>
        <div class="row" style="padding-top:1px; padding-bottom:1px;" id="div_add_decution">
          <div class="col-md-12 col-xs-12 col-sm-4" style="text-align:right;">
            <a class="glyphicon glyphicon-plus" style="padding-top:10px;" onclick="add_decution()"></a>
            <a class="glyphicon glyphicon-trash" style="padding-top:10px;" onclick="remove_decution()"></a>
          </div>  
        </div>
                                </td>
                              </tr>
                              <tr class="success">
                                <td style="text-align:right;">
                                    <div class="col-md-5 col-xs-12 col-sm-4">
                                        <label for="comment">TOTAL ALLOWANCE</label>
                                    </div> 
                                    <div class="col-md-7 col-xs-12 col-sm-5">
                                        <input type="text" class="form-control" id=""> 
                                    </div>
                                </td>
                                <td style="text-align:right;">
                                    <div class="col-md-5 col-xs-12 col-sm-4">
                                        <label for="comment">TOTAL ALLOWANCE</label>
                                    </div> 
                                    <div class="col-md-7 col-xs-12 col-sm-5">
                                        <input type="text" class="form-control" id=""> 
                                    </div>
                                </td>
                              </tr>
                              <tr class="info">
                                  <td colspan="2" style="text-align:center;">
                                    <div class="col-md-2 col-xs-12 col-sm-4">
                                        <label for="comment">TOTAL ALLOWANCE</label>
                                    </div> 
                                    <div class="col-md-10 col-xs-12 col-sm-5">
                                        <input type="text" class="form-control" id=""> 
                                    </div>
                                  </td>
                              </tr>
                            </table>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-xs-12 col-sm-12" style="padding-top:10px; padding-bottom:10px;">
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                <a href="<?php echo base_url('dashboard/payroll_list'); ?>"><input type="button" class="btn btn-block btn-success" value="Submit"></a>
                            </div>
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                <a href="<?php echo base_url('dashboard/payroll_list'); ?>"><input type="button" class="btn btn-block btn-save-as" value="Save As Draft"></a>
                            </div>
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                <a href="<?php echo base_url('dashboard/payroll_list'); ?>"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
                            </div>  
                        </div>


            </div>
        </div>
      </div>
    </div>
    </section>
</div>


<script>
function add_allowance() {
    var i = 0;
    var original = document.getElementById('div_allowance');
    var clone = original.cloneNode(true); // "deep" clone
    clone.id = "div_allowance" + ++i;

    var firstInput = clone.getElementsByTagName("input")[0];
    var secondInput = clone.getElementsByTagName("input")[1];
    firstInput.value = "";
    secondInput.value = "";
    //original.parentNode.appendChild(clone);
    original.parentNode.insertBefore(clone, document.getElementById("div_add_allowance"));
    //original.insertAdjacentHTML('afterend',clone.outerHTML);
}
function remove_allowance() {
   var original = document.getElementById('div_allowance');
   var length = original.parentNode.getElementsByTagName("div").length;
   var lastDiv = original.parentNode.getElementsByTagName("div")[length-2];
   original.parentNode.remove(lastDiv);
}
</script>

<script>
function add_decution() {
    var i = 0;
    var original = document.getElementById('div_decution');
    var clone = original.cloneNode(true); // "deep" clone
    clone.id = "div_decution" + ++i;

    var firstInput = clone.getElementsByTagName("input")[0];
    var secondInput = clone.getElementsByTagName("input")[1];
    firstInput.value = "";
    secondInput.value = "";
    //original.parentNode.appendChild(clone);
    original.parentNode.insertBefore(clone, document.getElementById("div_add_decution"));
    //original.insertAdjacentHTML('afterend',clone.outerHTML);
}
</script>

<!--
<script type="text/javascript">
    $(document).ready(function() {
      $(".departemen").select2();
    });
    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      $scope.sortType     = 'no'; // set the default sort type
      $scope.sortReverse  = false;  // set the default sort order
      $scope.search   = ''; 
      $scope.limit =25;
      $scope.dari="";
      $scope.to=""; 
      $scope.depart="";  
      $scope.jumlah = 0;
                   
      
      $scope.employee = <?php echo $employee ?>;
      
      
      $scope.departemen = [
        {nama : "Purchasing"},
        {nama : "Research"},
        {nama : "HRD"}
        ];
        
        $scope.reset = function () {
            $scope.depart =  "";     
        } 
        
        
       $scope.currentPage = 1;  
       $scope.totalItems = $scope.employee.length;  
       $scope.numPerPage = $scope.limit;    
       
       $scope.limitPage = function() {
         $scope.numPerPage = $scope.limit;
         if($scope.currentPage * $scope.numPerPage > $scope.employee.length){
            $scope.currentPage = 1;   
         }
      };
       
       $scope.lastPage = function() {      
         $scope.currentPage=$scope.pageCount();
      };
      
      $scope.firstPage = function() {
         $scope.currentPage=1;
      };
       
       $scope.nextPage = function() {
        
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
      };
      
      $scope.jumlahPerpage = function (value) {
        $scope.numPerPage = value;
      } 
      
      $scope.prevPage = function() {
        if ($scope.currentPage > 1) {
          $scope.currentPage--;
        }
      };
      
       $scope.pageCount = function() {
        return Math.ceil($scope.jumlah/$scope.numPerPage);
      };  
      
      $scope.checkAll = function () {
        angular.forEach($scope.employee, function (item) {
            item.Selected = $scope.selectAll;
        });
      };
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    app.filter("dateRange", function() {      
      return function(items, dari, to) {
        if(dari.length==0){
            var dari = +new Date("1980-01-01");
        }else{
            var dari = +new Date(dari);
        }
        
        if(to.length==0){
            var to = +new Date();
        }else{
            var to = +new Date(to);
        }
        var df = dari;
        var dt = to ;
        var arrayToReturn = [];        
        for (var i=0; i<items.length; i++){
            var tf = +new Date(items[i].join);
            if ((tf > df && tf < dt) || (tf==dt) )  {
                arrayToReturn.push(items[i]);
            }
        }
        
        return arrayToReturn;
      };
        
    });
    
    
    app.filter("paging", function() {      
      return function(items, limit, currentPage) {
        if (typeof limit === 'string' || limit instanceof String){
           limit = parseInt(limit);
         }
         
         var begin, end, index;  
         begin = (currentPage - 1) * limit;  
         end = begin + limit;
         var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (begin <= i && i < end )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    
    app.filter("departemen", function() {      
      return function(items, depart) {
      if(depart.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].dept == depart )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    
    app.filter("nama", function() {      
      return function(items, search) {
      if(search.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].name.toUpperCase().indexOf(search.toUpperCase()) != -1 )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    

</script>