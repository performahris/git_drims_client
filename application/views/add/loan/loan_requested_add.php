<link href="../../asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<div class="content-wrapper">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Loan</li>
            <li class="active">Requested</li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Loan | Requested | Add</h3>
                        <hr>
                    </div>
                    <div class="box-body">
                    	<div class="col-md-12 col-sm-12 xs-12">
			           		<center><h3>PERMOHONAN PINJAMAN KARYAWAN</h3></center>
			           		<hr>
			            </div>
			            <div class="panel panel-default">
                    		<div class="panel-body">
                    			<div class="col-md-12">
				                	<div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Employee Code</label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                            <input type="text" class="form-control" id="" placeholder="Employee Code"> 
				                        </div>
				                    </div>
				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Employee Name</label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                            <input type="text" class="form-control" id="" disabled> 
				                        </div>
				                    </div>
				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Position</label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                            <input type="text" class="form-control" id="" disabled> 
				                        </div>
				                    </div>
				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Years of Employment</label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                            <input type="text" class="form-control" id="" disabled> 
				                        </div>
				                    </div>
				        <hr>
				                	<div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Pinjaman Yang Diajukan</label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                            <input type="text" class="form-control" id=""> 
				                        </div>
				                    </div>
				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Angsuran Perbulan</label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                            <input type="text" class="form-control" id=""> 
				                        </div>
				                    </div>
				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Mulai Bulan</label>
				                        </div> 
				                        <div class="col-md-2 col-xs-12 col-sm-5">
				                            <select class="form-control">
                                                <option fisible="false">List of Month</option>
                                                <option>January</option>
                                                <option>February</option>
                                                <option>March</option>
                                                <option>April</option>
                                                <option>May</option>
                                                <option>June</option>
                                                <option>July</option>
                                                <option>August</option>
                                                <option>September</option>
                                                <option>October</option>
                                                <option>November</option>
                                                <option>December</option>                                                            
                                            </select>
				                        </div>
				                    </div>
				        <hr>
				                	<div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Untuk Keperluan</label>
				                        </div> 
				                        <div class="col-md-9 col-xs-12 col-sm-5">
				                        	<div class="col-md-7" style="padding-bottom:5px; font-size:15px;">
				                   				<input type="checkbox">Kematian Istri / Suami / Anak / Orang tua kandung 
				                        	</div>
				                        	<div class="col-md-7" style="padding-bottom:5px; font-size:15px;">
				                        		<input type="checkbox">Sakit Berat
				                        	</div>
				                        	<div class="col-md-7" style="padding-bottom:5px; font-size:15px;">
				                        		<input type="checkbox">Musibah <input type="text" class="form-control" id=""> 
				                        	</div>
				                        </div>
				                    </div>

				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Bukti Pendukung</label>
				                        </div> 
				                        <div class="col-md-9 col-xs-12 col-sm-5">
				                        	<div class="col-md-7" style="padding-bottom:10px; font-size:15px;">
				                   				<center>
	       										<input type="file" name="photo" class="form-control">
	       										</center> 
				                        	</div>
				                        	<div class="col-md-7" style="padding-bottom:10px; font-size:15px;">
				                        		<center>
	       										<input type="file" name="photo" class="form-control">
	       										</center> 
				                        	</div>
				                        </div>
				                    </div>
				                    <hr>

				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Sisa Pinjaman Lama </label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                        	<input type="text" class="form-control" id="" value="Rp." disabled>
				                        </div>
				                        <div class="col-md-2 col-xs-12 col-sm-4">
				                            <label for="comment">Angsuran Perbulan </label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                        	<input type="text" class="form-control" id="" value="Rp." disabled>
				                        </div>
				                    </div>
				                    
				                </div>
				            </div>
				        </div>
				        <div class="col-md-8 col-xs-12 col-sm-12" style="padding-top:10px; padding-bottom:10px;">
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                <a href="<?php echo base_url('dashboard/loan_requested_list'); ?>"><input type="button" class="btn btn-block btn-success" value="Submit"></a>
                            </div>
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                <a href="<?php echo base_url('dashboard/loan_requested_list'); ?>"><input type="button" class="btn btn-block btn-save-as" value="Save As Draft"></a>
                            </div>
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                <a href="<?php echo base_url('dashboard/loan_requested_list'); ?>"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
                            </div>	
                        </div>


				    </div>
				</div>
			</div>
		</div>
    </section>
</div>




<script type="text/javascript">
    $(document).ready(function() {
      $(".departemen").select2();
    });
    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      $scope.sortType     = 'no'; // set the default sort type
      $scope.sortReverse  = false;  // set the default sort order
      $scope.search   = ''; 
      $scope.limit =25;
      $scope.dari="";
      $scope.to=""; 
      $scope.depart="";  
      $scope.jumlah = 0;
                   
      
      $scope.employee = <?php echo $employee ?>;
      
      
      $scope.departemen = [
        {nama : "Purchasing"},
        {nama : "Research"},
        {nama : "HRD"}
        ];
        
        $scope.reset = function () {
            $scope.depart =  "";     
        } 
        
        
       $scope.currentPage = 1;  
       $scope.totalItems = $scope.employee.length;  
       $scope.numPerPage = $scope.limit;    
       
       $scope.limitPage = function() {
         $scope.numPerPage = $scope.limit;
         if($scope.currentPage * $scope.numPerPage > $scope.employee.length){
            $scope.currentPage = 1;   
         }
      };
       
       $scope.lastPage = function() {      
         $scope.currentPage=$scope.pageCount();
      };
      
      $scope.firstPage = function() {
         $scope.currentPage=1;
      };
       
       $scope.nextPage = function() {
        
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
      };
      
      $scope.jumlahPerpage = function (value) {
        $scope.numPerPage = value;
      } 
      
      $scope.prevPage = function() {
        if ($scope.currentPage > 1) {
          $scope.currentPage--;
        }
      };
      
       $scope.pageCount = function() {
        return Math.ceil($scope.jumlah/$scope.numPerPage);
      };  
      
      $scope.checkAll = function () {
        angular.forEach($scope.employee, function (item) {
            item.Selected = $scope.selectAll;
        });
      };
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    app.filter("dateRange", function() {      
      return function(items, dari, to) {
        if(dari.length==0){
            var dari = +new Date("1980-01-01");
        }else{
            var dari = +new Date(dari);
        }
        
        if(to.length==0){
            var to = +new Date();
        }else{
            var to = +new Date(to);
        }
        var df = dari;
        var dt = to ;
        var arrayToReturn = [];        
        for (var i=0; i<items.length; i++){
            var tf = +new Date(items[i].join);
            if ((tf > df && tf < dt) || (tf==dt) )  {
                arrayToReturn.push(items[i]);
            }
        }
        
        return arrayToReturn;
      };
        
    });
    
    
    app.filter("paging", function() {      
      return function(items, limit, currentPage) {
        if (typeof limit === 'string' || limit instanceof String){
           limit = parseInt(limit);
         }
         
         var begin, end, index;  
         begin = (currentPage - 1) * limit;  
         end = begin + limit;
         var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (begin <= i && i < end )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    
    app.filter("departemen", function() {      
      return function(items, depart) {
      if(depart.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].dept == depart )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    
    app.filter("nama", function() {      
      return function(items, search) {
      if(search.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].name.toUpperCase().indexOf(search.toUpperCase()) != -1 )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    

</script>