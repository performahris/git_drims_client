<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
           <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">SPPD</li>
            <li class="active">Requested</li>
            <li class="active">Add Realisasi</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">SPPD | Requested | Add Penyelesaian Biaya Creation</h3>
                        <hr>
                    </div>
                    <div class="box-body table-responsive">
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label>
								<div class="col-md-12 col-xs-12 col-sm-12">No</div>
							</label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
									01/HRD/01/2016
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<div class="col-md-3 col-xs-12 col-sm-12">By</div>
									<div class="col-md-9 col-xs-12 col-sm-12" style="font-weight:normal">
										<b>Shinta</b>
									</div>
								</div>
							</label>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Nama</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:67%">
								<div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="nama" class="form-control"  placeholder ="Name"/>	
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Jabatan/Golongan</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="Jabatan" class="form-control"  placeholder ="Jabatan/Golongan"/>	
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Division</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="Jabatan" class="form-control"  placeholder ="Division"/>	
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Tujuan</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:90%">
								<div class="col-md-10 col-xs-12 col-sm-12" style="font-weight:normal">
									<textarea name="tujuan" rows="5" class="form-control" ></textarea>
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Periode</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
									 <input type="text" name="periode" class="form-control"  placeholder ="Periode"/>	
								</div>
							</label>		
						</div>
						<div style="height:40px; clear:both;">&nbsp;</div>
					    <table class="table table-striped" style="width:80%; margin:0px auto;">
							<tr>
								<th style="width:20%;"><center>Biaya</center></th>
								<th style="width:40%;"><center>Rupiah</center></th>
								<th style="width:40%;"><center>Keterangan</center></th>
							</tr>
							<tr>
								<td style="vertical-align:middle;"><center>Transportasi</center></td>
								<td colspan="2">
									<table class="table" id="table_transportasi">
										<tr>
											<td><input type="text" class="form-control"></td>
											<td><input type="text" class="form-control"></td>
										</tr>
										<tr>
											<td><input type="text" class="form-control"></td>
											<td><input type="text" class="form-control"></td>
										</tr>
										<tr>
											<td><input type="text" class="form-control"></td>
											<td><input type="text" class="form-control"></td>
										</tr>
										<tr>
											<td></td>
											<td style="text-align:right;"><a class="glyphicon glyphicon-plus" onclick="add_transportasi()"></a></td>
										</tr>
									</table>
								</td>
							</tr>
							    <td style="vertical-align:middle;"><center>Penginapan / Hotel</center></td>
								<td colspan="2">
									<table class="table" id="table_penginapan">
										<tr>
											<td> <input type="text" class="form-control"></td>
											<td><input type="text" class="form-control"></td>
										</tr>
										<tr>
											<td> <input type="text" class="form-control"></td>
											<td><input type="text" class="form-control"></td>
										</tr>
										<tr>
											<td> <input type="text" class="form-control"></td>
											<td><input type="text" class="form-control"></td>
										</tr>
										<tr>
											<td></td>
											<td style="text-align:right;"><a class="glyphicon glyphicon-plus" onclick="add_penginapan()"></a></td>
										</tr>
									</table>
								</td>
							<tr>
								<td style="vertical-align:middle;"><center>Uang Makan</center></td>
								<td colspan="2">
									<table class="table" id="table_uangmakan">
										<tr>
											<td> <input type="text" class="form-control"></td>
											<td><input type="text" class="form-control"></td>
										</tr>
										
										<tr>
											<td></td>
											<td style="text-align:right;"><a class="glyphicon glyphicon-plus" onclick="add_uangmakan()"></a></td>
										</tr>
									</table>
								</td>
							</tr>
							
							<tr>
								<td style="vertical-align:middle;"><center>Lain - lain</center></td>
								<td colspan="2">
								<table class="table" id="table_lainlain">
										<tr>
											<td><input type="text" class="form-control"></td>
											<td><input type="text" class="form-control"></td>
										</tr>
										
										<tr>
											<td></td>
											<td style="text-align:right;"><a class="glyphicon glyphicon-plus" onclick="add_lainlain()"></a></td>
										</tr>
								</table>
								</td>
							</tr>
							
							<tr>
								<td><center>Jumlah</center></td>
								<td colspan="2">
								  <div class="col-md-6">
								  <input type="text" class="form-control">
								  </div>
								</td>
							</tr>
							
							<tr>
								<td><center>Bon Sementara</center></td>
								<td colspan="2">
								<div class="col-md-6">
								<input type="text" class="form-control">
								</div>
								</td>
							</tr>
							
							<tr>
								<td><center>Kurang</center></td>
								<td colspan="2">
								<div class="col-md-6">
								<input type="text" class="form-control">
								</div>
								</td>
							</tr>
							
						
						</table>
						<!--
                        <table class="table table-striped">
                                <tr>
									<td width=20%>No</td>
									<td width=1%>:</td>
									<td>
										<div class="col-md-6 col-xs-12 col-sm-12 pull-left" >(Auto Generate)</div>
										<div class="col-md-6 col-xs-12 col-sm-12 pull-right" >By (Auto Generate)</div>
									</td>
                                </tr>
								<tr>
                                    <td>Nama</td>
									<td>:</td>
									<td>
										<input type="text" name="nama">
									</td>
                                </tr>
								<tr>
									<td>Jabatan / Golongan</td>
									<td>:</td>
									<td><input type="text" name="jabatan"></td>
								</tr>
								<tr>
									<td>Divisi</td>
									<td>:</td>
									<td><input type="text" name="divisi"></td>
								</tr>
								<tr>
									<td>Tujuan</td>
									<td>:</td>
									<td><textarea name="tujuan" rows="5" style="width:70%"></textarea></td>
								</tr>
								<tr>
									<td>Periode</td>
									<td>:</td>
									<td><input type="text" name="periode"/></td>
								</tr>
								
                        </table>
						<br/>
						<h4><strong>Sarana Transportasi Perjalanan Dinas</strong></h4>
						<table id="example1" class="table table-striped">
								
							<tr>
								<td colspan=4><input type="radio" name="sarana" value="pesawat"/> Pesawat</td>
							</tr>
							<tr>
								<td><input type="radio" name="sarana" value="kereta"/> Kereta Api / Bus / Kapal Laut</td>
								<td>Kelas</td>
								<td>:</td>
								<td><input type="text" name="kereta" /></td>
							</tr>
							<tr>
								<td><input type="radio" name="sarana" value="mobil"/> Mobil Operasional Perusahaan</td>
								<td>No Polisi</td>
								<td>:</td>
								<td>
									<select name="name">
										<option>B 3 MO</option>
										<option>B 3 CAK</option>
									</select>
								</td>
							</tr>
							<tr>
								<td><input type="radio" name="sarana" value="lain-lain"/> Lain - Lain</td>
								<td colspan="3"><input type="text" name="lain-lain"/></td>
							</tr>
						</table>
						<br/>
						<h4><strong>KAS BON YANG DIAJUKAN</strong></h4>
						<table id="example1" class="table table-striped" style="width:50%">
							<tr>
								<td>Uang Makan</td>
								<td>:</td>
								<td>IDR</td>
								<td ><input type="text" name="uangmakan"/></td>
							</tr>
							<tr>
								<td>Hotel</td>
								<td>:</td>
								<td>IDR</td>
								<td><input type="text" name="hotel"/></td>
							</tr>
							<tr>
								<td>Transportasi / Taksi P.P</td>
								<td>:</td>
								<td>IDR</td>
								<td ><input type="text" name="transport"/></td>
							</tr>
							<tr>
								<td>Lain - Lain</td>
								<td>:</td>
								<td>IDR</td>
								<td><input type="text" name="lainlain"/></td>
							</tr>
							<tr>
								<td colspan="4"></td>
							</tr>
							<tr>
								<td><strong>Total</strong></td>
								<td>:</td>
								<td><strong>IDR</strong></td>
								<td ><strong>(auto generate)</strong></td>
							</tr>
						</table>
						-->
						<br/>
						<div class="col-md-4 col-xs-12 col-sm-12">
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="<?php echo base_url()."dashboard/sppd_requested" ?>"><input type="button" class="btn btn-block btn-success" value="Submit"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="<?php echo base_url()."dashboard/sppd_requested" ?>"><input type="button" class="btn btn-block btn-save-as" value="Save as Draft"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="<?php echo base_url()."dashboard/sppd_requested" ?>"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);
    app.controller('mainController', function($scope) {
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
</script>

<script>
function add_transportasi() {
    var table = document.getElementById("table_transportasi");
    var row = table.insertRow(table.rows.length-1);
    var cell1 = row.insertCell(0);
            var element1 = document.createElement("input");
            element1.type = "textbox";
           	element1.className = 'form-control  ';
           	cell1.appendChild(element1);
    var cell2 = row.insertCell(1);
            var element2 = document.createElement("input");
            element2.type = "textbox";
            element2.className = 'form-control  ';
            cell2.appendChild(element2);
}

function add_penginapan() {
    var table = document.getElementById("table_penginapan");
    var row = table.insertRow(table.rows.length-1);
    var cell1 = row.insertCell(0);
            var element1 = document.createElement("input");
            element1.type = "textbox";
           	element1.className = 'form-control  ';
           	cell1.appendChild(element1);
    var cell2 = row.insertCell(1);
            var element2 = document.createElement("input");
            element2.type = "textbox";
            element2.className = 'form-control  ';
            cell2.appendChild(element2);
}

function add_uangmakan() {
    var table = document.getElementById("table_uangmakan");
    var row = table.insertRow(table.rows.length-1);
    var cell1 = row.insertCell(0);
            var element1 = document.createElement("input");
            element1.type = "textbox";
           	element1.className = 'form-control  ';
           	cell1.appendChild(element1);
    var cell2 = row.insertCell(1);
            var element2 = document.createElement("input");
            element2.type = "textbox";
            element2.className = 'form-control  ';
            cell2.appendChild(element2);
}

function add_lainlain() {
    var table = document.getElementById("table_lainlain");
    var row = table.insertRow(table.rows.length-1);
    var cell1 = row.insertCell(0);
            var element1 = document.createElement("input");
            element1.type = "textbox";
           	element1.className = 'form-control  ';
           	cell1.appendChild(element1);
    var cell2 = row.insertCell(1);
            var element2 = document.createElement("input");
            element2.type = "textbox";
            element2.className = 'form-control  ';
            cell2.appendChild(element2);
}
</script>

 			
            
            