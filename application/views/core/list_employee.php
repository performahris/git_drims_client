<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div id="content" ng-app="sortApp" ng-controller="mainController">
        <table align="left" width = 100%>
        
        	
            <tr>
            	<td colspan="3" class="titlepage"><strong>Ijin| List Form</strong></td>
            </tr>
        </table>
    <div style="float:right;padding:5px;">
        <form >
            <input type="checkbox" name="chk" ng-model="chk">
            <select ng-model="depart" class="departemen">
                <option value= "">Choose Departemen</option>
                <option value="{{ dept.nama }}" ng-repeat="dept in departemen">{{ dept.nama }}</option>
            </select> 
            <input name="reset" type="submit" value="Reset" ng-click="reset()" />
            <input type="text" placeholder="Seacrh Employee Name" name="keyword" ng-model="search"/>
            <input type="text" name="from" ng-model="from" id="datepicker2"/>
            <input type="text" name="to" ng-model="to" ng-change="filterDateAdded()" id="datepicker1"/>                       
        </form>
    </div>
    <div class="main-content">
        <form method="post">
            <table cellpadding="0" cellspacing="0" style="width: 100%;">
            	<thead>
            	  <tr> 
            		<th >No</th> 
            		<th >Employee ID</th>
            		<th >
                        <a href="#" ng-click="sortType = 'name'; sortReverse = !sortReverse">
                        Emloyee Name
                        <span ng-show="sortType == 'name' && !sortReverse"></span>
                        <span ng-show="sortType == 'name' && sortReverse"></span>
                        </a>
                    </th>
            		<th >
                        <a href="#" ng-click="sortType = 'dept'; sortReverse = !sortReverse">
                        Departement
                        <span ng-show="sortType == 'dept' && !sortReverse"></span>
                        <span ng-show="sortType == 'dept' && sortReverse"></span>
                        </a>
                    </th>
            		<th >
                        <a href="#" ng-click="sortType = 'join'; sortReverse = !sortReverse">
                        Joined Date
                        <span ng-show="sortType == 'join' && !sortReverse"></span>
                        <span ng-show="sortType == 'join' && sortReverse"></span>
                        </a>
                    </th>
                    <th ng-if="chk">Ongoing Project</th>
                    <th ng-if="chk">Next Project</th>
            		<th >Status</th>
                    <th><input type="checkbox" ng-model="selectAll" ng-click="checkAll()" /></th>
            	  </tr>	  
            	</thead>
                <tbody>
                    <tr ng-repeat="em in employee | orderBy:sortType:sortReverse | departemen:depart | nama:search | dateRange:from:to | paging:limit:currentPage">
                        <td>{{($index+1)+((currentPage-1)*limit)}}</td>
                        <td>{{em.id}}</td>
                        <td>{{em.name}}</td>
                        <td>{{em.dept}}</td>
                        <td>{{em.join |date:'dd MMMM yyyy'}}</td>
                        <td ng-if="chk">{{em.ongoing}}</td>
                        <td ng-if="chk">{{em.nextproject}}</td>
                        <td>{{em.status}}</td>  
                        <td><input type="checkbox" ng-model="em.Selected" /></td>
                     </tr>   
                </tbody>
             
            </table>
            <div ng-show="false">{{jumlah = (employee | departemen:depart | nama:search | dateRange:from:to ).length }}</div>
                        
        </form>
    </div>
	<div class="clear"></div>
	<div class="toolbar">
		<div class="paging">
			<div style="padding-top:12px;float:left;">
				<a href="" ng-click="firstPage()"><i class="fa fa-fast-backward" style="color:black"></i></a>&nbsp;
				<a href="" ng-click="prevPage()"><i class="fa fa-backward" style="color:black"></i></a>&nbsp;&nbsp;PAGE&nbsp;&nbsp;
			</div>
			<input type="text" class="pager" value="{{currentPage}}" /></a>
			<div style="padding-top:12px;float:right;">
				&nbsp;&nbsp;OF {{pageCount()}}&nbsp;&nbsp;
				<a href="" ng-click="nextPage()"><i class="fa fa-forward" style="color:black"></i></a>&nbsp;
				<a href="" ng-click="lastPage()"><i class="fa fa-fast-forward" style="color:black"></i></a>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
      $(".departemen").select2();
    });
    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      $scope.sortType     = 'no'; // set the default sort type
      $scope.sortReverse  = false;  // set the default sort order
      $scope.search   = ''; 
      $scope.limit =25;
      $scope.from="";
      $scope.to=""; 
      $scope.depart="";  
      $scope.jumlah = 0;
                   
      
      $scope.employee = [
        { no: '1', id: 'E1', name: 'Danny', dept: 'HRD', join: "2013-02-01", status: 'Single', ongoing : 'PA', nextproject : 'PZ'},
        { no: '2', id: 'E2', name: 'Erry', dept: 'Purchasing', join: "2013-02-01", status: 'Single', ongoing : 'PB', nextproject : 'PZ'},
        { no: '3', id: 'E3', name: 'Tri', dept: 'Purchasing', join: "2013-02-01", status: 'Single', ongoing : 'PC', nextproject : 'PZ'},
        { no: '1', id: 'E4', name: 'Handhika', dept: 'Purchasing', join: "2013-02-01", status: 'Single', ongoing : 'PA', nextproject : 'PZ'},
        { no: '2', id: 'E5', name: 'Bemper', dept: 'Purchasing', join: "2013-02-01", status: 'Single', ongoing : 'PA', nextproject : 'PZ'},
        { no: '3', id: 'E6', name: 'Kili', dept: 'HRD', join: "2013-02-01", status: 'Single', ongoing : 'PC', nextproject : 'PZ,PX'},
        { no: '1', id: 'E7', name: 'Rahasia', dept: 'HRD', join: "2013-02-10", status: 'Single', ongoing : '-', nextproject : '-'},
        { no: '2', id: 'E8', name: 'Aku', dept: 'HRD', join: "2013-03-01", status: 'Single', ongoing : '-', nextproject : '-'},
        { no: '3', id: 'E9', name: 'Kamu', dept: 'HRD', join: "2013-02-01", status: 'Single', ongoing : '-', nextproject : '-'},
        { no: '4', id: 'E10', name: 'Dirinya', dept: 'Research', join: "2013-02-01", status: 'Jomblo', ongoing : '-', nextproject : '-'}
      ];
      
      
      $scope.departemen = [
        {nama : "Purchasing"},
        {nama : "Research"},
        {nama : "HRD"}
        ];
        
        $scope.reset = function () {
            $scope.depart =  "";     
        } 
        
        
       $scope.currentPage = 1;  
       $scope.totalItems = $scope.employee.length;  
       $scope.numPerPage = $scope.limit;    
       
       $scope.limitPage = function() {
         $scope.numPerPage = $scope.limit;
         if($scope.currentPage * $scope.numPerPage > $scope.employee.length){
            $scope.currentPage = 1;   
         }
      };
       
       $scope.lastPage = function() {      
         $scope.currentPage=$scope.pageCount();
      };
      
      $scope.firstPage = function() {
         $scope.currentPage=1;
      };
       
       $scope.nextPage = function() {
        
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
      };
      
      $scope.jumlahPerpage = function (value) {
        $scope.numPerPage = value;
      } 
      
      $scope.prevPage = function() {
        if ($scope.currentPage > 1) {
          $scope.currentPage--;
        }
      };
      
       $scope.pageCount = function() {
        return Math.ceil($scope.jumlah/$scope.numPerPage);
      };  
      
      $scope.checkAll = function () {
        angular.forEach($scope.employee, function (item) {
            item.Selected = $scope.selectAll;
        });
      };
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    app.filter("dateRange", function() {      
      return function(items, from, to) {
        if(from.length==0){
            var from = +new Date("1980-01-01");
        }else{
            var from = +new Date(from);
        }
        
        if(to.length==0){
            var to = +new Date();
        }else{
            var to = +new Date(to);
        }
        var df = from;
        var dt = to ;
        var arrayToReturn = [];        
        for (var i=0; i<items.length; i++){
            var tf = +new Date(items[i].join);
            if ((tf > df && tf < dt) || (tf==dt) )  {
                arrayToReturn.push(items[i]);
            }
        }
        
        return arrayToReturn;
      };
        
    });
    
    
    app.filter("paging", function() {      
      return function(items, limit, currentPage) {
        if (typeof limit === 'string' || limit instanceof String){
           limit = parseInt(limit);
         }
         
         var begin, end, index;  
         begin = (currentPage - 1) * limit;  
         end = begin + limit;
         var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (begin <= i && i < end )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    
    app.filter("departemen", function() {      
      return function(items, depart) {
      if(depart.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].dept == depart )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    
    app.filter("nama", function() {      
      return function(items, search) {
      if(search.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].name.toUpperCase().indexOf(search.toUpperCase()) != -1 )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    

</script>