<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header">&nbsp;</li>
            <li>
                <a href="<?php echo base_url('dashboard'); ?>">
                    <i class="glyphicon glyphicon-home"></i> <span>Dashboard</span>
                </a>
            </li>
            <!--1-->
            <li>
                <a href="#">
                    <i class="fa fa-users"></i> <span>Employeement</span> <i class="fa fa-angle-left pull-right"></i>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('dashboard/list_all_employee'); ?>"><i class="fa fa-user"></i>Employee</a></li>
                        <li><a href="#"><i class="fa fa-file-text"></i>Contract</a></li>
                        <li><a href="<?php echo base_url('dashboard/list_all_warning_report'); ?>"><i class="fa fa-file"></i>Warning Report</a></li>
                        <li><a href="<?php echo base_url('dashboard/list_surat_keterangan_kerja'); ?>"><i class="fa fa-file-text-o"></i>Surat Keterangan Kerja</a></li>
                        <li><a href="<?php echo base_url('dashboard/list_surat_keterangan_pengalaman_kerja'); ?>"><i class="fa fa-file-text-o"></i>Surat Keterangan 
                                    <br><span style="padding-left:20px;">Pengalaman Kerja</span></a></li>
                        <li><a href="#"><i class="fa fa-file-text-o"></i>Surat Keterangan Gaji</a></li>
                        <li><a href="#"><i class="fa fa-diamond"></i>Facilities</a></li>
                    </ul>
                </a>
            </li>
            <!--2-->
            <li>
                <a href="#">
                    <i class="fa fa-desktop"></i> <span>Personnel Schedule</span> <i class="fa fa-angle-left pull-right"></i>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('dashboard/schedule_list'); ?>"><i class="fa fa-star-o"></i>Scheduled</a></li>
                        <li><a href="<?php echo base_url('dashboard/adjustment_list'); ?>"><i class="fa fa-gear"></i>Adjustment</a></li>
                        <li><a href="<?php echo base_url('dashboard/actualization_list'); ?>"><i class="fa fa-street-view"></i>Actualization</a></li>
                    </ul>
                </a>
            </li>
            <!--3-->
            <li>
                <a href="#">
                    <i class="fa fa-reorder"></i> <span>Leave Request</span> <i class="fa fa-angle-left pull-right"></i>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('dashboard/leave_requested');?>"><i class="fa fa-edit"></i>Requested</a></li>
                        <li><a href="<?php echo base_url('dashboard/leave_released');?>"><i class="fa fa-check-square-o"></i>Released</a></li>
                    </ul>
                </a>
            </li>
            <!--4-->
            <li>
                <a href="#">
                    <i class="fa fa-file-text"></i> <span>SPPD</span> <i class="fa fa-angle-left pull-right"></i>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('dashboard/sppd_requested'); ?>"><i class="fa fa-edit"></i>Requested</a></li>
                        <li><a href="<?php echo base_url('dashboard/sppd_released'); ?>"><i class="fa fa-check-square-o"></i>Released</a></li>
                    </ul>
                </a>
            </li>
            <!--5-->
            <li>
                <a href="#">
                    <i class="fa fa-file-text"></i> <span>Ijin</span> <i class="fa fa-angle-left pull-right"></i>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('dashboard/ijin_requested'); ?>"><i class="fa fa-edit"></i>Requested</a></li>
                        <li><a href="<?php echo base_url('dashboard/ijin_released'); ?>"><i class="fa fa-check-square-o"></i>Released</a></li>
                    </ul>
                </a>
            </li>
            <!--6-->
            <li>
                <a href="#">
                    <i class="glyphicon glyphicon-usd"></i> <span>Tunjangan</span> <i class="fa fa-angle-left pull-right"></i>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('dashboard/tunjangan_requested');?>"><i class="fa fa-edit"></i>Requested</a></li>
                        <li><a href="<?php echo base_url('dashboard/tunjangan_released');?>"><i class="fa fa-check-square-o"></i>Released</a></li>
                    </ul>
                </a>
            </li>
            <!--7-->
            <li>
                <a href="#">
                    <i class="fa fa-institution"></i> <span>Booking Room</span> <i class="fa fa-angle-left pull-right"></i>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('dashboard/booking_room_requested_list');?>"><i class="fa fa-edit"></i>Requested</a></li>
                        <li><a href="<?php echo base_url('dashboard/booking_room_released_list');?>"><i class="fa fa-check-square-o"></i>Released</a></li>
                    </ul>
                </a>
            </li>
            <!--8-->
            <li>
                <a href="#">
                    <i class="glyphicon glyphicon-usd"></i> <span>Loan</span> <i class="fa fa-angle-left pull-right"></i>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('dashboard/loan_requested_list'); ?>"><i class="fa fa-edit"></i>Requested</a></li>
                        <li><a href="<?php echo base_url('dashboard/loan_released_list'); ?>"><i class="fa fa-check-square-o"></i>Released</a></li>
                    </ul>
                </a>
            </li>
            <!--9-->
            <li>
                <a href="#">
                    <i class="glyphicon glyphicon-list-alt"></i> <span>Announcement</span> <i class="fa fa-angle-left pull-right"></i>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('dashboard/announcement_submision_list');?>"><i class="glyphicon glyphicon-open"></i>Submission</a></li>
                        <li><a href="<?php echo base_url('dashboard/released_announcement_list');?>"><i class="glyphicon glyphicon-tasks"></i>Released Annoucement</a></li>
                   </ul>
                </a>
            </li>
            <!--10-->
            <li>
                <a href="#">
                    <i class="fa fa-plane"></i> <span>Travel Authorization</span> <i class="fa fa-angle-left pull-right"></i>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('dashboard/travel_authorization_requested');?>"><i class="fa fa-edit"></i>Requested</a></li>
                        <li><a href="<?php echo base_url('dashboard/travel_authorization_released');?>"><i class="fa fa-check-square-o"></i>Released</a></li>
                    </ul>
                </a>
            </li>
            <!--11-->
            <li>
                <a href="#">
                    <i class="glyphicon glyphicon-user"></i> <span>Man Power Request</span> <i class="fa fa-angle-left pull-right"></i>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('dashboard/man_power_requested');?>"><i class="fa fa-edit"></i>Requested</a></li>
                        <li><a href="<?php echo base_url('dashboard/man_power_released');?>"><i class="fa fa-check-square-o"></i>Released</a></li>
                    </ul>
                </a>
            </li>
            <!--12-->
            <li>
                <a href="#">
                    <i class="fa fa-users"></i> <span>Recruitment</span> <i class="fa fa-angle-left pull-right"></i>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('dashboard/vacancy');?>"><i class="fa fa-users"></i>Vacancy</a></li>
                        <li><a href="<?php echo base_url('dashboard/applicant');?>"><i class="fa fa-user-plus"></i>Applicant</a></li>
                        <li><a href="<?php echo base_url('dashboard/penarikan_karyawan');?>"><i class="fa fa-user"></i>Penarikan Karyawan</a></li>
                    </ul>
                </a>
            </li>
            <!--13-->
            <li>
                <a href="#">
                    <i class="fa fa-suitcase"></i> <span>Benefit Management</span> <i class="fa fa-angle-left pull-right"></i>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('dashboard/benefit_sheet');?>"><i class="fa fa-user"></i>Employee Benefit</a></li>
                        <li><a href="<?php echo base_url('dashboard/employee_benefit');?>"><i class="fa fa-envelope-o"></i>Benefit Sheet</a></li>
                        <li><a href="<?php echo base_url('dashboard/payroll_list');?>"><i class="glyphicon glyphicon-usd"></i>Payroll</a></li>
                    </ul>
                </a>
            </li>
            <!--14-->
            <li>
                <a href="#">
                    <i class="fa fa-mail-reply"></i> <span>Exit Clearance</span> <i class="fa fa-angle-left pull-right"></i>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('dashboard/interview_list'); ?>"><i class="fa fa-user-secret"></i>Interview</a></li>
                        <li><a href="<?php echo base_url('dashboard/clearance_checklist_list'); ?>"><i class="fa fa-check-square-o"></i>Clearance Checklist</a></li>
                    </ul>
                </a>
            </li>
            <!--15-->
            <li>
                <a href="<?php echo base_url('dashboard/holiday_management');?>">
                    <i class="fa fa-plane"></i> <span>Holiday Management</span></i>
                </a>
            </li>
            <!--16-->
            <li>
                <a href="<?php echo base_url('dashboard/training_managements');?>"><i class="fa fa-angle-left pull-right"></i>
                    <i class="fa fa-comment-o"></i> <span>Training Management</span></i>
					<ul class="treeview-menu">
                        <li><a href="<?php echo base_url('dashboard/training_requested'); ?>"><i class="fa fa-user-secret"></i>Requested</a></li>
                        <li><a href="<?php echo base_url('dashboard/training_released'); ?>"><i class="fa fa-check-square-o"></i>Released</a></li>
                    </ul>
				</a>
            </li>
            <!--17-->
            <li>
                <a href="#">
                    <i class="glyphicon glyphicon-print"></i> <span>Report</span> <i class="fa fa-angle-left pull-right"></i>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('dashboard/report_employee'); ?>"><i class="fa fa-user"></i>Employee</a></li>
                        <li><a href="<?php echo base_url('dashboard/report_leave_request'); ?>"><i class="fa fa-reorder"></i>Leave Request</a></li>
                        <li><a href="#"><i class="glyphicon glyphicon-usd"></i>Payroll</a></li>
                        <li><a href="<?php echo base_url('dashboard/report_loan'); ?>"><i class="glyphicon glyphicon-usd"></i>Loan</a></li>
                    </ul>
                </a>
            </li>

            <!--
            <li>
                <a href="<?php echo base_url('dashboard/department_list'); ?>">
                    <i class="glyphicon glyphicon-th-large"></i> <span>Department</span>
                </a>
            </li>
            
            <li class="treeview <?php if($this->uri->segment(2)=="list_man_power_request"){echo "active";} ?> ">
                <a href="#">
                    <i class="glyphicon glyphicon-list"></i> <span>Man Power</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if($this->uri->segment(2)=="list_man_power_request"){echo "active";} ?>"><a href="<?php echo base_url('dashboard/list_man_power_request');?>"><i class="glyphicon glyphicon-align-left"></i> List Man Power</a></li>
                    <li>
                        <a href="#">
                            <i class="glyphicon glyphicon-save"></i> <span>Recuitment</span> <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li class="<?php if($this->uri->segment(2)=="list_vacancy"){echo "active";} ?>"><a href="<?php echo base_url('dashboard/list_vacancy');?>"><i class="glyphicon glyphicon-file"></i> Vacancy</a></li>
                            <li><a href="<?php echo base_url('dashboard/list_applicant'); ?>"><i class="glyphicon glyphicon-list-alt"></i> Applicant</a></li>
                            <li><a href="<?php echo base_url('dashboard/list_penarikan_karyawan'); ?>"><i class="glyphicon glyphicon-user"></i> Penarikan Karyawan</a></li>
                            <!--<li><a href=<?php echo base_url('dashboard/Inter_Record');?>><i class="fa fa-circle-o"></i> Interview Record</a></li>
                            <li><a href="<?php echo base_url('dashboard/penarikan_karyawan'); ?>"><i class="fa fa-circle-o"></i> Penarikan Karyawan</a></li>-->
                        </ul>
                    </li>
                    <!--<li class="<?php if($this->uri->segment(2)=="man_power_request_release"){echo "active";} ?>"><a href="<?php echo base_url('dashboard/man_power_request_release');?>"><i class="glyphicon glyphicon-retweet"></i> Man Power Release</a></li>
                </ul>
            </li>
            <li>
                <a href="<?php echo base_url('dashboard/list_all_employee'); ?>">
                    <i class="glyphicon glyphicon-user"></i> <span>Employee</span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url('dashboard/employee_grade'); ?>">
                    <i class="glyphicon glyphicon-text-color"></i> <span>Employee Grade</span>
                </a>
            </li>            
            <li>
                <a href="<?php echo base_url('dashboard/project_schedule'); ?>">
                    <i class="glyphicon glyphicon-briefcase"></i> <span>Personnel Schedule</span>
                </a>
            </li>            
            <li>
                <a href="<?php echo base_url('dashboard/Bonus_sheet'); ?>">
                    <i class="glyphicon glyphicon-gift"></i> <span>Bonus Sheet</span>
                </a>
            </li>            
            <li>
                <a href="<?php echo base_url('dashboard/ijin_list'); ?>">
                    <i class="glyphicon glyphicon-envelope"></i> <span>Ijin</span>
                </a>
            </li>            
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Leave Master</span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('dashboard/leave_type_list');?>"><i class="fa fa-circle-o"></i> Leave Type</a></li>
                    <li><a href="<?php echo base_url('dashboard/leave_list');?>"><i class="fa fa-circle-o"></i> List Leave </a></li>
                </ul>
            </li>
            <li>
                <a href="<?php echo base_url('dashboard/sppd_list'); ?>">
                    <i class="glyphicon glyphicon-equalizer"></i> <span>SPPD</span>
                </a>
            </li>                        
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Tunjangan Master</span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('dashboard/list_tunjangan');?>"><i class="fa fa-circle-o"></i> Tunjangan</a></li>
                    <li><a href="<?php echo base_url('dashboard/list_tunjangan_luar_kota');?>"><i class="fa fa-circle-o"></i> Tunjangan Dinas Luar Kota</a></li>
                </ul>
            </li>
            -->
        </ul>
    </section>
</aside>