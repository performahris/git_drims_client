<script>
    $(function() {
        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li><a href="#"></i>Leave Request</a></li>
            <li><a href="#"></i>Requested</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Leave Request | Requested | Edit</h3>
                        <hr>
                    </div>
                    <div class="box-body table-responsive">
                        <div class="col-md-12 col-xs-12 col-sm-4">
                            <div class="col-md-12" style="text-align:right;">
                                <div class="col-md-6">
                                    &nbsp;
                                </div>
                                <div class="col-md-6">
                                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                        <div class="col-md-8 col-xs-12 col-sm-4">
                                            <label for="comment">Date</label>
                                        </div> 
                                        <div class="col-md-4 col-xs-12 col-sm-5">
                                            16 February 2016
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-3 col-xs-12 col-sm-4">
                                        <label for="comment">Employee Code</label>
                                    </div> 
                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                        <input type="text" class="form-control" id="" value="E_2"> 
                                    </div>
                                </div>
                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-3 col-xs-12 col-sm-4">
                                        <label for="comment">Employee Name</label>
                                    </div> 
                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                        <input type="text" class="form-control" id="" value="ARBONDES" disabled> 
                                    </div>
                                </div>
                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-3 col-xs-12 col-sm-4">
                                        <label for="comment">Position</label>
                                    </div> 
                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                        <input type="text" class="form-control" id="" value="Finance Director" disabled> 
                                    </div>
                                </div>
                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-3 col-xs-12 col-sm-4">
                                        <label for="comment">Location</label>
                                    </div> 
                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                        <input type="text" class="form-control" id="" value="Batavianet" disabled> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12"><hr></div>
	                        <div class="col-md-12">
	                            <div class="row">
	                                <div class="col-md-3 col-xs-12 col-sm-4">
	                                    <label for="comment">Hak Cuti Tahun</label>
	                                </div>
	                                <div class="col-md-9 col-xs-12 col-sm-12">
	                                    <label>
	                                        <div class="col-md-12" style="font-weight:normal">
	                                            <select class="form-control">
	                                                <option>2015</option>
	                                                <option>2016</option>
	                                            </select> 
	                                        </div>
	                                    </label>
	                                    <label><div class="col-md-12">=</div></label>
	                                    <label>
	                                        <div class="col-md-5">
	                                            <input type="text" class="form-control" value="5" disabled>	
	                                        </div>
	                                        <div class="col-md-7">Hari</div>
	                                    </label>
	                                </div>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-3 col-xs-12 col-sm-4">
	                                    <label for="comment">Cuti Sudah Diambil Tahun</label>
	                                </div>
	                                <div class="col-md-9 col-xs-12 col-sm-12">
	                                    <label>
	                                        <div class="col-md-12" style="font-weight:normal">
	                                            <select class="form-control">
	                                                <option>2015</option>
	                                                <option>2016</option>
	                                            </select> 
	                                        </div>
	                                    </label>
	                                    <label><div class="col-md-12">=</div></label>
	                                    <label>
	                                        <div class="col-md-5">
	                                            <input type="text" class="form-control" value="3" disabled>	
	                                        </div>
	                                        <div class="col-md-7">Hari</div>
	                                    </label>
	                                </div>
	                            </div>
	                        </div>
	                                <div class="col-md-12"><hr></div>
	                        <div class="col-md-12 col-xs-12 col-sm-4">
	                            <div class="row">
	                                <div class="col-md-3 col-xs-12 col-sm-4" style="text-align:right;">
	                                    <label>Sisa Cuti</label>
	                                </div>
	                                <div class="col-md-9 col-xs-12 col-sm-12">
	                                    <label>
	                                        <div class="col-md-12" style="font-weight:normal">
	                                            <select class="form-control">
	                                                <option>2015</option>
	                                                <option>2016</option>
	                                            </select> 
	                                        </div>
	                                    </label>
	                                    <label><div class="col-md-12">=</div></label>
	                                    <label>
	                                        <div class="col-md-5">
	                                            <input type="text" class="form-control" value="2" disabled>	
	                                        </div>
	                                        <div class="col-md-7">Hari</div>
	                                    </label>
	                                </div>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-3 col-xs-12 col-sm-4">
	                                    <label for="comment">Mengajukan Cuti</label>
	                                </div>
	                                <div class="col-md-9 col-xs-12 col-sm-12">
	                                    <label>
	                                        <div class="col-md-12" style="font-weight:normal">
	                                            <select class="form-control">
	                                                <option>2016</option>
	                                                <option>2015</option>
	                                            </select> 
	                                        </div>
	                                    </label>
	                                    <label><div class="col-md-12">=</div></label>
	                                    <label>
	                                        <div class="col-md-5">
	                                            <input type="text" class="form-control" value="2">	
	                                        </div>
	                                        <div class="col-md-7">Hari</div>
	                                    </label>
	                                </div>
	                            </div>
	                        </div>
	                                <div class="col-md-12"><hr></div>
	                        <div class="col-md-12 col-xs-12 col-sm-4">
		                        <div class="row">
		                            <div class="col-md-3 col-xs-12 col-sm-4" style="text-align:right;">
		                                <label for="comment">Sisa Cuti</label>
		                            </div>
		                            <div class="col-md-9 col-xs-12 col-sm-12">
		                                <label>
		                                    <div class="col-md-12" style="font-weight:normal">
		                                        <select class="form-control">
		                                            <option>2016</option>
		                                            <option>2015</option>
		                                        </select> 
		                                    </div>
		                                </label>
		                                <label><div class="col-md-12">=</div></label>
		                                <label>
		                                    <div class="col-md-5">
		                                        <input type="text" class="form-control" value="0" disabled>	
		                                    </div>
		                                    <div class="col-md-7">Hari</div>
		                                </label>
		                            </div>
		                        </div>
	                        </div>
	                                <div class="col-md-12"><hr></div>
	                        <div class="col-md-12 col-xs-12 col-sm-4">
	                            <div class="row" style="margin-top:5px; margin-bottom:10px;">
	                                <div class="col-md-2 col-xs-12 col-sm-4">
	                                    <label for="comment">Cuti Dari Tanggal</label>
	                                </div>
	                                <div class="col-md-7 col-xs-12 col-sm-12">
                                        <div class="col-md-5" style="font-weight:normal" disabled>
                                            <div class="input-group" style="width:100%;">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control" value="02/17/2016" id="datepicker1"/>
                                            </div> 
                                        </div>
                                        <div class="col-md-1">s/d</div></label>
                                        <div class="col-md-5" style="font-weight:normal" disabled>
                                            <div class="input-group" style="width:100%;">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control" value="02/18/2016" id="datepicker2"/>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top:5px;margin-bottom:5px;">
                                    <div class="col-md-2 col-xs-12 col-sm-4">
                                        <label for="comment">Hari Kerja : </label>
                                    </div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <div class="col-md-2" style="font-weight:normal">
                                            <input type="text" class="form-control" value="2" disabled> 
                                        </div>
                                        <div class="col-md-1"><label>Hari</label></div>
                                        <div class="col-md-3"><label>Hari Libur:</label></div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" value="0" disabled>	
                                        </div>
                                        <div class="col-md-1">Hari</div>
                                    </div>
                                    <div class="col-md-12"><hr></div>
                                </div>
	                        </div>
	                        <div class="col-md-12 col-xs-12 col-sm-4">
                                <div class="row" style="margin-top:5px;margin-bottom:5px;">
	                            	<div class="col-md-3">
	                            		<label for="comment">Tujuan Cuti</label>
	                            	</div>
	                                <div class="col-md-6">
	                                    <textarea class="form-control" rows="5" id="comment">
                                         Liburan Keluarga   
                                        </textarea>
	                                </div>
                                </div>
                                <div class="row" style="margin-top:5px;margin-bottom:5px;">
                                    <div class="col-md-3">
                                        <label for="comment">Alamat Selama Cuti</label>
                                    </div>
                                    <div class="col-md-6">
                                        <textarea class="form-control" rows="5" id="comment">
                                            Serang, Banten 
                                        </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-xs-12 col-sm-12" style="padding-top:10px; padding-bottom:10px;">
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                <a href="<?php echo base_url('dashboard/leave_requested'); ?>"><input type="button" class="btn btn-block btn-success" value="Submit"></a>
                            </div>
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                <a href="<?php echo base_url('dashboard/leave_requested'); ?>"><input type="button" class="btn btn-block btn-save-as" value="Save As Draft"></a>
                            </div>
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                <a href="<?php echo base_url('dashboard/leave_requested'); ?>"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
	                            </div>	
                            </div>	
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">

    $(function() {
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Timepicker
        $(".timepicker").timepicker({
            showInputs: false
        });
    });
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {


    });

    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
</script>









<script type="text/javascript">
    $(document).ready(function() {
        $(".departemen").select2();
    });

    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
        $scope.sortType = 'no'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order
        $scope.search = '';
        $scope.limit = 25;
        $scope.dari = "";
        $scope.to = "";
        $scope.depart = "";
        $scope.jumlah = 0;


        $scope.employee = <?php echo $employee ?>;


        $scope.departemen = [
            {nama: "Purchasing"},
            {nama: "Research"},
            {nama: "HRD"}
        ];

        $scope.reset = function() {
            $scope.depart = "";
        }


        $scope.currentPage = 1;
        $scope.totalItems = $scope.employee.length;
        $scope.numPerPage = $scope.limit;

        $scope.limitPage = function() {
            $scope.numPerPage = $scope.limit;
            if ($scope.currentPage * $scope.numPerPage > $scope.employee.length) {
                $scope.currentPage = 1;
            }
        };

        $scope.lastPage = function() {
            $scope.currentPage = $scope.pageCount();
        };

        $scope.firstPage = function() {
            $scope.currentPage = 1;
        };

        $scope.nextPage = function() {

            if ($scope.currentPage < $scope.pageCount()) {
                $scope.currentPage++;
            }
        };

        $scope.jumlahPerpage = function(value) {
            $scope.numPerPage = value;
        }

        $scope.prevPage = function() {
            if ($scope.currentPage > 1) {
                $scope.currentPage--;
            }
        };

        $scope.pageCount = function() {
            return Math.ceil($scope.jumlah / $scope.numPerPage);
        };

        $scope.checkAll = function() {
            angular.forEach($scope.employee, function(item) {
                item.Selected = $scope.selectAll;
            });
        };

    });

    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }


    app.filter("dateRange", function() {
        return function(items, dari, to) {
            if (dari.length == 0) {
                var dari = +new Date("1980-01-01");
            } else {
                var dari = +new Date(dari);
            }

            if (to.length == 0) {
                var to = +new Date();
            } else {
                var to = +new Date(to);
            }
            var df = dari;
            var dt = to;
            var arrayToReturn = [];
            for (var i = 0; i < items.length; i++) {
                var tf = +new Date(items[i].join);
                if ((tf > df && tf < dt) || (tf == dt)) {
                    arrayToReturn.push(items[i]);
                }
            }

            return arrayToReturn;
        };

    });


    app.filter("paging", function() {
        return function(items, limit, currentPage) {
            if (typeof limit === 'string' || limit instanceof String) {
                limit = parseInt(limit);
            }

            var begin, end, index;
            begin = (currentPage - 1) * limit;
            end = begin + limit;
            var arrayToReturn = [];
            for (var i = 0; i < items.length; i++) {
                if (begin <= i && i < end) {
                    arrayToReturn.push(items[i]);
                }
            }
            return arrayToReturn;
        };
    });

    app.filter("departemen", function() {
        return function(items, depart) {
            if (depart.length == 0) {
                return items;
            }
            var arrayToReturn = [];
            for (var i = 0; i < items.length; i++) {
                if (items[i].dept == depart) {
                    arrayToReturn.push(items[i]);
                }
            }
            return arrayToReturn;
        };
    });

    app.filter("nama", function() {
        return function(items, search) {
            if (search.length == 0) {
                return items;
            }
            var arrayToReturn = [];
            for (var i = 0; i < items.length; i++) {
                if (items[i].name.toUpperCase().indexOf(search.toUpperCase()) != -1) {
                    arrayToReturn.push(items[i]);
                }
            }
            return arrayToReturn;
        };
    });


</script>