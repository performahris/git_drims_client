<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#">LEAVE REQUEST</a></li>
            <li class="active">Leave Request Approval</li>
        </ol>
    </section>
	
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">LEAVE REQUEST | Leave Request Approval</h3>
                    </div>
                    <div class="box-body table-responsive">
						<div class="pull-right">Date : <?php echo date("d F Y"); ?></div>
                        <table id="example1" >
                                <tr>
									<td colspan="4" class="titlepage"><strong>Head Staff Approval</strong></td>
								</tr>
								<tr>
									<td width=15.33% >Employee ID</td><td width=1%	>:</td><td width = 20%>E_1</td>
								</tr>
								<tr>
									<td>Employee Name</td><td>:</td><td>Dery</td>
								</tr>
								<tr>
									<td>Request Number</td><td>:</td><td>1</td>
								</tr>
								<tr>
									<td>Sisa Cuti Tahun YYYY</td><td>:</td><td>10 Day</td>
								</tr>
								<tr>
									<td valign="top">Date From</td><td valign="top">:</td><td>7-02-2016 until 24-02-2016&nbsp;&nbsp;&nbsp;<div> Deduction : <input type="text" name="currencyID" class="required" value=""/></div></td>
								</tr>
								<tr>
									<td>Mengajukan Cuti</td><td>:</td><td>5 Day</td>
								</tr>
								<tr>
									<td>Approved Leave</td><td>:</td><td><input type="text" name="currencyID" class="required" value=""/></td>
								</tr>
								<tr>
									<td>Purpose</td><td>:</td><td>Wedding Ceremony</td>
								</tr>
								<tr>
									<td>Emergency Contact</td><td>:</td><td>Malang</td>
								</tr>
								<tr>
									<td>Number/Address</td><td></td><td></td>
								</tr>
								<tr>
									<td valign ="top">
										Remarks</td><td valign ="top">:</td><td>
										<textarea rows = 10 style="width:100%"></textarea>
									</td>
								</tr>
                        </table>
						<input name="cancel" type="submit" id="submit" value="Approval" />
						<input name="cancel" type="submit" id="submit" value="Reject" />
						<a href="<?php echo base_url() . 'dashboard/leave_request_summary'; ?>"><input name="cancel" type="button" id="submit" value="Review Later" /></a></td>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
 
    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
     
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

   
    

</script>
