<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#">SPDD</a></li>
            <li class="active">SPPD Edit</li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">SPPD | SPPD Edit</h3>
                        <hr>
                    </div>
                    <div class="box-body table-responsive">
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label>
								<div class="col-md-12 col-xs-12 col-sm-12">No</div>
							</label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
									01/HRD/01/2016
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<div class="col-md-3 col-xs-12 col-sm-12">By</div>
									<div class="col-md-9 col-xs-12 col-sm-12" style="font-weight:normal">
										<?php echo ("Shinta")?>
									</div>
								</div>
							</label>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Nama</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="nama" class="form-control"  value="Shinta" />	
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Jabatan/ Golongan</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="golongan" class="form-control"  value="Asistant Manager" />	
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Divisi</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="divisi" class="form-control"  value="Production"/>	
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Tujuan</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-8 col-xs-12 col-sm-12" style="font-weight:normal">
									<textarea name="tujuan" rows="5" class="form-control" > Agar Divisi Produksi bisa lebih meningkatkan produksi kerja dan 
 profit perusahaa.</textarea>
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Periode</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="periode" class="form-control"  value="10 Feb 2016" />	
								</div>
							</label>		
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12">
							<h4><strong>Sarana Transportasi Perjalanan Dinas</strong></h4>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12">
							<label >
								<div class="col-md-12 col-xs-12 col-sm-12">
									<input type="checkbox" name="sarana1" checked /> Pesawat
								</div>
							</label>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label >
								<div class="col-md-12 col-xs-12 col-sm-12">
									<input type="checkbox" name="sarana2" /> Kereta Api / Bus / Kapal Laut
								</div>
							</label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:40%">
								<div class="col-md-2 col-xs-12 col-sm-12">
									Kelas
								</div>
								<div class="col-md-8 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="kereta" class="form-control"/>
								</div>
							</label>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label >
								<div class="col-md-12 col-xs-12 col-sm-12">
									<input type="checkbox" name="sarana3" /> Mobil Operasional Perusahaan
								</div>
							</label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-1 col-xs-12 col-sm-12">
									No.Pol
								</div>
								<div class="col-md-7 col-xs-12 col-sm-12" style	="font-weight:normal">
									<select name="name">
										<option>B 32167 MKF</option>
										<option>B 27866 DFT</option>
										<option>B 12978 DBR</option>
									</select>
								</div>
							</label>
						</div>
						<!--<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12"><input type="checkbox" name="sarana" /> Lain - Lain</div></div></label>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="lainlain" class="form-control"  placeholder ="Lain - Lain"/>	
								</div>
							</label>		
						</div>-->
						<div class="col-md-12 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-2 col-xs-12 col-sm-12" >
								<input type="checkbox" name="sarana" />
									Lain-Lain
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12" style="width:34%;">
									<input type="text" name="kereta" class="form-control"/>
								</div>
							</label>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12">
							<h4><strong>Kas Bon Yang Diajukan</strong></h4>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12">
							<table id="example1" class="table table-striped" style="width:50%">
								<tr>
									<td><b>Uang Makan</b></td>
									<td>IDR</td>
									<td ><input type="text" name="uangmakan" value="150.000" /></td>
								</tr>
								<tr>
									<td><b>Hotel</b></td>
									<td>IDR</td>
									<td><input type="text" name="hotel"/ value="650.000"></td>
								</tr>
								<tr>
									<td><b>Transportasi / Taksi P.P</b></td>
									<td>IDR</td>
									<td ><input type="text" name="transport"/ value="200.000"></td>
								</tr>
								<tr>
									<td><b>Lain-lain</b></td>
									<td>IDR</td>
									<td><input type="text" name="lainlain"/></td>
								</tr>
								<tr>
									<td><strong>Total</strong></td>
									<td><strong>IDR</strong></td>
									<td ><strong>1.000.000</strong></td>
								</tr>
							</table>
						</div>
						<br>
						<div class="col-md-12 col-xs-12 col-sm-12">
							<table id="activity" class="table table-striped table-bordered" style="width:40%">
							<h5><b>Activity Log</b></h5>
							<div class="col-md-12 col-xs-12 col-sm-12">
							
							</div>
								<tr>
									<th><center>No</center></th>
									<th><center>Activity</center></th>
									<th><center>Date & time</center></th>
									<th><center>By</center></th>
								</tr>
								<tr>	
									<td><center>1</center></td>
									<td><center>Create Policy</center></td>
									<td><center>10 Feb 2016 11:00:00</center></td>
									<td><center>Shinta</center></td>
								</tr>
								<tr> 
									<td><center>2</center></td>
									<td><center>Submit Policy</center></td>
									<td><center>18 Feb 2016 09:05:37</center></td>
									<td><center>Dery</center></td>
								</tr>
								<tr>
									<td><center>3</center></td>
									<td><center>Review Policy</center></td>
									<td><center>21 Feb 2016 10:00:01</center></td>
									<td><center>Bhima</center></td>
								</tr>
								<tr>
									<td><center>4</center></td>
									<td><center>Approve Policy</center></td>
									<td><center>07 Mar 2016 23:01:57</center></td>
									<td><center>Badrizka</center></td>
								</tr>
							</table>
						</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="<?php echo base_url()."dashboard/sppd_list" ?>"><input type="button" class="btn btn-block btn-success" value="Submit"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="<?php echo base_url()."dashboard/sppd_list" ?>"><input type="button" class="btn btn-block btn-save-as" value="Save as Draft"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="<?php echo base_url()."dashboard/sppd_list" ?>"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
