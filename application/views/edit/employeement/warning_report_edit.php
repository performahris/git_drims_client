<link href="../../asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<div class="content-wrapper">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li><a href="#"></i>Employeement</a></li>
            <li><a href="#"></i>Warning Report</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Employeement | Warning Report | Edit</h3>
                        <hr>
                    </div>
                    		<div class="panel-body">
                    			<div class="col-md-12">
				                	<div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Employee Code</label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                            E_1
				                        </div>
				                    </div>
				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Employee Name</label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                            <input type="text" class="form-control" id="" value="SUDARMONO" disabled> 
				                        </div>
				                    </div>
				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Position</label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                            <input type="text" class="form-control" id="" value="Operation Director" disabled> 
				                        </div>
				                    </div>
				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Type of Warning Letter</label>
				                        </div> 
				                        <div class="col-md-2 col-xs-12 col-sm-5">
				                            <select class="form-control">
                                        <option>SP1</option>
                                        <option>SP2</option>
                                        <option>SP3</option>
                                    </select>
				                        </div>
				                    </div>
                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                <div class="col-md-3 col-xs-12 col-sm-4">
                                    <label for="comment">Remark</label>
                                </div> 
                                <div class="col-md-5 col-xs-12 col-sm-5">
                                    <textarea class="form-control" rows="3" id="comment">Tidak masuk 5 hari berturut-turut</textarea> 
                                </div>
                            </div>
				      </div>
				        <div class="col-md-8 col-xs-12 col-sm-12" style="padding-top:10px; padding-bottom:10px;">
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                <a href="<?php echo base_url('dashboard/list_all_warning_report'); ?>"><input type="button" class="btn btn-block btn-success" value="Submit"></a>
                            </div>
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                <a href="<?php echo base_url('dashboard/list_all_warning_report'); ?>"><input type="button" class="btn btn-block btn-save-as" value="Save As Draft"></a>
                            </div>
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                <a href="<?php echo base_url('dashboard/list_all_warning_report'); ?>"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
                            </div>	
                        </div>
				</div>
			</div>
		</div>
    </section>
</div>