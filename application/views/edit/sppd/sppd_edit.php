<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">SPPD</li>
            <li class="active">Requested</li>
            <li class="active">List</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">SPPD | Requested | Edit</h3>
                        <hr>
                    </div>
                    <div class="box-body table-responsive">
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label>
								<div class="col-md-12 col-xs-12 col-sm-12">No</div>
							</label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
									01/HRD/01/2016
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<div class="col-md-3 col-xs-12 col-sm-12">By</div>
									<div class="col-md-9 col-xs-12 col-sm-12" style="font-weight:normal">
										SUDARMONO
									</div>
								</div>
							</label>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Nama</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="nama" class="form-control"  value="SUDARMONO" />	
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Jabatan/ Golongan</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="golongan" class="form-control"  value="Operation Director" />	
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Divisi</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="divisi" class="form-control"  value="Production"/>	
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Tujuan</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-8 col-xs-12 col-sm-12" style="font-weight:normal">
									<textarea name="tujuan" rows="5" class="form-control" > Agar Divisi Produksi bisa lebih meningkatkan produksi kerja dan 
 profit perusahaa.</textarea>
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Periode</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
									<div class="input-group" style="width:100%;">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
									    </div>
										<input type="text" class="form-control" name="date" id="datepicker1"/>
									</div>	
								</div>
								<div class="col-md-1">
									<label>to</label>	
								</div>
								<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
									<div class="input-group" style="width:100%;">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
									    </div>
										<input type="text" class="form-control" name="date" id="datepicker2"/>
									</div>	
								</div>
							</label>		
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12">
							<h4><strong>Sarana Transportasi Perjalanan Dinas</strong></h4>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12">
							<label >
								<div class="col-md-12 col-xs-12 col-sm-12">
									<input type="checkbox" name="sarana1" checked /> Pesawat
								</div>
							</label>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label >
								<div class="col-md-12 col-xs-12 col-sm-12">
									<input type="checkbox" name="sarana2" /> Kereta Api / Bus / Kapal Laut
								</div>
							</label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:40%">
								<div class="col-md-2 col-xs-12 col-sm-12">
									Kelas
								</div>
								<div class="col-md-8 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="kereta" class="form-control"/>
								</div>
							</label>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label >
								<div class="col-md-12 col-xs-12 col-sm-12">
									<input type="checkbox" name="sarana3" /> Mobil Operasional Perusahaan
								</div>
							</label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:40%">
								<div class="col-md-2 col-xs-12 col-sm-12">
									No.Pol
								</div>
								<div class="col-md-8 col-xs-12 col-sm-12" style	="font-weight:normal">
									<select class="form-control" name="name">
										<option>B 3216 MKF</option>
										<option>B 2786 DFT</option>
										<option>B 1297 DBR</option>
									</select>
								</div>
							</label>
						</div>
						<div class="col-md-2 col-xs-12 col-sm-12">
							<label >
								<div class="col-md-12 col-xs-12 col-sm-12">
									<input type="checkbox" name="sarana" />
									Lain-Lain
								</div>
							</label>
						</div>
						<div class="col-md-10 col-xs-12 col-sm-12">
							<label style="width:40%">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<input type="text" name="kereta" class="form-control"/>
								</div>
							</label>
						</div>

						<div class="col-md-12 col-xs-12 col-sm-12">
							<h4><strong>Kas Bon Yang Diajukan</strong></h4>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label >
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label>Uang Makan</label>
								</div>
							</label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:40%">
								<div class="col-md-2 col-xs-12 col-sm-12">
									<label>IDR</label>
								</div>
								<div class="col-md-8 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="uangmakan" class="form-control" value="150.000">
								</div>
							</label>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label >
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label>Hotel</label>
								</div>
							</label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:40%">
								<div class="col-md-2 col-xs-12 col-sm-12">
									<label>IDR</label>
								</div>
								<div class="col-md-8 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="hotel" class="form-control" value="650.000">
								</div>
							</label>
						</div>

						<div class="col-md-3 col-xs-12 col-sm-12">
							<label >
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label>Transport / Taksi P.P</label>
								</div>
							</label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:40%">
								<div class="col-md-2 col-xs-12 col-sm-12">
									<label>IDR</label>
								</div>
								<div class="col-md-8 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="transpot" class="form-control" value="200.000">
								</div>
							</label>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label >
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label>Lain - Lain</label>
								</div>
							</label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:40%">
								<div class="col-md-2 col-xs-12 col-sm-12">
									<label>IDR</label>
								</div>
								<div class="col-md-8 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="transpot" class="form-control">
								</div>
							</label>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12" >
							<label >
								<div class="col-md-12 col-xs-12 col-sm-12" style="text-align:right;">
									<label>Total</label>
								</div>
							</label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:40%">
								<div class="col-md-2 col-xs-12 col-sm-12">
									<label>IDR</label>
								</div>
								<div class="col-md-8 col-xs-12 col-sm-12" style="font-weight:normal">
									<input type="text" name="transpot" class="form-control" value="1.000.000" disabled>
								</div>
							</label>
						</div>

						
						<br>
						<div class="col-md-12 col-xs-12 col-sm-12">
							<table id="activity" class="table table-striped table-bordered" style="width:40%">
							<h5><b>Activity Log</b></h5>
							<div class="col-md-12 col-xs-12 col-sm-12">
							
							</div>
								<tr class="success">
									<th><center>No</center></th>
									<th><center>Activity</center></th>
									<th><center>Date & time</center></th>
									<th><center>By</center></th>
								</tr>
								<tr>	
									<td><center>1</center></td>
									<td><center>Create Policy</center></td>
									<td><center>10 Feb 2016 11:00:00</center></td>
									<td><center>Shinta</center></td>
								</tr>
								<tr> 
									<td><center>2</center></td>
									<td><center>Submit Policy</center></td>
									<td><center>18 Feb 2016 09:05:37</center></td>
									<td><center>Dery</center></td>
								</tr>
								<tr>
									<td><center>3</center></td>
									<td><center>Review Policy</center></td>
									<td><center>21 Feb 2016 10:00:01</center></td>
									<td><center>Bhima</center></td>
								</tr>
								<tr>
									<td><center>4</center></td>
									<td><center>Approve Policy</center></td>
									<td><center>07 Mar 2016 23:01:57</center></td>
									<td><center>Badrizka</center></td>
								</tr>
							</table>
						</div>
						<div class="col-md-8 col-xs-12 col-sm-12">
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="<?php echo base_url()."dashboard/sppd_requested" ?>"><input type="button" class="btn btn-block btn-success" value="Submit"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="<?php echo base_url()."dashboard/sppd_requested" ?>"><input type="button" class="btn btn-block btn-save-as" value="Save as Draft"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="<?php echo base_url()."dashboard/sppd_requested" ?>"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
