<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Man Power Request</li>
            <li class="active">Requested</li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Man Power Request | Requested | Edit</h3>
                        <hr>
                    </div>

					<div class="col-md-6 col-xs-12 col-sm-12" >
						<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left"> <label for="comment">Date Request</label></div>
							<div class="col-md-8 col-xs-12 col-sm-12 ">12 October 2015</div>
						</div>
						<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
						
						<div class="col-md-4 col-xs-12 col-sm-12 pull-left"> <label for="comment">Tittle Needed</label></div>
							<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" name="tittle" ng-model="tittle" value="Staff"/></div>
						</div>
						<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left"> <label for="comment">Qty</label></div>
							<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" name="qty" ng-model="qty" value="7" /></div>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 col-sm-12" >
						<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left"> <label for="comment">Departement</label></div>
							<?php
	                            $department=array("Produksi","Pemasaran","Keuangan","Layanan Pelanggan",
	                            "Purchasing","Logistic","Project","HRD","Supervisor","Personalia");

	                            if ($_SERVER['REQUEST_METHOD']==="POST") {
	                              if (isset($_POST['name_department'])) {
	                                  if (in_array($_POST['name_department'],$department)) {
	                                      echo "You selected ".$_POST['name_department']."!";
	                                      exit;
	                                  }
	                              }
	                            }
	                        ?>
							<div class="col-md-8 col-xs-12 col-sm-12 ">
								<select class="form-control" name="name_department">
									<?php
                        	            foreach ($department as $name_department) {
                                        echo '<option value="'.$name_department.'">'.$name_department.'</option>';
                            	        }
                                    ?>
								</select>
							</div>
						</div>
						<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left"> <label for="comment">Location</label></div>
							<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" name="tittle" ng-model="tittle" value="Ramba" /></div>
						</div>
						<div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left"> <label for="comment">Direct Supervisior</label></div>
							<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" name="qty" ng-model="qty" value="ACHMAD MUHARIYAH"/></div>
						</div>
					</div>
					<div>&nbsp;</div>
					<div class="box-header" >
						<div class="col-md-12 col-xs-12 col-sm-12 ">
							<h4><b>ALASAN UNTUK MEREKRUT</b></h4>
						</div>
					</div>
                    <div class="box-body">
						<div class="col-md-3 col-xs-12 col-sm-12 pull-left">
							<input type="checkbox" name="posisi" id="optionsRadios1" value="Posisi Baru"> Posisi Baru
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12 pull-left">
							<input type="checkbox" name="posisi" id="optionsRadios2" value="Tambahan Karyawan"> Tambahan Karyawan
						</div>	
						<div class="col-md-3 col-xs-12 col-sm-12 pull-left">
							<input type="checkbox" name="posisi" id="optionsRadios3" value="Penggantian (Replacement) "checked> Penggantian (Replacement)
						</div>
						<div style="padding-left:15px;">
							<input type="checkbox" name="posisi" id="optionsRadios4" value="Perpanjangan Kontrak"> Perpanjangan Kontrak
						</div>
						<div class="box-header" >
							<h4><b>PERTIMBANGAN UNTUK MEREKRUT</b></h4>
							<b>(Jelaskan mengapa posisi tersebut perlu ditambah dan bagaimana posisi tersebut memberikan kontribusi dalam meningkatkan produktivity / profitability)</b>
						</div>
						<textarea name="alasan" rows= "8" style="width:80%" class="form-control">Mencari karyawan yang siap dan mampu Dalam melakukan pemasaran produk kepada konsumen.</textarea>
						<div class="box-header" >
							<h4><b>RINGKASAN DAN TANGGUNGJAWAB PEKERJAAN</b></h4>
						</div>
						<textarea name="ringkasan" rows= "8" style="width:80%" class="form-control">Membuat, merumuskan, menyusun, menetapkan konsep dan rencana umum perusahaan, mengarahkan dan memberikan kebijakan/keputusan atas segala rancang bangun dan implementasi manajemen pemasaran, penjualan dan promosi ke arah pertumbuhan dan perkembangan perusahaan.</textarea>
						<div class="box-header" >
							<h4><b>GAMBARKAN STRUKTUR ORGANISASI TERAKHIR DEPARTEMEN TERSEBUT DAN POSISI CALON KARYAWAN</b></h4>
						</div>
						<textarea name="struktur_organisasi"  rows= "8" style="width:80%" class="form-control">- Mempelajari kebutuhan dan keinginan konsumen 
- Mengembangkan suatu konsep produk yang ditujukan untuk memuaskan/ melayani kebutuhan 
- Membuat desain produk
						</textarea>
						<div>&nbsp;</div>
						<div class="box-header" ><h4><b>JENIS REKRUTMEN</h4></b></div>
						<div class="table-responsive">
							<table id="example1" class="table table-striped">
								<tr class="success">
									<th style="padding-left:30px;">
										Kandidat Dari Luar
										
									</th>
									<th style="padding-left:30px;text-align:center" >
										Periode
									   
									</th>
									<th style="padding-left:30px;">
										Kandidat Dari Dalam
										
									</th>
								</tr>
								<tr>
									<td style="padding-left:30px;"><input type="checkbox" name="kandidatluar" value="Tetap"/> Tetap</td>
									<td style="padding-left:30px;"><input type="text" name="periode1" class="form-control"/></td>
									<td style="padding-left:30px;"><input type="checkbox" name="kandidatdalam" value="Mutasi"/>Mutasi</td>
								</tr>
								<tr>
									<td style="padding-left:30px;"><input type="checkbox" name="kandidatluar" value="Darurat"/>Darurat</td>
									<td style="padding-left:30px;"><input type="text" name="periode2" class="form-control"/></td>
									<td style="padding-left:30px;"><input type="checkbox" name="kandidatdalam" value="Penugasan"/>Penugasan</td>
								</tr>
								<tr>
									<td style="padding-left:30px;"><input type="checkbox" name="kandidatluar" value="Kontrak" checked/>Kontrak</td>
									<td style="padding-left:30px;"><input type="text" name="periode3" class="form-control" value="2015-2016" /></td>
									<td style="padding-left:30px;"><input type="checkbox" name="kandidatdalam" value="Lain-Lain"/>Lain-Lain</td>
								</tr>
								<tr>
									<td style="padding-left:30px;"><input type="checkbox" name="kandidatluar" value="Lain-Lain"/>Lain-Lain</td>
									<td style="padding-left:30px;"><input type="text" name="periode4" class="form-control" /></td>
									<td style="padding-left:30px;">&nbsp;</td>
								</tr>
							</table>
						</div>
						 <div class="box-header" >
                            <h4><b>Spesifikasi Jabatan</b></h4>
                        </div>
                        <div class="box-body" >
						<div class="col-md-5 col-xs-12 col-sm-12" >
							<div class="col-md-5" style="margin-top:5px;margin-bottom:5px;">
								 <label for="comment">Jenis Kelamin</label>
							</div>
							<div class="col-md-7" style="margin-top:5px;margin-bottom:5px;">
								<select name="jenis_kelamin" class="form-control">
									<option>Pria</option>
									<option>Wanita</option>
								</select>
							</div>
						</div>
						<div class="col-md-7 col-xs-12 col-sm-12" >
							<div class="col-md-5" style="margin-top:5px;margin-bottom:5px;">
								 <label for="comment">Pengalaman Yang Dibutuhkan</label>
							</div>
							<div class="col-md-7" style="margin-top:5px;margin-bottom:5px;">
								<input type="text" class="form-control" name="pengalaman" value="Kemampuan Berkomunikasi Dengan Baik" />
							</div>
						</div>
						<div class="col-md-5 col-xs-12 col-sm-12" >
							<div class="col-md-5" style="margin-top:5px;margin-bottom:5px;">
								 <label for="comment">Penidikan</label>
							</div>
							<div class="col-md-7" style="margin-top:5px;margin-bottom:5px;">
								<select name="pendidikan" class="form-control">
									<option>SMP</option>
									<option>SMA</option>
									<option>S1</option>
									<option>S2</option>
								</select>
							</div>
						</div>
						<div class="col-md-7 col-xs-12 col-sm-12" >
							<div class="col-md-5" style="margin-top:5px;margin-bottom:5px;">
								 <label for="comment">Usia</label>
							</div>
							<div class="col-md-7" value="20" style="margin-top:5px;margin-bottom:5px;">
								  <div class="input-group">
									<input type="text" class="form-control" value="23" />
									<div class="input-group-addon">
									  Tahun
									</div>
								  </div><!-- /.input group -->
								
							</div>
						</div>
						<div class="col-md-5 col-xs-12 col-sm-12" >
							<div class="col-md-5" style="margin-top:5px;margin-bottom:5px;">
								 <label for="comment">Keahlian Yang Dibutuhkan</label>
							</div>
							<div class="col-md-7" style="margin-top:5px;margin-bottom:5px;">
								<input type="text" name="keahlian" class="form-control" value="Mampu Memahami Kebutuhan Para Pelanggan" />
							</div>
						</div>
						<div class="col-md-7 col-xs-12 col-sm-12" >
							<div class="col-md-5" style="margin-top:5px;margin-bottom:5px;">
								 <label for="comment">Kondisi Lingkungan Kerja</label>
							</div>
							<div class="col-md-7" style="margin-top:5px;margin-bottom:5px;">								
								 <input type="text" name="kondisi" class="form-control" value="Layak" />
							</div>
						</div>
						</div>
						<div class="box-header" ><h4><b>Didalam dan diluar kantor</b></h4></div>
						<div class="col-md-12 col-xs-12 col-sm-12" >
							<div class="col-md-2" style="margin-top:5px;margin-bottom:5px;">
								 <label for="comment">Lain Lain</label>
							</div>
							<div class="col-md-6" style="margin-top:5px;margin-bottom:5px;">
								<input type="text" name="lainlain" class="form-control"/>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12" >
							<div class="col-md-2" style="margin-top:5px;margin-bottom:20px;">
								 <label for="comment">Kandidat Yang Diusulkan</label>
							</div>
							<div class="col-md-3" style="margin-top:5px;margin-bottom:20px;">
								<?php
                                            $employee=array("SUBARI.M","SUDARMONO","ARBONDES","ISKANDAR","ANTON PRIADI",
                                            "I KETUT ADI PUJA ASTAWA","SUNARDI","BAMBANG IRAWAN","CHAIRUL AZWAN","AHMAD SARURI");

                                            if ($_SERVER['REQUEST_METHOD']==="POST") {
                                              if (isset($_POST['name_employee'])) {
                                                  if (in_array($_POST['name_employee'],$employee)) {
                                                      echo "You selected ".$_POST['name_employee']."!";
                                                      exit;
                                                  }
                                              }
                                            }
                                        ?>
								<select name="kandidat" class="form-control">
											<?php
                                            foreach ($employee as $name_employee) {
                                                echo '<option value="'.$name_employee.'">'.$name_employee.'</option>';
                                            }
                                            ?>
										</select>
							</div>
							<div class="col-md-1 col-xs-12 col-sm-12" style="text-align:right; margin-top:5px;margin-bottom:20px;">
									 <label for="comment">PT</label>
							</div>
							<div class="col-md-2 col-xs-6 col-sm-6" style="margin-top:5px;margin-bottom:20px;">
								<select class="form-control">
									<option>Batavianet</option>
									<option>G-Media</option>
								</select>
							</div>
							<div class="col-md-1 col-xs-12 col-sm-12" style="margin-top:5px;margin-bottom:20px;">
									 <label for="comment">Departement</label>
							</div>
							<div class="col-md-3 col-xs-6 col-sm-6" style="margin-top:5px;margin-bottom:20px;">
								<select class="form-control">
									<?php
                    	            	foreach ($department as $name_department) {
                                    	echo '<option value="'.$name_department.'">'.$name_department.'</option>';
                        	        	}
                                	?>
								</select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12">
							<div class="col-md-5 col-xs-12 col-sm-12" style="margin-top:5px;margin-bottom:5px;">
  								<table class="table table-bordered">
    								<thead>
      									<tr class="info">
      									<th>No</th>
								        <th>Activity</th>
    								    <th>Date & Time</th>
								        <th>By</th>
								        </tr>
    								</thead>
    							<tbody>
	      							<tr>
	      								<td>1</td>
							        	<td>Create Policy</td>
 							    	    <td>20 mei 2015 11:00:00</td>
        								<td>Dimas</td>
      								</tr>
      								<tr>
        								<td>2</td>
							        	<td>Submit Policy</td>
 							    	    <td>22 mei 2015 09:05:37</td>
        								<td>Dimas</td>
    	  							</tr>
 	     							<tr>
    							    	<td>3</td>
							        	<td>Review Policy</td>
 							    	    <td>22 mei 2015 10:00:01</td>
        								<td>Adi Bagus</td>
      								</tr>
      								<tr>
    							    	<td>4</td>
							        	<td>Approve Policy</td>
 							    	    <td>23 mei 2015 23:01:57</td>
        								<td>Andy Rikie Lam</td>
      								</tr>
    							</tbody>
  								</table>
							</div>
						</div>
						<div class="col-md-7 col-xs-12 col-sm-12">
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="<?php echo base_url('dashboard/man_power_requested'); ?>"><input type="button" class="btn btn-block btn-success" value="Submit"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="<?php echo base_url('dashboard/man_power_requested'); ?>"><input type="button" class="btn btn-block" style="background-color:#32cd32; color:white;" value="Save as Draft"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="<?php echo base_url('dashboard/man_power_requested'); ?>"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

