<meta charset="utf-8">
  <title>jQuery UI Datepicker - Default functionality</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
  $(function() {
    $( "#datepickering" ).datepicker({dateFormat:'dd-mm-yy', changeMonth: true, changeYear: true});
  });
  $(function() {
    $( "#datepickering1" ).datepicker({dateFormat:'dd-mm-yy', changeMonth: true, changeYear: true});
  });
  </script>


<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>

<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Recruitment</li>
            <li class="active">Vacancy</li>
            <li class="active">Vacancy Trainer</li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class=" col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
					<div class="box-header">
                        <h3 class="box-title">Recruitment | Vacancy | Vacancy Trainer | Edit</h3>
                        <hr>
                    </div>
          <div class="box-body  table-responsive">
          		<table class="table table-bordered">
    							<tbody>
      								<tr class="info">
        								<td> 
        									<h5><b>Urutan Tugas Dan Tanggung Jawab Anda Pada Jabatan Terakhir<b></h5>
                        </td>
      								</tr>
      								<tr>
        								<td>
												<textarea class="form-control" rows="5" id="comment">mengecek semua administrasi dan transaksi berhubungan dengan jalannya perusahaan.</textarea>
        								</td>
      								</tr>
   								</tbody>
  							</table>
            </div>
            <div class="box-body  table-responsive">
              <table class="table table-bordered">
                  <tbody>
                      <tr class="info">
                        <td>
                            <h5><b>Gambarkan Struktur Organisasi Yang memperlihatkan Posisi Anda Di tempat Terakhir<b></h5>
                        </td>
                      </tr>
                      <tr>
                        <td>
                        <textarea class="form-control" rows="5" id="comment">- CMT bertugas untuk mengurus hal hal berkaitan dengan pihak Outsourcing. 
- Accounting bertugas untuk melakukan membukukan transaksi yang terjadi. 
- Kasir bertugas untuk membuat laporan penerimaan dan pengeluaran uang harian.</textarea>
                        </td>
                      </tr>
                  </tbody>
                </table>
            </div>

		 				<div class="col-sm-12" style="text-align:right;">
       Page 4 of 5
      <div style="height:10px;">&nbsp; </div>
      </div>

        <div class="box-footer" style="border-top:none;">
            <a href="<?php echo base_url('view/vacancy_trainer'); ?>"><button type="submit" class="btn btn-danger">Cancel</button></a>
            <a href="vacancy_trainer_edit_5" class="btn btn-primary pull-right" >Next</a>
            <a href="vacancy_trainer_edit_3" class="btn btn-primary pull-right" style="margin-right:5px;">Back</a>&nbsp;
            <a href="<?php echo base_url('view/vacancy_trainer'); ?>"><button type="submit" class="btn" style="background-color:#32cd32; color:white;">Save As Draft</button></a>
            <a href="<?php echo base_url('view/vacancy_trainer'); ?>"><button type="submit" class="btn btn-success">Submit</button></a> 
        </div>
		
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
