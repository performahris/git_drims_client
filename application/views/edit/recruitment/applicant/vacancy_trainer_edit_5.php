<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Recruitment</li>
            <li class="active">Vacancy</li>
            <li class="active">Vacancy Trainer</li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class=" col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
		    <div class="box-header">
                        <h3 class="box-title">Recruitment | Vacancy | Vacancy Trainer | Edit</h3>
                    <hr>
		    </div>
			<div class="col-md- col-xs-12 col-sm-12" >
			      <div class="col-md-8">
				   <!--<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Search</div>-->
				   <!--<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" placeholder="Enter ..." ng-model="search"/></div>-->
			      </div>
			 </div>
                    <div class="box-body  table-responsive">
                        <table border="1" id="example1" class="table table-bordered table-striped">
                                <tr class="info">
                                   <th style="width:40%;"><center>Pertanyaan</center></th>
				   <th class="text-center">Ya</th>
				   <th class="text-center">Tidak</th>
				   <th class="text-center">Keterangan</th>
                                </tr>
			      
		        <!--DISINI-->
				   <tr>
					<td><div>Apakah anda pernah melamar di group perusahaan ini sebelumnya? kapan dan sebagai apa?</div></td>
					<td class="text-center"><input type="radio" name="r1" class="minimal"></td>
					<td class="text-center"><input type="radio" name="r1" class="minimal" checked></td>
					<td><input type="varchar" class="form-control" id="pertanyaan1"></td>
				   </tr>
				   <tr>
					<td><div>Selain di perusahaan ini, di perusahaan mana lagi anda melamar saat ini? sebagai apa?</div></td>
					<td class="text-center"><input type="radio" name="r2" class="minimal" checked></td>
					<td class="text-center"><input type="radio" name="r2" class="minimal" ></td>
					<td><input type="varchar" class="form-control" id="pertanyaan2" value="PT.Nutrifood, sebagai Operator"></td>
				   </tr>
				   <tr>
					<td><div>Apakah anda terkait kontrak dengan perusahaan tempat anda kerja saat ini </div></td>
					<td class="text-center"><input type="radio" name="r3" class="minimal"></td>
					<td class="text-center"><input type="radio" name="r3" class="minimal" checked></td>
					<td><input type="varchar" class="form-control" id="pertanyaan3"></td>
				   </tr>
				   <tr>
					<td><div>Apakah anda mempunyai pekerjaan sampingan? dimana dan sebagai apa?</div></td>
					<td class="text-center"><input type="radio" name="r4" class="minimal" checked></td>
					<td class="text-center"><input type="radio" name="r4" class="minimal"></td>
					<td><input type="varchar" class="form-control" id="pertanyaan4" value="Dirumah, sebagai guru les"></td>
				   </tr>
				   <tr>
					<td><div>Apakah anda keberatan jika kami minta referensi pada perusahaan tempat anda bekerja?</div></td>
					<td class="text-center"><input type="radio" name="r5" class="minimal" ></td>
					<td class="text-center"><input type="radio" name="r5" class="minimal" checked></td>
					<td><input type="varchar" class="form-control" id="pertanyaan5"></td>
				   </tr>
				   <tr>
					<td><div>Apakah anda mempunyai teman/keluarga yang bekerja di group perusahaan ini? sebutkan nama, jabatan dan nama perusahaan?</div></td>
					<td class="text-center"><input type="radio" name="r6" class="minimal" checked></td>
					<td class="text-center"><input type="radio" name="r6" class="minimal" ></td>
					<td><input type="varchar" class="form-control" id="pertanyaan6" value="Ridwan Maulana"></td>
				   </tr>
				   <tr>
					<td><div>Apakah anda pernah menderita sakit kronis/sakit keras/kecelakaan/operasi? Bilamana dan macam apa? jelaskan.</div></td>
					<td class="text-center"><input type="radio" name="r7" class="minimal" ></td>
					<td class="text-center"><input type="radio" name="r7" class="minimal" checked></td>
					<td><input type="varchar" class="form-control" id="pertanyaan7"></td>
				   </tr>
				   <tr>
					<td><div>Apakah anda pernah berurusan dengan polisi karena tindak kejahatan?</div></td>
					<td class="text-center"><input type="radio" name="r8" class="minimal" ></td>
					<td class="text-center"><input type="radio" name="r8" class="minimal" checked></td>
					<td><input type="varchar" class="form-control" id="pertanyaan8"></td>
				   </tr>
				   <tr>
					<td><div>Bila terima, apakah anda bersedia bertugas keluar kota?</div></td>
					<td class="text-center"><input type="radio" name="r9" class="minimal" checked></td>
					<td class="text-center"><input type="radio" name="r9" class="minimal" ></td>
					<td><input type="varchar" class="form-control" id="pertanyaan9"></td>
				   </tr>
				   <tr>
					<td><div>Bila terima, Apakah anda bersedia ditempatkan keluar kota? sebutkan nama kotanya.</div></td>
					<td class="text-center"><input type="radio" name="r10" class="minimal" checked></td>
					<td class="text-center"><input type="radio" name="r10" class="minimal" ></td>
					<td><input type="varchar" class="form-control" id="pertanyaan10" value="Bekasi"></td>
				   </tr>
				    <tr>
					<td><div>Macam pekerjaan apa yang sesuai dengan cita-cita anda?</div></td>
					<td colspan="3"><input type="varchar" class="form-control" id="pertanyaan10" value="Programer Web dan Desktop"></td>
				   </tr>
				   <tr>
					<td><div>macam pekerjaan bagaimana yang anda tidak suka?</div></td>
					<td colspan="3"><input type="varchar" class="form-control" id="pertanyaan11" value="Promosi dan marketing"></td>
				   </tr>
				   <tr>
					<td><div>Berapa besar penghasilan anda saat ini? dan apa saja fasilitas yang anda terima?</div></td>
					<td colspan="3"><input type="varchar" class="form-control" id="pertanyaan12" value="Rp.2.000.000 dan fasilitas laptop"></td>
				   </tr>
				    <tr>
					<td><div>Bila terima, berapa besar gaji dan fasilitas yang anda harapkan?</div></td>
					<td colspan="3"><input type="varchar" class="form-control" id="pertanyaan13" value="Rp.4.000.000"></td>
				   </tr>
				   <tr>
					<td><div>Bila terima, kapankah anda dapat mulai bekerja</div></td>
					<td colspan="3"><input type="varchar" class="form-control" id="pertanyaan14" value="Bulan Juli"></td>
				   </tr>
					<tr style="background:none;">
					     <td colspan="4">
						  <div class="col-md-1" style="padding:0px; margin:0px;">
						  <center><input type="checkbox" name="y" class="minimal"></center>
						  </div>
						  <div class="col-md-4" style="padding:0px; margin:0px;">
						       Dengan ini saya menyatakan bahwa keterangan yang saya berikan diatas, benar isinya. bilamanan terdapat ketidakbenaran, saya bertanggung jawab penuh atas akibatnya dan bersedia mengundurkan diri tanpa kompensasi dalam bentuk apapun 
						  </div>
						  <div class="col-md-7" style="">
						   
						  </div>
						   <div class="col-md-9" style="">
						   
						  </div>
						  
						  <div class="col-md-3" style="padding:0px; margin:0px; text-align:center;">
						      29 Februari 2016
						      <br>
						      <br>
						      <br>
						      <br>
						       Dery/ 29 Februari 2016/10.45
						  </div>
					     </td>
					      
					</tr>
			        
			      </table>
		        
			<br/>
			<div class="col-sm-12" style="text-align:right;">
			 Page 5 of 5
			<div style="height:10px;">&nbsp; </div>
			</div>

		    <div class="box-footer" style="border-top:none;">
			 	<a href="<?php echo base_url('view/vacancy_trainer'); ?>"><button type="submit" class="btn btn-danger">Cancel</button></a>
			  	<a href="vacancy_trainer_edit_4" class="btn btn-primary pull-right">Back</a>
			 	<a href="<?php echo base_url('view/vacancy_trainer'); ?>"><button type="submit" class="btn" style="background-color:#32cd32; color:white;">Save As Draft</button></a>
				<a href="<?php echo base_url('view/vacancy_trainer'); ?>"><button type="submit" class="btn btn-success">Submit</button></a>
		    </div>
		
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
