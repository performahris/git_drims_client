<meta charset="utf-8">
<title>jQuery UI Datepicker - Default functionality</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
    $(function() {
        $("#datepickering").datepicker({dateFormat: 'dd-mm-yy', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepickerin").datepicker({dateFormat: 'dd-mm-yy', changeMonth: true, changeYear: true});
    });
</script>

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<script>
    $(function() {
        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
</script>

<script src="../dist/js/app.min.js" type="text/javascript"></script>
<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>


<div class="content-wrapper" >
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
             <li><a href="#">Employee</a></li>
            <li><a href="../dashboard/list_all_employee">List of All Employee</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Employee | Edit</h3>
                        <hr>
                    </div>
                    <div class="box-body table-responsive">
                        <?php echo form_open('add/employee_register'); ?>
                        <div class="nav-tabs-custom">
                            
                            <ul class="nav nav-tabs">
                                <li><a href="<?php echo base_url('edit/employee_edit'); ?>">Biodata</a></li>
                                <li class="active"><a href="<?php echo base_url('edit/employee_edit_keluarga'); ?>">Keluarga</a></li>
                                <li><a href="<?php echo base_url('edit/employee_edit_karyawan'); ?>">Karyawan</a></li>
                            </ul>
                            <br>
                            <div class="tab-content no-padding">
                                <div class="chart tab-pane active">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <!--Form Keluaarga-->
                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Family Card No</label>
                                                </div> 
                                                <div class="col-md-4 col-xs-12 col-sm-5">
                                                    <input type="text" class="form-control" id="" placeholder=""> 
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Father's Name</label>
                                                </div> 
                                                <div class="col-md-6 col-xs-12 col-sm-5">
                                                    <input type="text" class="form-control" id="" placeholder="">
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Mother's Name</label>
                                                </div> 
                                                <div class="col-md-6 col-xs-12 col-sm-5">
                                                    <input type="text" class="form-control" id="" placeholder="">
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Number Of Siblings</label>
                                                </div> 
                                                <div class="col-md-6 col-xs-12 col-sm-5">
                                                    <input type="text" class="form-control" id="" placeholder="">
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Marital Status</label>
                                                </div> 
                                                <div class="col-md-4 col-xs-12 col-sm-5">
                                                    <select class="form-control">
                                                        <option fisible="false">List Off Asigment Type</option>
                                                        <option>HRD</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Husband / Wife Name</label>
                                                </div> 
                                                <div class="col-md-6 col-xs-12 col-sm-5">
                                                    <input type="text" class="form-control" id="" placeholder="">
                                                </div>
                                            </div>

                                            <!--anak-->
                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-10 col-xs-12 col-sm-10 ">
                                                    <div class="box box-default collapsed-box">
                                                        <div class="box-header with-border">
                                                            <h3 class="box-title">1st Child</h3>
                                                            <div class="box-tools pull-right">
                                                                <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo"><i class="fa fa-plus"></i></button>
                                                                <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo"><i class="fa fa-times"></i></button>
                                                            </div>
                                                            <div class="row" style="padding-top:5px;">
                                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                                    <label for="comment">Name</label>
                                                                </div> 
                                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                                    <input type="text" class="form-control" id="" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="row" style="padding-top:5px;">
                                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                                    <label for="comment">Date Off Birth</label>
                                                                </div> 
                                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </div>
                                                                        <input type="text" id="datepickering" class="form-control pull-rigth active"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                                    <label for="comment">Gender</label>
                                                                </div> 
                                                                <div class="col-md-2 col-xs-4 col-sm-2">
                                                                    <h4><input type="radio" name="gender" value="point1">Male</h4>
                                                                </div>
                                                                <div class="col-md-2 col-xs-4 col-sm-2">
                                                                    <h4><input type="radio" name="gender" value="point2">Female</h4>
                                                                </div>
                                                            </div>
                                                        </div><!-- /.box-header -->
                                                        <div id="demo" class="collapse">
                                                            <div class="box box-default collapsed-box">
                                                                <div class="box-header with-border">
                                                                    <h3 class="box-title">2nd Child</h3>
                                                                    <div class="box-tools pull-right">
                                                                        <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#dem"><i class="fa fa-plus"></i></button>
                                                                    </div>
                                                                    <div class="row" style="padding-top:5px;">
                                                                        <div class="col-md-4 col-xs-12 col-sm-4">
                                                                            <label for="comment">Name</label>
                                                                        </div> 
                                                                        <div class="col-md-4 col-xs-12 col-sm-4">
                                                                            <input type="text" class="form-control" id="" placeholder="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row" style="padding-top:5px;">
                                                                        <div class="col-md-4 col-xs-12 col-sm-4">
                                                                            <label for="comment">Date Off Birth</label>
                                                                        </div> 
                                                                        <div class="col-md-4 col-xs-12 col-sm-4">
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">
                                                                                    <i class="fa fa-calendar"></i>
                                                                                </div>
                                                                                <input type="text" id="datepickerin" class="form-control pull-rigth active"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                                        <div class="col-md-4 col-xs-12 col-sm-4">
                                                                            <label for="comment">Gender</label>
                                                                        </div> 
                                                                        <div class="col-md-2 col-xs-4 col-sm-2">
                                                                            <h4><input type="radio" name="gender" value="point1">Male</h4>
                                                                        </div>
                                                                        <div class="col-md-2 col-xs-4 col-sm-2">
                                                                            <h4><input type="radio" name="gender" value="point2">Female</h4>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="dem" class="collapse">
                                                            <div class="box box-default collapsed-box">
                                                                <div class="box-header with-border">
                                                                    <h3 class="box-title">3rd Child</h3>
                                                                    <div class="box-tools pull-right">
                                                                        <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#dem"><i class="fa fa-plus"></i></button>
                                                                    </div>
                                                                    <div class="row" style="padding-top:5px;">
                                                                        <div class="col-md-4 col-xs-12 col-sm-4">
                                                                            <label for="comment">Name</label>
                                                                        </div> 
                                                                        <div class="col-md-4 col-xs-12 col-sm-4">
                                                                            <input type="text" class="form-control" id="" placeholder="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row" style="padding-top:5px;">
                                                                        <div class="col-md-4 col-xs-12 col-sm-4">
                                                                            <label for="comment">Date Off Birth</label>
                                                                        </div> 
                                                                        <div class="col-md-4 col-xs-12 col-sm-4">
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">
                                                                                    <i class="fa fa-calendar"></i>
                                                                                </div>
                                                                                <input type="text" id="datepickering" class="form-control pull-rigth active"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                                        <div class="col-md-4 col-xs-12 col-sm-4">
                                                                            <label for="comment">Gender</label>
                                                                        </div> 
                                                                        <div class="col-md-2 col-xs-4 col-sm-2">
                                                                            <h4><input type="radio" name="gender" value="point1">Male</h4>
                                                                        </div>
                                                                        <div class="col-md-2 col-xs-4 col-sm-2">
                                                                            <h4><input type="radio" name="gender" value="point2">Female</h4>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div><!-- /.box -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-xs-12 col-sm-12" style="padding-top:10px; padding-bottom:10px;">
                                            <div class="col-md-4 col-xs-4 col-sm-4">
                                                <a href="#"><input type="submit" class="btn btn-block btn-success" value="Save"></a>
                                            </div>
                                            <div class="col-md-4 col-xs-4 col-sm-4">
                                                <a href="#"><input type="button" class="btn btn-block btn-warning" value="Save As Draft"></a>
                                            </div>
                                            <div class="col-md-4 col-xs-4 col-sm-4">
                                                <a href="<?php echo base_url('dashboard/list_all_employee'); ?>"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
