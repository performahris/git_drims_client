<meta charset="utf-8">
<title>jQuery UI Datepicker - Default functionality</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
    $(function() {
        $("#datepickering").datepicker({dateFormat: 'dd-mm-yy', changeMonth: true, changeYear: true});
    });
</script>


<script>
    $(function() {
        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
</script>
<div class="content-wrapper" >
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
             <li><a href="#">Employee</a></li>
            <li><a href="../dashboard/list_all_employee">List of All Employee</a></li>
            <li class="active">karyawan</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Employee | Edit</h3>
                        <hr>
                    </div>
                    <div class="box-body table-responsive">
                        <?php echo form_open('add/employee_register'); ?>
                        <div class="nav-tabs-custom">
                            
                            <ul class="nav nav-tabs">
                                <li><a href="<?php echo base_url('edit/employee_edit'); ?>">Biodata</a></li>
                                <li><a href="<?php echo base_url('edit/employee_edit_keluarga'); ?>">Keluarga</a></li>
                                <li class="active"><a href="<?php echo base_url('edit/employee_edit_karyawan'); ?>">Karyawan</a></li>
                            </ul>
                            <br>
                            <div class="tab-content no-padding">
                                <div class="chart tab-pane active">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <!--Form karyawan-->
                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Join Date</label>
                                                </div> 
                                                <div class="col-md-4 col-xs-12 col-sm-5">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" id="datepickering" class="form-control pull-rigth active"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Personnel Assignment Type</label>
                                                </div> 
                                                <div class="col-md-4 col-xs-12 col-sm-5">
                                                    <select class="form-control">
                                                        <option fisible="false">List Off Asigment Type</option>
                                                        <option>HRD</option>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Personal Schedule Type</label>
                                                </div> 
                                                <div class="col-md-4 col-xs-12 col-sm-5">
                                                    <select class="form-control">
                                                        <option fisible="false">List Off Schedule Type</option>
                                                        <option>HRD</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Rational Work Days</label>
                                                </div> 
                                                <div class="col-md-2 col-xs-6 col-sm-3">
                                                    <input type="text" class="form-control" id="" placeholder="">
                                                </div>
                                                <h5><b>Days</b></h5>
                                            </div>

                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Rational Off Days</label>
                                                </div> 
                                                <div class="col-md-2 col-xs-6 col-sm-3">
                                                    <input type="text" class="form-control" id="" placeholder="">
                                                </div>
                                                <h5><b>Days</b></h5>
                                            </div>

                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Annual Leave Entitlement</label>
                                                </div> 
                                                <div class="col-md-4 col-xs-12 col-sm-5">
                                                    <input type="text" class="form-control" id="" placeholder="">
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Direct Supervisor Name</label>
                                                </div> 
                                                <div class="col-md-4 col-xs-12 col-sm-5">
                                                    <select class="form-control">
                                                        <option fisible="false">List Off Supervisor</option>
                                                        <option>HRD</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Department</label>
                                                </div> 
                                                <div class="col-md-4 col-xs-12 col-sm-5">
                                                    <select class="form-control">
                                                        <option fisible="false">List Off Department</option>
                                                        <option>HRD</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Position</label>
                                                </div> 
                                                <div class="col-md-4 col-xs-12 col-sm-5">
                                                    <select class="form-control">
                                                        <option fisible="false">List Off Position</option>
                                                        <option>HRD</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Employee Grade</label>
                                                </div> 
                                                <div class="col-md-4 col-xs-12 col-sm-5">
                                                    <select class="form-control">
                                                        <option fisible="false">List Off Employee Grade</option>
                                                        <option>HRD</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Company Name</label>
                                                </div> 
                                                <div class="col-md-4 col-xs-12 col-sm-5">
                                                    <select class="form-control">
                                                        <option fisible="false">List Off Company</option>
                                                        <option>HRD</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Point Off Hire</label>
                                                </div> 
                                                <div class="col-md-4 col-xs-12 col-sm-5">
                                                    <input type="text" class="form-control" id="" placeholder="">
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Currency Base</label>
                                                </div> 
                                                <div class="col-md-4 col-xs-12 col-sm-5">
                                                    <select class="form-control">
                                                        <option fisible="false">List Off Curency</option>
                                                        <option>HRD</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">personnel Salary Type</label>
                                                </div> 
                                                <div class="col-md-4 col-xs-12 col-sm-5">
                                                    <select class="form-control">
                                                        <option fisible="false">List Off Salary Type</option>
                                                        <option>HRD</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Salary Currency</label>
                                                </div> 
                                                <div class="col-md-4 col-xs-12 col-sm-5">
                                                    <select class="form-control">
                                                        <option fisible="false">List Off Currency</option>
                                                        <option>HRD</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Basic Salary</label>
                                                </div> 
                                                <div class="col-md-4 col-xs-12 col-sm-5">
                                                    <input type="text" class="form-control" id="" placeholder="">
                                                </div>
                                            </div>
                                            
                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Payment Method</label>
                                                </div> 
                                                <div class="col-md-4 col-xs-12 col-sm-5">
                                                    <select class="form-control">
                                                        <option fisible="false">List Payment Method</option>
                                                        <option>HRD</option>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Bank Name</label>
                                                </div> 
                                                <div class="col-md-4 col-xs-12 col-sm-5">
                                                    <input type="text" class="form-control" id="" placeholder="">
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Bank Code</label>
                                                </div> 
                                                <div class="col-md-4 col-xs-12 col-sm-5">
                                                    <input type="text" class="form-control" id="" placeholder="">
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                <div class="col-md-4 col-xs-12 col-sm-4">
                                                    <label for="comment">Branch Code & Bank Account No</label>
                                                </div> 
                                                <div class="col-md-4 col-xs-12 col-sm-5">
                                                    <input type="text" class="form-control" id="" placeholder="">
                                                </div>
                                            </div>
                                            <!--batas-->
                                        </div>
                                        <div class="col-md-8 col-xs-12 col-sm-12" style="padding-top:10px; padding-bottom:10px;">
                                            <div class="col-md-4 col-xs-4 col-sm-4">
                                                <a href="#"><input type="submit" class="btn btn-block btn-success" value="Save"></a>
                                            </div>
                                            <div class="col-md-4 col-xs-4 col-sm-4">
                                                <a href="#"><input type="button" class="btn btn-block btn-warning" value="Save As Draft"></a>
                                            </div>
                                            <div class="col-md-4 col-xs-4 col-sm-4">
                                                <a href="<?php echo base_url('dashboard/list_all_employee'); ?>"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
