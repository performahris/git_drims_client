<link href="../../asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<div class="content-wrapper">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Booking Room</li>
            <li class="active">Requested</li>
            <li class="active">View</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Booking Room | Requested | View</h3>
                        <hr>
                    </div>
                    		<div class="panel-body">
                    			<div class="col-md-12">
				                	<div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Event Name</label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                            Meeting HRD 
				                        </div>
				                    </div>
				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Date From</label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                            18 Aug 2014 08:30
                                </div> 
				                    </div>
				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                <div class="col-md-3 col-xs-12 col-sm-4">
                                    <label for="comment">Date To</label>
                                </div> 
                                <div class="col-md-3 col-xs-12 col-sm-5">
                                    19 Aug 2014 12:00
                                </div>
                            </div>
				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Location / Venue</label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                            Ruangan B Lt.1
                                </div>
				                    </div>
				                	<div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Logistic Suport</label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                            Laptop | TV LCD | Whiteboard 
				                        </div> 
                                <div class="col-md-3 col-xs-12 col-sm-5">
                                    <span class="label label-success"><i class="fa fa-check">&nbsp;</i>Available</span>
                                </div>
				                    </div>
				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">PIC</label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                            SUDARMONO
				                        </div>
				                    </div>
				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">External Guest</label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                        	   -
				                        </div>
				                    </div>
				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                <div class="col-md-3 col-xs-12 col-sm-4">
                                    <label for="comment">Agenda</label>
                                </div> 
                                <div class="col-md-3 col-xs-12 col-sm-5">
                                    Membahas tentang HRD
                                </div>
                            </div>
				                    
				                </div>

                  <div class="col-md-8 col-xs-12 col-sm-12" style="padding-top:10px; padding-bottom:10px;">
                    <div class="col-md-4 col-xs-4 col-sm-4">
                      <a href="<?php echo base_url('dashboard/booking_room_requested_list'); ?>"><input type="button" class="btn btn-block btn-info" value="Approve"></a>
                    </div>
                    <div class="col-md-4 col-xs-4 col-sm-4">
                      <a href="<?php echo base_url('dashboard/booking_room_requested_list'); ?>"><input type="button" class="btn btn-block btn-success" value="Reject"></a>
                    </div>
                    <div class="col-md-4 col-xs-4 col-sm-4">
                      <a href="<?php echo base_url('dashboard/booking_room_requested_list'); ?>"><input type="button" class="btn btn-block btn-danger" value="Review Later"></a>
                            </div>  
                        </div>
                    </div>
                  </div>
				        </div>
				    </div>
				</div>
			</div>
		</div>
    </section>
</div>

<script type="text/javascript">

    $(function() {
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Timepicker
        $(".timepicker").timepicker({
            showInputs: false
        });
    });
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {


    });

    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    $(function() {
        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker3").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker4").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker5").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
</script>


<script type="text/javascript">
    $(document).ready(function() {
      $(".departemen").select2();
    });
    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      $scope.sortType     = 'no'; // set the default sort type
      $scope.sortReverse  = false;  // set the default sort order
      $scope.search   = ''; 
      $scope.limit =25;
      $scope.dari="";
      $scope.to=""; 
      $scope.depart="";  
      $scope.jumlah = 0;
                   
      
      $scope.employee = <?php echo $employee ?>;
      
      
      $scope.departemen = [
        {nama : "Purchasing"},
        {nama : "Research"},
        {nama : "HRD"}
        ];
        
        $scope.reset = function () {
            $scope.depart =  "";     
        } 
        
        
       $scope.currentPage = 1;  
       $scope.totalItems = $scope.employee.length;  
       $scope.numPerPage = $scope.limit;    
       
       $scope.limitPage = function() {
         $scope.numPerPage = $scope.limit;
         if($scope.currentPage * $scope.numPerPage > $scope.employee.length){
            $scope.currentPage = 1;   
         }
      };
       
       $scope.lastPage = function() {      
         $scope.currentPage=$scope.pageCount();
      };
      
      $scope.firstPage = function() {
         $scope.currentPage=1;
      };
       
       $scope.nextPage = function() {
        
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
      };
      
      $scope.jumlahPerpage = function (value) {
        $scope.numPerPage = value;
      } 
      
      $scope.prevPage = function() {
        if ($scope.currentPage > 1) {
          $scope.currentPage--;
        }
      };
      
       $scope.pageCount = function() {
        return Math.ceil($scope.jumlah/$scope.numPerPage);
      };  
      
      $scope.checkAll = function () {
        angular.forEach($scope.employee, function (item) {
            item.Selected = $scope.selectAll;
        });
      };
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    app.filter("dateRange", function() {      
      return function(items, dari, to) {
        if(dari.length==0){
            var dari = +new Date("1980-01-01");
        }else{
            var dari = +new Date(dari);
        }
        
        if(to.length==0){
            var to = +new Date();
        }else{
            var to = +new Date(to);
        }
        var df = dari;
        var dt = to ;
        var arrayToReturn = [];        
        for (var i=0; i<items.length; i++){
            var tf = +new Date(items[i].join);
            if ((tf > df && tf < dt) || (tf==dt) )  {
                arrayToReturn.push(items[i]);
            }
        }
        
        return arrayToReturn;
      };
        
    });
    
    
    app.filter("paging", function() {      
      return function(items, limit, currentPage) {
        if (typeof limit === 'string' || limit instanceof String){
           limit = parseInt(limit);
         }
         
         var begin, end, index;  
         begin = (currentPage - 1) * limit;  
         end = begin + limit;
         var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (begin <= i && i < end )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    
    app.filter("departemen", function() {      
      return function(items, depart) {
      if(depart.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].dept == depart )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    
    app.filter("nama", function() {      
      return function(items, search) {
      if(search.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].name.toUpperCase().indexOf(search.toUpperCase()) != -1 )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    

</script>