<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">SPPD</li>
            <li class="active">Requested</li>
            <li class="active">View</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">SPPD | Requested | View</h3>
                        <hr>
                    </div>
                    <div class="box-body table-responsive">
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label>
								<div class="col-md-12 col-xs-12 col-sm-12">No</div>
							</label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
									01/HRD/01/2016
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<div class="col-md-3 col-xs-12 col-sm-12">By</div>
									<div class="col-md-9 col-xs-12 col-sm-12" style="font-weight:normal">
										SUDARMONO
									</div>
								</div>
							</label>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Nama</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
									SUDARMONO
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Jabatan/ Golongan</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
									Operation Director	
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Divisi</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
									Production
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Tujuan</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-8 col-xs-12 col-sm-12" style="font-weight:normal">
									Agar Divisi Produksi bisa lebih meningkatkan produksi kerja dan profit perusahaan.
								</div>
							</label>		
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label ><div class="col-md-12 col-xs-12 col-sm-12">Periode</div></label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:100%">
								<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
									10 Feb 2016
								</div>
							</label>		
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12">
							<h4><strong>Sarana Transportasi Perjalanan Dinas</strong></h4>
						</div>

						<div class="col-md-12 col-xs-12 col-sm-12">
							<div class="col-md-12">
								<div class="row" style="padding-top:5px; padding-bottom:5px;">
									<div class="col-md-12 col-xs-12 col-sm-12">
										<label >
												<input type="checkbox" name="sarana1" checked disabled/> Pesawat
										</label>
									</div>
								</div>
								<div class="row" style="padding-top:5px; padding-bottom:5px;">
									<div class="col-md-3 col-xs-12 col-sm-12">
										<label >
												<input type="checkbox" name="sarana2" disabled /> Kereta Api / Bus / Kapal Laut
										</label>
									</div>
									<div class="col-md-1 col-xs-12 col-sm-12">
										<label>Kelas</label>
									</div>
									<div class="col-md-7 col-xs-12 col-sm-12" style="font-weight:normal">
										-
									</div>
								</div>

								<div class="row" style="padding-top:5px; padding-bottom:5px;">
									<div class="col-md-3 col-xs-12 col-sm-12">
										<label >
												<input type="checkbox" name="sarana3" disabled /> Mobil Operasional Perusahaan
										</label>
									</div>
									<div class="col-md-1 col-xs-12 col-sm-12">
											<label>No.Pol</label>
									</div>
									<div class="col-md-7 col-xs-12 col-sm-12" style="font-weight:normal">
											B 3216 MKF
									</div>
								</div>

								<div class="row" style="padding-top:5px; padding-bottom:5px;">
									<div class="col-md-3 col-xs-12 col-sm-12">
										<label>
											<input type="checkbox" name="sarana" disabled/> Lain-Lain
										</label>
									</div>
									<div class="col-md-8 col-xs-12 col-sm-12">
										-
									</div>
								</div>
							</div>
						</div>


						<div class="col-md-12 col-xs-12 col-sm-12">
							<h4><strong>Kas Bon Yang Diajukan</strong></h4>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label >
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label>Uang Makan</label>
								</div>
							</label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:40%">
								<div class="col-md-2 col-xs-12 col-sm-12">
									<label>IDR</label>
								</div>
								<div class="col-md-8 col-xs-12 col-sm-12" style="font-weight:normal">
									150.000
								</div>
							</label>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label >
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label>Hotel</label>
								</div>
							</label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:40%">
								<div class="col-md-2 col-xs-12 col-sm-12">
									<label>IDR</label>
								</div>
								<div class="col-md-8 col-xs-12 col-sm-12" style="font-weight:normal">
									650.000
								</div>
							</label>
						</div>

						<div class="col-md-3 col-xs-12 col-sm-12">
							<label >
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label>Transport / Taksi P.P</label>
								</div>
							</label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:40%">
								<div class="col-md-2 col-xs-12 col-sm-12">
									<label>IDR</label>
								</div>
								<div class="col-md-8 col-xs-12 col-sm-12" style="font-weight:normal">
									200.000
								</div>
							</label>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12">
							<label >
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label>Lain - Lain</label>
								</div>
							</label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:40%">
								<div class="col-md-2 col-xs-12 col-sm-12">
									<label>IDR</label>
								</div>
								<div class="col-md-8 col-xs-12 col-sm-12" style="font-weight:normal">
									-
								</div>
							</label>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-12" >
							<label >
								<div class="col-md-12 col-xs-12 col-sm-12" style="text-align:right;">
									<label>Total</label>
								</div>
							</label>
						</div>
						<div class="col-md-9 col-xs-12 col-sm-12">
							<label style="width:40%">
								<div class="col-md-2 col-xs-12 col-sm-12">
									<label>IDR</label>
								</div>
								<div class="col-md-8 col-xs-12 col-sm-12" style="font-weight:normal">
									1.000.000
								</div>
							</label>
						</div>

						
						<br>
						<div class="col-md-12 col-xs-12 col-sm-12">
							<table id="activity" class="table table-striped table-bordered" style="width:40%">
							<h5><b>Activity Log</b></h5>
							<div class="col-md-12 col-xs-12 col-sm-12">
							
							</div>
								<tr class="success">
									<th><center>No</center></th>
									<th><center>Activity</center></th>
									<th><center>Date & time</center></th>
									<th><center>By</center></th>
								</tr>
								<tr>	
									<td><center>1</center></td>
									<td><center>Create Policy</center></td>
									<td><center>10 Feb 2016 11:00:00</center></td>
									<td><center>Shinta</center></td>
								</tr>
								<tr> 
									<td><center>2</center></td>
									<td><center>Submit Policy</center></td>
									<td><center>18 Feb 2016 09:05:37</center></td>
									<td><center>Dery</center></td>
								</tr>
								<tr>
									<td><center>3</center></td>
									<td><center>Review Policy</center></td>
									<td><center>21 Feb 2016 10:00:01</center></td>
									<td><center>Bhima</center></td>
								</tr>
								<tr>
									<td><center>4</center></td>
									<td><center>Approve Policy</center></td>
									<td><center>07 Mar 2016 23:01:57</center></td>
									<td><center>Badrizka</center></td>
								</tr>
							</table>
						</div>
						<div class="col-md-8 col-xs-12 col-sm-12">
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="<?php echo base_url()."dashboard/sppd_requested" ?>"><input type="button" class="btn btn-block btn-danger" value="Back"></a>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
