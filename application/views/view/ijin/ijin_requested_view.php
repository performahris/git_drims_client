<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Ijin</li>
            <li class="active">Requested</li>
            <li class="active">View</li>
        </ol>
    </section>
	
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
					<div class="box-header">
                        <h3 class="box-title">Ijin | Requested | View</h3>
                        <hr>
                    </div>
                    <div class="box-body table-responsive">
						
						<div class="col-md-12">
							<div class="col-md-2 col-xs-12 col-sm-12">
								<label>
									<div class="col-md-12 col-xs-12 col-sm-12">Employee Code</div>
								</label>
							</div>
							<div class="col-md-10">
								<div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
									E_1
								</div>
								<div class="col-md-1"><label>Date :</label></div>
								<div class="col-md-5">16-Feb-2016</div>
							</div>
						</div>

						<div class="col-md-12">	
							<div class="col-md-2 col-xs-12 col-sm-12">
								<label ><div class="col-md-12 col-xs-12 col-sm-12">Employee Name</div></label>
							</div>
							<div class="col-md-10 col-xs-12 col-sm-12">
								<label style="width:100%">
									<div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
										SUDARMONO	
									</div>
								</label>		
							</div>
						</div>
						
						<div class="col-md-12">
							<div class="col-md-2 col-xs-12 col-sm-12">
								<label ><div class="col-md-12 col-xs-12 col-sm-12">Position</div></label>
							</div>
							<div class="col-md-10 col-xs-12 col-sm-12">
								<label style="width:100%">
									<div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
										Operation Director
									</div>
								</label>		
							</div>
						</div>

						<div class="col-md-12">
							<div class="col-md-2 col-xs-12 col-sm-12">
								<label ><div class="col-md-12 col-xs-12 col-sm-12">Location</div></label>
							</div>
							<div class="col-md-10 col-xs-12 col-sm-12">
								<label style="width:100%">
									<div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
										-
									</div>
								</label>		
							</div>
						</div>

						<div class="col-md-12">
							<div class="col-md-2 col-xs-12 col-sm-12">
								<label><div class="col-md-12 col-xs-12 col-sm-12">Category</div></label>
							</div>
							<div class="col-md-10 col-xs-12 col-sm-12">
								<label>
									<div class="col-md-12 col-xs-12 col-sm-12" style="font-weight:normal">
										Ijin Pulang Cepat
									</div>
								</label>		
							</div>
						</div>

						<div class="col-md-12">
							<div class="col-md-2 col-xs-12 col-sm-12">
								<label><div class="col-md-12 col-xs-12 col-sm-12">Time</div></label>
							</div>
							<div class="col-md-10 col-xs-12 col-sm-12">
								<label>
									<div class="col-md-12 col-xs-12 col-sm-12 bootstrap-timepicker" style="font-weight:normal">
										08:00 PM
									</div>
								</label>
								<label><div class="col-md-12 col-xs-12 col-sm-12">Until</div></label>
								<label>
									<div class="col-md-12 col-xs-12 col-sm-12 bootstrap-timepicker" style="font-weight:normal">
										12:00 PM
									</div>
								</label>
							</div>
						</div>

						<div class="col-md-12" style="padding-bottom:20px;">
							<div class="col-md-2 col-xs-12 col-sm-12">
								<label><div class="col-md-12 col-xs-12 col-sm-12">Note</div></label>
							</div>
							<div class="col-md-10 col-xs-12 col-sm-12">
								<label style="width:100%">
									<div class="col-md-12 col-xs-12 col-sm-12" style="font-weight:normal">
										Dinas Keluar Kota
									</div>
								</label>		
							</div>
						</div>
						<!--
                        <table class="table table-striped">
                            <tr>
								<td><b>Employee ID</b></td>
								<td>
									<div class="col-md-6 col-xs-12 col-sm-12">
										<input type="text" name="employeeId" placeholder ="Employee ID"/>
									</div>
									<div class="col-md-6 col-xs-12 col-sm-12">
										<div class="col-md-4 col-xs-12 col-sm-12">
											Date
										</div>
										<div class="col-md-8 col-xs-12 col-sm-12">
											<input type="text" name="date" id="datepicker1"/>
										</div>
									</div>
								</td>
							</tr>

							<tr>
								<td><b>Employee Name</b></td>
								<td><div class="col-md-12 col-xs-12 col-sm-12"><input type="text" name="employeeName" placeholder ="Employee Name"/></div></td>
							</tr>

							<tr>
								<td><b>Category</b></td>
								<td><div class="col-md-12 col-xs-12 col-sm-12">
										<select name="category" ng-model="category">
											<option value="0">Ijin Pulang Cepat</option>
											<option value="1">Ijin terlambat</option>
											<option value="2">Ijin Keluar Kantor</option>
											<option value="3">Other</option>
										</select>
									<div style="padding-top:5px;" ng-if='category==3'><input type="text" name="addcategory" ng-model="addcategory" placeholder ="Other"/></div>
									</div>
								</td>
							</tr>
							<tr>
								<td><b> Time </b></td>
								<td>
									<div class="col-md-12 col-xs-12 col-sm-12">
										<div style="float:left"><input type="text" name="time1" class="number"  style="width: 50px;"/> </div>
										<div style="float:right;" ng-if="category==2"><b style="padding-right:20px;">Until : </b><input type="text" style="width: 50px;" name="time2" class="number" /></div>
									</div>	
								</td>
							</tr>
							<tr>
								<td valign="top"><b>Note</b></td >
								<td ><div class="col-md-10 col-xs-12 col-sm-12"><textarea style= "width:100%" rows = "15"> </textarea></div><td/>
                        </table>-->
						<div class="col-md-8 col-xs-12 col-sm-12">
							<div class="col-md-4 col-xs-4 col-sm-4">
	                            <a href="<?php echo base_url('dashboard/ijin_requested'); ?>"><input type="button" class="btn btn-block btn-info" value="Approve"></a>
	                        </div>
	                        <div class="col-md-4 col-xs-4 col-sm-4">
	                            <a href="<?php echo base_url('dashboard/ijin_requested'); ?>"><input type="button" class="btn btn-block btn-success" value="Reject"></a>
	                        </div>
	                        <div class="col-md-4 col-xs-4 col-sm-4">
	                            <a href="<?php echo base_url('dashboard/ijin_requested'); ?>"><input type="button" class="btn btn-block btn-danger" value="Review Later"></a>
	                        </div>
						</div>
							
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    $(function () {
		 $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });
    });
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
