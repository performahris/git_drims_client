<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" >
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>dashboard/department_list">HR</a></li>
            <li class="active">Holiday Management</li>
            <li class="active">View</li>
        </ol>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Holiday Management | View</h3>
                        <hr>
                    </div>
                    <div class="box-body table-responsive">
                        <?php echo form_open('add/departement_register_add');?>
                            <div class="col-md-12">
                                <div class="col-md-3 col-xs-12 col-sm-12">
                                    <label>Holiday</label>
                                </div>
                                <div class="col-md-9 col-xs-12 col-sm-12">
                                    New Year's Day
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-3 col-xs-12 col-sm-12">
                                    <label>Date</label>
                                </div>
                                <div class="col-md-9 col-xs-6 col-sm-5">
                                    01/01/2016
                                </div>
                            </div>
                            <div class="col-md-8 col-xs-12 col-sm-12" style="padding-top:10px;">
                                <div class="col-md-4 col-xs-4 col-sm-4">
                                    <a href="<?php echo base_url();?>dashboard/holiday_management"><input type="button" class="btn btn-block btn-danger" value="Back"></a>
                                </div>
                            </div>
                        <?php echo form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

