<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Tunjangan</li>
            <li class="active">Requested</li>
            <li class="active">View</li>
        </ol>
    </section>
	
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
					<div class="box-header">
                        <h3 class="box-title">Tunjangan | Requested | View</h3>
                        <hr>
                    </div>
                    <div class="box-body table-responsive">
						<div class="col-md-3 col-xs-12 col-sm-12">
								<label ><div class="col-md-12 col-xs-12 col-sm-12">Employee Code</div></label>
							</div>
							<div class="col-md-9 col-xs-12 col-sm-12">
								<label style="width:100%">
									<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
										E_1	
									</div>
								</label>		
							</div>

							<div class="col-md-3 col-xs-12 col-sm-12">
								<label ><div class="col-md-12 col-xs-12 col-sm-12">Employee Name</div></label>
							</div>
							<div class="col-md-9 col-xs-12 col-sm-12">
								<label style="width:100%">
									<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
										SUDARMONO	
									</div>
								</label>		
							</div>

							<div class="col-md-3 col-xs-12 col-sm-12">
								<label ><div class="col-md-12 col-xs-12 col-sm-12">Position</div></label>
							</div>
							<div class="col-md-9 col-xs-12 col-sm-12">
								<label style="width:100%">
									<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
										Office Boy	
									</div>
								</label>		
							</div>

							<div class="col-md-3 col-xs-12 col-sm-12">
								<label ><div class="col-md-12 col-xs-12 col-sm-12">Group</div></label>
							</div>
							<div class="col-md-9 col-xs-12 col-sm-12">
								<label style="width:100%">
									<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
										Non Staff	
									</div>
								</label>		
							</div>

							<div class="col-md-3 col-xs-12 col-sm-12">
								<label ><div class="col-md-12 col-xs-12 col-sm-12">Category</div></label>
							</div>
							<div class="col-md-9 col-xs-12 col-sm-12">
								<label style="width:100%">
									<div class="col-md-4 col-xs-12 col-sm-12" style="font-weight:normal">
										Tunjangan Perkawinan
									</div>
								</label>		
							</div>
							<div class="col-md-3 col-xs-12 col-sm-12">
								<label ><div class="col-md-12 col-xs-12 col-sm-12">Value</div></label>
							</div>
							<div class="col-md-9 col-xs-12 col-sm-12">
								<label style="width:67%">
									<div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:normal">
										2,000,000
									</div>
								</label>		
							</div>
						<!--
                        <table id="example1" class="table table-striped	 ">
							<tr>
								<td>Category</td>
								<td>:</td>
								<td>
									<select name="category">
										<option>Lorem</option>
										<option>Ipsum</option>
									</select>
								</td>
							</tr>
							<tr>
								<td>Position</td>
								<td>:</td>
								<td>
									<select name="position">
										<option>Lorem</option>
										<option>Ipsum</option>
									</select>
								</td>
							</tr>
							<tr>
								<td>Value</td>
								<td>:</td>
								<td><input type="text" name="value"/></td>
							</tr>
                        </table>
						-->
						<div class="col-md-12 col-xs-12 col-sm-12">
                            <div class="col-md-5 col-xs-12 col-sm-12" style="margin-top:5px;margin-bottom:5px;">
                            <label for="comment">Activity Log</label>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr class="info">
                                            <th>No</th>
                                            <th>Activity</th>
                                            <th>Date & Time</th>
                                            <th>By</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Create Policy</td>
                                            <td>20 mei 2015 11:00:00</td>
                                            <td>Dimas</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Submit Policy</td>
                                            <td>22 mei 2015 09:05:37</td>
                                            <td>Dimas</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Review Policy</td>
                                            <td>22 mei 2015 10:00:01</td>
                                            <td>Adi Bagus</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Approve Policy</td>
                                            <td>23 mei 2015 23:01:57</td>
                                            <td>Andy Rikie Lam</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
						<br>	
						<div class="col-md-8 col-xs-12 col-sm-12">
							<div class="col-md-4 col-xs-4 col-sm-4">
								<a href="<?php echo base_url()."dashboard/tunjangan_requested" ?>"><input type="submit" class="btn btn-block btn-info" value="Approve"></a>
							</div>
							<div class="col-md-4 col-xs-4 col-sm-4">
								<a href="<?php echo base_url()."dashboard/tunjangan_requested" ?>"><input type="button" class="btn btn-block btn-success" value="Reject"></a>
							</div>
							<div class="col-md-4 col-xs-4 col-sm-4">
								<a href="<?php echo base_url()."dashboard/tunjangan_requested" ?>"><input type="button" class="btn btn-block btn-danger" value="Review Later"></a>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
