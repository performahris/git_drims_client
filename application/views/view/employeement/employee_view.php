<script>
    $(function() {
        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
</script>

<style type="text/css">
    .scroll{
        width:auto;
        height:auto;
        overflow:auto;
    }
</style>

<div class="content-wrapper" >
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li><a href="#"></i>Employeement</a></li>
            <li><a href="#"></i>Employee</a></li>
            <li class="active">View</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Employeement | Employee | View</h3>
                        <hr>
                    </div>

                    <div class="col-md-12" align="right">
                        <img src="<?php echo base_url(); ?>upload/foto_employee/employee_1.jpg" width="80px" height="85px"/>
                    </div>

                    <div class="box-body table-responsive">    	
                        <div class="nav-tabs-custom">

                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#biodata">Biodata</a></li>
                                <li><a data-toggle="tab" href="#keluarga">Family</a></li>
                                <li><a data-toggle="tab" href="#karyawan">Employee</a></li>
                            </ul>
                            <br>
                            <div class="tab-content">
                                <div id="biodata" class="tab-pane fade in active">
                                    <div class="chart tab-pane active">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <!--Form Biodata-->
                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Employee Code</label>
                                                    </div> 
                                                    <div class="col-md-8 col-xs-12 col-sm-5">
                                                        E_1 
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Identity Number</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        1606072205780001
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Employee Name</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        SUDARMONO
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Address</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-7">
                                                        Komp Griya Suka Bangun Blok D7 RT.97 RW.09 Desa Sukajaya Kec. Sukarami
                            					    </div>
                            					</div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Address (Domicile)</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        -
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Postal Code</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        46867
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">City</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        Musi Banyuasin
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Province</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        Sumatera Selatan
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Photo</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        <div class="">
                                                           <left> 
                                                               <img src="<?php echo base_url(); ?>upload/foto_employee/employee_1.jpg" width="200px" height="250px"/>
                                                               <div style="height:10px;">&nbsp;</div>
                                                            </left>   
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Gender</label>
                                                    </div>
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        Male
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Place off Birth</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        Musi Banyuasin
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Birth Date</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        22/05/78
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Personel Nasionality</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        Indonesia
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Race (for visa purposes)</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        Master Card
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Personel Religion</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        Muslims
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Phone 1</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        0812-7882-7797
                                                    </div>
                                                </div>
                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Phone 2</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        -
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Tax ID</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        942737593
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Email</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        Sudarmono90@gmail.com
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">	
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Personal Email</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                       Sudarmono@batvianet.com
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Emergency Contact Name</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        Sumiati
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Emergency Contact Number</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        085323867450
                                                    </div>
                                                </div>
                                                <!--batas-->
                                            </div>
                                            <div class="col-md-8 col-xs-12 col-sm-12" style="padding-top:10px; padding-bottom:10px;">
                                                <div class="col-md-4 col-xs-4 col-sm-4">
                                                    <a href="<?php echo base_url('dashboard/list_all_employee'); ?>"><input type="button" class="btn btn-block btn-danger" value="Back"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="keluarga" class="tab-pane fade">
                                    <div class="chart tab-pane">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <!--Form Keluaarga-->
                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Family Card No</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        151710044 
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Father's Name</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        Firmansyah
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Mother's Name</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        Khotimah
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Number Of Siblings</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        1
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Marital Status</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        Maried
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Husband / Wife Name</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        Sumiati
                                                    </div>
                                                </div>

                                                <!--anak-->
                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-10 col-xs-12 col-sm-10 ">
                                                        <div class="box box-default collapsed-box">
                                                            <div class="box-header with-border">
                                                                <h3 class="box-title">1st Child</h3>
                                                                <div class="row" style="padding-top:5px;">
                                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                                        <label for="comment">Name</label>
                                                                    </div> 
                                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                                        Yunita Sari
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-top:5px;">
                                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                                        <label for="comment">Date Off Birth</label>
                                                                    </div> 
                                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                                        03/03/1998
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                                        <label for="comment">Gender</label>
                                                                    </div> 
                                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                                        Female
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="box-header with-border">
                                                                        <h3 class="box-title">2nd Child</h3>
                                                                        <div class="row" style="padding-top:5px;">
                                                                            <div class="col-md-4 col-xs-12 col-sm-4">
                                                                                <label for="comment">Name</label>
                                                                            </div> 
                                                                            <div class="col-md-4 col-xs-12 col-sm-4">
                                                                                Aulia Nur Rahma
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top:5px;">
                                                                            <div class="col-md-4 col-xs-12 col-sm-4">
                                                                                <label for="comment">Date Off Birth</label>
                                                                            </div> 
                                                                            <div class="col-md-4 col-xs-12 col-sm-4">
                                                                                04/01/2004
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                                            <div class="col-md-4 col-xs-12 col-sm-4">
                                                                                <label for="comment">Gender</label>
                                                                            </div> 
                                                                            <div class="col-md-4 col-xs-12 col-sm-4">
                                                                                Female
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    <div class="box-header with-border">
                                                                        <h3 class="box-title">3rd Child</h3>
                                                                        <div class="row" style="padding-top:5px;">
                                                                            <div class="col-md-4 col-xs-12 col-sm-4">
                                                                                <label for="comment">Name</label>
                                                                            </div> 
                                                                            <div class="col-md-4 col-xs-12 col-sm-4">
                                                                                M.Firdaus
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top:5px;">
                                                                            <div class="col-md-4 col-xs-12 col-sm-4">
                                                                                <label for="comment">Date Off Birth</label>
                                                                            </div> 
                                                                            <div class="col-md-4 col-xs-12 col-sm-4">
                                                                                16/09/2015
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                                            <div class="col-md-4 col-xs-12 col-sm-4">
                                                                                <label for="comment">Gender</label>
                                                                            </div> 
                                                                            <div class="col-md-4 col-xs-12 col-sm-4">
                                                                                Male
                                                                            </div>
                                                                        </div>

                                                                    </div><!-- /.box-header -->
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8 col-xs-12 col-sm-12" style="padding-top:10px; padding-bottom:10px;">
                                                <div class="col-md-4 col-xs-4 col-sm-4">
                                                    <a href="<?php echo base_url('dashboard/list_all_employee'); ?>"><input type="button" class="btn btn-block btn-danger" value="Back"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="karyawan" class="tab-pane fade">
                                    <div class="chart tab-pane">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <!--Form karyawan-->
                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Join Date</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        01/10/2015
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Personnel Assignment Type</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        -
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Personal Schedule Type</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        -
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Rational Work Days</label>
                                                    </div> 
                                                    <div class="col-md-2 col-xs-6 col-sm-3">
                                                        28:28
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Annual Leave Entitlement</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        5
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Direct Supervisor Name</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        ARBONDES
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Department</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        HRD
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Position</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        Asisten Admin II
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Employee Grade</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        A
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Company Name</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        -
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Point Off Hire</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        -
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Currency Base</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        -
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Personnel Salary Type</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        -
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Salary Currency</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        Rupiah
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Basic Salary</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        -
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Payment Method</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        -
                                                    </div>
                                                </div>


                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Bank Name</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        BCA
                                                    </div>
                                                </div>


                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Bank Code</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        21312352
                                                    </div>
                                                </div>


                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Branch Code & Bank Account No</label>
                                                    </div> 
                                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                                        -
                                                    </div>
                                                </div>
                                                <!--batas-->
                                            </div>
                                            <div class="col-md-8 col-xs-12 col-sm-12" style="padding-top:10px; padding-bottom:10px;">
                                                <div class="col-md-4 col-xs-4 col-sm-4">
                                                    <a href="<?php echo base_url('dashboard/list_all_employee'); ?>"><input type="button" class="btn btn-block btn-danger" value="Back"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <?php echo form_close() ?>
            </div>
        </div>
    </section>
</div>
