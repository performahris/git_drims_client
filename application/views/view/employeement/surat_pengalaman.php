<div class="content-wrapper" >
    <section class="content">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
					<a href="#" onclick="PrintDiv();"><i class="fa fa-print"></i> Cetak</a>
                    <div class="box-body table-responsive" id="print">
						Perihal	: Surat Keterangan Pengalaman Kerja</br>
						No	: Nama Perusahaan/SKK/Tanggal/Tahun/No Surat</br></br></br>
						<center><h3>Surat Keterangan Pengalaman Kerja</h3></center></br></br></br></br>
						Yang Bertanda Tangan dibawah ini, menerangkan bahwa :
						<p>Nama : </p>
						<p>Jabatan Terakhir : </p> 
						Adalah benar bekerja dengan jabatan tersebut di PT. Alam Bersemi Sentosa sejak .............. sampai dengan …...............
						</br>
						Selama bekerja yang bersangkutan telah menunjukan dedikasi dan loyalitas untuk perusahaan dan selama bekerja tidak pernah melakukan hal – hal yang merugikan perusahaan.</br>
						Demikian surat keterangan pengalaman kerja ini dibuat agar digunakan sebagaimana mestinya. </br></br></br>
						Jakarta,  ............................ </br></br></br></br>
						Nama HRD</br>
						Manager HRD</br></br></br></br></br>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">     
    function PrintDiv() {    
        var divToPrint = document.getElementById('print');
        var popupWin = window.open('', '_blank', 'width=600,height=600');
        popupWin.document.open();
        popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
        popupWin.document.close();
    }
</script>
