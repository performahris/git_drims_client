<script>
    $(function() {
        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
	function myFunction() {
		window.print();
	}
</script>

<style type="text/css">
    .scroll{
        width:auto;
        height:auto;
        overflow:auto;
    }
</style>

<div class="content-wrapper" >
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"></i>HR</a></li>
            <li><a href="#"></i>Employeement</a></li>
            <li><a href="#"></i>Surat Keterangan Pengalaman Kerja</a></li>
            <li class="active">View</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Employeement | Surat Keterangan Pengalaman Kerja | View</h3>
                        <hr>
                    </div>

                    <div class="box-body table-responsive">    	
                        <?php echo form_open('add/surat_keterangan_kerja_register'); ?>
                        <div class="nav-tabs-custom">

                            <br>
                            <div class="tab-content">
                                <div id="biodata" class="tab-pane fade in active">
                                    <div class="chart tab-pane active">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <!--Form Biodata-->
                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Department</label>
                                                    </div> 
                                                    <div class="col-md-8 col-xs-12 col-sm-5">
                                                        E_1 
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Division</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        EPL
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Employee Name</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-7">
                                                        SUDARMONO
                                                    </div>
                                                </div>
												
												<div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Last Position</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-7">
                                                        Manager
                                                    </div>
                                                </div>
												
												<div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Period of Employeement</label>
                                                    </div>
													<div class="col-md-1 col-xs-12 col-sm-7">
                                                        <label for="comment">From :</label>
                                                    </div>				
                                                    <div class="col-md-2 col-xs-12 col-sm-7">
                                                        26 - 05 - 2014
                                                    </div>
													 <div class="col-md-1 col-xs-12 col-sm-7">
                                                        <label for="comment">To :</label>
                                                    </div>
													<div class="col-md-2 col-xs-12 col-sm-7">
                                                        01 - 03 - 2017
                                                    </div>
                                                </div>
												
                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Identity Card Number</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        134351435143514
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Identity Card Address</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        Komp Griya Suka Bangun Blok D7 RT.97 RW.09 Desa Sukajaya Kec. Sukarami
                                                    </div>
                                                </div>
												<div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">Date Create</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        19 Juli 2016
                                                    </div>
                                                </div>
												<div class="row" style="padding-top:5px; padding-bottom:5px;">
                                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                                        <label for="comment">By</label>
                                                    </div> 
                                                    <div class="col-md-6 col-xs-12 col-sm-5">
                                                        Yek
                                                    </div>
                                                </div>
                                                <!--batas-->
                                            </div>
                                            <div class="col-md-8 col-xs-12 col-sm-12" style="padding-top:10px; padding-bottom:10px;">
                                                <div class="col-md-4 col-xs-4 col-sm-4">
                                                    <a href="<?php echo base_url('dashboard/list_surat_keterangan_pengalaman_kerja'); ?>"><input type="button" class="btn btn-block btn-danger" value="Back"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <?php echo form_close() ?>
            </div>
        </div>
    </section>
</div>
