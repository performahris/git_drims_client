<div class="content-wrapper" >
    <section class="content">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
					<a href="#" onclick="PrintDiv();"><i class="fa fa-print"></i> Cetak</a>
                    <br>
                    <a href="#" onclick="PrintDiv1();"><i class="fa fa-print"></i> Cetak to excel</a>
                    </br>
                    <div class="box-body table-responsive" id="print">
						<center><h3>Surat Keterangan Kerja</h3></center></br></br></br></br>
							Dengan ini perusahaan menyatakan bahwa yang bersangkutan :
							<p>Nama : </p>
							<p>No. KTP: </p>
							<p>Alamat KTP : </p>
							Adalah benar bahwa nama tersebut bekerja di PT. Alam Bersemi Sentosa sebagai .......... Div ......... sejak  ........ sampai dengan sekarang.
							</br>
							Demikian surat keterangan kerja ini dibuat, untuk dipergunakan sebagaimana perlunya. </br></br></br>
							Jakarta,  ............................ </br></br></br></br>
							Nama HRD</br>
							Departemen HRD</br></br></br></br></br>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">     
    function PrintDiv() {    
        var divToPrint = document.getElementById('print');
        var popupWin = window.open('', '_blank', 'width=600,height=600');
        popupWin.document.open();
        popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
        popupWin.document.close();
    }
</script>
<script type="text/javascript">     
    function PrintDiv1() {    
        var divToPrint = document.getElementById('print');
        var popupWin = window.open('', '_excel', 'width=600,height=600');
        popupWin.document.open();
        popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
        popupWin.document.close();
    }
</script>
