<link href="../../asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<div class="content-wrapper">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Personnel Schedule</li>
            <li class="active">Schedule</li>
            <li class="active">View</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Personnel Schedule | Schedule | View</h3>
                        <hr>
                    </div>
                    <div class="box-body">
                        <table class="table table-stripped table-bordered" >
                            <thead>
                            <tr align="center" class="success">
                                <th rowspan="2" class="text-center" style="vertical-align:middle;">No</th>
                                <th rowspan="2" class="text-center" style="vertical-align:middle;">Keterangan</th>                                
                                <th rowspan="2" class="text-center" style="vertical-align:middle;" colspan="2">Nama</th>
                                <th rowspan="2" class="text-center" style="vertical-align:middle;">Lokasi</th>
                                <th rowspan="2" class="text-center" style="vertical-align:middle;">Schedule</th>
                                <th colspan="31" class="text-center">Aug-15</th>
                            </tr>
                            <tr align="center" class="success">
                                <?php for($i = 1 ; $i<=31 ; $i++){ ?>
                                <th><?php echo $i; ?></th>                                
                                <?php }?>
                            </tr>
                            </thead>
                            <tbody>
                            <tr align="center">
                                <td rowspan="2" style="vertical-align:middle;">1</td>
                                <td rowspan="2" style="vertical-align:middle;">Rig Supt</td>
                                <td>1</td>
                                <td>Jakornad</td>
                                <td>Cikande</td>
                                <td>28:28</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td style="background-color: orange;">R</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                            </tr>
                            <tr align="center">
                                <td>2</td>
                                <td>Alexander Sirait</td>
                                <td>Cikande</td>
                                <td>28:28</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td style="background-color: orange;">T</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                            </tr>
                            <tr align="center">
                                <td rowspan="3" style="vertical-align:middle;">2</td>
                                <td rowspan="3" style="vertical-align:middle;">Toolpusher</td>
                                <td>1</td>
                                <td>Edy S</td>
                                <td>Cikande</td>
                                <td>28:14</td>
                                <td>C</td>
                                <td>C</td>
                                <td>C</td>
                                <td>C</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td style="background-color: orange;">R</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>    
                            </tr>
                            <tr align="center">
                                <td>2</td>
                                <td>Suryanto</td>
                                <td>Cikande</td>
                                <td>28:14</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td style="background-color: orange;">T</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                            </tr>
                            <tr align="center">
                                <td>3</td>
                                <td>Surya E</td>
                                <td>Cikande</td>
                                <td>28:14</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td style="background-color: orange;">R</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td style="background-color: orange;">T</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                            </tr>
                            <tr align="center">
                                <td rowspan="3" style="vertical-align:middle;">3</td>
                                <td rowspan="3" style="vertical-align:middle;">Driller</td>
                                <td>1</td>
                                <td>Dian Toro</td>
                                <td>Cikande</td>
                                <td>28:14</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td style="background-color: orange;">R</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>    
                            </tr>
                            <tr align="center">
                                <td>2</td>
                                <td>Samsuri</td>
                                <td>Cikande</td>
                                <td>28:14</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td style="background-color: orange;">T</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                            </tr>
                            <tr align="center">
                                <td>3</td>
                                <td>Sariyadi</td>
                                <td>Cikande</td>
                                <td>28:14</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td style="background-color: orange;">R</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td style="background-color: orange;">T</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                            </tr>
                            <tr align="center">
                                <td rowspan="2" style="vertical-align:middle;">4</td>
                                <td rowspan="2" style="vertical-align:middle;">Chief Mechanic</td>
                                <td>1</td>
                                <td>Heppy K</td>
                                <td>Cikande</td>
                                <td>28:28</td>
                                <td>C</td>
                                <td>C</td>
                                <td>C</td>
                                <td>C</td>
                                <td>C</td>
                                <td>C</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td style="background-color: orange;">T</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                            </tr>
                            <tr align="center">
                                <td>2</td>
                                <td>Deky</td>
                                <td>Cikande</td>
                                <td>28:28</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td style="background-color: orange;">R</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                            </tr>
                            <tr align="center">
                                <td rowspan="3" style="vertical-align:middle;">5</td>
                                <td rowspan="3" style="vertical-align:middle;">Mechanic</td>
                                <td>1</td>
                                <td>Baja Ardaut</td>
                                <td>Cikande</td>
                                <td>28:14</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td style="background-color: orange;">T</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                            </tr>
                            <tr align="center">
                                <td>2</td>
                                <td style="color:red">Erwin</td>
                                <td>Cikande</td>
                                <td>28:14</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td style="background-color: orange;">R</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                            </tr>
                            <tr align="center">
                                <td>3</td>
                                <td>Jerry</td>
                                <td>Cikande</td>
                                <td>28:14</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td style="background-color: orange;">R</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>W</td>
                                <td style="background-color: orange;">T</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                            </tr>
                            <tr align="center">
                                <td rowspan="2" style="vertical-align:middle;">6</td>
                                <td rowspan="2" style="vertical-align:middle;">Electric</td>
                                <td>1</td>
                                <td>Edy P</td>
                                <td>Cikande</td>
                                <td>28:28</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td style="background-color: orange;">T</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                            </tr>
                            <tr align="center">
                                <td>2</td>
                                <td>Supriono</td>
                                <td>Cikande</td>
                                <td>28:28</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td style="background-color: orange;">R</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                            </tr>
                            <tr align="center">
                                <td rowspan="2" style="vertical-align:middle;">7</td>
                                <td rowspan="2" style="vertical-align:middle;">Materialman</td>
                                <td>1</td>
                                <td>Evic Altur</td>
                                <td>Cikande</td>
                                <td>28:28</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td style="background-color: orange;">R</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                            </tr>
                            <tr align="center">
                                <td>2</td>
                                <td style="color:red">Herry S</td>
                                <td>Cikande</td>
                                <td>28:28</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td style="background-color: orange;">T</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                            </tr>
                            <tr align="center">
                                <td rowspan="2" style="vertical-align:middle;">8</td>
                                <td rowspan="2" style="vertical-align:middle;">HSE</td>
                                <td>1</td>
                                <td>Rahmat U</td>
                                <td>Cikande</td>
                                <td>28:28</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td style="background-color: orange;">R</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                            </tr>
                            <tr align="center">
                                <td>2</td>
                                <td>Nur Haryono</td>
                                <td>Cikande</td>
                                <td>28:28</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td style="background-color: orange;">T</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                            </tr>
                            <tr align="center">
                                <td rowspan="2" style="vertical-align:middle;">9</td>
                                <td rowspan="2" style="vertical-align:middle;">Truckpusher</td>
                                <td>1</td>
                                <td>Junaidi</td>
                                <td>Cikande</td>
                                <td>28:28</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td style="background-color: orange;">R</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                            </tr>
                            <tr align="center">
                                <td>2</td>
                                <td>Hanter S</td>
                                <td>Cikande</td>
                                <td>28:28</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td>O</td>
                                <td style="background-color: orange;">T</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                                <td>W</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>    
                </div>
                    <div class="col-md-5 col-xs-12 col-sm-12" style="padding-top:10px;">
                      <div class="col-md-6 col-xs-12 col-sm-12">
                          <a href="<?php echo base_url('dashboard/schedule_list'); ?>"><input type="button" class="btn btn-block btn-danger" value="Back"></a>
                      </div>
                    </div>
            </div>
        </div>
    </section>
</div>