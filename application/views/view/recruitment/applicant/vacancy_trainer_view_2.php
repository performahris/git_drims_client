<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Recruitment</li>
            <li class="active">Vacancy</li>
            <li class="active">Vacancy Trainer</li>
            <li class="active">View</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
		    <div class="box-header">
                    <h3 class="box-title">Recruitment | Vacancy | Vacancy Trainer | View</h3>
                    <hr>
		    </div>
			<div class="col-md- col-xs-12 col-sm-12" >
			      <div class="col-md-8">
				   <!--<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Search</div>-->
				   <!--<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" placeholder="Enter ..." ng-model="search"/></div>-->
			      </div>
			 </div>
                    <div class="box-body  table-responsive">
                        <table border="1" id="example1" class="table table-bordered table-striped">
                                <tr class="info">
                                    <th colspan="6">
					     <center>RIWAYAT PENDIDIKAN FORMAL/INFORMAL</center>
				    </th>
                                </tr>
			      
		        <!--DISINI-->
			
			        <tr>
				   <th>Tingkat</th>
				   <th>Nama Sekolah</th>
				   <th>Tempat/Kota</th>
				   <th>Jurusan</th>
				   <th>Tahun Lulus</th>
				   <th>Keterangan</th>
				</tr>
				<tr>
				   <td>SD</td>
				   <td>SD Sukajaya</td>
				   <td>Desa Sukajaya</td>
				   <td>JurusanSD</td>
				   <td>TahunSD</td>
				   <td>KeteranganSD</td>
				</tr>
				<tr>
				   <td>SMP</td>
				   <td>SMP Negeri 1 Sukajaya</td>
				   <td>Desa Sukajaya</td>
				   <td>JurusanSMP</td>
				   <td>TahunSMP</td>
				   <td>KeteranganSMP</td>
				</tr>
				<tr>
				   <td>SMA</td>
				   <td>SMA Negeri 1 Sukajaya</td>
				   <td>Desa Sukajaya</td>
				   <td>JurusanSMA</td>
				   <td>TahunSMA</td>
				   <td>KeteranganSMA</td>
				</tr>
				<tr>
				   <td>Akademi</td>
				   <td></td>
				   <td></td>
				   <td></td>
				   <td></td>
				   <td></td>
				</tr>
				<tr>
				   <td>Universitas</td>
				   <td></td>
				   <td></td>
				   <td></td>
				   <td></td>
				   <td></td>
				</tr>
				<tr>
				   <td></td>
				   <td></td>
				   <td></td>
				   <td></td>
				   <td></td>
				   <td></td>
				</tr>
				
				</table>
			<br>
			<table class="table table-bordered table-striped">
			       <tr >
                      	<td colspan="6" class="info">
					   		<b>Kursus/ Training<b> 
				    	</td>
                    </tr>
			        <tr>
				   <th>Bidang/Jenis</th>
				   <th>Penyelenggara</th>
				   <th>Lama Kursus</th>
				   <th>Tahun</th>
				   <th>Tahun Lulus</th>
				   <th>Dibiayai Oleh</th>
				</tr>
				<tr>
				   <td>IT Training and Certifikat</td>
				   <td>Binus Indonesia</td>
				   <td>3 Bulan</td>
				   <td>2000</td>
				   <td>2000</td>
				   <td>Pribadi</td>
				</tr>
				<tr>
				   <td></td>
				   <td></td>
				   <td></td>
				   <td></td>
				   <td></td>
				   <td></td>
				</tr>
			</table>
			<br>
			<table border="1" id="example1" class="table table-bordered table-striped">
				 <tr>
                    <td colspan="5" class="info">
					   <b>Pengetahuan Bahasa : (diisi dengan baik sekali, baik cukup )</b>
				    </td>
                 </tr>
			      
			     <tr>
				   <th>Macam Bahasa</th>
				   <th>Mendengar</th>
				   <th>Berbicara</th>
				   <th>Membaca</th>
				   <th>Menulis</th>
				  
				</tr>
				<tr>
				   <td>Indonesia</td>
				   <td>Sangat Baik</td>
				   <td>Sangat Baik</td>
				   <td>Sangat Baik</td>
				   <td>Sangat Baik</td>
				</tr>
				<tr>
				   <td>Inggris</td>
				   <td>Baik</td>
				   <td>Baik</td>
				   <td>Baik</td>
				   <td>Baik</td>
				</tr>
				
                        </table>
		<br>
			<table border="1" id="example1" class="table table-bordered table-striped">
			       <tr>
                    	<td colspan="4" class="info">
					  		<b>Kegiatan Sosial</b>
				    	</td>
                    </tr>
			        <tr>
				   <th>Organisasi</th>
				   <th>Macam Kegiatan</th>
				   <th>Jabatan</th>
				   <th>Tahun</th>
				</tr>
				<tr>
				   <td>IT Center</td>
				   <td>Forum sharing</td>
				   <td>Ketua</td>
				   <td>2004</td>
				</tr>
				<tr>
				   <td></td>
				   <td></td>
				   <td></td>
				   <td></td>
				</tr>
			      </table>
			
			<br>
			<table border="1" id="example1" class="table table-bordered table-striped">
			       <tr>
                      	<td colspan="2" class="info">
					 		<b>Hobi dan kegiatan di waktu luang :</b>
				    	</td>
                   </tr>
                   <br>
			        
				<tr>
				   <td colspan="2">Mempelajari Ilmu IT ketika ada waktu luang/ waktu senggang</td>
				  
				</tr>
				<tr>
				   <td style="width:50%;">Anda Membaca:
				   <br>
				   <div style="margin-left:80px;">
				   <br><input type="radio" name="baca1" class="minimal" checked> Banyak
				   <br><input type="radio" name="baca2" class="minimal" > Sedang
				   <br><input type="radio" name="baca3" class="minimal" > Sedikit
				   <div>
				   </td>
				   <td>
					Pokok - pokok yang dibaca:
					<br>
					<br>

					
					Saya membaca tentang suatu hal yang harus di lakukan seorang IT dan prosedur kerja IT
				   </td>	
					
				</tr>
			        <tr>
                        <td colspan="2" >
					<div class="col-sm-12">
					     Anda biasa membaca surat kabar/majalah apa saja? <br/> <br/>
					</div>
					<div class="col-sm-2">
					     Surat Kabar : 
					</div>
					<div class="col-sm-10">
					     KOMPAS
					     <div style="height:10px;">&nbsp; </div>
					</div>
					<div class="col-sm-2">
					     Majalah :
					</div>
					<div class="col-sm-10">
					     Majalah ICT
					</div>
					
				    </td>
                                </tr>
			      </table>
		        
			<br/>
			<div class="col-sm-12" style="text-align:right;">
			 Page 2 of 5
			<div style="height:10px;">&nbsp; </div>
			</div>

		    <div class="box-footer" style="border-top:none;">
			 	<a href="<?php echo base_url('view/vacancy_trainer'); ?>"><button type="submit" class="btn btn-danger">Cancel</button></a>
			  	<a href="vacancy_trainer_view_3" class="btn btn-primary pull-right" >Next</a>
			  	<a href="vacancy_trainer_view_1" class="btn btn-primary pull-right" style="margin-right:5px;">Back</a>&nbsp;
				
		    </div>
		
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
