<meta charset="utf-8">
  <title>jQuery UI Datepicker - Default functionality</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
  $(function() {
    $( "#datepickering" ).datepicker({dateFormat:'dd-mm-yy', changeMonth: true, changeYear: true});
  });
  $(function() {
    $( "#datepickering1" ).datepicker({dateFormat:'dd-mm-yy', changeMonth: true, changeYear: true});
  });
  </script>


<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>

<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Recruitment</li>
            <li class="active">Vacancy</li>
            <li class="active">Vacancy Trainer</li>
            <li class="active">View</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class=" col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
					<div class="box-header">
                        <h3 class="box-title">Recruitment | Vacancy | Vacancy Trainer | View</h3>
                        <hr>
                    </div>
						<div class="col-md-12 col-xs-12 col-sm-12">
							<center>
								<b><h4>RIWAYAT PEKERJAAN</h4></b>
								<H5>Dimulai Dengan Pekerjaan Sekarang Dan Diisi Lengkap</H5>
							</center>
							<!--<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Search</div>-->
							<!--<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" placeholder="Enter ..." ng-model="search"/></div>-->
						</div>
                    <div class="box-body  table-responsive">
                    		<table class="table table-bordered">
    							<tbody>
      								<tr>
        								<td> 
        									<div class="col-md-6 col-xs-12 col-sm-12">
        										<h5><label>Masuk : tgl.</label></h5> 
											</div>
											<div class="col-md-6 col-xs-12 col-sm-12">
                        <div style="margin-top:7px">20 Feb 2005</div>
											</div>
        								</td>
        								<td>
        									<div class="col-md-6 col-xs-12 col-sm-12">
												<h5><label>Nama/ALamat/Telepon Perusahaan :</label></h5>
											</div>
											<div class="col-md-6 col-xs-12 col-sm-12">
												<div style="margin-top:7px">Batavianet/Mutiara Palm No.10A/22564</div>
											</div>
        								</td>
      								</tr>
      								<tr>
        								<td> 
        									<div class="col-md-6 col-xs-12 col-sm-12">
        										<h5><label>Keluar : tgl.</label></h5> 
											</div>
											<div class="col-md-6 col-xs-12 col-sm-12">
                          <div style="margin-top:7px">31 Mar 2010</div>
											</div>
        								</td>
        								<td rowspan="3">
        									<div class="col-md-12 col-xs-12 col-sm-12">
												<h5><label>Jenis Usaha :</label></h5>
												- Web Development
                    <br>- Android IOS Development
                    <br>- Desktop Development
											</div>
        								</td>
      								</tr>
      								<tr>
        								<td> 
        									<div class="col-md-6 col-xs-12 col-sm-12">
        										<h5><label>Jabatan Awal :</label></h5> 
											</div>
											<div class="col-md-6 col-xs-12 col-sm-12">
                          <div style="margin-top:7px">Karyawan</div>
											</div>
        								</td>
        							</tr>
        							<tr>
        								<td> 
        									<div class="col-md-6 col-xs-12 col-sm-12">
        										<h5><label>Jabatan Akhir :</label></h5> 
											</div>
											<div class="col-md-6 col-xs-12 col-sm-12">
                            <div style="margin-top:7px">Supervisor</div>
											</div>
        								</td>
        							</tr>
        							<tr>
        								<td> 
        									<div class="col-md-6 col-xs-12 col-sm-12">
        										<h5><label>Nama Atasan Langsung :</label></h5> 
											</div>
											<div class="col-md-6 col-xs-12 col-sm-12">
                          <div style="margin-top:7px">Pratiwi</div>
											</div>
										</td>
										<td>
											<div class="col-md-5 col-xs-12 col-sm-12">
        										<h5><label>Nama Direktur</label></h5> 
											</div>
											<div class="col-md-7 col-xs-12 col-sm-12">
                           <div style="margin-top:7px">William</div>
											</div>
        								</td>
        							</tr>
        							<tr>
        								<td > 
        									<div class="col-md-6 col-xs-12 col-sm-12">
        										<h5><label>Alasan Berhenti : </label></h5> 
											</div>
											<div class="col-md-6 col-xs-12 col-sm-12">
                          <div style="margin-top:7px">Ingin kantor yang dekat dengan keluarga</div>
											</div>
										</td>
										<td>
											<div class="col-md-5 col-xs-12 col-sm-12">
        										<h5><label>Gaji Terakhir :</label></h5> 
											</div>
											<div class="col-md-7 col-xs-12 col-sm-12">
                            <div style="margin-top:7px">2,000,000</div>
											</div>
        								</td>
        							</tr>
        							<tr style="background:none;">
			 							<td colspan="2" style="text-align:right;"><a href="#" class="glyphicon glyphicon-plus"></a></td>
		    						</tr>
   								</tbody>
  							</table>
                        </div>
                        

                <div class="box-body  table-responsive">
                    		<table class="table table-bordered">
    							<thead>
      								<tr class="info">
        								<td colspan="5"> 
        									<label for="comment">Referensi (Kepada siapa Kami menanyakan diri anda lebih lengkap)</label>	
        								</td>
      								</tr>
      							</thead>
      							<thead>
      								<tr>
        								  <th><center>NAMA</center></th>
  								        <th><center>ALAMAT</center></th>
  								      	<th><center>TELP</center></th>
  								      	<th><center>HUBUNGAN</center></th>
  								      	<th><center>PEKERJAAN</center></th>
    							  	</tr>
   								</thead>
    							<tbody>
    								<tr>
        								<th style="font-weight:normal">INDRIANI</th>
  								      <th style="font-weight:normal">Jl Kartowinagun RT.25 RW 09 Desa Talang Betutu Kec Sukarami</th>
  								      <th style="font-weight:normal">0821-7705-5865</th>
  								      <th style="font-weight:normal">Ibu</th>
  								      <th style="font-weight:normal">HRD</th>
    							  </tr>
    							  <tr>
        								<th style="font-weight:normal">YUNITA SARI</th>
  								      <th style="font-weight:normal">Jl. M. Aguscik Bougenvile 047 RT.43 RW.06 Desa Karya Baru Kec .Sukarami</th>
  								      <th style="font-weight:normal">0852-6817-6241</th>
  								      <th style="font-weight:normal">Ibu Mertua</th>
  								      <th style="font-weight:normal">Supervisor</th>
    							  </tr>
   								</tbody>
  							</table>
                        </div>


                    <div class="box-body  table-responsive">
                    		<table class="table table-bordered">
    							<thead>
      								<tr class="info">
        								<td colspan="5"> 
        									<label for="comment">Orang yang Dihubungi Segera dalam keadaan Mendesak/Darurat</label>	
        								</td>
      								</tr>
      							</thead>
      							<thead>
      								<tr>
        								  <th><center>NAMA</center></th>
  								      	<th><center>AlAMAT</center></th>
  								      	<th><center>TELP</center></th>
  								      	<th><center>HUBUNGAN</center></th>
  								      	<th><center>PEKERJAAN</center></th>
    							  	</tr>
   								</thead>
    							<tbody>
    								<tr>
        							<tr>
                        <th style="font-weight:normal">INDRIANI</th>
                        <th style="font-weight:normal">Jl Kartowinagun RT.25 RW 09 Desa Talang Betutu Kec Sukarami</th>
                        <th style="font-weight:normal">0821-7705-5865</th>
                        <th style="font-weight:normal">Ibu</th>
                        <th style="font-weight:normal">HRD</th>
                    </tr>
                    <tr>
                        <th style="font-weight:normal">YUNITA SARI</th>
                        <th style="font-weight:normal">Jl. M. Aguscik Bougenvile 047 RT.43 RW.06 Desa Karya Baru Kec .Sukarami</th>
                        <th style="font-weight:normal">0852-6817-6241</th>
                        <th style="font-weight:normal">Ibu Mertua</th>
                        <th style="font-weight:normal">Supervisor</th>
                    </tr>
   								</tbody>
  							</table>
                        </div>

                        <div class="box-body  table-responsive">
                    		<table class="table table-bordered">
    							<thead>
      								<tr>
        								  <th><center>NO NPWP</center></th>
  								      	<th><center>NO BPJS KETENAGAKERJAAN</center></th>
  								      	<th><center>NO REKENING</center></th>
    							  	</tr>
   								</thead>
    							<tbody>
    								<tr>
        								  <th style="font-weight:normal">54.787.951.0-314.000</th>
  								      	<th style="font-weight:normal">-</th>
  								      	<th style="font-weight:normal">1001-01-003745-50-9</th>
    							  	</tr>
   								</tbody>
  							</table>
                        </div>

                        <div class="box-body  table-responsive">
                    		<table class="table table-bordered">
    							<thead>
      								<tr>
        								<td> 
        									<label for="comment">Tanggal MCU Terakhir :</label>	
        								</td>
        								<td> 
        									12 November 2009	
        								</td>
      								</tr>
      							</thead>
   							</table>
                        </div>

		 				<div class="col-sm-12" style="text-align:right;">
			 Page 3 of 5
			<div style="height:10px;">&nbsp; </div>
			</div>

		    <div class="box-footer" style="border-top:none;">
			     <a href="<?php echo base_url('view/vacancy_trainer'); ?>"><button type="submit" class="btn btn-danger">Cancel</button></a>
			     <a href="vacancy_trainer_view_4" class="btn btn-primary pull-right" >Next</a>
			     <a href="vacancy_trainer_view_2" class="btn btn-primary pull-right" style="margin-right:5px;">Back</a>&nbsp;
        </div>
		
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
