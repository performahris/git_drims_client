<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
        	<li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Recruitment</li>
            <li class="active">Vacancy</li>
            <li class="active">Vacancy Trainer</li>
            <li class="active">View</li>
            <!--<li class="active">Applicant Identity</li>-->
        </ol>
    </section>
    <section class="content">
        <div class="row">

            <div class=" col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
		    <div class="box-header">
                    <h3 class="box-title">Recruitment | Vacancy | Vacancy Trainer | View</h3>
                    <hr>
		    </div>
			<div class="col-md- col-xs-12 col-sm-12" >
			      <div class="col-md-8">
				   <!--<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Search</div>-->
				   <!--<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" placeholder="Enter ..." ng-model="search"/></div>-->
			      </div>
			 </div>
                    <div class="box-body  table-responsive">
                        <table border="1" id="example1" class="table table-bordered table-striped ">
                                <tr class="info">
                                    <th colspan="6">
									<center>DATA PRIBADI PELAMAR</center>
									</th>
                                </tr>
					
	<!--DISINI-->
	<tr>
	<td colspan="4" style="width:80%; margin-bottom:0px; padding-bottom:0px;"><form class="form-horizontal">
              
                <div class="form-group">
                  <border><label for="inputPekerjaan" class="col-sm-3 control-label">Pekerjaan yang dilamar</label></border>

                  <div class="col-sm-8">
                  		<div style="margin-top:7px">Programer</div>  
                  </div>
                </div>
				
                <div class="form-group">
                  <label for="inputNama" class="col-sm-3 control-label">Nama Lengkap / Nama Kecil</label>

                  <div class="col-sm-8">
                    	<div style="margin-top:7px">Sudarmono</div>
                  </div>
                </div>
             
			  
	         <div class="form-group">
                  <label for="inputAlamat" class="col-sm-3 control-label">Alamat di dalam Kota</label>

                  <div class="col-sm-8">
                    <div style="margin-top:7px">Komp Griya Suka Bangun Blok D7 RT.97 RW.09 Desa Sukajaya Kec. Sukarami</div>
		    <div style="height:10px;">&nbsp; </div>
                  </div>
				  
				  <label for="inputTelp" class="col-sm-5 control-label">Telp :</label>

                  <div class="col-sm-4">
                    <div style="margin-top:7px">0812-7882-7797</div>
                  </div>
				  
                </div>
              </div>
			  
	       <div class="form-group">
                  <label for="inputAlamat" class="col-sm-3 control-label">Alamat di luar kota</label>
					
                  <div class="col-sm-8">
                    <div style="margin-top:7px">-</div>
		    <div style="height:10px;">&nbsp; </div>
                  </div>
				  <label for="inputTelp" class="col-sm-5 control-label">Telp :</label>

                  <div class="col-sm-4">
                    <div style="margin-top:7px">-</div>
                  </div>
				  
                </div>
              </div>
              <!-- /.box-body -->
			  <!--<div class="col-md-6 col-xs-12 col-sm-12" style="padding-top:10px;">
                        <div class="col-md-4 col-xs-12 col-sm-12">
                        <a href="#"><input type="submit" class="btn btn-info btn-success" value="Save"></a>
                      </div>
                      <div class="col-md-4 col-xs-12 col-sm-12">
                        <a href="#"><input type="button" class="btn btn-block btn-warning" value="Save As Draft"></a>
                      </div>
                      <div class="col-md-4 col-xs-12 col-sm-12">
                        <a href="#"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
                      </div>
                  </div>-->
              <!-- /.box-footer -->
			  
			  <div class="">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
            <td class="col-sm-12" style="width:12%">
					<label>
					<input type="radio" name="r3" class="minimal" checked disabled> Pria
					</label>
					<br>
					<label>
					<input type="radio" name="r3" class="minimal" disabled> Wanita
					</label>
			</td>
			
            <td>
				  
              <div class="">
                <center><div class="col-sm-12">
                  <label for="tinggi1">Tinggi</label>
                  	<div>170</div>
				</div></center>
			  </div>
			  
			</td>
				  
            <td>
			
              <div class="">
                <center>
	        <div class="col-sm-12">
                  <label for="beratbadan1">Berat Badan</label>
                  <div>60</div>
	        </div>
		</center>
	       </div>
	      
	      </td>
			
            <td>
			
			<div class="">
                <center><div class="col-sm-12">
                  <label for="agama1">Agama</label>
                  	<div>Islam</div>
				</div></center>
			  </div>
			
			</td>
			
            <td>
			
			<div class="">
                <center><div class="col-sm-12">
                  <label for="bangsa1">Kebangsaan</label>
                  	<div>Indonesia</div>
				</div></center>
			  </div>
			
			</td>
                </tr>
                </thead>
                </table>
            </div>
			  
            </form>
			<!--BATAS-->
		</td>
	<!--PHOTO-->	
	<td colspan="2" >
            <div class="">
	       <center>	
	       <!--<img src="views/core/1.jpeg">-->
	       <div style="border:grey solid; width:200px; height:250px;">
	       		<img src="<?php echo base_url(); ?>upload/foto_employee/employee_1.jpg" width="200px" height="250px"/>
	       </div>
	    </center>	
	    </div>
	</td>
	
	</tr>
	<tr>
	<td colspan="2" style="width:30%">
	
			<div class="">
                <div class="col-sm-12">
                  <center><label for="bangsa1">LAHIR</label></center>
				</div>
			</div>
	
	</td>
	
	<td colspan="2" style="width:30%">
                  <center><label for="bangsa1">STATUS PERKAWINAN</label></center>
	</td>
	
	<td colspan="1" style="width:8%">
	  <div>
                <div class="col-sm-10">
                  <center><label for="bangsa1">No.KTP</label></center>
	        </div>
	  </div>
	</td>
	<td colspan="1" style="width:20%">
	
	<div class="">
                <center><div class="col-sm-12">
                 	1606072205780001
				</div></center>
			  </div>
	
	</td>
	<!--BATAS BAWAH-->
	
	</tr>
	<tr>
	<td colspan="1" style="width:10%">
	
			<div class="">
                <center><div class="col-sm-12">
                  <label for="bangsa1">Tempat</label>
                   <div>Musi Banyuasin</div> 
				</div></center>
			</div>
	
	</td>
	
	<td colspan="1" style="width:12%">
	
	<div class="">
        <center>
	    <div class="col-sm-11">
        	<label for="bangsa1">Tgl/Bln/Thn</label>
            <div>
            22-May-78
		</div>
		</center>
	</div>
	
	</td>
	<td colspan="1" style="width:15%">

		<label>
			<input type="radio" name="r1" class="minimal" checked disabled>Kawin
		</label>
				<br>
		<label>
			<input type="radio" name="r1" class="minimal" disabled>Belum Kawin
		</label>
	
	</td>

	<td class="col-sm-12"  style="width:15%" >
	
		<label>
			<input type="radio" name="r1" class="minimal" disabled>Janda/Duda
		</label>
				<br>
		<label>
			<input type="radio" name="r1" class="minimal" disabled>Cerai
		</label>
	
	</td>
	
	<td  style="width:8%"> 
	
			<div class="">
                <center><div class="col-sm-10">
                  <label for="bangsa1">No.SIM</label>
				</div></center>
			</div>
	
	</td>
	<td colspan="1" style="width:10%">
	
			<div class="form-group">
                  <div class="col-md-12">
                  151710072
                  </div>
            </div>
	
	</td>
	</tr>
	
	<tr>
	<td colspan="2" style="width:50%">
	  <div class="">
                <div class="col-sm-"><center>
                  <label for="bangsa1">RUMAH TEMPAT TINGGAL</label></center>
	        </div>
	  </div>
	</td>
	
        <td colspan="4" style="width:50%">
	  <div class="">
                <div class="col-sm-12">
                  <center>
		    <label for="bangsa1">KENDARAAN</label>
		  </center>
	        </div>
	  </div>
	</td>
	</tr>
	
	<tr>
		<td rowspan="2" style="width:25%">
			<label>
				<input type="radio" name="r5" class="minimal" checked disabled> Milik Sendiri
			</label>
				<br>
			<label>
				<input type="radio" name="r5" class="minimal" disabled> Milik Orang Tua
			</label>
			<br>
			<label>
				<input type="radio" name="r5" class="minimal" disabled> Lain-lain
			</label>
		</td>
	
		<td rowspan="2" style="width:25%">
			
			<label>
				<input type="radio" name="r5" class="minimal" disabled> Sewa/Kontrak
			</label>
				<br>
			<label>
				<input type="radio" name="r5" class="minimal" disabled> Indekost
			</label>
			
		</td>
			
		<td colspan="4" style="width:50%">
			<label for="inputkendaraan3" class="col-sm-4 control-label">Jenis/Merk/Tahun:</label>
            <div class="col-sm-8">
            	Motor, Yamaha Mio, 2009
			</div>
		</td>
			
	</tr>
	<tr style="background:none;">
	  <td style="vertical-align:middle; text-align:center;"><b>Milik</b></td>
	  <td>
	      
			<label>
				<input type="radio" name="r6" class="minimal" checked disabled> Sendiri
			</label>
				<br>
			<label>
				<input type="radio" name="r6" class="minimal" disabled> Orang Tua
			</label>
			      <br>
		       
			
	       
	  </td>
	   <td colspan="2">
	      
		
		        <label>
				<input type="radio" name="r6" class="minimal" disabled> Kantor
			</label>
				<br>
			<label>
				<input type="radio" name="r6" class="minimal" disabled> Lain-lain
			</label>
			
	       
	  </td>
	</tr>
	
	<!--DISINI-->
                        </table>
			<br/>
		 <table id="example2" class="table table-bordered table-striped">
		 <tr>
		    <th colspan="7" class="info"><center>Susunan Keluarga (termasuk diri saudara sendiri)</center></th>
		 </tr>
		 <tr>
		    <th rowspan="2" style="vertical-align:middle"><center>Hubungan Keluarga</center></th>
		    <th rowspan="2" style="vertical-align:middle"><center>Nama</center></th>
		    <th rowspan="2" style="vertical-align:middle"><center>L/P</center></th>
		    <th rowspan="2" style="vertical-align:middle"><center>Usia</center></th>
		    <th rowspan="2" style="vertical-align:middle"><center>Pendidikan Terakhir</center></th>
		    <th colspan="2" style="vertical-align:middle"><center>Pekerjaan Terakhir</center></th>
		   
		 </tr>
		 <tr style="background:none !important; ">
		    <th><center>Jabatan</center></th>
		    <th><center>Perusahaan</center></th>
		 </tr>
		 
		 <tr>
		    <td>Ayah</td>
		    <td>ZERI SULAIMAN</td>
		    <td>Laki-laki</td>
		    <td>65</td>
		    <td>SLTA Sederajat</td>
		    <td>Manager</td>
		    <td>PT. Tebak Saja</td>
		 </tr>
		  <tr>
		    <td>Ibu</td>
		    <td>INDRIANI</td>
		    <td>Perempuan</td>
		    <td>60</td>
		    <td>SLTA Sederajat</td>
		    <td>HRD</td>
		    <td>PT. Tak Ada</td>
		
		 </tr>
		   <tr>
		    <td>Ayah Mertua</td>
		    <td>TAUFIK</td>
		    <td>Laki-laki</td>
		    <td>65</td>
		    <td>S2 Hukum</td>
		    <td>Manager</td>
		    <td>PT. Tebak Saja</td>
		 </tr>
		    <tr>
		    <td>Ibu Mertua</td>
		    <td>YUNITA SARI</td>
		    <td>Perempuan</td>
		    <td>60</td>
		    <td>SLTA Sederajat</td>
		    <td>Supervisor</td>
		    <td>PT. Tak Ada</td>
		 </tr>
		    <tr>
		    <td>Saudara 1</td>
		    <td>CHAIRUL AZWAN</td>
		    <td>Laki-Laki</td>
		    <td>28</td>
		    <td>S1</td>
		    <td>Guru</td>
		    <td>SMK Manjur Jakarta</td>
		
		 </tr>
		    <tr>
		    <td>Saudara 2</td>
		   	<td>YUZERI</td>
		    <td>Laki-laki</td>
		    <td>35</td>
		    <td>S1</td>
		    <td>Staff</td>
		    <td>PT. Yangmana</td>
		 
		 </tr>
		    
		    <tr style="background:none;">
			 <td colspan="7" style="text-align:right;"><a href="#" class="glyphicon glyphicon-plus"></a></td>
		    </tr>
		    <tr>
		    <tr>
			<td>Istri/Suami</td>
		    <td>Sumiati</td>
		    <td>Perempuan</td>
		 	<td>40</td>
		    <td>D3</td>
		    <td>Staff</td>
		    <td>PT. Takada</td>
		    </tr>
		    
		   
		    <td>Anak 1</td>
		    <td>Aulia Nur Rahma</td>
		    <td>Perempuan</td>
		    <td>20</td>
		    <td>D1</td>
		    <td>Staff</td>
		    <td>PT. Manada</td>
		 </tr>
		    <tr>
		    <td>Anak 2</td>
		    <td>Azizah Puspita</td>
		    <td>Perempuan</td>
		    <td>5</td>
		    <td>-</td>
		    <td>-</td>
		    <td>-</td>
		 </tr>
		      <tr style="background:none;">
			 <td colspan="7" style="text-align:right;"><a href="#" class="glyphicon glyphicon-plus"></a></td>
		    </tr>
		 </table>
		 
		
			<br/>
			<div class="col-sm-12" style="text-align:right;">
			 Page 1 of 5
			<div style="height:10px;">&nbsp; </div>
			</div>

		    <div class="box-footer" style="border-top:none;">
                <a href="<?php echo base_url('view/vacancy_trainer'); ?>"><button type="submit" class="btn btn-danger">Cancel</button></a>
                <a href="vacancy_trainer_view_2" class="btn btn-primary pull-right">Next</a>
         	</div>
		
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
