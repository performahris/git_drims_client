<script>
    $(function() {
        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Man Power Request</li>
            <li class="active">Requested</li>
            <li class="active">View</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Man Power Request | Requested | View</h3>
                        <hr>
                    
                        <div class="col-md-6 col-xs-12 col-sm-12" >
                            <div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
                                <div class="col-md-4 col-xs-12 col-sm-12 pull-left"><label for="comment">Date Request</label></div>
                                <div class="col-md-8 col-xs-12 col-sm-12 ">11 Oktober 2015</div>
                            </div>
                            <div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
                                <div class="col-md-4 col-xs-12 col-sm-12 pull-left"><label for="comment">Tittle Needed</label></div>
                                <div class="col-md-8 col-xs-12 col-sm-12 ">Staff</div>
                            </div>
                            <div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
                                <div class="col-md-4 col-xs-12 col-sm-12 pull-left"><label for="comment">Qty</label></div>
                                <div class="col-md-8 col-xs-12 col-sm-12 ">10</div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-12" >
                            <div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
                                <div class="col-md-4 col-xs-12 col-sm-12 pull-left"><label for="comment">Departement</label></div>
                                <div class="col-md-8 col-xs-12 col-sm-12 ">Keuangan</div>
                            </div>
                            <div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
                                <div class="col-md-4 col-xs-12 col-sm-12 pull-left"><label for="comment">Location</label></div>
                                <div class="col-md-8 col-xs-12 col-sm-12 ">Ramba</div>
                            </div>
                            <div class="col-md-12" style="margin-top:5px;margin-bottom:5px;">
                                <div class="col-md-4 col-xs-12 col-sm-12 pull-left"><label for="comment">Direct Supervisior</label></div>
                                <div class="col-md-8 col-xs-12 col-sm-12 ">BUDI YUWONO</div>
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        <div class="box-header">
                            <h4><b>ALASAN UNTUK MEREKRUT</b></h4>
                            Penggantian (Replacement)
                        </div>

                        <div class="box-header" >
                            <h4><b>PERTIMBANGAN UNTUK MEREKRUT</b></h4>
                            <b>(Jelaskan mengapa posisi tersebut perlu ditambah dan bagaimana posisi tersebut memberikan kontribusi dalam meningkatkan produktivity / profitability)</b>
                        </div>
                        <div class="box-body" >
                            Mencari karyawan yang siap dan mampu bekerjasama
                        </div>
                        <div class="box-header" >
                            <h4><b>RINGKASAN DAN TANGGUNGJAWAB PEKERJAAN</b></h4>
                        </div>
                        <div class="box-body" >
                            Bagian ini akan mengecek semua administrasi dan transaksi berhubungan dengan jalannya perusahaan.
                        </div>
                        <div class="box-header" >
                            <h4><b>GAMBARKAN STRUKTUR ORGANISASI TERAKHIR DEPARTEMEN TERSEBUT DAN POSISI CALON KARYAWAN</b></h4>
                        </div>
                        <div class="box-body" >
                    &nbsp;&nbsp;- CMT bertugas untuk mengurus hal hal berkaitan dengan pihak Outsourcing.
                <br>&nbsp;&nbsp;- Accounting bertugas untuk melakukan membukukan transaksi yang terjadi.
                <br>&nbsp;&nbsp;- Kasir bertugas untuk membuat laporan penerimaan dan pengeluaran uang harian.
                        </div>
                        <div>&nbsp;</div>
                        <div class="box-header">
                            <h4><b>JENIS REKRUTMEN</b></h4>
                        </div>
                        <div class="table-responsive">
                            <table id="example1" class="table table-striped">
                                <tr class="success">
                                    <th style="padding-left:30px;">
                                        Kandidat Dari Luar
                                    </th>
                                    <th style="padding-left:30px;" >
                                        Periode
                                    </th>
                                </tr>
                                <tr>
                                    <td style="padding-left:30px;">Kontrak</td>
                                    <td style="padding-left:30px;">2015-2016</td>
                                </tr>
                            </table>
                        </div>
                        <div class="box-header" >
                            <h4><b>Spesifikasi Jabatan</b></h4>
                        </div>
                        <div class="box-body" >
                            <div class="col-md-6 col-xs-12 col-sm-12" >
                                <div class="col-md-6" style="margin-top:5px;margin-bottom:5px;">
                                    <label for="comment">Jenis Kelamin</label>
                                </div>
                                <div class="col-md-6" style="margin-top:5px;margin-bottom:5px;">
                                    Wanita
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-12" >
                                <div class="col-md-6" style="margin-top:5px;margin-bottom:5px;">
                                    <label for="comment">Pengalaman Yang Dibutuhkan</label>
                                </div>
                                <div class="col-md-6" style="margin-top:5px;margin-bottom:5px;">
                                    Mampu menggunakan MS.Word, MS.Excel, MS.Power Point
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-12" >
                                <div class="col-md-6" style="margin-top:5px;margin-bottom:5px;">
                                    <label for="comment">Penidikan</label>
                                </div>
                                <div class="col-md-6" style="margin-top:5px;margin-bottom:5px;">
                                    D3
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-12" >
                                <div class="col-md-6" style="margin-top:5px;margin-bottom:5px;">
                                    <label for="comment">Usia</label>
                                </div>
                                <div class="col-md-6" style="margin-top:5px;margin-bottom:5px;">
                                    23 Tahun
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-12" >
                                <div class="col-md-6" style="margin-top:5px;margin-bottom:5px;">
                                    <label for="comment">Keahlian Yang Dibutuhkan</label>
                                </div>
                                <div class="col-md-6" style="margin-top:5px;margin-bottom:5px;">
                                    Mengetahui akan hitungan, aritmatik, aljabar, geometri, kalkulus , serta statistik
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-12" >
                                <div class="col-md-6" style="margin-top:5px;margin-bottom:5px;">
                                    <label for="comment">Kondisi Lingkungan Kerja</label>
                                </div>
                                <div class="col-md-6" style="margin-top:5px;margin-bottom:5px;">								
                                    Layak
                                </div>
                            </div>
                        </div>

                        <div class="box-header" >
                            <h4><b>Didalam dan diluar kantor</b></h4>
                        </div>
                        <div class="box-body">
                            <div class="col-md-5 col-xs-6 col-sm-6" >
                                <div class="col-md-6" style="margin-top:5px;margin-bottom:5px;">
                                    <label for="comment">Lain Lain</label>
                                </div>
                                <div class="col-md-6" style="margin-top:5px;margin-bottom:5px;">
                                    Lorem Ipsum
                                </div>
                            </div>
                        </div>
                        <div class="box-body" >
                            <div class="col-md-5 col-xs-12 col-sm-12" >
                                <div class="col-md-6" style="margin-top:5px;margin-bottom:5px;">
                                    <label for="comment">Kandidat Yang Diusulkan</label>
                                </div>
                                <div class="col-md-6" style="margin-top:5px;margin-bottom:5px;">
                                    YUNITA SARI
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12 col-sm-12" >
                                <div class="col-md-1" style="margin-top:5px;margin-bottom:5px;">
                                    <label for="comment">PT</label>
                                </div>
                                <div class="col-md-10" style="margin-top:5px;margin-bottom:5px;">
                                    Batavianet
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12 col-sm-12" >
                                <div class="col-md-5" style="margin-top:5px;margin-bottom:5px;">
                                    <label for="comment">Departement</label>
                                </div>
                                <div class="col-md-7" style="margin-top:5px;margin-bottom:5px;">
                                    Keuangan
                                </div>
                            </div>
                        </div>


                        <div class="col-md-2" col-xs-12 col-sm-2 style="margin-top:5px;margin-bottom:5px;">
                            <label for="comment">Remarks</label>
                        </div>
                        <div class="col-md-10" col-xs-12 col-sm-10 style="margin-top:5px;margin-bottom:5px;">
                            <textarea class="form-control" rows="5" id="comment"></textarea>
                        </div>

                        <div class="col-md-12 col-xs-12 col-sm-12">
                            <div class="col-md-5 col-xs-12 col-sm-12" style="margin-top:5px;margin-bottom:5px;">
                            <label for="comment">Activity Log</label>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr class="info">
                                            <th>No</th>
                                            <th>Activity</th>
                                            <th>Date & Time</th>
                                            <th>By</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Create Policy</td>
                                            <td>20 mei 2015 11:00:00</td>
                                            <td>Dimas</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Submit Policy</td>
                                            <td>22 mei 2015 09:05:37</td>
                                            <td>Dimas</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Review Policy</td>
                                            <td>22 mei 2015 10:00:01</td>
                                            <td>Adi Bagus</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Approve Policy</td>
                                            <td>23 mei 2015 23:01:57</td>
                                            <td>Andy Rikie Lam</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-7 col-xs-12 col-sm-12" style="padding-top:10px;">
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <a href="<?php echo base_url(); ?>dashboard/man_power_requested"><input type="button" class="btn btn-block btn-danger" value="Back"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>
</div>

