<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li><a href="#">Man Power Request</a></li>
            <li><a href="#">Request</a></li>
            <li class="active">Employee On Man PowerRequest</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">

            <div class=" col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Man Power Request | Request | Employee On Man PowerRequest</h3>
                        <hr>
                    </div>


					 <div class="box-body"> 
                        <!-- BEGIN OF SEARCH -->
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-8 col-md-8">
                                    <label for="searchby" class="col-md-12">Search By :</label>
                                </div>
                            </div>
                 </div> 
                <div class="col-md-12">                
                    <div class="col-xs-12 col-sm-8 col-md-8">
                        <div class="row form-group">
                            <div class="col-xs-12 col-sm-5 col-md-5">
                                <select class="form-control">
                                    <option>-- Select Field Name --</option>
                                    <option>Employee Code</option>
                                    <option>Employee Name</option>
                                    <option>Ongoing Poject</option>
                                    <option>Next Project</option>
                                    <option>Status</option>
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-2 col-md-1">
                                <label for="code">Value</label>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <input type="text" class="form-control" name=""/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-12 col-sm-5 col-md-5">
                                <select class="form-control">
                                    <option>-- Select Field Name --</option>
                                    <option>Employee Code</option>
                                    <option>Employee Name</option>
                                    <option>Ongoing Poject</option>
                                    <option>Next Project</option>
                                    <option>Status</option>
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-1 col-md-1">
                                <label for="code">Value</label>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <input type="text" class="form-control" name=""/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-12 col-sm-5 col-md-5">
                                <select class="form-control">
                                    <option>-- Select Field Name --</option>
                                    <option>Employee Code</option>
                                    <option>Employee Name</option>
                                    <option>Ongoing Poject</option>
                                    <option>Next Project</option>
                                    <option>Status</option>
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-1 col-md-1">
                                <label for="code">Value</label>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <input type="text" class="form-control" name=""/>
                            </div>
                        </div>
                        <div class="row form-group">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <label for="operator">Operator</label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <input type="radio" name="operator" class="flat-red" checked="true" value="AND"/> AND
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="radio" name="operator" class="flat-red" value="OR"/> OR
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div>
                        <div class="col-xs-12 col-sm-8 col-md-4">
                            <a href="">
                                <button type="button" class="btn btn-info btn-block"><i class="fa fa-search"></i> Search</button>
                            </a>
                        </div>
                    </div>
                 </div>
                   <div class="col-md-12">
                            <div class="col-md-2 col-xs-12 xol-sm-12">
                                <a href="<?php echo base_url('add/man_power_reques'); ?>">
                                    <button type="button" class="btn btn-success btn-block"> New</button>
                                </a>
                            </div>
                        </div>


                    <!--SEMENTARA-->
                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <tr class="success"> 
                                <th class="text-center">No</th> 
                                <th class="text-center">Employee Code</th>
                                <th class="text-center">Employee Name</th>
                                <th class="text-center">Ongoing Project</th>
                                <th class="text-center">Next Project</th>
                                <th class="text-center">Status</th>
                            </tr>   
                            <tr>
                                <td class="text-center">1</td>
                                <td>E_1</td>
                                <td>SUDARMONO</td>
                                <td>PA</td>
                                <td>PZ</td>
                                <td class="text-center">
                                    <span class="label label-success" ng-show="{{m.status}}==1"><i class="fa fa-check">&nbsp;</i>Assigned</span>
                                </td>  
                             </tr>
                             <tr>
                                <td class="text-center">2</td>
                                <td>E_2</td>
                                <td>ARBONDES</td>
                                <td>PB</td>
                                <td>PZ</td>
                                <td class="text-center">
                                    <span class="label label-success" ng-show="{{m.status}}==1"><i class="fa fa-check">&nbsp;</i>Assigned</span>
                                </td>  
                             </tr>
                             <tr>
                                <td class="text-center">3</td>
                                <td>E_3</td>
                                <td>ISKANDAR</td>
                                <td>PC</td>
                                <td>PZ</td>
                                <td class="text-center">
                                    <span class="label label-danger" ng-show="{{m.status}}==0"><i class="fa fa-remove">&nbsp;</i>Not Assigned</span>
                                </td>  
                             </tr>
                             <tr>
                                <td class="text-center">4</td>
                                <td>E_4</td>
                                <td>ANTON PRIADI</td>
                                <td>PA</td>
                                <td>PZ</td>
                                <td class="text-center">
                                    <span class="label label-success" ng-show="{{m.status}}==1"><i class="fa fa-check">&nbsp;</i>Assigned</span>
                                </td>  
                             </tr>
                             <tr>
                                <td class="text-center">5</td>
                                <td>E_5</td>
                                <td>I KETUT ADI PUJA ASTAWA</td>
                                <td>PA</td>
                                <td>PZ</td>
                                <td class="text-center">
                                    <span class="label label-danger" ng-show="{{m.status}}==0"><i class="fa fa-remove">&nbsp;</i>Not Assigned</span>
                                </td>  
                             </tr>
                             <tr>
                                <td class="text-center">6</td>
                                <td>E_6</td>
                                <td>SUBARI.M</td>
                                <td>PD</td>
                                <td>PZ</td>
                                <td class="text-center">
                                    <span class="label label-success" ng-show="{{m.status}}==1"><i class="fa fa-check">&nbsp;</i>Assigned</span>
                                </td>  
                             </tr>  
                        </table>
                       
                    </div>

                    <div class="col-md-12 col-xs-12 col-sm-12 pull-left" style="margin-top:20px;margin-bottom:20px;">
                        <div class="col-md-2">
                            <a href="<?php echo base_url('dashboard/man_power_requested'); ?>"><input type="button" class="btn btn-block btn-danger" value="Back"></a>
                        </div>
                        <div class="col-md-10">
                            <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                                <ul class="pagination">
                                    <li class="paginate_button previous disabled" id="example1_previous"><a href="#" aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a></li>
                                    <li class="paginate_button active"><a href="#" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a></li>
                                    <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a></li>
                                    <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" tabindex="0">3</a></li>
                                    <li class="paginate_button next" id="example1_next"><a href="#" aria-controls="example1" data-dt-idx="7" tabindex="0">Next</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

<!-------------------------------------------------ORIGINAL---------------------------------------------------------------->
                <!--<div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <tr> 
                                <th >No</th> 
                                <th >Employee ID</th>
                                <th >
                                    <a href="#" ng-click="sortType = 'creator'; sortReverse = !sortReverse">
                                    Employee Name
                                    <span ng-show="sortType == 'creator' && !sortReverse" class="fa fa-caret-down"></span>
                                    <span ng-show="sortType == 'creator' && sortReverse" class="fa fa-caret-up"></span>
                                    </a>
                                </th>
                                <th >
                                    <a href="#" ng-click="sortType = 'ongoing'; sortReverse = !sortReverse">
                                    Ongoing Project
                                    <span ng-show="sortType == 'ongoing' && !sortReverse" class="fa fa-caret-down"></span>
                                    <span ng-show="sortType == 'ongoing' && sortReverse" class="fa fa-caret-up"></span>
                                    </a>
                                </th>
                                <th >
                                    <a href="#" ng-click="sortType = 'nextproject'; sortReverse = !sortReverse">
                                    Next Project
                                    <span ng-show="sortType == 'nextproject' && !sortReverse" class="fa fa-caret-down"></span>
                                    <span ng-show="sortType == 'nextproject' && sortReverse" class="fa fa-caret-up"></span>
                                    </a>
                                </th>
                                <th >Status</th>
                            </tr>   
                            <tr ng-repeat="m in mpr | orderBy:sortType:sortReverse | filter:seacrh">
                                <td>{{$index+1}}</td>
                                <td>{{m.id}}</td>
                                <td>{{m.name}}</td>
                                <td>{{m.ongoing}}</td>
                                <td>{{m.nextproject}}</td>
                                <td>
                                    <span class="label label-success" ng-show="{{m.status}}==1"><i class="fa fa-check">&nbsp;</i>Assigned</span>
                                    <span class="label label-danger" ng-show="{{m.status}}==0"><i class="fa fa-remove">&nbsp;</i>Not Assigned</span>
                                </td>  
                             </tr>  
                        </table>
                    </div>
<!-------------------------------------------------ORIGINAL---------------------------------------------------------------->

                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
      $(".departemen").select2();
    });
    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      $scope.sortType     = 'no'; // set the default sort type
      $scope.sortReverse  = false;  // set the default sort order
      $scope.search   = ''; 
      $scope.limit =25;
      $scope.depart="";  
      $scope.jumlah = 0;                        
      
      $scope.mpr = [
        { id: 'E1', name: 'Danny', ongoing : 'PA', nextproject : 'PZ', status : 1},
        { id: 'E2', name: 'Erry', ongoing : 'PB', nextproject : 'PZ', status : 1},
        { id: 'E3', name: 'Tri', ongoing : 'PC', nextproject : 'PZ', status : 1},
        { id: 'E4', name: 'Handhika', ongoing : 'PA', nextproject : 'PZ', status : 1},
        { id: 'E5', name: 'Bemper', ongoing : 'PA', nextproject : 'PZ', status : 0},
        { id: 'E6', name: 'Kili', ongoing : 'PC', nextproject : 'PZ,PX', status : 1}
      ];
      
       $scope.currentPage = 1;  
       $scope.totalItems = $scope.mpr.length;  
       $scope.numPerPage = $scope.limit;    
       
       $scope.limitPage = function() {
         $scope.numPerPage = $scope.limit;
         if($scope.currentPage * $scope.numPerPage > $scope.mpr.length){
            $scope.currentPage = 1;   
         }
      };
       
       $scope.lastPage = function() {      
         $scope.currentPage=$scope.pageCount();
      };
      
      $scope.firstPage = function() {
         $scope.currentPage=1;
      };
       
       $scope.nextPage = function() {
        
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
      };
      
      $scope.jumlahPerpage = function (value) {
        $scope.numPerPage = value;
      } 
      
      $scope.prevPage = function() {
        if ($scope.currentPage > 1) {
          $scope.currentPage--;
        }
      };
      
       $scope.pageCount = function() {
        return Math.ceil($scope.jumlah/$scope.numPerPage);
      };        
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

        
    

</script>
