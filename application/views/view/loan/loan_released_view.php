<link href="../../asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<div class="content-wrapper">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Loan</li>
            <li class="active">Released</li>
            <li class="active">View</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Loan | Released | View</h3>
                        <hr>
                    </div>
                    <div class="box-body">
                    	<div class="col-md-12 col-sm-12 xs-12">
			           		<center><h3>PERMOHONAN PINJAMAN KARYAWAN</h3></center>
			           		<hr>
			            </div>
			            <div class="panel panel-default">
                    		<div class="panel-body">
                    			<div class="col-md-12">
				                	<div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Employee Code</label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                            151710072 
				                        </div>
				                    </div>
				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Employee Name</label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                            SUDARMONO 
				                        </div>
				                    </div>
				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Position</label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                            Operation Director 
				                        </div>
				                    </div>
				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Years of Employment</label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                            1 Years 2 Month 
				                        </div>
				                    </div>
				        <hr>
				                	<div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Pinjaman Yang Diajukan</label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                            Rp.5,000,000. 
				                        </div>
				                    </div>
				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Angsuran Perbulan</label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                            Rp.500,000. 
				                        </div>
				                    </div>
				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Mulai Bulan</label>
				                        </div> 
				                        <div class="col-md-2 col-xs-12 col-sm-5">
				                            January
				                        </div>
				                    </div>
				        <hr>
				                	<div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Untuk Keperluan</label>
				                        </div> 
				                        <div class="col-md-9 col-xs-12 col-sm-5">
                                  <div class="col-md-7" style="padding-bottom:5px; font-size:15px;">
                                  <input type="checkbox" disabled>Kematian Istri / Suami / Anak / Orang tua kandung 
                                  </div>
                                  <div class="col-md-7" style="padding-bottom:5px; font-size:15px;">
                                    <input type="checkbox" checked disabled>Sakit Berat
                                  </div>
                                  <div class="col-md-7" style="padding-bottom:5px; font-size:15px;">
                                    <input type="checkbox" disabled>Musibah <input type="text" class="form-control" id="" disabled> 
                                  </div>
                                </div>
				                    </div>

				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Bukti Pendukung</label>
				                        </div> 
				                        <div class="col-md-9 col-xs-12 col-sm-5">
				                        	<div class="col-md-7" style="padding-bottom:10px; font-size:15px;">
				                   				   <a href="https://www.rotamart.com/image/cache/sedia-vicks-obat-batuk-formula-44-sirup---100-ml.-800x800.jpg"><input type="submit" value="Open"></a><span style="padding-left:5px;">Foto Obat</span>
	       										      </div>
				                        	<div class="col-md-7" style="padding-bottom:10px; font-size:15px;">
				                        		 <a href="http://sentramedis.com/imgupl/20140912150815maxresdefault.jpg"><input type="submit" value="Open"></a><span style="padding-left:5px;">Foto Rumah Sakit</span>
				                        	</div>
				                        </div>
				                    </div>
				                    <hr>

				                    <div class="row" style="padding-top:5px; padding-bottom:5px;">
				                        <div class="col-md-3 col-xs-12 col-sm-4">
				                            <label for="comment">Sisa Pinjaman Lama </label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                        	<input type="text" class="form-control" id="" value="Rp.150,000" disabled>
				                        </div>
				                        <div class="col-md-2 col-xs-12 col-sm-4">
				                            <label for="comment">Angsuran Perbulan </label>
				                        </div> 
				                        <div class="col-md-3 col-xs-12 col-sm-5">
				                        	<input type="text" class="form-control" id="" value="Rp.150,000" disabled>
				                        </div>
				                    </div>
				                    
				                </div>
				            </div>
				        </div>
                <div class="col-md-8 col-xs-12 col-sm-12" style="padding-top:10px;">
                      <div class="col-md-4 col-xs-4 col-sm-4">
                          <a href="<?php echo base_url('dashboard/loan_released_list'); ?>"><input type="button" class="btn btn-block btn-danger" value="Back"></a>
                      </div>
                </div>

				    </div>
				</div>
			</div>
		</div>
    </section>
</div>




<script type="text/javascript">
    $(document).ready(function() {
      $(".departemen").select2();
    });
    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      $scope.sortType     = 'no'; // set the default sort type
      $scope.sortReverse  = false;  // set the default sort order
      $scope.search   = ''; 
      $scope.limit =25;
      $scope.dari="";
      $scope.to=""; 
      $scope.depart="";  
      $scope.jumlah = 0;
                   
      
      $scope.employee = <?php echo $employee ?>;
      
      
      $scope.departemen = [
        {nama : "Purchasing"},
        {nama : "Research"},
        {nama : "HRD"}
        ];
        
        $scope.reset = function () {
            $scope.depart =  "";     
        } 
        
        
       $scope.currentPage = 1;  
       $scope.totalItems = $scope.employee.length;  
       $scope.numPerPage = $scope.limit;    
       
       $scope.limitPage = function() {
         $scope.numPerPage = $scope.limit;
         if($scope.currentPage * $scope.numPerPage > $scope.employee.length){
            $scope.currentPage = 1;   
         }
      };
       
       $scope.lastPage = function() {      
         $scope.currentPage=$scope.pageCount();
      };
      
      $scope.firstPage = function() {
         $scope.currentPage=1;
      };
       
       $scope.nextPage = function() {
        
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
      };
      
      $scope.jumlahPerpage = function (value) {
        $scope.numPerPage = value;
      } 
      
      $scope.prevPage = function() {
        if ($scope.currentPage > 1) {
          $scope.currentPage--;
        }
      };
      
       $scope.pageCount = function() {
        return Math.ceil($scope.jumlah/$scope.numPerPage);
      };  
      
      $scope.checkAll = function () {
        angular.forEach($scope.employee, function (item) {
            item.Selected = $scope.selectAll;
        });
      };
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    app.filter("dateRange", function() {      
      return function(items, dari, to) {
        if(dari.length==0){
            var dari = +new Date("1980-01-01");
        }else{
            var dari = +new Date(dari);
        }
        
        if(to.length==0){
            var to = +new Date();
        }else{
            var to = +new Date(to);
        }
        var df = dari;
        var dt = to ;
        var arrayToReturn = [];        
        for (var i=0; i<items.length; i++){
            var tf = +new Date(items[i].join);
            if ((tf > df && tf < dt) || (tf==dt) )  {
                arrayToReturn.push(items[i]);
            }
        }
        
        return arrayToReturn;
      };
        
    });
    
    
    app.filter("paging", function() {      
      return function(items, limit, currentPage) {
        if (typeof limit === 'string' || limit instanceof String){
           limit = parseInt(limit);
         }
         
         var begin, end, index;  
         begin = (currentPage - 1) * limit;  
         end = begin + limit;
         var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (begin <= i && i < end )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    
    app.filter("departemen", function() {      
      return function(items, depart) {
      if(depart.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].dept == depart )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    
    app.filter("nama", function() {      
      return function(items, search) {
      if(search.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].name.toUpperCase().indexOf(search.toUpperCase()) != -1 )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    

</script>