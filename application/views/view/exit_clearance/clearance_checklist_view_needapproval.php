<link href="../../asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<div class="content-wrapper">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Exit Clearance</li>
            <li class="active">Clearance Checklist</li>
            <li class="active">View</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Exit Clearance | Clearance Checklist | View</h3>
                        <hr>
                    </div>
                    <div class="box-body">
                    	<div class="col-md-12 col-sm-12 xs-12">
			           		<center><h3>CLEARANCE CHECKLIST</h3></center>
			           		<hr>
			            </div>
			            <div class="panel panel-default">
                    <!--1-->
                    		<div class="panel-body">
                          <label for="comment">A. Informasi Umum</label>
                          <div class="col-md-12">
                            <div class="col-md-6">
                              <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                  <div class="col-md-6 col-xs-12 col-sm-4" >
                                      <label for="comment">Employee Code</label>
                                  </div> 
                                  <div class="col-md-6 col-xs-12 col-sm-5">
                                      151710066
                                  </div>
                              </div>
                              <div class="row" style="padding-top:5px; padding-bottom:5px;">
  				                        <div class="col-md-6 col-xs-12 col-sm-4">
  				                            <label for="comment">Employee Name</label>
  				                        </div> 
  				                        <div class="col-md-6 col-xs-12 col-sm-5">
  				                            ANHARUDDIN
  				                        </div>
  				                    </div>
                              <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                  <div class="col-md-6 col-xs-12 col-sm-4">
                                      <label for="comment">Name of supervisor</label>
                                  </div> 
                                  <div class="col-md-6 col-xs-12 col-sm-5">
                                      SUBARI.M
                                  </div>
                              </div>
                              <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                  <div class="col-md-6 col-xs-12 col-sm-4">
                                      <label for="comment">Length of Time with Company</label>
                                  </div> 
                                  <div class="col-md-6 col-xs-12 col-sm-5">
                                      3 Years 2 Month
                                  </div>
                              </div>
				                    </div>

                            <div class="col-md-6">
                              <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                  <div class="col-md-3 col-xs-12 col-sm-4">
                                      <label for="comment">Department</label>
                                  </div> 
                                  <div class="col-md-6 col-xs-12 col-sm-5">
                                      Layanan Pelanggan
                                  </div>
                              </div>
                              <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                  <div class="col-md-3 col-xs-12 col-sm-4">
                                      <label for="comment">Position</label>
                                  </div> 
                                  <div class="col-md-6 col-xs-12 col-sm-5">
                                      Finance Director
                                  </div>
                              </div>
                              <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                  <div class="col-md-3 col-xs-12 col-sm-4">
                                      <label for="comment">Location</label>
                                  </div> 
                                  <div class="col-md-6 col-xs-12 col-sm-5">
                                      Cikande
                                  </div>
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                  <div class="col-md-3 col-xs-12 col-sm-4">
                                      <label for="comment">Alasan Berhenti</label>
                                  </div> 
                                  <div class="col-md-6 col-xs-12 col-sm-5">
                                      Tidak Mendapatkan Salary yang Sesuai 
                                  </div>
                              </div>
                            </div>
				                </div>
                      </div>
                    <!--2-->
                      <div class="panel-body">
                        <label for="comment">B. Inventaris Perusahaan Yang Harus Dikembalikan</label>
                        <div class="box-body table-responsive">
                            <div class="col-md-6">
                                <div class="row" align="center">
                                    <div class="col-md-6">
                                        &nbsp;
                                    </div>
                                    <div class="col-md-1" >
                                        &nbsp;
                                    </div>
                                    <div class="col-md-5">
                                        <label for="comment">Keterangan</label>
                                    </div>
                                </div>
                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-6 col-xs-12 col-sm-4" >
                                        <label for="comment">1. Uang Muka/Kas Bon</label>
                                    </div>
                                    <div class="col-md-1" >
                                        <input type="checkbox" value="" disabled>
                                    </div>
                                    <div class="col-md-5 col-xs-12 col-sm-5">
                                        -
                                    </div>
                                </div>
                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-6 col-xs-12 col-sm-4" >
                                        <label for="comment">2. Kunci Ruangan</label>
                                    </div>
                                    <div class="col-md-1" >
                                        <input type="checkbox" value="" checked disabled>
                                    </div>
                                    <div class="col-md-5 col-xs-12 col-sm-5">
                                        Kunci Ruang Penyimpanan 
                                    </div>
                                </div>
                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-6 col-xs-12 col-sm-4" >
                                        <label for="comment">3. Dokumen Perusahaan</label>
                                    </div>
                                    <div class="col-md-1" >
                                        <input type="checkbox" value="" disabled>
                                    </div>
                                    <div class="col-md-5 col-xs-12 col-sm-5">
                                        -
                                    </div>
                                </div>
                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-6 col-xs-12 col-sm-4" >
                                        <label for="comment">4. Peralatan Security</label>
                                    </div>
                                    <div class="col-md-1" >
                                        <input type="checkbox" value="" disabled>
                                    </div>
                                    <div class="col-md-5 col-xs-12 col-sm-5">
                                        -
                                    </div>
                                </div>
                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-6 col-xs-12 col-sm-4" >
                                        <label for="comment">5. Kendaraan</label>
                                    </div>
                                    <div class="col-md-1" >
                                        <input type="checkbox" value="" checked disabled>
                                    </div>
                                    <div class="col-md-5 col-xs-12 col-sm-5">
                                        Mobil Zenia 
                                    </div>
                                </div>
                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-6 col-xs-12 col-sm-4" >
                                        <label for="comment">6. Kunci kendaraan</label>
                                    </div>
                                    <div class="col-md-1" >
                                        <input type="checkbox" value="" checked disabled>
                                    </div>
                                    <div class="col-md-5 col-xs-12 col-sm-5">
                                        Kunci Mobil
                                    </div>
                                </div>
                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-6 col-xs-12 col-sm-4" >
                                        <label for="comment">7. Peralatan Kerja</label>
                                    </div>
                                    <div class="col-md-1" >
                                        <input type="checkbox" value="" checked disabled>
                                    </div>
                                    <div class="col-md-5 col-xs-12 col-sm-5">
                                        Kacamata dan Helm Safety 
                                    </div>
                                </div>
                              </div>

                            <div class="col-md-6">
                             <div class="row" align="center">
                                    <div class="col-md-6">
                                        &nbsp;
                                    </div>
                                    <div class="col-md-1" >
                                        &nbsp;
                                    </div>
                                    <div class="col-md-5" >
                                        <label for="comment">Keterangan</label>
                                    </div>
                              </div>
                              <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-6 col-xs-12 col-sm-4" >
                                        <label for="comment">8. Dokumen Kendaraan</label>
                                    </div>
                                    <div class="col-md-1" >
                                        <input type="checkbox" value="" checked disabled>
                                    </div>
                                    <div class="col-md-5 col-xs-12 col-sm-5">
                                        STNK 
                                    </div>
                              </div>
                              <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-6 col-xs-12 col-sm-4" >
                                        <label for="comment">9. Pakaian dinas/seragam</label>
                                    </div>
                                    <div class="col-md-1" >
                                        <input type="checkbox" value="" checked disabled>
                                    </div>
                                    <div class="col-md-5 col-xs-12 col-sm-5">
                                        Baju meeting Client 
                                    </div>
                              </div>
                              <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-6 col-xs-12 col-sm-4" >
                                        <label for="comment">10. Komputer/notebook</label>
                                    </div>
                                    <div class="col-md-1" >
                                        <input type="checkbox" value="" checked disabled>
                                    </div>
                                    <div class="col-md-5 col-xs-12 col-sm-5">
                                        Notebook Acer 
                                    </div>
                              </div>
                              <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-6 col-xs-12 col-sm-4" >
                                        <label for="comment">11. Buku/Manual</label>
                                    </div>
                                    <div class="col-md-1" >
                                        <input type="checkbox" value="" disabled>
                                    </div>
                                    <div class="col-md-5 col-xs-12 col-sm-5">
                                        -
                                    </div>
                                </div>
                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-6 col-xs-12 col-sm-4" >
                                        <label for="comment">12. ATK</label>
                                    </div>
                                    <div class="col-md-1" >
                                        <input type="checkbox" value="" checked disabled>
                                    </div>
                                    <div class="col-md-5 col-xs-12 col-sm-5">
                                        pulpen 
                                    </div>
                                </div>
                                <div class="row" style="padding-top:5px; padding-bottom:5px;">
                                    <div class="col-md-6 col-xs-12 col-sm-4" >
                                        <label for="comment">13. Lain-lain</label>
                                    </div>
                                    <div class="col-md-1" >
                                        <input type="checkbox" value="" disabled>
                                    </div>
                                    <div class="col-md-5 col-xs-12 col-sm-5">
                                        - 
                                    </div>
                                </div>
                            </div>
                        </div>
				            </div>
                  <!--3-->
                    <div class="panel-body">
                        <label for="comment">C. Pekerjaan Yang Belum Selesai / Dalam Proses Penyelesaian</label>
                        <div class="box-body table-responsive">
                          <label for="comment">1. Pengalihan Pekerjaan</label>
                            <table id="example1" class="table table-bordered">
                            <tr align="center" class="success">
                              <th rowspan="3" class="text-center" style="vertical-align:middle; width:5%">No</th>
                              <th rowspan="3" class="text-center" style="vertical-align:middle;">Jenis Pekerjaan</th>
                              <th colspan="3" class="text-center" style="vertical-align:middle;">Hubungan Kerja Diluar Metro Group </th>
                              <th rowspan="3" class="text-center" style="vertical-align:middle;">Dialihkan Kepada</th>
                              <th rowspan="3" class="text-center" style="vertical-align:middle; width:15%">Tanggal</th>
                              <th rowspan="3" class="text-center" style="vertical-align:middle;">Keterangan</th>
                            <tr>
                            <tr align="center" class="success">
                              <th class="text-center">Perusahaan</th>
                              <th class="text-center">Pihak Yang Dihubungi</th>
                              <th class="text-center">Telepon / Fax</th>
                            </tr>
                            <tr>
                              <td class="text-center" style="vertical-align:middle;">1</td>
                              <td>packing</td>
                              <td>PT.Packing Indonesia</td>
                              <td>Indah Sari</td>
                              <td>0812-735034-76</td>
                              <td>IRZANDI MARZUKI</td>
                              <td>06/20/2016</td>
                              <td>-</td>
                            </tr>
                            <tr>
                              <td colspan="8" style="text-align:right;"><a href="#" class="glyphicon glyphicon-plus"></a></td>
                            </tr>
                          </table>
                        </div>

                        <div class="box-body table-responsive">
                          <label for="comment">2. Project File</label>  
                            <table id="example1" class="table table-bordered">
                            <tr align="center" class="success">
                              <th class="text-center">No</th>
                              <th class="text-center">Nama Project</th>
                              <th class="text-center">Kegiatan</th>
                              <th class="text-center">Jenis File</th>
                              <th class="text-center">Isi File</th>
                              <th class="text-center">Diserahkan Kepada</th>
                            <tr>
                            <tr>
                              <td class="text-center" style="vertical-align:middle;">1</td>
                              <td>Drims-IMS</td>
                              <td>Hubungkan ke database</td>
                              <td>Rar</td>
                              <td>Aplikasi Drims</td>
                              <td>IRZANDI MARZUKI</td>
                            </tr>
                            <tr>
                              <td colspan="8" style="text-align:right;"><a href="#" class="glyphicon glyphicon-plus"></a></td>
                            </tr>
                          </table>
                        </div>
                    </div>

                    <!--4-->
                    <div class="panel-body">
                        <label for="comment">D. Karyawan Secara Professional diharapkan agar tidak melakukan hal-hal berikut :</label>
                        <div class="box-body">
                          1. Pengalihan Pekerjaan
                          <br>2. Membocorkan Patent
                          <br>3. Membocorkan Informasi klien/supplier/konsumen
                          <br>4. Membuat Copy dokumen Perusahaan
                          <br>5. Mencemarkan Nama Perusahaan / Manajement
                        </div>
                        <label for="comment">Dengan ini saya menyatakan bahwa semua asset dan barang-barang (dari B1 s/d B 13) milik Perusahaan telah Dikembalikan.</label>
                        <label for="comment">Serah Terima pekerjaan dan file pekerjaan Telah dilakukan dan saya akan memenuhi persyaratan D1 s/d D5 Diatas.</label>
                    </div>

				        </div>
				            <div class="col-md-8 col-xs-12 col-sm-12" style="padding-top:10px; padding-bottom:10px;">
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                <a href="<?php echo base_url('dashboard/clearance_checklist_list'); ?>"><input type="button" class="btn btn-block btn-info" value="Approve"></a>
                            </div>
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                <a href="<?php echo base_url('dashboard/clearance_checklist_list'); ?>"><input type="button" class="btn btn-block btn-success" value="Reject"></a>
                            </div>
                            <div class="col-md-4 col-xs-4 col-sm-4">
                                <a href="<?php echo base_url('dashboard/clearance_checklist_list'); ?>"><input type="button" class="btn btn-block btn-danger" value="Review Later"></a>
                            </div>	
                    </div>


				    </div>
				</div>
			</div>
		</div>
    </section>
</div>

<style>
input:checked {
    height: 25px;
    width: 25px;
}
</style>

<script type="text/javascript">

    $(function () {
     $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });
    });
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
</script>

<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>

<script type="text/javascript">
    $(document).ready(function() {
      $(".departemen").select2();
    });
    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      $scope.sortType     = 'no'; // set the default sort type
      $scope.sortReverse  = false;  // set the default sort order
      $scope.search   = ''; 
      $scope.limit =25;
      $scope.dari="";
      $scope.to=""; 
      $scope.depart="";  
      $scope.jumlah = 0;
                   
      
      $scope.employee = <?php echo $employee ?>;
      
      
      $scope.departemen = [
        {nama : "Purchasing"},
        {nama : "Research"},
        {nama : "HRD"}
        ];
        
        $scope.reset = function () {
            $scope.depart =  "";     
        } 
        
        
       $scope.currentPage = 1;  
       $scope.totalItems = $scope.employee.length;  
       $scope.numPerPage = $scope.limit;    
       
       $scope.limitPage = function() {
         $scope.numPerPage = $scope.limit;
         if($scope.currentPage * $scope.numPerPage > $scope.employee.length){
            $scope.currentPage = 1;   
         }
      };
       
       $scope.lastPage = function() {      
         $scope.currentPage=$scope.pageCount();
      };
      
      $scope.firstPage = function() {
         $scope.currentPage=1;
      };
       
       $scope.nextPage = function() {
        
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
      };
      
      $scope.jumlahPerpage = function (value) {
        $scope.numPerPage = value;
      } 
      
      $scope.prevPage = function() {
        if ($scope.currentPage > 1) {
          $scope.currentPage--;
        }
      };
      
       $scope.pageCount = function() {
        return Math.ceil($scope.jumlah/$scope.numPerPage);
      };  
      
      $scope.checkAll = function () {
        angular.forEach($scope.employee, function (item) {
            item.Selected = $scope.selectAll;
        });
      };
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    app.filter("dateRange", function() {      
      return function(items, dari, to) {
        if(dari.length==0){
            var dari = +new Date("1980-01-01");
        }else{
            var dari = +new Date(dari);
        }
        
        if(to.length==0){
            var to = +new Date();
        }else{
            var to = +new Date(to);
        }
        var df = dari;
        var dt = to ;
        var arrayToReturn = [];        
        for (var i=0; i<items.length; i++){
            var tf = +new Date(items[i].join);
            if ((tf > df && tf < dt) || (tf==dt) )  {
                arrayToReturn.push(items[i]);
            }
        }
        
        return arrayToReturn;
      };
        
    });
    
    
    app.filter("paging", function() {      
      return function(items, limit, currentPage) {
        if (typeof limit === 'string' || limit instanceof String){
           limit = parseInt(limit);
         }
         
         var begin, end, index;  
         begin = (currentPage - 1) * limit;  
         end = begin + limit;
         var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (begin <= i && i < end )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    
    app.filter("departemen", function() {      
      return function(items, depart) {
      if(depart.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].dept == depart )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    
    app.filter("nama", function() {      
      return function(items, search) {
      if(search.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].name.toUpperCase().indexOf(search.toUpperCase()) != -1 )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    

</script>