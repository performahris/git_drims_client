<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"></i>HR</a></li>
            <li><a href="#"></i>Leave Request</a></li>
            <li><a href="#"></i>Released</a></li>
            <li class="active">List</li>
        </ol>
    </section>
	
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
					<div class="box-header">
                <h3 class="box-title">Leave Request | Released | List</h3>
			          <hr>
          </div>

          <div class="box-body"> 
    <!-- BEGIN OF SEARCH -->
                         <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-8 col-md-8">
                                    <label for="searchby" class="col-md-12">Search By :</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Employee Code</option>
                                        <option>Employee Name</option>
                                        <option>Date</option>
                                        <option>Sisa Cuti</option>
                                        <option>Ambil Cuti</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Employee Code</option>
                                        <option>Employee Name</option>
                                        <option>Date</option>
                                        <option>Sisa Cuti</option>
                                        <option>Ambil Cuti</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Employee Code</option>
                                        <option>Employee Name</option>
                                        <option>Date</option>
                                        <option>Sisa Cuti</option>
                                        <option>Ambil Cuti</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="operator">Operator</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <input type="radio" name="operator" class="flat-red" checked="true" value="AND"/> AND
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="operator" class="flat-red" value="OR"/> OR
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div>
                            <div class="col-xs-12 col-sm-8 col-md-4">
                                <a href="">
                                    <button type="button" class="btn btn-info btn-block"><i class="fa fa-search" data-toggle="tooltip" title="View">&nbsp;</i>Search</button>
                                </a>
                            </div>
                            </div>
                        </div>
                        <!--END OF SEARCH-->
	                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
              							   <tr class="success">
                									<th class="text-center">No</th>
                									<th class="text-center">Employee Code</th>
                									<th class="text-center">Employee Name</th>
                									<th class="text-center">Date</th>
                                  <th class="text-center">Sisa Cuti</th>
                									<th class="text-center">Ambil Cuti</th>
                									<th class="text-center">Status</th>
                                  <th class="text-center">Action</th>
              							   </tr>
								            </thead>
								            <tbody>
									             <tr>
										              <td align="center">1</td>
										              <td>E_1</td>
										              <td>SUDARMONO</td>
										              <td align="center">15-03-2016</td>
                                  <td align="center">10</td>
              										<td align="center">3</td>
              										<td><center><span class="open_status">Approved</span></center></td>
                                  <td class="text-center">
                                      <a href="<?php echo base_url(); ?>view/leave_released_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                  </td>
									             </tr>
                               <tr>
                                  <td align="center">2</td>
                                  <td>E_2</td>
                                  <td>ARBONDES</td>
                                  <td align="center">16-02-2016</td>
                                  <td align="center">9</td>
                                  <td align="center">3</td>
                                  <td><center><span class="reject_status">Rejected</span></center></td>
                                  <td class="text-center">
                                      <a href="<?php echo base_url(); ?>view/leave_released_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                  </td>
                               </tr>
                               <tr>
                                  <td align="center">3</td>
                                  <td>E_3</td>
                                  <td>ISKANDAR</td>
                                  <td align="center">16-02-2016</td>
                                  <td align="center">9</td>
                                  <td align="center">3</td>
                                  <td><center><span class="open_status">Approved</span></center></td>
                                  <td class="text-center">
                                      <a href="<?php echo base_url(); ?>view/leave_released_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                  </td>
                               </tr>
                               <tr>
                                  <td align="center">4</td>
                                  <td>E_4</td>
                                  <td>ANTON PRIADI</td>
                                  <td align="center">17-02-2016</td>
                                  <td align="center">7</td>
                                  <td align="center">2</td>
                                  <td><center><span class="open_status">Approved</span></center></td>
                                  <td class="text-center">
                                      <a href="<?php echo base_url(); ?>view/leave_released_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                  </td>
                               </tr>
                               <tr>
                                  <td align="center">5</td>
                                  <td>E_5</td>
                                  <td>I KETUT ADI PUJA ASTAWA</td>
                                  <td align="center">21-02-2016</td>
                                  <td align="center">3</td>
                                  <td align="center">1</td>
                                  <td><center><span class="open_status">Approved</span></center></td>
                                  <td class="text-center">
                                      <a href="<?php echo base_url(); ?>view/leave_released_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                  </td>
                               </tr>
                               <tr>
                                  <td align="center">6</td>
                                  <td>E_6</td>
                                  <td>SUBARI.M</td>
                                  <td align="center">30-02-2016</td>
                                  <td align="center">8</td>
                                  <td align="center">5</td>
                                  <td><center><span class="open_status">Approved</span></center></td>
                                  <td class="text-center">
                                      <a href="<?php echo base_url(); ?>view/leave_released_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                  </td>
                               </tr>
                               <tr>
                                  <td align="center">7</td>
                                  <td>E_7</td>
                                  <td>SUNARDI</td>
                                  <td align="center">07-03-2016</td>
                                  <td align="center">1</td>
                                  <td align="center">1</td>
                                  <td><center><span class="reject_status">Rejected</span></center></td>
                                  <td class="text-center">
                                      <a href="<?php echo base_url(); ?>view/leave_released_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                  </td>
                               </tr>
                               <tr>
                                  <td align="center">8</td>
                                  <td>E_8</td>
                                  <td>BAMBANG IRAWAN</td>
                                  <td align="center">19-03-2016</td>
                                  <td align="center">3</td>
                                  <td align="center">2</td>
                                  <td><center><span class="reject_status">Rejected</span></center></td>
                                  <td class="text-center">
                                      <a href="<?php echo base_url(); ?>view/leave_released_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                  </td>
                               </tr>
                               <tr>
                                  <td align="center">9</td>
                                  <td>E_9</td>
                                  <td>CHAIRUL AZWAN</td>
                                  <td align="center">23-03-2016</td>
                                  <td align="center">10</td>
                                  <td align="center">2</td>
                                  <td><center><span class="reject_status">Rejected</span></center></td>
                                  <td class="text-center">
                                      <a href="<?php echo base_url(); ?>view/leave_released_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                  </td>
                               </tr>
                               <tr>
                                  <td align="center">10</td>
                                  <td>E_10</td>
                                  <td>AHMAD SARURI</td>
                                  <td align="center">01-04-2016</td>
                                  <td align="center">7</td>
                                  <td align="center">4</td>
                                  <td><center><span class="reject_status">Rejected</span></center></td>
                                  <td class="text-center">
                                      <a href="<?php echo base_url(); ?>view/leave_released_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                  </td>
                               </tr>				
								            </tbody>
                        </table>
                        	
                      </div>
                                    <div class="col-xs-11 col-sm-12 col-md-12">
                                      <ul class="pagination pull-right" >
                                      <li><a href="#">Previous</a></li>
                                      <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                                      <li><a href="#">2</a></li>
                                      <li><a href="#">3</a></li>
                                      <li><a href="#">Next</a></li>
                                    </ul>
                                  </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
