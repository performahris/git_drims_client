<?php 
	date_default_timezone_set("Asia/Bangkok");
?>
<script>
	$(function() {
		$( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
	});
	$(function() {
		$( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
	});
</script>
<script>
	$(document).ready(function(){
		$("#button-form").click(function(){
			if($(this).hasClass("u")){
					$("#rotate").addClass("fa-angle-double-down");
					$("#rotate").removeClass("fa-angle-double-up");
					$(this).removeClass("u");
					$("#div-form").slideUp(1000);
			}
			else 
			{
				$("#rotate").removeClass("fa-angle-double-down");
				$("#rotate").addClass("fa-angle-double-up");
				$(this).addClass("u");
				$("#div-form").slideDown(1000);
			}
		});
		$("#check-all").click(function(){
			$("#form-all").slideToggle();
		});
		$("#cost-center-any").click(function(){
			$("#div-cost-center").slideToggle();
		});
		$("#job-status-any").click(function(){
			$("#div-job-status").slideToggle();
		});
		$("#job-grade-any").click(function(){
			$("#div-job-grade").slideToggle();
		});
		$("#position-any").click(function(){
			$("#div-position").slideToggle();
		});
		$("#employment-status-any").click(function(){
			$("#div-employment-status").slideToggle();
		});
		$("#religion-any").click(function(){
			$("#div-religion").slideToggle();
		});
	});
</script>

<div class="content-wrapper">
	<section class="content-header">
		<h1>&nbsp;</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
			<li><a href="#"></i>Benefit Management</a></li>
			<li class="active">Payroll Process</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12 col-xs-12 col-sm-12">
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">Benefit Management | Payroll Process</h3>
						<hr>
					</div>
					<div class="box-body"> 
					<!-- BEGIN OF SEARCH -->
						<div class="col-xs-12 col-md-12">
							<div class="row form-group">
								<div class="col-xs-12 col-sm-6 col-md-9">
									<a href="#" class="u" id="button-form" style="float: right;"><label for="code" style="font-size: 20pt; ">Inquiry Form &nbsp;</label><i id="rotate" class="fa fa-2x fa-angle-double-up"></i></a> &nbsp; 
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-9" id="div-form" style="border: 1px solid #ebebe0; padding: 15px;">
							<div class="row form-group">
								<div class="col-xs-12 col-sm-6 col-md-8">
									<label for="employee" >Employee (<span data-multiple-target-count>0</span>)</label> &nbsp;
									<input id="check-all" type="checkbox" name="employee" value="all"> All
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<label for="period" >Period</label> &nbsp;
									<input type="radio" name="period" value="all"> All &nbsp;
									<input type="radio" name="period" value="monthly"> Monthly &nbsp;
									<input type="radio" name="period" value="thr"> THR &nbsp;
								</div>		
							</div>
							<div class="col-xs-12 col-sm-6 col-md-8" id="form-all">
								<div class="row form-group">
									<div class="col-xs-12 col-sm-5 col-md-5">
										<input type="text" class="form-control" name=""/>
									</div>
									<div class="col-xs-12 col-sm-3 col-md-3">
										<button type="button" class="btn btn-default btn-block btn-sm" data-toggle="modal" data-target="#myModal">Filter</button>
									</div>
									<div class="col-xs-12 col-sm-3 col-md-3">
										<button type="reset" class="btn btn-default btn-block btn-sm">Clear</button>
									</div>    
								</div>
								<div class="row form-group">
									<div class="col-xs-12 col-sm-4 col-md-4">
										<select data-multiple-source multiple class="form-control">
											<option>Fatma Lian</option>
											<option>Fatimah Zahra</option>
											<option>Feli Wayan</option>
											<option>Ananda</option>
											<option>Karinina</option>
											<option>Felice</option>
											<option>Rubiyat</option>
											<option>Kanra</option>
											<option>Lukita</option>
											<option>Aira</option>
											<option>Airlangga</option>
											<option>Dharmawangsa</option>
											<option>Fernandez</option>
											<option>Samuel</option>
											<option>Santi</option>
											<option>Laila</option>
											<option>Geneva</option>
											<option>Airal</option>
											<option>Lopi</option>
											<option>Rogue</option>
										</select>
									</div>
									<div class="col-xs-12 col-sm-4 col-md-4">
										<button data-multiple-trigger-right type="button" class="btn btn-default btn-block btn-sm"><i class="fa fa-angle-double-right"></i></button></br>
										<button data-multiple-trigger-left type="button" class="btn btn-default btn-block btn-sm"><i class="fa fa-angle-double-left"></i></button>
									</div>
									<div class="col-xs-12 col-sm-4 col-md-4">
										<select data-multiple-target class="form-control" multiple>
												
										</select>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-4" style="float: right">
								<div class="col-xs-12 col-sm-12 col-md-12">
									<select class="form-control">
										<option>MTH0001 - 25 Januari 2016 (Monthly)</option>
										<option>MTH0003 - 03 Maret 2014 (Monthly)</option>
										<option>THR0001 - 1 July 2013 (THR)</option>
										<option>BNS0001 - 2 Februari 2016 (Bonus)</option>
									</select>		
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div>
									<div class="col-xs-12 col-sm-5 col-md-6">
										<button type="button" class="btn btn-default btn-block btn-sm">Generate</button>
									</div>
									<div class="col-xs-12 col-sm-5 col-md-6">
										<button type="button" class="btn btn-default btn-block btn-sm">Reset</button>
									</div>
								</div>
							</div>
						</div>
					<!--END OF SEARCH-->

					<!--
					<div class="col-md-12 col-xs-12 col-sm-12">
					<select ng-model="depart" class="departemen" >
						<option value= "">List Of Department</option>
						<option value="{{ dept.nama }}" ng-repeat="dept in departemen">{{ dept.nama }}</option>
					</select>

					<select ng-model="depart" class="departemen" >
						<option value= "">List Of EmployeeID</option>
						<option value="{{ emp.ID }}" ng-repeat="emp in employee">{{ emp.ID }}</option>
					</select> 

					<select ng-model="depart" class="departemen" >
						<option value= "">List Of Total Payroll</option>
						<option value="{{ joi.date }}" ng-repeat="joi in joined">{{ joi.date }}</option>
					</select> 
					</div>
					-->

					<!--<div class="col-md-2">
						<a href="<?php echo base_url(); ?>add/employee_register"><input type="submit" class="btn btn-block btn-success" value="Tambah"></a>
						</div>
					</div>-->    
					
					
					<div class="box-body table-responsive " style="width:100%">
						<div class="col-xs-12 col-sm-4 col-md-4 text-center" id="example1_paginate" style="padding: 10px; border: 1px solid #ebebe0; border-bottom: none">
							<a href="#"><label for="code" class="fa fa-angle-double-left fa-lg"></a></label> &nbsp; 
							<a href="#"><label for="code" class="fa fa-angle-left fa-lg"></a></label> &nbsp;
							<label for="code">Page 1 to 10</label> &nbsp;
							<a href="#"><label for="code" class="fa fa-angle-right fa-lg"></a></label> &nbsp;
							<a href="#"><label for="code" class="fa fa-angle-double-right fa-lg"></a></label> &nbsp;
							<label for="code">20 Rows per Page</label> &nbsp;
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6">
							<label class="label-2" for="code"><small><?php echo date("l");?>, <em><?php echo date("j / m / Y");?></em></small></label></br>
							<label for="code" style="float: right"><small>Your amount below is 200 row</small></label>
						</div>
						<div class="col-xs-12 col-sm-2 col-md-2">
							<button type="button" class="btn btn-info btn-block" style="float: right">Enroll Payment?</button>
						</div>
						<table class="table table-striped table-bordered">
							<tr class="success">
								<th >No</th> 
								<th >
									<!--<a href="#" ng-click="sortType = 'name'; sortReverse = !sortReverse">-->
									<center>Name</center>
									<span ng-show="sortType == 'name' && !sortReverse"></span>
									<span ng-show="sortType == 'name' && sortReverse"></span>
									<!--</a>-->
								</th>
								<th >
									<!--<a href="#" ng-click="sortType = 'name'; sortReverse = !sortReverse">-->
									<center>Basic Salary</center>
									<span ng-show="sortType == 'name' && !sortReverse"></span>
									<span ng-show="sortType == 'name' && sortReverse"></span>
									<!--</a>-->
								</th>								
								<th >
									<!--<a href="#" ng-click="sortType = 'join'; sortReverse = !sortReverse">-->
									<center>Uang Makan</center>
									<span ng-show="sortType == 'join' && !sortReverse"></span>
									<span ng-show="sortType == 'join' && sortReverse"></span>
									<!--</a>-->
								</th>								
								<th >
									<!--<a<a href="#" ng-click="sortType = 'join'; sortReverse = !sortReverse">-->
									<center>Transport</center>
									<span ng-show="sortType == 'join' && !sortReverse"></span>
									<span ng-show="sortType == 'join' && sortReverse"></span>
									<!--</a>-->
								</th>																
								<th >
									<!--<a href="#" ng-click="sortType = 'join'; sortReverse = !sortReverse">-->
									<center>Medical</center>
									<span ng-show="sortType == 'join' && !sortReverse"></span>
									<span ng-show="sortType == 'join' && sortReverse"></span>
									<!--</a>-->
								</th>
								<th >
									<!--<a href="#" ng-click="sortType = 'join'; sortReverse = !sortReverse">-->
									<center>Overtime</center>
									<span ng-show="sortType == 'join' && !sortReverse"></span>
									<span ng-show="sortType == 'join' && sortReverse"></span>
									<!--</a>-->
								</th>
								<th >
									<!--<a href="#" ng-click="sortType = 'join'; sortReverse = !sortReverse">-->
									<center>Unpaid Leave</center>
									<span ng-show="sortType == 'join' && !sortReverse"></span>
									<span ng-show="sortType == 'join' && sortReverse"></span>
									<!--</a>-->
								</th>
								<th >
									<!--<a href="#" ng-click="sortType = 'join'; sortReverse = !sortReverse">-->
									<center>PPH</center>
									<span ng-show="sortType == 'join' && !sortReverse"></span>
									<span ng-show="sortType == 'join' && sortReverse"></span>
									<!--</a>-->
								</th>
								<th >
									<!--<a href="#" ng-click="sortType = 'join'; sortReverse = !sortReverse">-->
									<center>Pension</center>
									<span ng-show="sortType == 'join' && !sortReverse"></span>
									<span ng-show="sortType == 'join' && sortReverse"></span>
									<!--</a>-->
								</th>
								<th >
									<!--<a href="#" ng-click="sortType = 'join'; sortReverse = !sortReverse">-->
									<center>Bonus</center>
									<span ng-show="sortType == 'join' && !sortReverse"></span>
									<span ng-show="sortType == 'join' && sortReverse"></span>
									<!--</a>-->
								</th>	
								<th >
									<!--<a href="#" ng-click="sortType = 'join'; sortReverse = !sortReverse">-->
									<center>THR</center>
									<span ng-show="sortType == 'join' && !sortReverse"></span>
									<span ng-show="sortType == 'join' && sortReverse"></span>
									<!--</a>-->
								</th>
								<!--<th><input type="checkbox" ng-model="selectAll" ng-click="checkAll()" /></th>-->
							</tr>
							<tr ng-repeat="em in employee | orderBy:sortType:sortReverse ">
								<!--<td>1</td>
								<td>Debi</td>
								<td>Sani</td>
								<td>nnns</td>
								<td>ahdsahd</td>
								
								<td>{{em.employee_date_of_birth |date:'dd MMMM yyyy'}}</td>-->
								
								<!--<td><input type="checkbox" ng-model="em.Selected" /></td>-->
							</tr>
		
							<tr>
								<td>1</td>
								<td><center>Ferry Arlian</center></td>
								<td><center>3300000</center></td>
								<td><center>220000</center></td>
								<td><center>150000</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
							</tr> 
								
							<tr>
								<td>2</td>
								<td><center>Fatima Zahra</center></td>
								<td><center>3300000</center></td>
								<td><center>220000</center></td>
								<td><center>150000</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
							</tr> 

							<tr>
								<td>3</td>
								<td><center>Feli Wayan</center></td>
								<td><center>3300000</center></td>
								<td><center>220000</center></td>
								<td><center>150000</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
							</tr>

							<tr>
								<td>4</td>
								<td><center>Ananda</center></td>
								<td><center>3300000</center></td>
								<td><center>220000</center></td>
								<td><center>150000</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
							</tr> 

							<tr>
								<td>5</td>
								<td><center>Karinina</center></td>
								<td><center>3300000</center></td>
								<td><center>220000</center></td>
								<td><center>150000</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td> 
							</tr> 

							<tr>
								<td>6</td>
								<td><center>Felice</center></td>
								<td><center>3300000</center></td>
								<td><center>220000</center></td>
								<td><center>150000</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
							</tr> 

							<tr>
								<td>7</td>
								<td><center>Rubiyat</center></td>
								<td><center>3300000</center></td>
								<td><center>220000</center></td>
								<td><center>150000</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
							</tr> 

							<tr>
								<td>8</td>
								<td><center>Kanra</center></td>
								<td><center>3300000</center></td>
								<td><center>220000</center></td>
								<td><center>150000</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td> 
							</tr> 

							<tr>
								<td>9</td>
								<td><center>Lukita</center></td>
								<td><center>3300000</center></td>
								<td><center>220000</center></td>
								<td><center>150000</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
							</tr> 
							
							<tr>
								<td>10</td>
								<td><center>Aira</center></td>
								<td><center>3300000</center></td>
								<td><center>220000</center></td>
								<td><center>150000</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
							</tr>

							<tr>
								<td>11</td>
								<td><center>Airlangga</center></td>
								<td><center>3300000</center></td>
								<td><center>220000</center></td>
								<td><center>150000</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
							</tr>

							<tr>
								<td>12</td>
								<td><center>Dharmawangsa</center></td>
								<td><center>3300000</center></td>
								<td><center>220000</center></td>
								<td><center>150000</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
							</tr>

							<tr>
								<td>13</td>
								<td><center>Fernandez</center></td>
								<td><center>3300000</center></td>
								<td><center>220000</center></td>
								<td><center>150000</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
							</tr>

							<tr>
								<td>14</td>
								<td><center>Samuel</center></td>
								<td><center>3300000</center></td>
								<td><center>220000</center></td>
								<td><center>150000</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
							</tr>

							<tr>
								<td>15</td>
								<td><center>Santi</center></td>
								<td><center>3300000</center></td>
								<td><center>220000</center></td>
								<td><center>150000</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
							</tr>

							<tr>
								<td>16</td>
								<td><center>Laila</center></td>
								<td><center>3300000</center></td>
								<td><center>220000</center></td>
								<td><center>150000</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
							</tr>

							<tr>
								<td>17</td>
								<td><center>Geneva</center></td>
								<td><center>3300000</center></td>
								<td><center>220000</center></td>
								<td><center>150000</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
							</tr>

							<tr>
								<td>18</td>
								<td><center>Airal</center></td>
								<td><center>3300000</center></td>
								<td><center>220000</center></td>
								<td><center>150000</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
							</tr>

							<tr>
								<td>19</td>
								<td><center>Lopi</center></td>
								<td><center>3300000</center></td>
								<td><center>220000</center></td>
								<td><center>150000</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
							</tr>

							<tr>
								<td>20</td>
								<td><center>Rogue</center></td>
								<td><center>3300000</center></td>
								<td><center>220000</center></td>
								<td><center>150000</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
								<td><center>0</center></td>
							</tr>
						</table>
						<!--<div ng-show="false">{{jumlah = (employee | departemen:depart | nama:search | dateRange:dari:to ).length }}</div>-->
						<div class="col-xs-12 col-sm-4 col-md-4 text-center" id="example1_paginate" style="padding: 10px; border: 1px solid #ebebe0; border-top: none">
							<a href="#"><label for="code" class="fa fa-angle-double-left fa-lg"></a></label> &nbsp; 
							<a href="#"><label for="code" class="fa fa-angle-left fa-lg"></a></label> &nbsp;
							<label for="code">Page 1 to 10</label> &nbsp;
							<a href="#"><label for="code" class="fa fa-angle-right fa-lg"></a></label> &nbsp;
							<a href="#"><label for="code" class="fa fa-angle-double-right fa-lg"></a></label> &nbsp;
							<label for="code">20 Rows per Page</label> &nbsp;
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title">Payroll > Payment Process > Filter</h4>
			</div>
			<div class="modal-body">
				<div class="row" style="padding-top:5px; padding-bottom:5px;">
					<div class="col-md-4 col-xs-12 col-sm-4">
						<label for="comment">Organization Unit</label>
					</div> 
					<div class="col-md-4 col-xs-12 col-sm-5 checkbox-inline">
						<input type="checkbox" name="employee" value="all"> All
					</div>
				</div>

				<div class="row" style="padding-top:5px; padding-bottom:5px;">
					<div class="col-md-4 col-xs-12 col-sm-4">
						<label for="comment">Work Location<label>
					</div> 
					<div class="col-md-6 col-xs-12 col-sm-5">
						<select class="form-control">
							<option>Not Specify</option>
							<option>MTH0003 - 03 Maret 2014 (Monthly)</option>
							<option>THR0001 - 1 July 2013 (THR)</option>
							<option>BNS0001 - 2 Februari 2016 (Bonus)</option>
						</select>
					</div>
				</div>

				<div class="row" style="padding-top:5px; padding-bottom:5px;">
					<div class="col-md-4 col-xs-12 col-sm-4">
						<label for="comment">Employee Status</label>
					</div> 
					<div class="col-md-6 col-xs-12 col-sm-7">
						<label class="radio-inline">
						  <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> Inactive
						</label>
						<label class="radio-inline">
						  <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> Active
						</label>
						<label class="radio-inline">
						  <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3"> Any
						</label>
					</div>
				</div>

				<div class="row" style="padding-top:5px; padding-bottom:5px;">
					<div class="col-md-4 col-xs-12 col-sm-4">
						<label for="comment">Cost Center</label>
					</div> 
					<div class="col-md-6 col-xs-12 col-sm-5 checkbox-inline">
						<input id="cost-center-any" type="checkbox" name="employee" value="all"> Any
					</div>
					<div class="col-md-6 col-xs-12 col-sm-5" id="div-cost-center">
						<select data-multiple-source multiple class="form-control">
							<option>Account Executive</option>
							<option>Board of Directors</option>
							<option>Consulting</option>
							<option>Development and Technology</option>
							<option>Finance</option>
						</select>
					</div>
				</div>

				<div class="row" style="padding-top:5px; padding-bottom:5px;">
					<div class="col-md-4 col-xs-12 col-sm-4">
						<label for="comment">Job Status</label>
					</div> 
					<div class="col-md-6 col-xs-12 col-sm-5 checkbox-inline">
						<input id="job-status-any" type="checkbox" name="employee" value="all"> Any
					</div>
					<div class="col-md-6 col-xs-12 col-sm-5" id="div-job-status">
						<select data-multiple-source multiple class="form-control">
							<option>Direct Worker</option>
							<option>Indirect Worker</option>
						</select>
					</div>
				</div>

				<div class="row" style="padding-top:5px; padding-bottom:5px;">
					<div class="col-md-4 col-xs-12 col-sm-4">
						<label for="comment">Job Grade</label>
					</div> 
					<div class="col-md-6 col-xs-12 col-sm-5 checkbox-inline">
						<input id="job-grade-any" type="checkbox" name="employee" value="all"> Any
					</div>
					<div class="col-md-6 col-xs-12 col-sm-5" id="div-job-grade">
						<select data-multiple-source multiple class="form-control">
							<option>2. Staff</option>
							<option>3. Supervisor</option>
							<option>4. Manager</option>
							<option>5. Director</option>
						</select>
					</div>
				</div>

				<div class="row" style="padding-top:5px; padding-bottom:5px;">
					<div class="col-md-4 col-xs-12 col-sm-4">
						<label for="comment">Position</label>
					</div>
					<div class="col-md-6 col-xs-12 col-sm-5 checkbox-inline">
						<input id="position-any" type="checkbox" name="employee" value="all"> Any
					</div>
					<div class="col-md-6 col-xs-12 col-sm-5" id="div-position">
						<select data-multiple-source multiple class="form-control">
							<option>Account Manager [SLSTAFF]</option>
							<option>Accounting Staff [FINSPV]</option>
							<option>Assistant Project Manager [PROASS]</option>
							<option>Data Entry [DE]</option>
							<option>Development Manager [DEVMGR]</option>
						</select>
					</div>
				</div>

				<div class="row" style="padding-top:5px; padding-bottom:5px;">
					<div class="col-md-4 col-xs-12 col-sm-4">
						<label for="comment">Employment Status</label>
					</div> 
					<div class="col-md-6 col-xs-12 col-sm-5 checkbox-inline">
						<input id="employment-status-any" type="checkbox" name="employee" value="all"> Any
					</div>
					<div class="col-md-6 col-xs-12 col-sm-5" id="div-employment-status">
						<select data-multiple-source multiple class="form-control">
							<option>Contract I</option>
							<option>Contract II</option>
							<option>Internship</option>
							<option>Outsource</option>
							<option>Permanent</option>
						</select>
					</div>
				</div>

				<div class="row" style="padding-top:5px; padding-bottom:5px;">
					<div class="col-md-4 col-xs-12 col-sm-4">
						<label for="comment">Gender</label>
					</div> 
					<div class="col-md-6 col-xs-12 col-sm-5">
						<label class="radio-inline">
						  <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> Female
						</label>
						<label class="radio-inline">
						  <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> Male
						</label>
						<label class="radio-inline">
						  <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3"> Any
						</label>
					</div>
				</div>

				<div class="row" style="padding-top:5px; padding-bottom:5px;">
					<div class="col-md-4 col-xs-12 col-sm-4">
						<label for="comment">Marital Status</label>
					</div> 
					<div class="col-md-6 col-xs-12 col-sm-5">
						<select class="form-control">
							<option>Any</option>
							<option>Single</option>
							<option>Married</option>
							<option>Divorced</option>
							<option>Widowed</option>
						</select>
					</div>
				</div>

				<div class="row" style="padding-top:5px; padding-bottom:5px;">
					<div class="col-md-4 col-xs-12 col-sm-4">
						<label for="comment">Religion</label>
					</div>
					<div class="col-md-6 col-xs-12 col-sm-5 checkbox-inline">
						<input id="religion-any" type="checkbox" name="employee" value="all"> Any
					</div>	
					<div class="col-md-6 col-xs-12 col-sm-5">
						<select data-multiple-source multiple class="form-control" id="div-religion">
							<option>Buddhist</option>
							<option>Catholic</option>
							<option>Moslem</option>
							<option>Christianity</option>
							<option>Hindu</option>
							<option>Confucianism</option>
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" value="submit" class="btn btn-default">Submit</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="reset" value="reset" class="btn btn-default">Reset</button>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function() {
	  $(".departemen").select2();
	});
	
	var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

	app.controller('mainController', function($scope) {
	  $scope.sortType     = 'no'; // set the default sort type
	  $scope.sortReverse  = false;  // set the default sort order
	  $scope.search   = ''; 
	  $scope.limit =25;
	  $scope.dari="";
	  $scope.to=""; 
	  $scope.depart="";  
	  $scope.jumlah = 0;
				   
	  
	  $scope.employee = <?php echo $employee ?>;
	  
	  
	  $scope.departemen = [
		{nama : "Purchasing"},
		{nama : "Research"},
		{nama : "HRD"}
		];
		
		$scope.reset = function () {
			$scope.depart =  "";     
		} 
		
		
	   $scope.currentPage = 1;  
	   $scope.totalItems = $scope.employee.length;  
	   $scope.numPerPage = $scope.limit;    
	   
	   $scope.limitPage = function() {
		 $scope.numPerPage = $scope.limit;
		 if($scope.currentPage * $scope.numPerPage > $scope.employee.length){
			$scope.currentPage = 1;   
		 }
	  };
	   
	   $scope.lastPage = function() {      
		 $scope.currentPage=$scope.pageCount();
	  };
	  
	  $scope.firstPage = function() {
		 $scope.currentPage=1;
	  };
	   
	   $scope.nextPage = function() {
		
		if ($scope.currentPage < $scope.pageCount()) {
			$scope.currentPage++;
		}
	  };
	  
	  $scope.jumlahPerpage = function (value) {
		$scope.numPerPage = value;
	  } 
	  
	  $scope.prevPage = function() {
		if ($scope.currentPage > 1) {
		  $scope.currentPage--;
		}
	  };
	  
	   $scope.pageCount = function() {
		return Math.ceil($scope.jumlah/$scope.numPerPage);
	  };  
	  
	  $scope.checkAll = function () {
		angular.forEach($scope.employee, function (item) {
			item.Selected = $scope.selectAll;
		});
	  };
		  
	});
	
	function MyCtrl($scope) {
		$scope.dateInput = new Date();
		$scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
	}
	

	app.filter("dateRange", function() {      
	  return function(items, dari, to) {
		if(dari.length==0){
			var dari = +new Date("1980-01-01");
		}else{
			var dari = +new Date(dari);
		}
		
		if(to.length==0){
			var to = +new Date();
		}else{
			var to = +new Date(to);
		}
		var df = dari;
		var dt = to ;
		var arrayToReturn = [];        
		for (var i=0; i<items.length; i++){
			var tf = +new Date(items[i].join);
			if ((tf > df && tf < dt) || (tf==dt) )  {
				arrayToReturn.push(items[i]);
			}
		}
		
		return arrayToReturn;
	  };
		
	});
	
	
	app.filter("paging", function() {      
	  return function(items, limit, currentPage) {
		if (typeof limit === 'string' || limit instanceof String){
		   limit = parseInt(limit);
		 }
		 
		 var begin, end, index;  
		 begin = (currentPage - 1) * limit;  
		 end = begin + limit;
		 var arrayToReturn = [];   
		 for (var i=0; i<items.length; i++){
			if (begin <= i && i < end )  {
				arrayToReturn.push(items[i]);
			}
		}
		return arrayToReturn;
	  };   
	});
	
	app.filter("departemen", function() {      
	  return function(items, depart) {
	  if(depart.length==0){
		return items;
	  }
	  var arrayToReturn = [];   
		 for (var i=0; i<items.length; i++){
			if (items[i].dept == depart )  {
				arrayToReturn.push(items[i]);
			}
		}
		return arrayToReturn;
	  };   
	});
	
	app.filter("nama", function() {      
	  return function(items, search) {
	  if(search.length==0){
		return items;
	  }
	  var arrayToReturn = [];   
		 for (var i=0; i<items.length; i++){
			if (items[i].name.toUpperCase().indexOf(search.toUpperCase()) != -1 )  {
				arrayToReturn.push(items[i]);
			}
		}
		return arrayToReturn;
	  };   
	});
	

</script>