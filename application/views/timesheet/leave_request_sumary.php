<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
	 <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"></i>LEAVE REQUEST</a></li>
            <li class="active">Leave Request Sumarry</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
					<div class="box-header">
                        <h3 class="box-title">LEAVE REQUEST | Leave Request Sumarry</h3>
                    </div>
					<div class="col-md-12 col-xs-12 col-sm-12" style="margin:20px;0px;20px;0px;">
						<div class="col-md-4">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Search</div>
							<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" placeholder="Enter ..." ng-model="search"/></div>
						</div>
					</div>
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                                <tr>
                                    <th >No</th>
									<th>Request Number</th>
									<th>Date</th>
									<th>Employee ID</th>
									<th>Employee Name</th>
									<th>Leave Avaiable</th>
									<th>Leave Take</th>
									<th>Detail</th>
									<th>Status</th>
                                </tr>
								<tr >
                                    <td>1</td>
									<td>1</td>
									<td>17-02-2016</td>
									<td>E_3</td>
									<td>Bhima</td>
									<td>10</td>
									<td>3</td>
									<td class="action">
										<center>
										   <a href="#" class="button"><i class="fa fa-search add">&nbsp;</i></a>
										</center>
									</td>
									<td class="action">
										<span class="label label-success" ng-show="true"><i class="fa fa-check">&nbsp;</i>Approved</span>
										<a href="<?php echo base_url() . 'add/leave_approval'; ?>"><span class="label label-danger" ng-show="false"><i class="fa fa-remove">&nbsp;</i>Waiting Approval</span></a>
									</td>
                                </tr>
								<tr >
                                    <td>2</td>
									<td>2</td>
									<td>17-02-2016</td>
									<td>E_4</td>
									<td>Badrun</td>
									<td>10</td>
									<td>3</td>
									<td class="action">
										<center>
										   <a href="#" class="button"><i class="fa fa-search add">&nbsp;</i></a>
										</center>
									</td>
									<td class="action">
										<span class="label label-success" ng-show="false"><i class="fa fa-check">&nbsp;</i>Approved</span>
										<a href="<?php echo base_url() . 'add/leave_approval'?>"><span class="label label-danger" ng-show="true"><i class="fa fa-remove">&nbsp;</i>Waiting Approval</span></a>
									</td>
                                </tr>
                        </table>
						
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

   $(document).ready(function() {
		$("a.button").click(function(){
			$.fancybox(
			'<div style="height:350px;width:450px;overflow:hidden;"><iframe src="<?php echo base_url(); ?>fix_asset/leave_request" frameborder="0" scrolling="no" style="width:100%;height:800px;margin-top:-200px;"></iframe></div>',
				{
					'autoDimensions'	: false,
					'width'         	: 'auto',
					'height'        	: 'auto',
					'transitionIn'		: 'none',
					'transitionOut'		: 'none'
				}
			);
		});
	});
	
	
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    
	
    

</script>
