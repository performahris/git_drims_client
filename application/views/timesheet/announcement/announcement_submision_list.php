<link href="../../asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Announcement</li>
            <li class="active">Submission</li>
            <li class="active">List</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Announcement | Submission | List</h3>
                        <hr>
                    </div>
                    <div class="box-body">
                        
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-8 col-md-8">
                                    <label for="searchby" class="col-md-12">Search By :</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Employee Name</option>
                                        <option>Title</option>
                                        <option>Selected User</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Employee Name</option>
                                        <option>Title</option>
                                        <option>Selected User</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Employee Name</option>
                                        <option>Title</option>
                                        <option>Selected User</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="operator">Operator</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <input type="radio" name="operator" class="flat-red" checked="true" value="AND"/> AND
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="operator" class="flat-red" value="OR"/> OR
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div>
                            <div class="col-xs-12 col-sm-8 col-md-4">
                                <a href="">
                                    <button type="button" class="btn btn-info btn-block"><i class="fa fa-search" data-toggle="tooltip" title="View">&nbsp;</i>Search</button>
                                </a>
                            </div>
                            </div>
                        </div>
                        <!--END OF SEARCH-->

                        <!--
                        <div class="col-md-12 col-xs-12 col-sm-12">
                                <select ng-model="depart" class="departemen" >
                                    <option value= "">List Of Department</option>
                                    <option value="{{ dept.nama }}" ng-repeat="dept in departemen">{{ dept.nama }}</option>
                                </select>
                                
                                <select ng-model="depart" class="departemen" >
                                    <option value= "">List Of EmployeeID</option>
                                    <option value="{{ emp.ID }}" ng-repeat="emp in employee">{{ emp.ID }}</option>
                                </select> 
                                
                                <select ng-model="depart" class="departemen" >
                                    <option value= "">List Of Joined Date</option>
                                    <option value="{{ joi.date }}" ng-repeat="joi in joined">{{ joi.date }}</option>
                                </select> 
                        </div>
                        -->
                        
                        <div class="col-md-12">
                            <div class="col-md-2 col-xs-12 xol-sm-12">
                                <a href="<?php echo base_url('add/announcement_submision_add'); ?>">
                                    <button type="button" class="btn btn-success btn-block"> New</button>
                                </a>
                            </div>
                        </div>                    
                  <div class="box-body table-responsive " style="width:100%">
                            <table class="table table-striped table-bordered">
                                <tr class="success">
                                    <th ><center>No</center></th>
                                    <th >
                                        <!--<a href="#" ng-click="sortType = 'name'; sortReverse = !sortReverse">-->
                                        <center>Employee Name</center>
                                        <span ng-show="sortType == 'name' && !sortReverse"></span>
                                        <span ng-show="sortType == 'name' && sortReverse"></span>
                                        <!--</a>-->
                                    </th>
                                    <th >
                                        <!--<a href="#" ng-click="sortType = 'join'; sortReverse = !sortReverse">-->
                                        <center>Title</center>
                                        <span ng-show="sortType == 'join' && !sortReverse"></span>
                                        <span ng-show="sortType == 'join' && sortReverse"></span>
                                        <!--</a>-->
                                    </th>
                                    <th >
                                        <!--<a<a href="#" ng-click="sortType = 'join'; sortReverse = !sortReverse">-->
                                        <center>Selected User</center>
                                        <span ng-show="sortType == 'join' && !sortReverse"></span>
                                        <span ng-show="sortType == 'join' && sortReverse"></span>
                                        <!--</a>-->
                                    </th>
                                    <th >
                                        <!--<a href="#" ng-click="sortType = 'join'; sortReverse = !sortReverse">-->
                                        <center>Status</center>
                                        <span ng-show="sortType == 'join' && !sortReverse"></span>
                                        <span ng-show="sortType == 'join' && sortReverse"></span>
                                        <!--</a>-->
                                    </th>
                                    <th >
                                        <!--<a href="#" ng-click="sortType = 'join'; sortReverse = !sortReverse">-->
                                        <center>Action</center>
                                        <span ng-show="sortType == 'join' && !sortReverse"></span>
                                        <span ng-show="sortType == 'join' && sortReverse"></span>
                                        <!--</a>-->
                                    </th>
                                    
                                    <!--<th><input type="checkbox" ng-model="selectAll" ng-click="checkAll()" /></th>-->
                            </tr>   
                            <tr>
                                    <td><center>1</center></td>
                                    <td>SUDARMONO</td>
                                    <td>Undangan Rapat</td>
                                    <td align="center">Selected</td>
                                    <td><center><span class="need_aproval_status">Need Aproval</span></center></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>view/announcement_submision_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    </td> 
                            </tr> 
                                    
                            <tr>
                                    <td><center>2</center></td>
                                    <td>ARBONDES</td>
                                    <td>Pemberitahuan Kegiatan Family Gathering</td>
                                    <td><center>All</center></td>
                                    <td><center><span class="draft_status">Draft</span></center></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>edit/announcement_submision_edit"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                    </td> 
                            </tr> 
                
                            <tr>
                                    <td><center>3</center></td>
                                    <td>ISKANDAR</td>
                                    <td>Acara Buka Puasa Bersama</td>
                                    <td><center>All</center></td>
                                    <td><center><span class="need_aproval_status">Need Aproval</span></center></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>view/announcement_submision_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    </td> 
                            </tr>
                    
                    
                            <tr>
                                    <td><center>4</center></td>
                                    <td>ANTON PRIADI</td>
                                    <td>Meeting HRD</td>
                                    <td align="center">Selected</td>
                                    <td><center><span class="need_aproval_status">Need Aproval</span></center></td>
                                    <td class="text-center">
                                       <a href="<?php echo base_url(); ?>view/announcement_submision_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a> 
                                    </td> 
                            </tr> 
                    
                
                            <tr>
                                    <td><center>5</center></td>
                                    <td>I KETUT ADI PUJA ASTAWA</td>
                                    <td>Kunjungan Ke Museum</td>
                                    <td><center>All</center></td>
                                    <td><center><span class="need_aproval_status">Need Aproval</span></center></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>view/announcement_submision_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    </td> 
                            </tr> 
                    
                            <tr>
                                    <td><center>6</center></td>
                                    <td>SUBARI.M</td>
                                    <td>Rapat Divisi</td>
                                    <td align="center">Selected</td>
                                    <td><center><span class="need_aproval_status">Need Aproval</span></center></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>view/announcement_submision_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    </td> 
                            </tr> 

                            <tr>
                                    <td><center>7</center></td>
                                    <td>SUNARDI</td>
                                    <td>Pelatihan Simulasi Kebakaran</td>
                                    <td><center>All</center></td>
                                    <td><center><span class="draft_status">Draft</span></center></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>edit/announcement_submision_edit"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                    </td> 
                            </tr> 
                    
                    
                            <tr>
                                    <td><center>8</center></td>
                                    <td>BAMBANG IRAWAN</td>
                                    <td>Pemutusan Koneksi Internet Sementara</td>
                                    <td><center>All</center></td>
                                    <td><center><span class="draft_status">Draft</span></center></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>edit/announcement_submision_edit"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                    </td> 
                            </tr> 

                            <tr>
                                    <td><center>9</center></td>
                                    <td>CHAIRUL AZWAN</td>
                                    <td>Rapat Gudang</td>
                                    <td align="center">Selected</td>
                                    <td><center><span class="draft_status">Draft</span></center></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>edit/announcement_submision_edit"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                    </td>
                            </tr> 
                                 
                                 
                            <tr>
                                    <td><center>10</center></td>
                                    <td>AHMAD SARURI</td>
                                    <td>Rapat Manager</td>
                                    <td align="center">Selected</td>
                                    <td><center><span class="draft_status">Draft</span></center></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>edit/announcement_submision_edit"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                    </td>
                            </tr>
                        </table>
                        
                        
                        <!--<div ng-show="false">{{jumlah = (employee | departemen:depart | nama:search | dateRange:dari:to ).length }}</div>-->
                        
                            <div class="col-xs-11 col-sm-12 col-md-12"><br>
                                <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                   <ul class="pagination">
                    <li class="paginate_button previous disabled" id="example1_previous"><a href="#" aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a></li>
                    <li class="paginate_button active"><a href="#" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a></li>
                    <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a></li>
                    <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" tabindex="0">3</a></li>
                    <li class="paginate_button next" id="example1_next"><a href="#" aria-controls="example1" data-dt-idx="7" tabindex="0">Next</a></li>
                   </ul></div>
                            </div>
                    </div>                  
                </div>
            </div>
        </div>

    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
</script>

