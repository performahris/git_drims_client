<div class="content-wrapper">
	<section class="content-header">
		<h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#">Ijin</a></li>
            <li class="active">List</li>
        </ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
                        <h3 class="box-title">Ijin | List</h3>
                        <hr/>
                    </div>
                    <div class="box-body"> <!-- BEGIN OF SEARCH -->
                        <div class="row form-group">
                            <label for="searchby" class="col-md-3">Search By :</label>
                        </div>
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Transfer Code</option>
                                        <option>Warehouse From</option>
                                        <option>Warehouse To</option>
                                        <option>Due Date</option>
                                        <option>Created By</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-2 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Transfer Code</option>
                                        <option>Warehouse From</option>
                                        <option>Warehouse To</option>
                                        <option>Due Date</option>
                                        <option>Created By</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Transfer Code</option>
                                        <option>Warehouse From</option>
                                        <option>Warehouse To</option>
                                        <option>Due Date</option>
                                        <option>Created By</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Transfer Code</option>
                                        <option>Warehouse From</option>
                                        <option>Warehouse To</option>
                                        <option>Due Date</option>
                                        <option>Created By</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="operator">Operator</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <input type="radio" name="operator" class="flat-red" checked="true" value="AND"/> AND
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="operator" class="flat-red" value="OR"/> OR
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div>
                            <div class="col-xs-12 col-sm-8 col-md-4">
                                <a href="">
                                    <button type="button" class="btn btn-info btn-block"><i class="fa fa-search"></i> Search</button>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-12">&nbsp;</div>
                        <!-- END OF SEARCH -->
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <a href="<?php echo base_url('add/ijin_pulang_cepat'); ?>">
                                    <button type="button" class="btn btn-success btn-block"><i class="fa fa-plus-circle"></i> Add</button>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-12">&nbsp;</div>
                        <div class="col-md-12">
                        <table class="table table-bordered table-striped">
                            
                                <tr class="success">
                                    <th class="text-center" rowspan="2">No</th>
                                    <th class="text-center" rowspan="2">Request Number</th>
                                    <th class="text-center" rowspan="2">Date</th>
                                    <th class="text-center" rowspan="2">Employee id</th>
                                    <th class="text-center" rowspan="2">Employee Name</th>
                                    <th class="text-center" rowspan="2">Category</th>
                                    <th class="text-center" colspan="2">Time</th>
                                    <th class="text-center" rowspan="2">Action</th>
                                    <th class="text-center" rowspan="2">Status</th>
                                </tr>
                                <tr class="success">
                                    <th><center>Form</center></th>
                                    <th><center>To</center></th>
                                </tr>
                            <tbody>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td>1</td>
                                    <td>16-Feb-2016</td>
                                    <td>E_1</td>
                                    <td class="text-center">Shinta</td>
                                    <td>Pulang Cepat</td>
                                    <td>14.00</td>
                                    <td>--.--</td>
                                    
                                    <td class="action">

                                        <a href="">
                                            <center><i class="fa fa-search fa-lg" data-toggle="modal" title="Detail-row" data-target="#myModal1"></i></center>
                                        </a>
                                        &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal1" role="dialog">
                                                <div class="modal-dialog modal-sm">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Information Detail</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <p>No                 : 1</p>
                                                          <p>Request Number     : 1</p>
                                                          <p>Date               : 16-Feb-2016</p>
                                                          <p>Employee ID        : E_1</p>
                                                          <p>Employee Name      : Shinta</p>
                                                          <p>Category           : Pulang Cepat</p>
                                                          <p>Time               : 14.00 - --.--</p>
                                                          <p>Status             : <span class="label label-success"><i class="fa fa-check">&nbsp;</i>Approve</span></p>
                                                        
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                    </td>
                                    <td class="text-center">
                                        <span class="label label-danger">Rejected</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">2</td>
                                    <td>2</td>
                                    <td>17-Feb-2016</td>
                                    <td>E_2</td>
                                    <td class="text-center">Dery</td>
                                    <td>Keluar</td>
                                    <td>10.00</td>
                                    <td>13.00</td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url('dashboard/stock_transfer_approval'); ?>"  data-toggle="tooltip" title="Approval">
                                            <i class="fa fa-search fa-lg"></i>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url('dashboard/stock_transfer_approval'); ?>"  data-toggle="tooltip" title="Approval">
                                            <span class="label label-warning">Need Approval</span>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">3</td>
                                    <td>3</td>
                                    <td>17-Feb-2016</td>
                                    <td>E_3</td>
                                    <td class="text-center">Bhima</td>
                                    <td>Keluar</td>
                                    <td>13.00</td>
                                    <td>15.00</td>
                                    
                                    <td class="text-center">
                                        <a href="<?php echo base_url('dashboard/stock_transfer_view'); ?>">
                                            <i class="fa fa-search fa-lg"></i>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <span class="label label-success">Approved</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">4</td>
                                    <td>4</td>
                                    <td>18-Feb-2016</td>
                                    <td>E_4</td>
                                    <td class="text-center">Badrizka</td>
                                    <td>Ijin Pulang Cepat</td>
                                    <td>10.00</td>
                                    <td>--.00</td>

                                    <td class="text-center">
                                        <a href="<?php echo base_url('dashboard/stock_transfer_view'); ?>">
                                            <i class="fa fa-search fa-lg"></i>
                                        </a>
                                    </td>
                                    <!--<td class="text-center">
                                        <a href="<?php echo base_url('dashboard/stock_transfer_edit'); ?>">
                                            <i class="fa fa-pencil-square fa-lg"></i>
                                        </a>
                                        &nbsp;
                                        <a href="#">
                                            <i class="fa fa-trash fa-lg"></i>
                                        </a>
                                    </td>-->
                                    <td class="text-center">
                                        <span class="label label-info">Draft</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">5</td>
                                    <td>5</td>
                                    <td>18-Feb-2016</td>
                                    <td>E_5</td>
                                    <td class="text-center">Indra</td>
                                    <td>Keluar</td>
                                    <td>12.00</td>
                                    <td>16.00</td>
                                    
                                    <td class="text-center">
                                        <a href="<?php echo base_url('dashboard/stock_transfer_view'); ?>">
                                            <i class="fa fa-search fa-lg"></i>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <span class="label label-success">Approved</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">6</td>
                                    <td>6</td>
                                    <td>19-Feb-2016</td>
                                    <td>E_6</td>
                                    <td class="text-center">Arip</td>
                                    <td>Ijin Pulang Cepat</td>
                                    <td>15.00</td>
                                    <td>--.--</td>
                                    
                                    <td class="text-center">
                                        <a href="<?php echo base_url('dashboard/stock_transfer_view'); ?>">
                                            <i class="fa fa-search fa-lg"></i>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <span class="label label-success">Approved</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">7</td>
                                    <td>7</td>
                                    <td>19-Feb-2016</td>
                                    <td>E_7</td>
                                    <td class="text-center">Davi</td>
                                    <td>Ijin Pulang Cepat</td>
                                    <td>09.00</td>
                                    <td>--.--</td>
                                    
                                    <td class="text-center">
                                        <a href="<?php echo base_url('dashboard/stock_transfer_view'); ?>">
                                            <i class="fa fa-search fa-lg"></i>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <span class="label label-success">Approved</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">8</td>
                                    <td>8</td>
                                    <td>25-Feb-2016</td>
                                    <td>E_8</td>
                                    <td class="text-center">Pratiwi</td>
                                    <td>Keluar</td>
                                    <td>13.00</td>
                                    <td>16.00</td>
                                    
                                    <td class="text-center">
                                        <a href="<?php echo base_url('dashboard/stock_transfer_view'); ?>">
                                            <i class="fa fa-search fa-lg"></i>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <span class="label label-success">Approved</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">9</td>
                                    <td>9</td>
                                    <td>07-Mar-2016</td>
                                    <td>E_9</td>
                                    <td class="text-center">Fahmi</td>
                                    <td>Keluar</td>
                                    <td>13.00</td>
                                    <td>16.30</td>
                                    
                                    <td class="text-center">
                                        <a href="<?php echo base_url('dashboard/stock_transfer_view'); ?>">
                                            <i class="fa fa-search fa-lg"></i>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <span class="label label-success">Approved</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">10</td>
                                    <td>10</td>
                                    <td>09-Mar-2016</td>
                                    <td>E_10</td>
                                    <td class="text-center">Devita</td>
                                    <td>Ijin Pulang Cepat</td>
                                    <td>15.00</td>
                                    <td>--.--</td>
                                    
                                    <td class="text-center">
                                        <a href="<?php echo base_url('dashboard/stock_transfer_view'); ?>">
                                            <i class="fa fa-search fa-lg"></i>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <span class="label label-success">Approved</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                        <div class="col-xs-11 col-sm-12 col-md-12">
                            <ul class="pagination pull-right" >
    						  <li class="disabled"><a href="#"><i class="fa fa-arrow-circle-left"></i></a></li>
    						  <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
    						  <li><a href="#">2</a></li>
    						  <li><a href="#">3</a></li>
    						  <li><a href="#">4</a></li>
    						  <li><a href="#">5</a></li>
    						  <li><a href="#"><i class="fa fa-arrow-circle-right"></i></a></li>
    						</ul>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</section>
</div>