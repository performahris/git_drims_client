<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"> SPPD</a></li>
            <li class="active">List SPPD</li>
            <li class="active">List</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
					<div class="box-header">
                        <h3 class="box-title">SPPD | List SPPD</h3>
                        <hr>
                    </div>
                                        <div class="box-body"> <!-- BEGIN OF SEARCH -->
                            <div class="col-md-12">
                                  <div class="row form-group">
                                      <label for="searchby" class="col-md-3">Search By :</label>
                                  </div>
                                  <div class="col-xs-12 col-sm-8 col-md-8">
                                      <div class="row form-group">
                                          <div class="col-xs-12 col-sm-5 col-md-5">
                                              <select class="form-control">
                                                  <option>-- Select Field Name --</option>
                                                  <option>No SPPD</option>
                                                  <option>Name</option>
                                                  <option>Division</option>
                                                  <option>Date</option>
                                                  <option>Status</option>
                                                  
                                              </select>
                                          </div>
                                          <div class="col-xs-12 col-sm-2 col-md-1">
                                              <label for="code">Value</label>
                                          </div>
                                          <div class="col-xs-12 col-sm-6 col-md-6">
                                              <input type="text" class="form-control" name=""/>
                                          </div>
                                      </div>
                                      <div class="row form-group">
                                          <div class="col-xs-12 col-sm-5 col-md-5">
                                              <select class="form-control">
                                                  <option>-- Select Field Name --</option>
                                                  <option>No SPPD</option>
                                                  <option>Name</option>
                                                  <option>Division</option>
                                                  <option>Date</option>
                                                  <option>Status</option>
                                              </select>
                                          </div>
                                          <div class="col-xs-12 col-sm-1 col-md-1">
                                              <label for="code">Value</label>
                                          </div>
                                          <div class="col-xs-12 col-sm-6 col-md-6">
                                              <input type="text" class="form-control" name=""/>
                                          </div>
                                      </div>
                                      <div class="row form-group">
                                          <div class="col-xs-12 col-sm-5 col-md-5">
                                              <select class="form-control">
                                                  <option>-- Select Field Name --</option>
                                                  <option>No SPPD</option>
                                                  <option>Name</option>
                                                  <option>Division</option>
                                                  <option>Date</option>
                                                  <option>Status</option>
                                              </select>
                                          </div>
                                          <div class="col-xs-12 col-sm-1 col-md-1">
                                              <label for="code">Value</label>
                                          </div>
                                          <div class="col-xs-12 col-sm-6 col-md-6">
                                              <input type="text" class="form-control" name=""/>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-xs-12 col-sm-4 col-md-4">
                                      <div class="col-xs-12 col-sm-12 col-md-12">
                                          <label for="operator">Operator</label>
                                      </div>
                                      <div class="col-xs-12 col-sm-12 col-md-12">
                                          <input type="radio" name="operator" class="flat-red" checked="true" value="AND"/> AND
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                          <input type="radio" name="operator" class="flat-red" value="OR"/> OR
                                      </div>
                                      <div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div>
                                      <div class="col-xs-12 col-sm-8 col-md-4">
                                          <a href="">
                                              <button type="button" class="btn btn-info btn-block"><i class="fa fa-search"></i>&nbsp; Search</button>
                                          </a>
                                      </div>
                                  </div>
                        </div>
                        <div class="col-md-12">&nbsp;</div>
                        <!-- END OF SEARCH -->
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <a href="<?php echo base_url('add/sppd_creation'); ?>">
                                    <button type="button" class="btn btn-success btn-block"> New</button>
                                </a>
                            </div>
                        </div>
            					<!--<div class="col-md-6 col-xs-12 col-sm-12" style="margin-top:20px;margin-bottom:20px;">
            						<div class="col-md-4">
            							<a href="<?php echo base_url()."add/sppd_creation" ?>"><input type="button" class="btn btn-block btn-success" value="new"></a>
            						</div>
            					</div>
            					<div class="col-md-6 col-xs-12 col-sm-12" style="margin-top:20px;margin-bottom:20px;">
            						<div class="col-md-8">
            							<div class="col-md-4 col-xs-12 col-sm-12 pull-left"></div>
            							<i><div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" placeholder="Search" ng-model="search"/></div></i>
            						</div>
                                    <div class="col-md-2 pull-left" >
                                        <a href="<?php echo base_url()."add/tunjangan_creation" ?>"><input type="button" class="btn btn-block btn-success" style="background-color:#316CFF;" value="GO"></a>
                                    </div>
            					</div>-->
                    <div class="box-body  table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                                <tr class="success">
                                    <th class="text-center">No</th>
                  									<th class="text-center">No SPPD</th>
                  									<th class="text-center">Name</th>
                  									<th class="text-center">Division</th>
                  									<th class="text-center">Date</th>
                  									<th class="text-center">Status</th>
                  									<th class="text-center">Action</th>
                                </tr>
								                <tr>
                                    <td align="center">1</td>
                  									<td align="center">01/HRD/01/2016</td>
                  									<td>Devita</td>
                  									<td>Production</td>
                  									<td>10 Feb 2016</td>
                  									<td>Rejected</td>
                  									<td align="center">

                                        <a><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal1"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal1" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Information Detail</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <p>No         : 1</p>
                                                          <p>No SPPD    : 01/HRD/01/2016</p>
                                                          <p>Name       : Shinta</p>
                                                          <p>Division   : Production</p>
                                                          <p>Date       : 10 Feb 2016</p>
                                                          <p>Status     : Rejected</p>
                                                        
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>

                										<!--<i class="fa fa-search">-->
                										&nbsp;
                										<a href="<?php echo base_url() . 'edit/sppd_edit' ?>" ><i class="fa fa-pencil"></i></i></a>
									               </td>
                              </tr>
                              <tr>
                                    <td align="center">2</td>
                                    <td align="center">02/HRD/02/2016</td>
                                    <td>Andrey</td>
                                    <td>Accounting</td>
                                    <td>18 Feb 2016</td>
                                    <td>Need Approval</td>
                                    <td align="center">
                                        <a><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal2"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal2" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Information Detail</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <p>No         : 2</p>
                                                          <p>No SPPD    : 02/HRD/02/2016</p>
                                                          <p>Name       : Dery</p>
                                                          <p>Division   : Accounting</p>
                                                          <p>Date       : 18 Feb 2016</p>
                                                          <p>Status     : Need Approval</p>
                                                        
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>

                                        <!--<i class="fa fa-search">-->
                                        &nbsp;
                                        <a href="<?php echo base_url() . 'edit/sppd_edit' ?>" ><i class="fa fa-pencil"></i></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">3</td>
                                    <td align="center">03/HRD/03/2016</td>
                                    <td>Ridwan</td>
                                    <td>Marketing</td>
                                    <td>21 Feb 2016</td>
                                    <td>Approved</td>
                                    <td align="center">
                                        <a><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal3"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal3" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Information Detail</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <p>No         : 3</p>
                                                          <p>No SPPD    : 03/HRD/03/2016</p>
                                                          <p>Name       : Bhima</p>
                                                          <p>Division   : Marketing</p>
                                                          <p>Date       : 21 Feb 2016</p>
                                                          <p>Status     : Approved</p>
                                                        
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>

                                        <!--<i class="fa fa-search">-->
                                        &nbsp;
                                        <a href="<?php echo base_url() . 'edit/sppd_edit' ?>" ><i class="fa fa-pencil"></i></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">4</td>
                                    <td align="center">04/HRD/04/2016</td>
                                    <td>Donny</td>
                                    <td>Human Resource</td>
                                    <td>07 Mar 2016</td>
                                    <td>Need Approval</td>
                                    <td align="center">
                                        <a><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal4"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal4" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Information Detail</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <p>No         : 4</p>
                                                          <p>No SPPD    : 04/HRD/04/2016</p>
                                                          <p>Name       : Badrizka</p>
                                                          <p>Division   : Human Resource</p>
                                                          <p>Date       : 07 Mar 2016</p>
                                                          <p>Status     : Need Approval</p>
                                                        
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>

                                        <!--<i class="fa fa-search">-->
                                        &nbsp;
                                        <a href="<?php echo base_url() . 'edit/sppd_edit' ?>" ><i class="fa fa-pencil"></i></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">5</td>
                                    <td align="center">05/HRD/05/2016</td>
                                    <td>Debi</td>
                                    <td>Project Coord. JOB PPEJ</td>
                                    <td>14 Mar 2016</td>
                                    <td>Approved</td>
                                    <td align="center">
                                        <a><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal5"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal5" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Information Detail</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <p>No         : 5</p>
                                                          <p>No SPPD    : 05/HRD/05/2016</p>
                                                          <p>Name       : Indra</p>
                                                          <p>Division   : Administrasi</p>
                                                          <p>Date       : 10 Mar 2016</p>
                                                          <p>Status     : Approved</p>
                                                        
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>

                                        <!--<i class="fa fa-search">-->
                                        &nbsp;
                                        <a href="<?php echo base_url() . 'edit/sppd_edit' ?>" ><i class="fa fa-pencil"></i></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">6</td>
                                    <td align="center">06/HRD/06/2016</td>
                                    <td>Arip</td>
                                    <td>Koord. Crew Supervisor</td>
                                    <td>18 Mar 2016</td>
                                    <td>Rejected</td>
                                    <td align="center">
                                        <a><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal6"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal6" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Information Detail</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <p>No         : 6</p>
                                                          <p>No SPPD    : 06/HRD/06/2016</p>
                                                          <p>Name       : Arip</p>
                                                          <p>Division   : Domestic</p>
                                                          <p>Date       : 18 Mar 2016</p>
                                                          <p>Status     : Rejected</p>
                                                        
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>

                                        <!--<i class="fa fa-search">-->
                                        &nbsp;
                                        <a href="<?php echo base_url() . 'edit/sppd_edit' ?>" ><i class="fa fa-pencil"></i></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">7</td>
                                    <td align="center">07/HRD/07/2016</td>
                                    <td>Anto</td>
                                    <td>HRGA Supervisor</td>
                                    <td>23 Mar 2016</td>
                                    <td>Approved</td>
                                    <td align="center">
                                        <a><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal7"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal7" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Information Detail</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <p>No         : 7</p>
                                                          <p>No SPPD    : 07/HRD/07/2016</p>
                                                          <p>Name       : Davi</p>
                                                          <p>Division   : Business Development</p>
                                                          <p>Date       : 23 Mar 2016</p>
                                                          <p>Status     : Approved</p>
                                                        
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>

                                        <!--<i class="fa fa-search">-->
                                        &nbsp;
                                        <a href="<?php echo base_url() . 'edit/sppd_edit' ?>" ><i class="fa fa-pencil"></i></i></a>
                                    </td>
                                </tr>
                                <!--<tr>
                                    <td align="center">8</td>
                                    <td align="center">08/HRD/08/2016</td>
                                    <td>Pratiwi</td>
                                    <td>Flight</td>
                                    <td>06 Apr 2016</td>
                                    <td>Need Approval</td>
                                    <td align="center">
                                        <a><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal8"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal8" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Information Detail</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <p>No         : 8</p>
                                                          <p>No SPPD    : 08/HRD/08/2016</p>
                                                          <p>Name       : Pratiwi</p>
                                                          <p>Division   : Flight</p>
                                                          <p>Date       : 06 Apr 2016</p>
                                                          <p>Status     : Need Approval</p>
                                                        
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>

                                        <!--<i class="fa fa-search">-->
                                       <!-- &nbsp;
                                        <a href="<?php echo base_url() . 'edit/sppd_edit' ?>" ><i class="fa fa-pencil"></i></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">9</td>
                                    <td align="center">09/HRD/09/2016</td>
                                    <td>Fahmi</td>
                                    <td>Finance</td>
                                    <td>13 Apr 2016</td>
                                    <td>Approved</td>
                                    <td align="center">
                                        <a><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal9"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal9" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Information Detail</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <p>No         : 9</p>
                                                          <p>No SPPD    : 09/HRD/09/2016</p>
                                                          <p>Name       : Fahmi</p>
                                                          <p>Division   : Finance</p>
                                                          <p>Date       : 13 Apr 2016</p>
                                                          <p>Status     : Approved</p>
                                                        
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>

                                        <!--<i class="fa fa-search">-->
                                        <!--&nbsp;
                                        <a href="<?php echo base_url() . 'edit/sppd_edit' ?>" ><i class="fa fa-pencil"></i></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">10</td>
                                    <td align="center">10/HRD/10/2016</td>
                                    <td>Devita</td>
                                    <td>Neo</td>
                                    <td>26 Apr 2016</td>
                                    <td>Rejected</td>
                                    <td align="center">
                                        <a><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal10"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal10" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Information Detail</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <p>No         : 10</p>
                                                          <p>No SPPD    : 10/HRD/10/2016</p>
                                                          <p>Name       : Devita</p>
                                                          <p>Division   : Neo</p>
                                                          <p>Date       : 26 Apr 2016</p>
                                                          <p>Status     : Rejected</p>
                                                        
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>

                                        <!--<i class="fa fa-search">-->
                                        <!--&nbsp;
                                        <a href="<?php echo base_url() . 'edit/sppd_edit' ?>" ><i class="fa fa-pencil"></i></i></a>
                                    </td>
                                </tr>-->
  
                        </table>
                    </div>
                        <div class="col-xs-11 col-sm-12 col-md-12">
                                <ul class="pagination pull-right" >
                                <li><a href="#">Previous</a></li>
                                <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">Next</a></li>
                                </ul>
                            </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
