<script>
    $(function() {
        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Man Power Request</li>
            <li class="active">Requested</li>
            <li class="active">List</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Man Power Request | Requested | List</h3>
                        <hr>
                    </div>

                    <!-- BEGIN OF SEARCH -->
                    <div class="box-body">
                        
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-8 col-md-8">
                                    <label for="searchby" class="col-md-12">Search By :</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Man Power Request Code</option>
                                        <option>Man Power Request Date</option>
                                        <option>Position</option>
                                        <option>Creator</option>
                                        <option>Departement</option>
                                        <option>Qty</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Man Power Request Code</option>
                                        <option>Man Power Request Date</option>
                                        <option>Position</option>
                                        <option>Creator</option>
                                        <option>Departement</option>
                                        <option>Qty</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Man Power Request Code</option>
                                        <option>Man Power Request Date</option>
                                        <option>Position</option>
                                        <option>Creator</option>
                                        <option>Departement</option>
                                        <option>Qty</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="operator">Operator</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <input type="radio" name="operator" class="flat-red" checked="true" value="AND"/> AND
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="operator" class="flat-red" value="OR"/> OR
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div>
                            <div class="col-xs-12 col-sm-8 col-md-4">
                                <a href="">
                                    <button type="button" class="btn btn-info btn-block"><i class="fa fa-search" data-toggle="tooltip" title="View">&nbsp;</i>Search</button>
                                </a>
                            </div>
                            </div>
                        </div>
                        <!--END OF SEARCH-->

                    <div class="col-md-6 col-xs-12 col-sm-12 pull-left" style="margin-top:5px;margin-bottom:5px;">
                        <div class="col-md-4">
                            <a href="<?php echo base_url('add/add_man_power_requested'); ?>"><input type="button" class="btn btn-block btn-success" value="New"></a>
                        </div>
                    </div>

                    <div class="box-body  table-responsive" style="width:100%">

                        <table class="table table-bordered table-striped">
                            <tr class="success"> 
                                <th class="text-center">No</th> 
                                <th class="text-center">Man Power Request Code</th>
                                <th class="text-center">Man Power Request Date</th>
                                <th class="text-center">Position</th>
                                <th class="text-center">Creator</th>
                                <th class="text-center">Departement</th>
                                <th class="text-center">Qty</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Action</th>
                            </tr>   
                            <tr >
                                <td class="text-center">1</td>
                                <td>E_1</td>
                                <td>10 October 2015</td>
                                <td>Staff</td>
                                <td>Mr.A</td>
                                <td>Research</td>
                                <td>5</td>
                                <td class="text-center">
                                    <span class="draft_status">Drafft</span>
                                </td>
                                <td >
                                    <a href="<?php echo base_url(); ?>edit/edit_man_power_requested"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                      <!--<a href="<?php echo base_url(); ?>edit/departement_edit/{{dat.departement_id}}"><i class="fa fa-edit"></i></a>
                                    -->
                                </td>  
                            </tr>
                            <tr >
                                <td class="text-center">2</td>
                                <td>E_2</td>
                                <td>23 December 2015</td>
                                <td>Staff</td>
                                <td>Mr.A</td>
                                <td>Produksi</td>
                                <td>5</td>
                                <td class="text-center">
                                    <span class="need_aproval_status">Need Approval</span>
                                </td>
                                <td >
                                    <a href="<?php echo base_url(); ?>view/man_power_request_needaproval"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                      <!--<a href="<?php echo base_url(); ?>edit/departement_edit/{{dat.departement_id}}"><i class="fa fa-edit"></i></a>
                                    -->
                                </td>  
                            </tr>
                            <tr >
                                <td class="text-center">3</td>
                                <td>E_3</td>
                                <td>11 October 2015</td>
                                <td>Staff</td>
                                <td>Mr.B</td>
                                <td>Keuangan</td>
                                <td>10</td>
                                <td class="text-center">
                                    <span class="close_status">Close</span>
                                </td>
                                <td >
                                     <a href="<?php echo base_url(); ?>view/man_power_request_closed"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo base_url(); ?>view/list_employee_on_mpr_close"><i class="fa fa-users"></i></a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo base_url(); ?>edit/edit_man_power_requested_closed"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                      <!--<a href="<?php echo base_url(); ?>edit/departement_edit/{{dat.departement_id}}"><i class="fa fa-edit"></i></a>
                                    -->
                                </td>  
                            </tr>
                            <tr >
                                <td class="text-center">4</td>
                                <td>E_4</td>
                                <td>12 October 2015</td>
                                <td>Staff</td>
                                <td>Mr.D</td>
                                <td>Pemasaran</td>
                                <td>7</td>
                                <td class="text-center">
                                    <span class="open_status">Open</span>
                                </td>
                                <td >
                                     <a href="<?php echo base_url(); ?>view/man_power_request_open"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo base_url(); ?>view/list_employee_on_mpr_open"><i class="fa fa-users"></i></a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo base_url(); ?>edit/edit_man_power_requested_open"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                      <!--<a href="<?php echo base_url(); ?>edit/departement_edit/{{dat.departement_id}}"><i class="fa fa-edit"></i></a>
                                    -->
                                </td>  
                            </tr>
                            <tr >
                                <td class="text-center">5</td>
                                <td>E_5</td>
                                <td>12 October 2015</td>
                                <td>Staff</td>
                                <td>Mr.D</td>
                                <td>Pemasaran</td>
                                <td>7</td>
                                <td class="text-center">
                                    <span class="open_status">Open</span>
                                </td>
                                <td >
                                     <a href="<?php echo base_url(); ?>view/man_power_request_open"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo base_url(); ?>view/list_employee_on_mpr_open"><i class="fa fa-users"></i></a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo base_url(); ?>edit/edit_man_power_requested_open"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                      <!--<a href="<?php echo base_url(); ?>edit/departement_edit/{{dat.departement_id}}"><i class="fa fa-edit"></i></a>
                                    -->
                                </td>  
                            </tr>
                            <tr >
                                <td class="text-center">6</td>
                                <td>E_6</td>
                                <td>12 October 2015</td>
                                <td>Staff</td>
                                <td>Mr.D</td>
                                <td>Pemasaran</td>
                                <td>7</td>
                                <td class="text-center">
                                    <span class="open_status">Open</span>
                                </td>
                                <td >
                                     <a href="<?php echo base_url(); ?>view/man_power_request_open"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo base_url(); ?>view/list_employee_on_mpr_open"><i class="fa fa-users"></i></a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo base_url(); ?>edit/edit_man_power_requested_open"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                      <!--<a href="<?php echo base_url(); ?>edit/departement_edit/{{dat.departement_id}}"><i class="fa fa-edit"></i></a>
                                    -->
                                </td>  
                            </tr>
                            <tr >
                                <td class="text-center">7</td>
                                <td>E_7</td>
                                <td>10 October 2015</td>
                                <td>Staff</td>
                                <td>Mr.A</td>
                                <td>Research</td>
                                <td>5</td>
                                <td class="text-center">
                                    <span class="draft_status">Drafft</span>
                                </td>
                                <td >
                                    <a href="<?php echo base_url(); ?>edit/edit_man_power_requested"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                      <!--<a href="<?php echo base_url(); ?>edit/departement_edit/{{dat.departement_id}}"><i class="fa fa-edit"></i></a>
                                    -->
                                </td>  
                            </tr>
                            <tr >
                                <td class="text-center">8</td>
                                <td>E_8</td>
                                <td>23 December 2015</td>
                                <td>Staff</td>
                                <td>Mr.A</td>
                                <td>Produksi</td>
                                <td>5</td>
                                <td class="text-center">
                                    <span class="need_aproval_status">Need Approval</span>
                                </td>
                                <td >
                                    <a href="<?php echo base_url(); ?>view/man_power_request_needaproval"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                      <!--<a href="<?php echo base_url(); ?>edit/departement_edit/{{dat.departement_id}}"><i class="fa fa-edit"></i></a>
                                    -->
                                </td>  
                            </tr>
                            <tr >
                                <td class="text-center">9</td>
                                <td>E_9</td>
                                <td>11 October 2015</td>
                                <td>Staff</td>
                                <td>Mr.B</td>
                                <td>Keuangan</td>
                                <td>10</td>
                                <td class="text-center">
                                    <span class="close_status">Close</span>
                                </td>
                                <td >
                                     <a href="<?php echo base_url(); ?>view/man_power_request_closed"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo base_url(); ?>view/list_employee_on_mpr_close"><i class="fa fa-users"></i></a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo base_url(); ?>edit/edit_man_power_requested_closed"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                      <!--<a href="<?php echo base_url(); ?>edit/departement_edit/{{dat.departement_id}}"><i class="fa fa-edit"></i></a>
                                    -->
                                </td>  
                            </tr>
                            <tr >
                                <td class="text-center">10</td>
                                <td>E_10</td>
                                <td>12 October 2015</td>
                                <td>Staff</td>
                                <td>Mr.D</td>
                                <td>Pemasaran</td>
                                <td>7</td>
                                <td class="text-center">
                                    <span class="open_status">Open</span>
                                </td>
                                <td >
                                     <a href="<?php echo base_url(); ?>view/man_power_request_open"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo base_url(); ?>view/list_employee_on_mpr_open"><i class="fa fa-users"></i></a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo base_url(); ?>edit/edit_man_power_requested_open"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                      <!--<a href="<?php echo base_url(); ?>edit/departement_edit/{{dat.departement_id}}"><i class="fa fa-edit"></i></a>
                                    -->
                                </td>  
                            </tr>
                        </table>

<!-----------------------------------------------------------------------------ORIGINAL----------------------------------------------------------------------------->

                        <!--<table class="table table-bordered table-striped">
                            <tr class="success"> 
                                <th >No</th> 
                                <th >Man Power Request Code</th>
                                <th >Man Power Request Date</th>
                                <th >Position</th>
                                <th >
                                    <a href="#" ng-click="sortType = 'creator';
                                                sortReverse = !sortReverse">
                                        Creator
                                        <span ng-show="sortType == 'creator' && !sortReverse" class="fa fa-caret-down"></span>
                                        <span ng-show="sortType == 'creator' && sortReverse" class="fa fa-caret-up"></span>
                                    </a>
                                </th>
                                <th >
                                    <a href="#" ng-click="sortType = 'dept';
                                                sortReverse = !sortReverse">
                                        Departement
                                        <span ng-show="sortType == 'dept' && !sortReverse" class="fa fa-caret-down"></span>
                                        <span ng-show="sortType == 'dept' && sortReverse" class="fa fa-caret-up"></span>
                                    </a>
                                </th>
                                <th >Qty</th>
                                <th>Status</th>
                                <th >Action</th>
                            </tr>	
                            <tr ng-repeat="m in mpr| orderBy:sortType:sortReverse | search:search">
                                <td>{{$index + 1}}</td>
                                <td>{{m.id}}</td>
                                <td>{{m.tanggal|date:'dd MMMM yyyy'}}</td>
                                <td>{{m.posisi}}</td>
                                <td>{{m.creator}}</td>
                                <td>{{m.dept}}</td>
                                <td>{{m.qty}}</td>
                                <td>
                                    <span class="label label-info" ng-show="'{{m.status}}'== 'drafft'">Closed</span>
                                    <span class="label label-warning" ng-show="'{{m.status}}'== 'need_approval'">Closed</span>
                                    <span class="label label-danger" ng-show="'{{m.status}}'== 'closed'">Closed</span>
                                    <span class="label label-success" ng-show="'{{m.status}}'== 'open'">Open</span>
                                </td>
                                <td >
                                     <a href="<?php echo base_url(); ?>view/man_power_request"><i class="fa fa-search"></i></a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo base_url(); ?>dashboard/list_employee_on_mpr/{{m.id}}"><i class="fa fa-users"></i></a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo base_url(); ?>edit/man_power_edit"><i class="fa fa-edit"></i></a>
                                      <!--<a href="<?php echo base_url(); ?>edit/departement_edit/{{dat.departement_id}}"><i class="fa fa-edit"></i></a>
                                    -->
                            <!--</td>  
                            </tr> 
                        </table> -->
<!-----------------------------------------------------------------------------BATAS ORIGINAL----------------------------------------------------------------------------->

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $(".departemen").select2();
    });

    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
        $scope.sortType = 'no'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order
        $scope.search = '';
        $scope.limit = 25;
        $scope.depart = "";
        $scope.jumlah = 0;

        $scope.mpr = [
            {id: 'E1', name: 'xxx', dept: 'Research', creator: "Mr. A", status: 'open', tanggal: '2015-10-10', posisi: 'Staff', qty: '5'},
            {id: 'E2', name: 'yyy', dept: 'Research', creator: "Mr. A", status: 'open', tanggal: '2015-12-23', posisi: 'Staff', qty: '5'},
            {id: 'E3', name: 'qqq', dept: 'Research', creator: "Mr. B", status: 'open', tanggal: '2015-10-11', posisi: 'Staff', qty: '7'},
            {id: 'E4', name: 'www', dept: 'Research', creator: "Mr. D", status: 'open', tanggal: '2015-10-12', posisi: 'Staff', qty: '9'},
            {id: 'E5', name: 'eee', dept: 'Research', creator: "Mr. C", status: 'open', tanggal: '2015-10-13', posisi: 'Staff', qty: '5'},
            {id: 'E6', name: 'rrr', dept: 'Research', creator: "Mr. D", status: 'open', tanggal: '2015-10-18', posisi: 'Staff', qty: '10'},
            {id: 'E7', name: 'ttt', dept: 'HRD', creator: "Mr. A", status: 'open', tanggal: '2015-10-29', posisi: 'Staff', qty: '5'},
            {id: 'E8', name: 'uuu', dept: 'HRD', creator: "Mr. A", status: 'open', tanggal: '2015-10-01', posisi: 'Staff', qty: '90'},
            {id: 'E9', name: 'jjj', dept: 'Purchasing', creator: "Mr. A", status: 'open', tanggal: '2015-10-20', posisi: 'Staff', qty: '5'},
            {id: 'E10', name: 'mmm', dept: 'HRD', creator: "Mr. A", status: 'closed', tanggal: '2015-10-10', posisi: 'Staff', qty: '5'}
        ];

        $scope.currentPage = 1;
        $scope.totalItems = $scope.mpr.length;
        $scope.numPerPage = $scope.limit;

        $scope.limitPage = function() {
            $scope.numPerPage = $scope.limit;
            if ($scope.currentPage * $scope.numPerPage > $scope.mpr.length) {
                $scope.currentPage = 1;
            }
        };

        $scope.lastPage = function() {
            $scope.currentPage = $scope.pageCount();
        };

        $scope.firstPage = function() {
            $scope.currentPage = 1;
        };

        $scope.nextPage = function() {

            if ($scope.currentPage < $scope.pageCount()) {
                $scope.currentPage++;
            }
        };

        $scope.jumlahPerpage = function(value) {
            $scope.numPerPage = value;
        }

        $scope.prevPage = function() {
            if ($scope.currentPage > 1) {
                $scope.currentPage--;
            }
        };

        $scope.pageCount = function() {
            return Math.ceil($scope.jumlah / $scope.numPerPage);
        };
    });

    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }

    app.filter("search", function() {
        return function(items, search) {
            if (search.length == 0) {
                return items;
            }
            var arrayToReturn = [];
            for (var i = 0; i < items.length; i++) {
                if (items[i].id.toUpperCase().indexOf(search.toUpperCase()) != -1) {
                    arrayToReturn.push(items[i]);
                }
            }
            return arrayToReturn;
        };
    });





</script>
