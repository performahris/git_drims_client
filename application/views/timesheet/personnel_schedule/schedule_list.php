<link href="../../asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<div class="content-wrapper">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Personnel Schedule</li>
            <li class="active">Schedule</li>
            <li class="active">List</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Personnel Schedule | Schedule | List</h3>
                        <hr>
                    </div>
                        <div class="box-body"> <!-- BEGIN OF SEARCH -->
                         <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-8 col-md-8">
                                    <label for="searchby" class="col-md-12">Search By :</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Project</option>   
                                        <option>Lokasi</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Project</option>   
                                        <option>Lokasi</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Project</option>   
                                        <option>Lokasi</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="operator">Operator</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <input type="radio" name="operator" class="flat-red" checked="true" value="AND"/> AND
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="operator" class="flat-red" value="OR"/> OR
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div>
                            <div class="col-xs-12 col-sm-8 col-md-4">
                                <a href="">
                                    <button type="button" class="btn btn-info btn-block"><i class="fa fa-search" data-toggle="tooltip" title="View">&nbsp;</i>Search</button>
                                </a>
                            </div>
                            </div>
                        </div>

                    <div class="box-body  table-responsive">        
                        <table class="table table-stripped table-bordered" >
                            <tr class="success">
                           
                                <th style="width:5%" class="text-center">No</th>
                                <th style="width:40%" class="text-center">Project</th>                                
                                <th style="width:40%" class="text-center">Lokasi</th>
                                <th style="width:15%" class="text-center">Action</th>
                               
                            </tr>
                            <tr>
                                <td class="text-center">1</td>
                                <td class="text-center">Cikande Yart</td>       
                                <td class="text-center">Cikande</td>
                                <td>
                                    <a href="<?php echo base_url('view/schedule_view'); ?>" ><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">2</td>
                                <td class="text-center">Cikande Yart</td>
                                <td class="text-center">Cikande</td>
                                <td>
                                    <a href="<?php echo base_url('view/schedule_view'); ?>" ><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">3</td>
                                <td class="text-center">Cikande Yart</td>                                
                                <td class="text-center">Cikande</td>
                                <td>
                                    <a href="<?php echo base_url('view/schedule_view'); ?>" ><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">4</td>
                                <td class="text-center">Cikande Yart</td>                                
                                <td class="text-center">Cikande</td>
                                <td>
                                    <a href="<?php echo base_url('view/schedule_view'); ?>" ><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">5</td>
                                <td class="text-center">Cikande Yart</td>                                
                                <td class="text-center">Cikande</td>
                                <td>
                                    <a href="<?php echo base_url('view/schedule_view'); ?>" ><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">6</td>
                                <td class="text-center">Cikande Yart</td>                                
                                <td class="text-center">Cikande</td>
                                <td>
                                    <a href="<?php echo base_url('view/schedule_view'); ?>" ><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">7</td>
                                <td class="text-center">Cikande Yart</td>                                
                                <td class="text-center">Cikande</td>
                                <td>
                                    <a href="<?php echo base_url('view/schedule_view'); ?>" ><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">8</td>
                                <td class="text-center">Cikande Yart</td>                                
                                <td class="text-center">Cikande</td>
                                <td>
                                    <a href="<?php echo base_url('view/schedule_view'); ?>" ><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">9</td>
                                <td class="text-center">Cikande Yart</td>                                
                                <td class="text-center">Cikande</td>
                                <td>
                                    <a href="<?php echo base_url('view/schedule_view'); ?>" ><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">10</td>
                                <td class="text-center">Cikande Yart</td>                                
                                <td class="text-center">Cikande</td>
                                <td>
                                    <a href="<?php echo base_url('view/schedule_view'); ?>" ><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                </td>
                            </tr>
                        </table>
                        <div class="col-xs-11 col-sm-12 col-md-12"><br>
                            <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                                <ul class="pagination">
                                    <li class="paginate_button previous disabled" id="example1_previous"><a href="#" aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a></li>
                                    <li class="paginate_button active"><a href="#" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a></li>
                                    <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a></li>
                                    <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" tabindex="0">3</a></li>
                                    <li class="paginate_button next" id="example1_next"><a href="#" aria-controls="example1" data-dt-idx="7" tabindex="0">Next</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>    
                </div>
            </div>
        </div>
    </section>
</div>