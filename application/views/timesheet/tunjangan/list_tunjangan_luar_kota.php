<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"> Tunjangan</a></li>
            <li class="active">List of Tunjangan Dinas Luar Kota</li>
            <li class="active">List</li>
        </ol>
    </section>
    <section class="content">
        <div class="row" style="width:100%">
            <div class="col-xs-12">
                <div class="box box-primary">
					<div class="box-header">
                        <h3 class="box-title">Tunjangan | List of Tunjangan Dinas Luar Kota</h3>
                        <hr>
                    </div>
                    <div class="box-body"> <!-- BEGIN OF SEARCH -->
                		<div class="col-md-12">
                        <div class="row form-group">
                            <label for="searchby" class="col-md-3">Search By :</label>
                        </div>
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Category</option>
                                        <option>Hotel</option>
                                        <option>Transport</option>
                                        <option>Uang Makan/hari</option>
                                        <option>Uang taksi (P.P)</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-2 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Category</option>
                                        <option>Hotel</option>
                                        <option>Transport</option>
                                        <option>Uang Makan/hari</option>
                                        <option>Uang taksi (P.P)</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Category</option>
                                        <option>Hotel</option>
                                        <option>Transport</option>
                                        <option>Uang Makan/hari</option>
                                        <option>Uang taksi (P.P)</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="operator">Operator</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <input type="radio" name="operator" class="flat-red" checked="true" value="AND"/> AND
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="operator" class="flat-red" value="OR"/> OR
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div>
                            <div class="col-xs-12 col-sm-8 col-md-4">
                                <a href="">
                                    <button type="button" class="btn btn-info btn-block"><i class="fa fa-search"></i>&nbsp; Search</button>
                                </a>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12">&nbsp;</div>
                        <!-- END OF SEARCH -->
					<!--<div class="col-md-6 col-xs-12 col-sm-12" style="margin-top:20px;margin-bottom:20px;">
						<div class="col-md-4">
							<a href="<?php echo base_url()."add/tunjangan_creation" ?>"><input type="button" class="btn btn-block btn-success" value="new"></a>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 col-sm-12" style="margin-top:20px;margin-bottom:20px;">
						<div class="col-md-8">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left"></div>
							<i><div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" placeholder="search" ng-model="search"/></div></i>
						</div>
						<div class="col-md-2 pull-left" >
							<a href="<?php echo base_url()."add/tunjangan_creation" ?>"><input type="button" class="btn btn-block btn-success" style="background-color:#316CFF;" value="GO"></a>
						</div>
					</div>-->
                    <div class="box-body  table-responsive">
                        <table id="example1" class="table table-bordered table-striped" width="100%">
                            <tr class="success">
									<th><center>No</center></th>
									<th width="10%"><center>Category</center></th>
									<th width="10%"><center>Hotel</center></th>
									<th width="40%"><center>Transport</center></th>
									<th width="20%"><center>Uang makan/hari</center></th>
									<th width="20%"><center>uang taksi(P.P)</center></th>
							</tr>
							<tr>
								<td align="center">1</td>
								<td><input type="text" class="form-control" id="bangsa1" value="A" readonly="true"></td>
								<td><input type="text" class="form-control" id="bangsa2" value="On Bill" readonly="true"></td>
								<td><input type="text" class="form-control" id="bangsa3" value="On Bill" readonly="true"></td>
								<td><input type="text" class="form-control" id="bangsa4" value="On Bill" readonly="true"></td>
								<td><input type="text" class="form-control" id="bangsa5" value="-" readonly="true"></td>
								<!--<td>
									<a href="<?php echo base_url()."edit/tunjangan";?>"><i class="fa fa-pencil"></i></a>
								</td>-->
							</tr>
							<tr>
								<td align="center">2</td>
								<td><input type="text" class="form-control" id="category" value="B" readonly="true"></td>
								<td><input type="text" class="form-control" id="hotel" value="650.000" readonly="true"></td>
								<td><input type="text" class="form-control" id="transport" value="Pesawat Udara" readonly="true"></td>
								<td><input type="text" class="form-control" id="uangmakan" value="On Bill" readonly="true"></td>
								<td><input type="text" class="form-control" id="uangtaksi" value="On Bill" readonly="true"></td>
							</tr>
							<tr>
								<td align="center">3</td>
								<td><input type="text" class="form-control" id="category" value="C" readonly="true"></td>
								<td><input type="text" class="form-control" id="hotel" 	value="650.000" readonly="true"></td>
								<td><input type="text" class="form-control" id="transport" value="Pesawat Udara/Kereta Api/Kapal Laut/Bus" readonly="true"></td>
								<td><input type="text" class="form-control" id="uangmakan" value="100.000" readonly="true" readonly="true"></td>
								<td><input type="text" class="form-control" id="uangtaksi" value="200.000" readonly="true"></td>
							</tr>
							<tr>
								<td align="center">4</td>
								<td><input type="text" class="form-control" id="category" value="D"></td>
								<td><input type="text" class="form-control" id="hotel" value="450.000"></td>
								<td><input type="text" class="form-control" id="transport" value="Pesawat Udara/Kereta Api/Kapal Laut/Bus"></td>
								<td><input type="text" class="form-control" id="uangmakan" value="75.000"></td>
								<td><input type="text" class="form-control" id="uangtaksi" value="200.000"></td>
							</tr>
							<tr>
								<td align="center">5</td>
								<td><input type="text" class="form-control" id="category" value="E"></td>
								<td><input type="text" class="form-control" id="hotel" value="350.000"></td>
								<td><input type="text" class="form-control" id="transport" value="Pesawat Udara/Kereta Api/Kapal Laut/Bus"></td>
								<td><input type="text" class="form-control" id="uangmakan" value="75.000"></td>
								<td><input type="text" class="form-control" id="uangtaksi" value="200.000"></td>
							</tr>
							<tr>
								<td align="center">6</td>
								<td><input type="text" class="form-control" id="category" value="F"></td>
								<td><input type="text" class="form-control" id="hotel" value="250.000"></td>
								<td><input type="text" class="form-control" id="transport" value="Kereta Api/Kapal Laut/Bus"></td>
								<td><input type="text" class="form-control" id="uangmakan" value="50.000"></td>
								<td><input type="text" class="form-control" id="uangtaksi" value="200.000"></td>
							</tr>
							<tr>
								<td align="center">7</td>
								<td><input type="text" class="form-control" id="category" value="G"></td>
								<td><input type="text" class="form-control" id="hotel" value="750.000"></td>
								<td><input type="text" class="form-control" id="transport" value="Pesawat Udara"></td>
								<td><input type="text" class="form-control" id="uangmakan" value="150.000"></td>
								<td><input type="text" class="form-control" id="uangtaksi" value="200.000"></td>
							</tr>
							<tr>
								<td align="center">8</td>
								<td><input type="text" class="form-control" id="category" value="H"></td>
								<td><input type="text" class="form-control" id="hotel" value="350.000"></td>
								<td><input type="text" class="form-control" id="transport" value="Kereta Api"></td>
								<td><input type="text" class="form-control" id="uangmakan" value="100.000"></td>
								<td><input type="text" class="form-control" id="uangtaksi" value="200.000"></td>
							</tr>
							<tr>
								<td align="center">9</td>
								<td><input type="text" class="form-control" id="category" value="I"></td>
								<td><input type="text" class="form-control" id="hotel" value="350.000"></td>
								<td><input type="text" class="form-control" id="transport" value="Bus"></td>
								<td><input type="text" class="form-control" id="uangmakan" value="50.000"></td>
								<td><input type="text" class="form-control" id="uangtaksi" value="200.000"></td>
							</tr>
							<tr>
								<td align="center">10</td>
								<td><input type="text" class="form-control" id="category" value="J"></td>
								<td><input type="text" class="form-control" id="hotel" value="500.000"></td>
								<td><input type="text" class="form-control" id="transport" value="Kapal Laut"></td>
								<td><input type="text" class="form-control" id="uangmakan" value="150.000"></td>
								<td><input type="text" class="form-control" id="uangtaksi" value="200.000"></td>
							</tr>
							
                        </table>
                        <br>
                        <div class="col-md-2">
							<a href="<?php echo base_url()."dashboard/list_tunjangan_luar_kota" ?>"><input type="button" class="btn btn-block btn-success" value="Save"></a>
						</div>

                        <!--<ul class="pagination pagination-sm no-margin pull-right">
									<li><a href="#">«</a></li>
									<li><a href="#">1</a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
									<li><a href="#">»</a></li>
						</ul>-->

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
