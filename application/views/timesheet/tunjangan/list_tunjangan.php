<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
             <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Tunjangan</li>
            <li class="active">Requested</li>
            <li class="active">List</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
					<div class="box-header">
                        <h3 class="box-title">Tunjangan | Requested | List</h3>
                        <hr>
                    </div>
                    <div class="box-body"> 

                    <!-- BEGIN OF SEARCH -->
                         <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-8 col-md-8">
                                    <label for="searchby" class="col-md-12">Search By :</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Employee Code</option>
                                        <option>Employee Name</option>
                                        <option>Group</option>
                                        <option>Category</option>
                                        <option>Value</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Employee Code</option>
                                        <option>Employee Name</option>
                                        <option>Group</option>
                                        <option>Category</option>
                                        <option>Value</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Employee Code</option>
                                        <option>Employee Name</option>
                                        <option>Group</option>
                                        <option>Category</option>
                                        <option>Value</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="operator">Operator</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <input type="radio" name="operator" class="flat-red" checked="true" value="AND"/> AND
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="operator" class="flat-red" value="OR"/> OR
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div>
                            <div class="col-xs-12 col-sm-8 col-md-4">
                                <a href="">
                                    <button type="button" class="btn btn-info btn-block"><i class="fa fa-search" data-toggle="tooltip" title="View">&nbsp;</i>Search</button>
                                </a>
                            </div>
                            </div>
                        </div>
                        <!--END OF SEARCH-->

                        <div class="col-md-12">
                            <div class="col-md-2">
                                <a href="<?php echo base_url('add/tunjangan_requested_add'); ?>">
                                    <button type="button" class="btn btn-success btn-block"> New</button>
                                </a>
                            </div>
                        </div>
					<!--<div class="col-md-6 col-xs-12 col-sm-12" style="margin-top:20px;margin-bottom:20px;">
						<div class="col-md-4">
							<a href="<?php echo base_url()."add/tunjangan_creation" ?>"><input type="button" class="btn btn-block btn-success" value="new"></a>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 col-sm-12" style="margin-top:20px;margin-bottom:20px;">
						<div class="col-md-8">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left"></div>
							<i><div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" placeholder="Search" ng-model="search"/></div></i>
						</div>
						<div class="col-md-2 pull-left" >
							<a href="<?php echo base_url()."add/tunjangan_creation" ?>"><input type="button" class="btn btn-block btn-success" style="background-color:#316CFF;" value="GO"></a>
						</div>
					</div>-->
                    <div class="box-body  table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <tr class="success">
								<th><center>No</center></th>
                                <th><center>Employee Code</center></th>
                                <th><center>Employee Name</center></th>
                                <th><center>Group</center></th>
                                <th><center>Category</center></th>
								<th><center>Value</center></th>
                                <th><center>Status</center></th>
								<th><center>Action</center></th>
							</tr>
							<tr>
								<td align="center">1</td>
                                <td align="center">E_1</td>
                                <td>SUDARMONO</td>
                                <td >Non Staff</td>
								<td>Tunjangan Perkawinan</td>
								<td align="center"><?php echo number_format("2000000", "0", ",", "."); ?></td>
								<td><center><span class="need_aproval_status">Need Aproval</span></center></td>
                                <td align="center">
									<a href="<?php echo base_url()."view/tunjangan_requested_view";?>"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
								</td>
							</tr>
							<tr>
								<td align="center">2</td>
                                <td align="center">E_2</td>
                                <td>ARBONDES</td>
                                <td>Staff</td>
                                <td>Tunjangan Perkawinan</td>
								<td align="center"><?php echo number_format("4000000", "0", ",", "."); ?></td>
								<td><center><span class="draft_status">Draft</span></center></td>
                                <td align="center">
									 <a href="<?php echo base_url(); ?>edit/tunjangan_requested_edit"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
								</td>
							</tr>
							<tr>
								<td align="center">3</td>
                                <td align="center">E_3</td>
                                <td>ISKANDAR</td>
                                <td >Senior Staff/Supervisor</td>
								<td>Tunjangan Perkawinan</td>
                                <td align="center"><?php echo number_format("7000000", "0", ",", "."); ?></td>
								<td><center><span class="need_aproval_status">Need Aproval</span></center></td>
                                <td align="center">
									<a href="<?php echo base_url()."view/tunjangan_requested_view";?>"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
								</td>
							</tr>
							<tr>
								<td align="center">4</td>
                                <td align="center">E_4</td>
                                <td>ANTON PRIADI</td>
								<td>Manager</td>
                                <td>Tunjangan Kematian (Suami/Istri)</td>
								<td align="center"><?php echo number_format("10000000", "0", ",", "."); ?></td>
								<td><center><span class="need_aproval_status">Need Aproval</span></center></td>
                                <td align="center">
									<a href="<?php echo base_url()."view/tunjangan_requested_view";?>"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
								</td>
							</tr>
							<tr>
								<td align="center">5</td>
                                <td align="center">E_5</td>
                                <td>I KETUT ADI PUJA ASTAWA</td>
								<td>Senior Staff/Supervisor</td>
                                <td>Tunjangan Kematian (Suami/Istri)</td>
								<td align="center"><?php echo number_format("7000000", "0", ",", "."); ?></td>
                                <td><center><span class="need_aproval_status">Need Aproval</span></center></td>
								<td align="center">
									<a href="<?php echo base_url()."view/tunjangan_requested_view";?>"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
								</td>
							</tr>
							<tr>
								<td align="center">6</td>
                                <td align="center">E_6</td>
                                <td>SUBARI.M</td>
                                <td>Staff</td>
                                <td>Tunjangan Kematian (Suami/Istri)</td>
								<td align="center"><?php echo number_format("4000000", "0", ",", "."); ?></td>
								<td><center><span class="need_aproval_status">Need Aproval</span></center></td>
                                <td align="center">
									<a href="<?php echo base_url()."view/tunjangan_requested_view";?>"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
								</td>
							</tr>
							<tr>
								<td align="center">7</td>
                                <td align="center">E_7</td>
                                <td>SUNARDI</td>
								<td>Non Staff</td>
                                <td>Tunjangan Kematian (Anak / Ayah / Ibu)</td>
								<td align="center"><?php echo number_format("1000000", "0", ",", "."); ?></td>
								<td><center><span class="draft_status">Draft</span></center></td>
                                <td align="center">
									<a href="<?php echo base_url(); ?>edit/tunjangan_requested_edit"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
								</td>
							</tr>
                            <tr>
                                <td align="center">8</td>
                                <td align="center">E_8</td>
                                <td>BAMBANG IRAWAN</td>
                                <td>Staff</td>
                                <td>Tunjangan Kematian (Anak / Ayah / Ibu)</td>
                                <td align="center"><?php echo number_format("2000000", "0", ",", "."); ?></td>
                                <td><center><span class="draft_status">Draft</span></center></td>
                                <td align="center">
                                   <a href="<?php echo base_url(); ?>edit/tunjangan_requested_edit"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">9</td>
                                <td align="center">E_9</td>
                                <td>CHAIRUL AZWANN</td>
                                <td>Senior Staff</td>
                                <td>Tunjangan Kematian (Anak / Ayah / Ibu)</td>
                                <td align="center"><?php echo number_format("3500000", "0", ",", "."); ?></td>
                                <td><center><span class="draft_status">Draft</span></center></td>
                                <td align="center">
                                   <a href="<?php echo base_url(); ?>edit/tunjangan_requested_edit"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">10</td>
                                <td align="center">E_10</td>
                                <td>AHMAD SARURI</td>
                                <td>Manager</td>
                                <td>Tunjangan Kematian (Anak / Ayah / Ibu)</td>
                                <td align="center"><?php echo number_format("5000000", "0", ",", "."); ?></td>
                                <td><center><span class="draft_status">Draft</span></center></td>
                                <td align="center">
                                 <a href="<?php echo base_url(); ?>edit/tunjangan_requested_edit"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    	<div class="col-xs-11 col-sm-12 col-md-12">
                            <ul class="pagination pull-right" >
    						  <li><a href="#">Previous</a></li>
    						  <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
    						  <li><a href="#">2</a></li>
    						  <li><a href="#">3</a></li>
    						  <li><a href="#">Next</a></li>
    						</ul>
                        </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
