<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"></i> Leave</a></li>
            <li class="active">Leave List</li>
        </ol>
    </section>
	
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
					<div class="box-header">
                        <h3 class="box-title">Training | Training Requested List</h3>
                    </div>
                    <div class="box-body"> <!-- BEGIN OF SEARCH -->
                      <div class="col-md-12">
                        <div class="row form-group">
                            <label for="searchby" class="col-md-3">Search By :</label>
                        </div>
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Employee Name</option>
                                        <option>Division</option>
                                        <option>Topic</option>
                                        <option>Date</option>
                                        <option>Place</option>
                                        <option>Total</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-2 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Request Number</option>
                                        <option>Date</option>
                                        <option>Employee ID</option>
                                        <option>Employee Name</option>
                                        <option>Sisa Cuti</option>
                                        <option>Ambil Cuti</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Request Number</option>
                                        <option>Date</option>
                                        <option>Employee ID</option>
                                        <option>Employee Name</option>
                                        <option>Sisa Cuti</option>
                                        <option>Ambil Cuti</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="operator">Operator</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <input type="radio" name="operator" class="flat-red" checked="true" value="AND"/> AND
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="operator" class="flat-red" value="OR"/> OR
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div>
                            <div class="col-xs-12 col-sm-8 col-md-4">
                                <a href="">
                                    <button type="button" class="btn btn-info btn-block"><i class="fa fa-search"></i>&nbsp;Search</button>
                                </a>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12">&nbsp;</div>
					<!--<div class="col-md-6 col-xs-12 col-sm-12" style="margin-top:20px;margin-bottom:20px;">
						<div class="col-md-4">
							<!--<a href="<?php echo base_url(); ?>add/leave_type"><input type="button" class="btn btn-block btn-success" value="New"></a>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 col-sm-12" style="margin-top:20px;margin-bottom:20px;">
						<div class="col-md-8">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left"></div>
							<i><div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" placeholder="Search"  ng-model="search"/></div></i>
						</div>
						<div class="col-md-2 pull-left" >
							<a href="<?php echo base_url()."add/tunjangan_creation" ?>"><input type="button" class="btn btn-block btn-success" style="background-color:#316CFF;" value="GO"></a>
						</div>
					</div>-->
                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                    								<tr class="success">
                    									<th class="text-center">No</th>
                    									<th class="text-center">Employee Name</th>
                    									<th class="text-center">Division</th>
                    									<th class="text-center">Topic</th>
                    									<th class="text-center">Date</th>
                    									<th class="text-center">Place</th>
                    									<th class="text-center">Nett</th>
                    									<th class="text-center">Total Nett</th>
                    									<th class="text-center">status</th>
                    								</tr>
								        </thead>
								<tbody>
									<tr>
										<td align="center">1</td>
										<td>Shinta</td>
										<td align="center">Marketing</td>
										<td>E_1</td>
										<td align="center">15-03-2016</td>
										<td align="center">10</td>
										<td align="center">3</td>
										<td align="center">
										<a href=""><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal1"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal1" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Information Detail</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <p>No             : 1</p>
                                                          <p>Request Number	: 1</p>
                                                          <p>Date           : 15-02-2016</p>
                                                          <p>Employee ID   	: E_1</p>
                                                          <p>Employee Name 	: Shinta</p>
                                                          <p>Sisa Cuti    	: 10</p>
                                                          <p>Ambil Cuti     : 3</p>
                                                          <p>Status 		    : <span class="open_status">Approved</span></p>
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
												<a href="/add/training_approval"><i class="fa fa-pencil" ></i></a>
                                        </td>

										<!--<td class="action">
										   <i class="fa fa-search"></i>-->
										</td>
										<td><span class="open_status">Approved</span></td>
									</tr>
									<tr>
										<td align="center">2</td>
										<td>Dery</td>
										<td align="center">Operating</td>
										<td >E_2</td>
										<td align="center">16-02-2016</td>
										<td align="center">9</td>
										<td align="center">3</td>
										<td align="center">
										<a href=""><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal2"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal2" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Information Detail</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <p>No 			: 2</p>
                                                          <p>Request Number	: 2</p>
                                                          <p>Date       	: 16-02-2016</p>
                                                          <p>Employee ID   	: E_2</p>
                                                          <p>Employee Name 	: Dery</p>
                                                          <p>Sisa Cuti    	: 9</p>
                                                          <p>Ambil Cuti 	: 3</p>
                                                          <p>Status 		: <span class="close_status">Rejected</span></p>
                                                        
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
												<a href="/add/training_approval"><i class="fa fa-pencil" ></i></a>
                                        </td>
										<!--<td class="action">
										   <i class="fa fa-search"></i>
										</td>-->
										<td><span class="close_status">Rejected</span></td>
									</tr>
									<tr>
										<td align="center">3</td>
										<td>Bhima</td>
										<td align="center">Operational</td>
										<td>E_3</td>
										<td align="center">16-02-2016</td>
										<td align="center">9</td>
										<td align="center">3</td>
										<td align="center">
										<a href=""><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal3"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal3" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Information Detail</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <p>No 			: 3</p>
                                                          <p>Request Number	: 3</p>
                                                          <p>Date       	: 16-02-2016</p>
                                                          <p>Employee ID   	: E_3</p>
                                                          <p>Employee Name 	: Bhima</p>
                                                          <p>Sisa Cuti    	: 9</p>
                                                          <p>Ambil Cuti 	: 3</p>
                                                          <p>Status 		: <span class="need_aproval_status">Need Approval</span></p>
                                                        
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
												<a href="/add/training_approval"><i class="fa fa-pencil" ></i></a>
                                        </td>
										<!--<td class="action">
										   <i class="fa fa-search"></i>
										</td>-->
										<td><span class="need_aproval_status">Need Approval</span></td>
									</tr>
                  <tr>
                    <td align="center">4</td>
					<td>Badrizka</td>
                    <td align="center">Finance</td>
					<td>E_4</td>
                    <td align="center">17-02-2016</td>
                    <td align="center">7</td>
                    <td align="center">2</td>
                    <td align="center">
                    <a href=""><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal4"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal4" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Information Detail</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <p>No       : 4</p>
                                                          <p>Request Number : 4</p>
                                                          <p>Date         : 17-02-2016</p>
                                                          <p>Employee ID    : E_4</p>
                                                          <p>Employee Name  : Badrizka</p>
                                                          <p>Sisa Cuti      : 7</p>
                                                          <p>Ambil Cuti   : 2</p>
                                                          <p>Status     : <span class="open_status">Approved</span></p>
                                                        
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
												<a href="/add/training_approval"><i class="fa fa-pencil" ></i></a>
                                        </td>
                    <!--<td class="action">
                       <i class="fa fa-search"></i>
                    </td>-->
                    <td><span class="open_status">Approved</span></td>
                  </tr>
                  <tr>
                    <td align="center">5</td>
					<td>Indra</td>
                    <td align="center">Finance</td>
					<td>E_5</td>
                    <td align="center">21-02-2016</td>
                    <td align="center">3</td>
                    <td align="center">1</td>
                    <td align="center">
                    <a href=""><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal5"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal5" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Information Detail</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <p>No       : 5</p>
                                                          <p>Request Number : 5</p>
                                                          <p>Date         : 21-02-2016</p>
                                                          <p>Employee ID    : E_3</p>
                                                          <p>Employee Name  : Indra</p>
                                                          <p>Sisa Cuti      : 3</p>
                                                          <p>Ambil Cuti   : 1</p>
                                                          <p>Status     : <span class="need_aproval_status">Need Approval</span></p>
                                                        
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
												<a href="/add/training_approval"><i class="fa fa-pencil" ></i></a>
                                        </td>
                    <!--<td class="action">
                       <i class="fa fa-search"></i>
                    </td>-->
                    <td><span class="need_aproval_status">Need Approval</span></a></td>
                  </tr>
                  <tr>
                    <td align="center">6</td>
					<td>Ihsan</td>
                    <td align="center">General Affair</td>
					<td>E_6</td>
                    <td align="center">30-02-2016</td>
                    <td align="center">8</td>
                    <td align="center">5</td>
                    <td align="center">
                    <a href=""><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal6"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal6" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Information Detail</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <p>No       : 6</p>
                                                          <p>Request Number : 6</p>
                                                          <p>Date         : 30-02-2016</p>
                                                          <p>Employee ID    : E_6</p>
                                                          <p>Employee Name  : Ihsan</p>
                                                          <p>Sisa Cuti      : 8</p>
                                                          <p>Ambil Cuti   : 5</p>
                                                          <p>Status     : <span class="close_status">Rejected</span></p>
                                                        
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
												<a href="/add/training_approval"><i class="fa fa-pencil" ></i></a>
                                        </td>
                    <!--<td class="action">
                       <i class="fa fa-search"></i>
                    </td>-->
                    <td><span class="close_status">Rejected</span></td>
                  </tr>
                  <tr>
                    <td align="center">7</td>
					<td>Davi</td>
                    <td align="center">Operational</td>
					<td>E_7</td>
                    <td align="center">07-03-2016</td>
                    <td align="center">1</td>
                    <td align="center">1</td>
                    <td align="center">
                    <a href=""><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal7"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal7" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Information Detail</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <p>No       : 7</p>
                                                          <p>Request Number : 7</p>
                                                          <p>Date         : 07-03-2016</p>
                                                          <p>Employee ID    : E_7</p>
                                                          <p>Employee Name  : Davi</p>
                                                          <p>Sisa Cuti      : 1</p>
                                                          <p>Ambil Cuti   : 1</p>
                                                          <p>Status     : <span class="close_status">Rejected</span></p>
                                                        
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
												<a href="/add/training_approval"><i class="fa fa-pencil" ></i></a>
                                        </td>
                    <!--<td class="action">
                       <i class="fa fa-search"></i>
                    </td>-->
                    <td><span class="close_status">Rejected</span></td>
                  </tr>
                  <tr>
                    <td align="center">8</td>
					<td>Pratiwi</td>
                    <td align="center">Marketing</td>
					<td>E_8</td>
                    <td align="center">19-03-2016</td>
                    <td align="center">3</td>
                    <td align="center">2</td>
                    <td align="center">
                    <a href=""><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal8"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal8" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Information Detail</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <p>No       : 8</p>
                                                          <p>Request Number : 8</p>
                                                          <p>Date         : 19-03-2016</p>
                                                          <p>Employee ID    : E_8</p>
                                                          <p>Employee Name  : Pratiwi</p>
                                                          <p>Sisa Cuti      : 3</p>
                                                          <p>Ambil Cuti   : 2</p>
                                                          <p>Status     : <span class="open_status">Approved</span></p>
                                                        
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
												<a href="/add/training_approval"><i class="fa fa-pencil" ></i></a>
                                        </td>
                    <!--<td class="action">
                       <i class="fa fa-search"></i>
                    </td>-->
                    <td><span class="open_status">Approved</span></td>
                  </tr>
                  <!--<tr>
                    <td align="center">9</td>
                    <td align="center">9</td>
                    <td align="center">23-03-2016</td>
                    <td>E_9</td>
                    <td>Indra</td>
                    <td align="center">10</td>
                    <td align="center">2</td>
                    <td align="center">
                    <a><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal9"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal9" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Information Detail</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <p>No             : 9</p>
                                                          <p>Request Number : 9</p>
                                                          <p>Date           : 23-03-2016</p>
                                                          <p>Employee ID    : E_9</p>
                                                          <p>Employee Name  : Indra</p>
                                                          <p>Sisa Cuti      : 10</p>
                                                          <p>Ambil Cuti     : 2</p>
                                                          <p>Status         : <span class="label label-danger">Rejected</span></p>
                                                        
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                        </td>
                    <!--<td class="action">
                       <i class="fa fa-search"></i>
                    </td>-->
                    <!--<td><span class="label label-danger">Rejected</span></td>
                  </tr>
                  <tr>
                    <td align="center">10</td>
                    <td align="center">10</td>
                    <td align="center">01-04-2016</td>
                    <td>E_10</td>
                    <td>Shinta</td>
                    <td align="center">7</td>
                    <td align="center">4</td>
                    <td align="center">
                    <a><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal10"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal10" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Information Detail</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <p>No             : 10</p>
                                                          <p>Request Number : 10</p>
                                                          <p>Date           : 01-04-2016</p>
                                                          <p>Employee ID    : E_10</p>
                                                          <p>Employee Name  : Shinta</p>
                                                          <p>Sisa Cuti      : 7</p>
                                                          <p>Ambil Cuti     : 4</p>
                                                          <p>Status         : <span class="label label-primary">Need Approval</span></p>
                                                        
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                        </td>
                    <!--<td class="action">
                       <i class="fa fa-search"></i>
                    </td>-->
                    <!--<td><a href="<?php echo base_url()."edit/leave_approval"  ?>"><span class="label label-primary">Need Approval</span></a></td>
                  </tr>-->
							
								</tbody>
                        </table>
                        	
                      </div>
                                    <div class="col-xs-11 col-sm-12 col-md-12">
                                      <ul class="pagination pull-right" >
                                      <li><a href="#">Previous</a></li>
                                      <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                                      <li><a href="#">2</a></li>
                                      <li><a href="#">3</a></li>
                                      <li><a href="#">Next</a></li>
                                    </ul>
                                  </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
