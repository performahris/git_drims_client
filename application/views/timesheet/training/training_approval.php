<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"></i>HR</a></li>
            <li><a href="#"></i>Leave Request</a></li>
            <li><a href="#"></i>Requested</a></li>
            <li class="active">List</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
    
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
    <div class="box-header">
            <h3 class="box-title">Training | Training Released </h3>
      <hr>
    </div>
    <div class="box-body"> 
    <!-- BEGIN OF SEARCH -->
                         <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-8 col-md-8">
                                    <label for="searchby" class="col-md-12">Search By :</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Employee ID</option>
                                        <option>Employee Name</option>
                                        <option>Ongoing Project</option>
                                        <option>Next Project</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Employee ID</option>
                                        <option>Employee Name</option>
                                        <option>Ongoing Project</option>
                                        <option>Next Project</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Employee ID</option>
                                        <option>Employee Name</option>
                                        <option>Ongoing Project</option>
                                        <option>Next Project</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="operator">Operator</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <input type="radio" name="operator" class="flat-red" checked="true" value="AND"/> AND
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="operator" class="flat-red" value="OR"/> OR
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div>
                            <div class="col-xs-12 col-sm-8 col-md-4">
                                <a href="">
                                    <button type="button" class="btn btn-info btn-block"><i class="fa fa-search" data-toggle="tooltip" title="View">&nbsp;</i>Search</button>
                                </a>
                            </div>
                            </div>
                        </div>
                        <!--END OF SEARCH-->
                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
							<tr> 
								<th >No</th> 
								<th class="text-center">Employee Name</th>
                    			<th class="text-center">Division</th>
                    			<th class="text-center">Topic</th>
                    		    <th class="text-center">Date</th>
								<th >Status</th>
							</tr>	
							<!--<tr ng-repeat="m in mpr | orderBy:sortType:sortReverse | filter:seacrh">-->
              <tr>
								<td>1</td>
								<td>E1</td>
								<td>BUDI YUWONO</td>
								<td>PA</td>
								<td>PZ</td>
								<td>
									<span class="label label-success"><i class="fa fa-check">&nbsp;</i>Assigned</span>
								</td>  
							 </tr>
               <tr>
                <td>2</td>
                <td>E2</td>
                <td>IBRAHIM LACONI</td>
                <td>PS</td>
                <td>CS</td>
                <td>
                  <span class="label label-danger"><i class="fa fa-remove">&nbsp;</i>Not Assigned</span>  
                </td>  
               </tr>
               <tr>
                <td>3</td>
                <td>E3</td>
                <td>RUSLI YANTO</td>
                <td>PA</td>
                <td>PZ</td>
                <td>
                  <span class="label label-success"><i class="fa fa-check">&nbsp;</i>Assigned</span>
                </td>  
               </tr>
               <tr>
                <td>4</td>
                <td>E4</td>
                <td>SURATMAN</td>
                <td>SP</td>
                <td>CS</td>
                <td>
                  <span class="label label-success"><i class="fa fa-check">&nbsp;</i>Assigned</span>
                </td>  
               </tr>
               <tr>
                <td>5</td>
                <td>E5</td>
                <td>RUSTAM EFFENDI</td>
                <td>PO</td>
                <td>PZ</td>
                <td>
                  <span class="label label-success"><i class="fa fa-check">&nbsp;</i>Assigned</span>
                </td>  
               </tr>
               <tr>
                <td>6</td>
                <td>E2</td>
                <td>EDI RISWANTO</td>
                <td>PS</td>
                <td>CS</td>
                <td>
                  <span class="label label-danger"><i class="fa fa-remove">&nbsp;</i>Not Assigned</span>  
                </td>  
               </tr>
               <tr>
                <td>7</td>
                <td>E7</td>
                <td>SUHARYANTO</td>
                <td>PP</td>
                <td>PX</td>
                <td>
                  <span class="label label-danger"><i class="fa fa-remove">&nbsp;</i>Not Assigned</span>
                </td>  
               </tr>
               <tr>
                <td>8</td>
                <td>E8</td>
                <td>MUSTARYONO</td>
                <td>SS</td>
                <td>CS</td>
                <td>
                  <span class="label label-danger"><i class="fa fa-remove">&nbsp;</i>Not Assigned</span>  
                </td>  
               </tr>
               <tr>
                <td>9</td>
                <td>E9</td>
                <td>ACHMAD FIRDAUS</td>
                <td>PB</td>
                <td>PZ</td>
                <td>
                  <span class="label label-success"><i class="fa fa-check">&nbsp;</i>Assigned</span>
                </td>  
               </tr>
               <tr>
                <td>10</td>
                <td>E10</td>
                <td>DODY AGUSTIANSYAH</td>
                <td>PS</td>
                <td>CS</td>
                <td>
                  <span class="label label-success"><i class="fa fa-check">&nbsp;</i>Assigned</span>
                </td>  
               </tr>
               
                        </table>
						
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
      $(".departemen").select2();
    });
    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      $scope.sortType     = 'no'; // set the default sort type
      $scope.sortReverse  = false;  // set the default sort order
      $scope.search   = ''; 
      $scope.limit =25;
      $scope.depart="";  
      $scope.jumlah = 0;                        
      
      $scope.mpr = [
        { id: 'E1', name: 'Danny', ongoing : 'PA', nextproject : 'PZ', status : 1},
        { id: 'E2', name: 'Erry', ongoing : 'PB', nextproject : 'PZ', status : 1},
        { id: 'E3', name: 'Tri', ongoing : 'PC', nextproject : 'PZ', status : 1},
        { id: 'E4', name: 'Handhika', ongoing : 'PA', nextproject : 'PZ', status : 1},
        { id: 'E5', name: 'Bemper', ongoing : 'PA', nextproject : 'PZ', status : 0},
        { id: 'E6', name: 'Kili', ongoing : 'PC', nextproject : 'PZ,PX', status : 1}
      ];
      
       $scope.currentPage = 1;  
       $scope.totalItems = $scope.mpr.length;  
       $scope.numPerPage = $scope.limit;    
       
       $scope.limitPage = function() {
         $scope.numPerPage = $scope.limit;
         if($scope.currentPage * $scope.numPerPage > $scope.mpr.length){
            $scope.currentPage = 1;   
         }
      };
       
       $scope.lastPage = function() {      
         $scope.currentPage=$scope.pageCount();
      };
      
      $scope.firstPage = function() {
         $scope.currentPage=1;
      };
       
       $scope.nextPage = function() {
        
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
      };
      
      $scope.jumlahPerpage = function (value) {
        $scope.numPerPage = value;
      } 
      
      $scope.prevPage = function() {
        if ($scope.currentPage > 1) {
          $scope.currentPage--;
        }
      };
      
       $scope.pageCount = function() {
        return Math.ceil($scope.jumlah/$scope.numPerPage);
      };        
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

        
    

</script>
