<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    
</script>

<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Recruitment</li>
            <li class="active">Penarikan Karyawan</li>
            <li class="active">Add</li>
        </ol>
    </section>
	
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Recruitment | Penarikan Karyawan | Form Penarikan Karyawan</h3><br>
                        <p><hr></p>
                    </div>
                    <div class="col-md-6 col-xs-12 col-sm-12 pull-left">
                        <label for="comment">By         : HRD</label>
                    </div>
                    <div class="box-body table-responsive " style="width:100%">
<thead>
        <div class="col-md-12 col-xs-12 col-sm-12" style="margin-top:5px;margin-bottom:5px;">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color:#80ff80;"><center><label>DATA PRIBADI</label></center></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4 col-xs-12 col-sm-12">
                            <label for="comment">Nama Karyawan</label>
                        </div> 
                        <div class="col-md-8 col-xs-12 col-sm-12">
                            Sudarmono
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-xs-12 col-sm-12">
                            <label for="comment">Tempat/Tanggal Lahir</label>
                        </div> 
                        <div class="col-md-8 col-xs-12 col-sm-12">
                            Musi Banyuasin, 22 May 1978
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-xs-12 col-sm-12">
                            <label for="comment">Agama</label>
                        </div> 
                        <div class="col-md-8 col-xs-12 col-sm-12">
                            Islam
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-xs-12 col-sm-12">
                            <label for="comment">Jenis Kelamin</label>
                        </div> 
                        <div class="col-md-8 col-xs-12 col-sm-12">
                            Laki-Laki
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-xs-12 col-sm-12">
                            <label for="comment">Status Perkawinan</label>
                        </div> 
                        <div class="col-md-8 col-xs-12 col-sm-12">
                            Kawin
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-xs-12 col-sm-12">
                            <label for="comment">Alamat</label>
                        </div> 
                        <div class="col-md-8 col-xs-12 col-sm-12">
                            Komp Griya Suka Bangun Blok D7 RT.97 RW.09 Desa Sukajaya Kec. Sukarami
                        </div>
                    </div>
                </div>

                <div class="panel-heading" style="background-color:#80ff80;"><center><label>DATA PENDIDIKAN DAN PENGALAMAN KERJA</label></center></div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-4 col-xs-12 col-sm-12">
                            <label for="comment">1. Pendidikan Terakhir</label>
                        </div> 
                        <div class="col-md-8 col-xs-12 col-sm-12">
                            S1
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-xs-12 col-sm-12">
                            <label for="comment">2. Gaji & Tunjangan Terakhir</label> 
                        </div> 
                        <div class="col-md-8 col-xs-12 col-sm-12">
                            
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-xs-12 col-sm-12">
                            <label for="comment">&nbsp&nbsp&nbsp A. Gaji pokok</label>
                        </div> 
                        <div class="col-md-8 col-xs-12 col-sm-12">
                            Rp 2.000.000
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-xs-12 col-sm-12">
                            <label for="comment">&nbsp&nbsp&nbsp B. Tunjangan Makan</label>
                        </div> 
                        <div class="col-md-8 col-xs-12 col-sm-12">
                            Rp 500.000
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-xs-12 col-sm-12">
                            <label for="comment">&nbsp&nbsp&nbsp C. Tunjangan Transport</label>
                        </div> 
                        <div class="col-md-8 col-xs-12 col-sm-12">
                            Rp 500.000
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-xs-12 col-sm-12">
                            <label for="comment">&nbsp&nbsp&nbsp D. Tunjangan Lain - Lain</label>
                        </div> 
                        <div class="col-md-8 col-xs-12 col-sm-12">
                            Rp 1.000.000
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-xs-12 col-sm-12">
                            <label for="comment">3. Jabatan Terakhir</label>
                        </div> 
                        <div class="col-md-8 col-xs-12 col-sm-12">
                            Supervisor
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-xs-12 col-sm-12">
                            <label for="comment">4. Masa Kerja dibidang YBS</label>
                        </div> 
                        <div class="col-md-8 col-xs-12 col-sm-12">
                            2 Tahun
                        </div>
                    </div>

                </div>

                <div class="panel-heading" style="background-color:#80ff80;"><center><label>ALASAN DIREKOMENDASIKAN</label></center></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12 col-sm-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="background-color:#80ff80;"><center><label>DIVISI YANG BERSANGKUTAN</label></center></div>
                                <div class="panel-body">
                                    <textarea class="form-control" rows="5" id="comment"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12 col-sm-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="background-color:#80ff80;"><center><label>DIVISI YANG BERSANGKUTAN</label></center></div>
                                <div class="panel-body">
                                    <p><center>(AUTO GENERADE)</center></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel-heading" style="background-color:#80ff80;"><center><label>DATA PENDIDIKAN DAN PENGALAMAN KERJA</label></center></div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="row" style="padding-top:5px;">
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <label for="comment">1.Jabatan</label>
                            </div> 
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <select class="form-control">
                                    <option fisible="false">Pilih Jabatan</option>
                                    <option>Asisten Admin II</option>
                                    <option>Pembantu Teknisi ICT</option>
                                    <option>Ops Crane</option>
                                    <option>Petugas Admin III</option>
                                    <option>Pemuka Lindungan Lingkungan</option>
                                    <option>Ops Alat Berat</option>
                                    <option>Petugas Admin 1</option>
                                    <option>Pemuka Inspeksi</option>
                                    <option>Petugas Admin II</option>
                                    <option>Pengemudi Truck</option>
                                </select> 
                            </div>
                        </div>

                        <div class="row" style="padding-top:5px;">
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <label for="comment">2. Departemen / Divisi</label>
                            </div> 
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <select class="form-control">
                                    <option fisible="false">Pilih Department / Divisi</option>
                                    <option>Produksi</option>
                                    <option>Pemasaran</option>
                                    <option>Keuangan</option>
                                    <option>Layanan Pelanggan</option>
                                    <option>Purchasing</option>
                                    <option>Logistic</option>
                                    <option>Project</option>
                                    <option>HRD</option>
                                    <option>Personalia</option>
                                </select>
                            </div>
                        </div>

                        <div class="row" style="padding-top:5px;">
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <label for="comment">3.Status hubungan Kerja</label>
                            </div> 
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <select class="form-control">
                                    <option>Pilih Status Hubungan Kerja</option> 
                                    <option>Kontrak</option>
                                    <option>Permanent</option>
                                </select>
                            </div>
                        </div>

                        <div class="row" style="padding-top:5px;">
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <label for="comment">4. Lokasi Kerja</label>
                            </div> 
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <input type="text" class="form-control" id="" placeholder="Masukan Lokasi Kerja">
                            </div>
                        </div>

                        <div class="row" style="padding-top:5px;">
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <label for="comment">5. Golongan</label>
                            </div> 
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <input type="text" class="form-control" id="" placeholder="Golongan">
                            </div>
                        </div>

                        <div class="row" style="padding-top:5px;">
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <label for="comment">6. Gaji Yang Diminta</label>
                            </div> 
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <input type="text" class="form-control" id="" placeholder="Masukan Nominal">
                            </div>
                        </div>

                        <div class="row" style="padding-top:5px;">
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <label for="comment">7. Tunjangan / Fasilitas yang diminta</label>
                            </div> 
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <input type="text" class="form-control" id="" placeholder="Masukan Tunjangan / fasilitas yang diminta">
                            </div>
                        </div>

                        <div class="row" style="padding-top:5px;">
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <label for="comment">8. Gaji yang diusulkan</label>
                            </div> 
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <input type="text" class="form-control" id="" placeholder="Masukan Nominal gaji yang diusulkan">
                            </div>
                        </div>

                        <div class="row" style="padding-top:5px;">
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <label for="comment">9. Tunjangan / Fasilitas yang disetujui</label>
                            </div> 
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <input type="text" class="form-control" id="" placeholder="Masukan Tunjangan / Fasilitas yang disetujui">
                            </div>
                        </div>

                        <div class="row" style="padding-top:5px;">
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <label for="comment">10. Gaji yang disetujui</label>
                            </div> 
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <input type="text" class="form-control" id="" placeholder="Masukan Nominal gaji yang disetujui">
                            </div>
                        </div>

                        <div class="row" style="padding-top:5px;">
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <label for="comment">11. Mulai bekerja pada tanggal</label>
                            </div> 
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar">
                                        </i>
                                    </div>
                                    <input type="text" id="datepicker1" class="form-control pull-rigth active"/>
                                </div>
                            </div>                     
                        </div>
                </div>

            </div>
        </div>    
</thead>
                <div class="col-md-7 col-xs-12 col-sm-12">
                        <div class="col-md-4 col-xs-12 col-sm-12">
                            <a href="<?php echo base_url('dashboard/penarikan_karyawan'); ?>"><input type="button" class="btn btn-block btn-success" value="Save"></a>
                        </div>
                        <div class="col-md-4 col-xs-12 col-sm-12">
                            <a href="<?php echo base_url('dashboard/penarikan_karyawan'); ?>"><input type="button" class="btn btn-block" style="background-color:#32cd32; color:white;" value="Save as Draft"></a>
                        </div>
                        <div class="col-md-4 col-xs-12 col-sm-12">
                            <a href="<?php echo base_url('dashboard/penarikan_karyawan'); ?>"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
                        </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
      $("a.button").click(function(){
		$.fancybox(
		'<div style="height:350px;width:450px;overflow:hidden;"><iframe src="<?php echo base_url(); ?>dashboard/departement_detail" frameborder="0" scrolling="no" style="width:100%;height:800px;margin-top:-200px;"></iframe></div>',
			{
				'autoDimensions'	: false,
				'width'         	: 'auto',
				'height'        	: 'auto',
				'transitionIn'		: 'none',
				'transitionOut'		: 'none'
			}
		);
	});
    });
    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      $scope.sortType     = 'no'; // set the default sort type
      $scope.sortReverse  = false;  // set the default sort order
      $scope.search   = ''; 
      $scope.limit =25;
      $scope.jumlah = 0;
                                          
      
      $scope.data = <?php echo $departement; ?>
         
       $scope.currentPage = 1;  
       $scope.totalItems = $scope.data.length;  
       $scope.numPerPage = $scope.limit;    
       
       $scope.limitPage = function() {
         $scope.numPerPage = $scope.limit;
         if($scope.currentPage * $scope.numPerPage > $scope.data.length){
            $scope.currentPage = 1;   
         }
      };
       
       $scope.lastPage = function() {      
         $scope.currentPage=$scope.pageCount();
      };
      
      $scope.firstPage = function() {
         $scope.currentPage=1;
      };
       
       $scope.nextPage = function() {
        
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
      };
      
      $scope.jumlahPerpage = function (value) {
        $scope.numPerPage = value;
      } 
      
      $scope.prevPage = function() {
        if ($scope.currentPage > 1) {
          $scope.currentPage--;
        }
      };
      
       $scope.pageCount = function() {
        return Math.ceil($scope.jumlah/$scope.numPerPage);
      };        
    });
    
    app.filter("paging", function() {      
      return function(items, limit, currentPage) {
        if (typeof limit === 'string' || limit instanceof String){
           limit = parseInt(limit);
         }
         
         var begin, end, index;  
         begin = (currentPage - 1) * limit;  
         end = begin + limit;
         var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (begin <= i && i < end )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
     
    app.filter("nama", function() {      
      return function(items, search) {
      if(search.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].name.toUpperCase().indexOf(search.toUpperCase()) != -1 )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });

</script>
