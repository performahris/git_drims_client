<script>
    $(function() {
        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Recruitment</li>
            <li class="active">Applicant</li>
            <li class="active">Interview Record</li>
            <li class="active">List</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Recruitment | Applicant | Interview Record</h3><br>
                        <hr>
                    </div>

                    <tread>
                        <div class="col-md-12 col-xs-12 col-sm-12" style="margin-top:5px;margin-bottom:5px;">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                        <label for="comment">Nama Karyawan</label>
                                    </div> 
                                    <div class="col-md-8 col-xs-12 col-sm-7">
                                        SUDARMONO
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                        <label for="comment">Tempat/Tanggal Lahir</label>
                                    </div> 
                                    <div class="col-md-8 col-xs-12 col-sm-7">
                                        Musi Banyuasin, 22 May 1978
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                        <label for="comment">Pendidikan Terakhir</label>
                                    </div> 
                                    <div class="col-md-8 col-xs-12 col-sm-7">
                                        S1
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                        <label for="comment">Jabatan yang dilamar</label>
                                    </div> 
                                    <div class="col-md-8 col-xs-12 col-sm-7">
                                        PROGRAMER
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 col-xs-12 col-sm-5">
                                        <label for="comment">Tanggal interview</label>
                                    </div> 
                                    <div class="col-md-8 col-xs-12 col-sm-7">
                                        <?php echo date("d F Y") ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </tread>

                    <div class="box-body table-responsive " style="width:100%">
                        <center><h4>HASIL INTERVIEW</h4></center>
                        <center>
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="success">
                                        <th colspan="5"><center>Petunjuk : Pilih 1 point</center></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td class="info">
                                            <font size="4">VERBALISASI/KOMUNIKASI</font><br>
                                            <font size="3">kemampuan untuk berkomunikasi dan menjelaskan suatu hal</font> 
                                        </td>
                                        <!--<form action="">-->
                                        <td>
                                <center>
                                    <input type="radio" name="gender" value="point1">1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="gender" value="point2">2<br>
                                    <h7>Kacau tidak berfokus membingungkan</h7>
                                </center>
                                </td>
                                <td>
                                <center>
                                    <input type="radio" name="gender" value="point3">3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="gender" value="point4">4<br>
                                    <h7>Terarah, Relevan, Jelas, Lancar</h7>
                                </center>
                                </td>
                                <td>
                                <center>
                                    <input type="radio" name="gender" value="point5">5 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="gender" value="point6">6<br>
                                    <h7>Sangat Lancar, Sangat Terarah, Sangat Mudah dimengerti</h7>
                                </center>
                                </td>
                                <!--</form>-->
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td class="info">
                                        <font size="4">PENGETAHUAN</font><br>
                                        <font size="3">Sejauh mana pengetahuan tentang bidang kerja yang akan di tekuni</font> 
                                    </td>
                                    <!--<form action="">-->
                                    <td>
                                <center>
                                    <input type="radio" name="gender" value="point1">1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="gender" value="point2">2<br>
                                    <h7>minim, hanya tau garis besar</h7>
                                </center>
                                </td>
                                <td>
                                <center>
                                    <input type="radio" name="gender" value="point3">3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="gender" value="point4">4<br>
                                    <h7>Tahu banyak, kenal seluruh aktivitasnya</h7>
                                </center>
                                </td>
                                <td>
                                <center>
                                    <input type="radio" name="gender" value="point5">5 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="gender" value="point6">6<br>
                                    <h7>Sangat mendalam</h7>
                                </center>
                                </td>
                                <!--</form>-->
                                </tr>

                                <tr>
                                    <td>3</td>
                                    <td class="info">
                                        <font size="4">PENGALAMAN</font><br>
                                        <font size="3">Pengalaman Kerja / Belajar yang meningkatkan kompetensinya</font> 
                                    </td>
                                    <!--<form action="">-->
                                    <td>
                                <center>
                                    <input type="radio" name="gender" value="point1">1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="gender" value="point2">2<br>
                                    <h7>Kurang Kompeten</h7>
                                </center>
                                </td>
                                <td>
                                <center>
                                    <input type="radio" name="gender" value="point3">3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="gender" value="point4">4<br>
                                    <h7>Kompeten</h7>
                                </center>
                                </td>
                                <td>
                                <center>
                                    <input type="radio" name="gender" value="point5">5 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="gender" value="point6">6<br>
                                    <h7>Sangat Kompeten</h7>
                                </center>
                                </td>
                                <!--</form>-->
                                </tr>

                                <tr>
                                    <td>4</td>
                                    <td class="info">
                                        <font size="4">PENDIDIKAN</font><br>
                                        <font size="3">Kualifikasi pendidikan yang diraih</font> 
                                    </td>

                                    <!--<form action="">-->
                                    <td>
                                <center>
                                    <input type="radio" name="gender" value="point1">1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="gender" value="point2">2<br>
                                    <h7>Kurang</h7>
                                </center>
                                </td>
                                <td>
                                <center>
                                    <input type="radio" name="gender" value="point3">3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="gender" value="point4">4<br>
                                    <h7>Baik</h7>
                                </center>
                                </td>
                                <td>
                                <center>
                                    <input type="radio" name="gender" value="point5">5 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="gender" value="point6">6<br>
                                    <h7>Sangat Baik</h7>
                                </center>
                                </td>
                                </form>
                                </tr>

                                <tr>
                                    <td>5</td>
                                    <td class="info">
                                        <font size="4">PENGENDALIAN DIRI</font><br>
                                        <font size="3">Kepercayaan diri dan sikap saat berkomunikasi dengan banyak orang</font> 
                                    </td>
                                <form action="">
                                    <td>
                                    <center>
                                        <input type="radio" name="gender" value="point1">1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="gender" value="point2">2<br>
                                        <h7>Gugup, Salah tingkah, Cemas</h7>
                                    </center>
                                    </td>
                                    <td>
                                    <center>
                                        <input type="radio" name="gender" value="point3">3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="gender" value="point4">4<br>
                                        <h7>Cukup tenang, Rileks</h7>
                                    </center>
                                    </td>
                                    <td>
                                    <center>
                                        <input type="radio" name="gender" value="point5">5 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="gender" value="point6">6<br>
                                        <h7>Sangat leluasa, Spontan, Mantap</h7>
                                    </center>
                                    </td>
                                </form>
                                </tr>

                                <tr>
                                    <td>6</td>
                                    <td class="info">
                                        <font size="4">ANTUSIASME</font><br>
                                        <font size="3">Minat calon untuk bergabung dan berkarya di perusahaan</font> 
                                    </td>
                                <form action="">
                                    <td>
                                    <center>
                                        <input type="radio" name="gender" value="point1">1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="gender" value="point2">2<br>
                                        <h7>Tampa acuh, Kurang berminat pada pekerjaan</h7>
                                    </center>
                                    </td>
                                    <td>
                                    <center>
                                        <input type="radio" name="gender" value="point3">3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="gender" value="point4">4<br>
                                        <h7>Menunjukan minat yang kuat dalam pekerjaan</h7>
                                    </center>
                                    </td>
                                    <td>
                                    <center>
                                        <input type="radio" name="gender" value="point5">5 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="gender" value="point6">6<br>
                                        <h7>Sangat bersemangat, punya motivasi yang tinggi untuk bekerja</h7>
                                    </center>
                                    </td>
                                </form>
                                </tr>

                                <tr>
                                    <td>7</td>
                                    <td class="info">
                                        <font size="4">INISIATIF</font><br>
                                        <font size="3">Keinginan untuk tidak menjadi objek, tapi juga menjadi subjek / pelaku</font> 
                                    </td>
                                <form action="">
                                    <td>
                                    <center>
                                        <input type="radio" name="gender" value="point1">1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="gender" value="point2">2<br>
                                        <h7>Pasif, bersifat menunggu</h7>
                                    </center>
                                    </td>
                                    <td>
                                    <center>
                                        <input type="radio" name="gender" value="point3">3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="gender" value="point4">4<br>
                                        <h7>Menjawab secara lengkap, ikut mengendalikan pembicaraan</h7>
                                    </center>
                                    </td>
                                    <td>
                                    <center>
                                        <input type="radio" name="gender" value="point5">5 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="gender" value="point6">6<br>
                                        <h7>Mengajukan pertanyaan yang relevan, <i>Self Starter</i></h7>
                                    </center>
                                    </td>
                                </form>
                                </tr>

                                <tr>
                                    <td>8</td>
                                    <td class="info">
                                        <font size="4">KESIAPAN DIRI</font><br>
                                        <font size="3">Kemudahan untuk beradaptasi dan mengambil langkah terhadap situasi baru</font> 
                                    </td>
                                <form action="">
                                    <td>
                                    <center>
                                        <input type="radio" name="gender" value="point1">1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="gender" value="point2">2<br>
                                        <h7>Reaksi lambat, Terkesan bingung / tidak mengerti</h7>
                                    </center>
                                    </td>
                                    <td>
                                    <center>
                                        <input type="radio" name="gender" value="point3">3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="gender" value="point4">4<br>
                                        <h7>Cepat memahami masalah, cepat dalam bereaksi</h7>
                                    </center>
                                    </td>
                                    <td>
                                    <center>
                                        <input type="radio" name="gender" value="point5">5 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="gender" value="point6">6<br>
                                        <h7>Sangat mudah memahami persoalan, Cepat dan Antisipasif</h7>
                                    </center>
                                    </td>
                                </form>
                                </tr>

                                <tr>
                                    <td>9</td>
                                    <td class="info">
                                        <font size="4">KEHANGATAN / SOSIALISASI</font><br>
                                        <font size="3">Sifat yang ditampilkan saat wawancara</font> 
                                    </td>
                                <form action="">
                                    <td>
                                    <center>
                                        <input type="radio" name="gender" value="point1">1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="gender" value="point2">2<br>
                                        <h7>Dingin, Tampak angkuh, Tidak bersahabat</h7>
                                    </center>
                                    </td>
                                    <td>
                                    <center>
                                        <input type="radio" name="gender" value="point3">3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="gender" value="point4">4<br>
                                        <h7>Ramah, Menyenangkan , Hangat</h7>
                                    </center>
                                    </td>
                                    <td>
                                    <center>
                                        <input type="radio" name="gender" value="point5">5 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="gender" value="point6">6<br>
                                        <h7>Kepribadian yang sangat menarik, Mengagumkan</h7>
                                    </center>
                                    </td>
                                </form>
                                </tr>

                                <tr>
                                    <td>10</td>
                                    <td class="info">
                                        <font size="4">PENAMPILAN</font><br>
                                        <font size="3">Penampilan yang bisa menjadi cermin kepribadian calon</font> 
                                    </td>
                                <form action="">
                                    <td>
                                    <center>
                                        <input type="radio" name="gender" value="point1">1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="gender" value="point2">2<br>
                                        <h7>Kusut, Tidak merawat diri, Dandanan berlebihan</h7>
                                    </center>
                                    </td>
                                    <td>
                                    <center>
                                        <input type="radio" name="gender" value="point3">3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="gender" value="point4">4<br>
                                        <h7>Cukup rapih, cukup bersih</h7>
                                    </center>
                                    </td>
                                    <td>
                                    <center>
                                        <input type="radio" name="gender" value="point5">5 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="gender" value="point6">6<br>
                                        <h7>Impresif, sangat rapih</h7>
                                    </center>
                                    </td>
                                </form>
                                </tr>
                                </tbody>
                            </table>
                        </center>

                        <div class="col-md-12" style="padding-top:10px; padding-bottom:5px;">
                            <label for="comment">KOMENTAR/CATATAN</label>
                            <textarea class="form-control" rows="5" id="comment"></textarea>
                        </div>
                        <div class="col-md-12">
                            <label for="comment">Saran : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            <input type="radio" name="gender" value="point5">Disarankan Untuk Diterima &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="radio" name="gender" value="point6">Tidak Disarankan Untuk Diterima<br>
                        </div>
                        <div class="col-md-8 col-xs-12 col-sm-12" style="padding-top:10px; padding-bottom:5px;">
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <a href="<?php echo base_url('dashboard/applicant'); ?>"><input type="button" class="btn btn-block btn-success" value="Submit"></a>
                            </div>
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <a href="<?php echo base_url('dashboard/applicant'); ?>"><input type="button" class="btn btn-block" style="background-color:#32cd32; color:white;" value="Save as Draft"></a>
                            </div>
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <a href="<?php echo base_url('dashboard/applicant'); ?>"><input type="button" class="btn btn-block btn-danger" value="Cancel"></a>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("a.button").click(function() {
            $.fancybox(
                    '<div style="height:350px;width:450px;overflow:hidden;"><iframe src="<?php echo base_url(); ?>dashboard/departement_detail" frameborder="0" scrolling="no" style="width:100%;height:800px;margin-top:-200px;"></iframe></div>',
                    {
                        'autoDimensions': false,
                        'width': 'auto',
                        'height': 'auto',
                        'transitionIn': 'none',
                        'transitionOut': 'none'
                    }
            );
        });
    });

    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
        $scope.sortType = 'no'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order
        $scope.search = '';
        $scope.limit = 25;
        $scope.jumlah = 0;


        $scope.data = <?php echo $departement; ?>

        $scope.currentPage = 1;
        $scope.totalItems = $scope.data.length;
        $scope.numPerPage = $scope.limit;

        $scope.limitPage = function() {
            $scope.numPerPage = $scope.limit;
            if ($scope.currentPage * $scope.numPerPage > $scope.data.length) {
                $scope.currentPage = 1;
            }
        };

        $scope.lastPage = function() {
            $scope.currentPage = $scope.pageCount();
        };

        $scope.firstPage = function() {
            $scope.currentPage = 1;
        };

        $scope.nextPage = function() {

            if ($scope.currentPage < $scope.pageCount()) {
                $scope.currentPage++;
            }
        };

        $scope.jumlahPerpage = function(value) {
            $scope.numPerPage = value;
        }

        $scope.prevPage = function() {
            if ($scope.currentPage > 1) {
                $scope.currentPage--;
            }
        };

        $scope.pageCount = function() {
            return Math.ceil($scope.jumlah / $scope.numPerPage);
        };
    });

    app.filter("paging", function() {
        return function(items, limit, currentPage) {
            if (typeof limit === 'string' || limit instanceof String) {
                limit = parseInt(limit);
            }

            var begin, end, index;
            begin = (currentPage - 1) * limit;
            end = begin + limit;
            var arrayToReturn = [];
            for (var i = 0; i < items.length; i++) {
                if (begin <= i && i < end) {
                    arrayToReturn.push(items[i]);
                }
            }
            return arrayToReturn;
        };
    });

    app.filter("nama", function() {
        return function(items, search) {
            if (search.length == 0) {
                return items;
            }
            var arrayToReturn = [];
            for (var i = 0; i < items.length; i++) {
                if (items[i].name.toUpperCase().indexOf(search.toUpperCase()) != -1) {
                    arrayToReturn.push(items[i]);
                }
            }
            return arrayToReturn;
        };
    });

</script>
