<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
             <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Recruitment</li>
            <li class="active">Vacancy</li>
            <li class="active">List</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">

            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
					<div class="box-header">
                        <h3 class="box-title">Recruitment | Vacancy | List</h3>
                        <hr>
                    </div>
					
                    <div class="box-body"> 
                    <!-- BEGIN OF SEARCH -->
                         <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-8 col-md-8">
                                    <label for="searchby" class="col-md-12">Search By :</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Request By</option>   
                                        <option>Departement</option>
                                        <option>Vancancy Name</option>   
                                        <option>Due Date</option>    
                                        <option>Number of Request</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Request By</option>   
                                        <option>Departement</option>
                                        <option>Vancancy Name</option>   
                                        <option>Due Date</option>    
                                        <option>Number of Request</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Request By</option>
                                        <option>Departement</option>
                                        <option>Vancancy Name</option>   
                                        <option>Due Date</option>    
                                        <option>Number of Request</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="operator">Operator</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <input type="radio" name="operator" class="flat-red" checked="true" value="AND"/> AND
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="operator" class="flat-red" value="OR"/> OR
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div>
                            <div class="col-xs-12 col-sm-8 col-md-4">
                                <a href="">
                                    <button type="button" class="btn btn-info btn-block"><i class="fa fa-search">&nbsp;</i>Search</button>
                                </a>
                            </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-2 col-xs-12 xol-sm-12">
                                <a href="<?php echo base_url('add/vacancy_add'); ?>">
                                    <button type="button" class="btn btn-success btn-block"> New</button>
                                </a>
                            </div>
                        </div>

                    <div class="box-body  table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                                <tr class="success">
                                    <th class="text-center">No</th>
									<th class="text-center">Request By</th>
									<th class="text-center">Departement</th>
									<th class="text-center">Vancancy Name</th>
									<th class="text-center">Due Date</th>
									<th class="text-center">Number of Request</th>
									<th class="text-center">Action</th>
                                </tr>
								<tr>
                                    <td class="text-center">1</td>
									<td>E_1</td>
									<td>HRD</td>
									<td>Asisten Admin II</td>
									<td>10 Feb 2016</td>
									<td>3</td>
									<td>
        								<a href="<?php echo base_url() . 'view/vacancy_view' ?>" ><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                        &nbsp;
        								<a href="<?php echo base_url() . 'edit/vacancy_edit' ?>" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></i></a>
        								&nbsp;
                                        <a href="<?php echo base_url() . 'view/vacancy_trainer' ?>"><i class="fa fa-users" data-toggle="tooltip" title="View Vacancy Trainer"></i></a>
                                        &nbsp;
                                        <a href="#"><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">2</td>
                                    <td>E_2</td>
                                    <td>Pemasaran</td>
                                    <td>Pembantu Teknisi ICT</td>
                                    <td>21 Sep 2016</td>
                                    <td>5</td>
                                    <td>
                                <a href="<?php echo base_url() . 'view/vacancy_view' ?>" ><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                &nbsp;
                                <a href="<?php echo base_url() . 'edit/vacancy_edit' ?>" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></i></a>
                                &nbsp;
                                <a href="<?php echo base_url() . 'view/vacancy_trainer' ?>"><i class="fa fa-users" data-toggle="tooltip" title="View Vacancy Trainer"></i></a>
                                &nbsp;
                                <a href="#"><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">3</td>
                                    <td>E_3</td>
                                    <td>Supervisor</td>
                                    <td>Ops Crane</td>
                                    <td>15 Mei 2016</td>
                                    <td>1</td>
                                    <td>
                                <a href="<?php echo base_url() . 'view/vacancy_view' ?>" ><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                &nbsp;
                                <a href="<?php echo base_url() . 'edit/vacancy_edit' ?>" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></i></a>
                                &nbsp;
                                <a href="<?php echo base_url() . 'view/vacancy_trainer' ?>"><i class="fa fa-users" data-toggle="tooltip" title="View Vacancy Trainer"></i></a>
                                &nbsp;
                                <a href="#"><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">4</td>
                                    <td>E_4</td>
                                    <td>Logistic</td>
                                    <td>Petugas Admin III</td>
                                    <td>10 Feb 2016</td>
                                    <td>5</td>
                                    <td>
                                <a href="<?php echo base_url() . 'view/vacancy_view' ?>" ><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                &nbsp;
                                <a href="<?php echo base_url() . 'edit/vacancy_edit' ?>" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></i></a>
                                &nbsp;
                                <a href="<?php echo base_url() . 'view/vacancy_trainer' ?>"><i class="fa fa-users" data-toggle="tooltip" title="View Vacancy Trainer"></i></a>
                                &nbsp;
                                <a href="#"><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">5</td>
                                    <td>E_5</td>
                                    <td>HRD</td>
                                    <td>Pemuka Lindungan Lingkungan</td>
                                    <td>15 Jan 2016</td>
                                    <td>5</td>
                                    <td>
                                <a href="<?php echo base_url() . 'view/vacancy_view' ?>" ><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                &nbsp;
                                <a href="<?php echo base_url() . 'edit/vacancy_edit' ?>" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></i></a>
                                &nbsp;
                                <a href="<?php echo base_url() . 'view/vacancy_trainer' ?>"><i class="fa fa-users" data-toggle="tooltip" title="View Vacancy Trainer"></i></a>
                                &nbsp;
                                <a href="#"><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">6</td>
                                    <td>E_6</td>
                                    <td>HRD</td>
                                    <td>Ops Alat Berat</td>
                                    <td>15 Feb 2016</td>
                                    <td>10</td>
                                    <td>
                                <a href="<?php echo base_url() . 'view/vacancy_view' ?>" ><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                &nbsp;
                                <a href="<?php echo base_url() . 'edit/vacancy_edit' ?>" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></i></a>
                                &nbsp;
                                <a href="<?php echo base_url() . 'view/vacancy_trainer' ?>"><i class="fa fa-users" data-toggle="tooltip" title="View Vacancy Trainer"></i></a>
                                &nbsp;
                                <a href="#"><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">7</td>
                                    <td>E_7</td>
                                    <td>Keuangan</td>
                                    <td>Petugas Admin 1</td>
                                    <td>1 Feb 2016</td>
                                    <td>3</td>
                                    <td>
                                <a href="<?php echo base_url() . 'view/vacancy_view' ?>" ><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                &nbsp;
                                <a href="<?php echo base_url() . 'edit/vacancy_edit' ?>" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></i></a>
                                &nbsp;
                                <a href="<?php echo base_url() . 'view/vacancy_trainer' ?>"><i class="fa fa-users" data-toggle="tooltip" title="View Vacancy Trainer"></i></a>
                                &nbsp;
                                <a href="#"><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">8</td>
                                    <td>E_8</td>
                                    <td>Project</td>
                                    <td>Pemuka Inspeksi</td>
                                    <td>10 Apr 2016</td>
                                    <td>5</td>
                                    <td>
                                <a href="<?php echo base_url() . 'view/vacancy_view' ?>" ><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                &nbsp;
                                <a href="<?php echo base_url() . 'edit/vacancy_edit' ?>" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></i></a>
                                &nbsp;
                                <a href="<?php echo base_url() . 'view/vacancy_trainer' ?>"><i class="fa fa-users" data-toggle="tooltip" title="View Vacancy Trainer"></i></a>
                                &nbsp;
                                <a href="#"><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">9</td>
                                    <td>E_9</td>
                                    <td>HRD</td>
                                    <td>Petugas Admin II</td>
                                    <td>11 Mar 2016</td>
                                    <td>3</td>
                                    <td>
                                <a href="<?php echo base_url() . 'view/vacancy_view' ?>" ><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                &nbsp;
                                <a href="<?php echo base_url() . 'edit/vacancy_edit' ?>" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></i></a>
                                &nbsp;
                                <a href="<?php echo base_url() . 'view/vacancy_trainer' ?>"><i class="fa fa-users" data-toggle="tooltip" title="View Vacancy Trainer"></i></a>
                                &nbsp;
                                <a href="#"><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">10</td>
                                    <td>E_10</td>
                                    <td>Personalia</td>
                                    <td>Pengemudi Truck</td>
                                    <td>10 Feb 2016</td>
                                    <td>3</td>
                                    <td>
                                <a href="<?php echo base_url() . 'view/vacancy_view' ?>" ><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                &nbsp;
                                <a href="<?php echo base_url() . 'edit/vacancy_edit' ?>" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></i></a>
                                &nbsp;
                                <a href="<?php echo base_url() . 'view/vacancy_trainer' ?>"><i class="fa fa-users" data-toggle="tooltip" title="View Vacancy Trainer"></i></a>
                                &nbsp;
                                <a href="#"><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></a>
                                    </td>
                                </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
