<link href="../../asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<div class="content-wrapper">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Loan</li>
            <li class="active">Requested</li>
            <li class="active">List</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Loan | Requested | List</h3>
                        <hr>
                    </div>
                    <div class="box-body">
                        
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-8 col-md-8">
                                    <label for="searchby" class="col-md-12">Search By :</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Employee Name</option>
                                        <option>Position</option>
                                        <option>Years of Employment</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Employee Name</option>
                                        <option>Position</option>
                                        <option>Years of Employment</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Employee Name</option>
                                        <option>Position</option>
                                        <option>Years of Employment</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="operator">Operator</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <input type="radio" name="operator" class="flat-red" checked="true" value="AND"/> AND
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="operator" class="flat-red" value="OR"/> OR
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div>
                            <div class="col-xs-12 col-sm-8 col-md-4">
                                <a href="">
                                    <button type="button" class="btn btn-info btn-block"><i class="fa fa-search" data-toggle="tooltip" title="View">&nbsp;</i>Search</button>
                                </a>
                            </div>
                            </div>
                        </div>
                        <!--END OF SEARCH-->

                        <!--
                        <div class="col-md-12 col-xs-12 col-sm-12">
                                <select ng-model="depart" class="departemen" >
                                    <option value= "">List Of Department</option>
                                    <option value="{{ dept.nama }}" ng-repeat="dept in departemen">{{ dept.nama }}</option>
                                </select>
                                
                                <select ng-model="depart" class="departemen" >
                                    <option value= "">List Of EmployeeID</option>
                                    <option value="{{ emp.ID }}" ng-repeat="emp in employee">{{ emp.ID }}</option>
                                </select> 
                                
                                <select ng-model="depart" class="departemen" >
                                    <option value= "">List Of Joined Date</option>
                                    <option value="{{ joi.date }}" ng-repeat="joi in joined">{{ joi.date }}</option>
                                </select> 
                        </div>
                        -->
        
                        <div class="col-md-12">
                            <div class="col-md-2 col-xs-12 xol-sm-12">
                                <a href="<?php echo base_url('add/loan_requested_add'); ?>">
                                    <button type="button" class="btn btn-success btn-block"> New</button>
                                </a>
                            </div>
                        </div>

                                    <!--<div class="col-md-2">
                                        <a href="<?php echo base_url(); ?>add/employee_register"><input type="submit" class="btn btn-block btn-success" value="Tambah"></a>
                                        </div>
                                    </div>-->    
                    
                  <div class="box-body table-responsive " style="width:100%">
                            <table class="table table-striped table-bordered">
                                <tr class="success">
                                    <th class="text-center">No</th>
                                    <th class="text-center">Employee Name</th>
                                    <th class="text-center">Position</th>
                                    <th class="text-center">Years of Employment</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            <tr>
                                    <td class="text-center">1</td>
                                    <td>SUDARMONO</td>
                                    <td>Operation Director</td>
                                    <td class="text-center">1 Years 2 Month</td>
                                    <td class="text-center"><span class="need_aproval_status">Need Aproval</span></center></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>view/loan_requested_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    </td> 
                            </tr> 
                                    
                            <tr>
                                    <td class="text-center">2</td>
                                    <td>ARBONDES</td>
                                    <td>Finance Director</td>
                                    <td class="text-center">3 Years 2 Month</td>
                                    <td class="text-center"><span class="draft_status">Draft</span></center></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>edit/loan_requested_edit"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                    </td> 
                            </tr> 
                    
                            <tr>
                                    <td class="text-center">3</td>
                                    <td>ISKANDAR</td>
                                    <td>Drilling Manager</td>
                                    <td class="text-center">1 Years 4 Month</td>
                                    <td class="text-center"><span class="need_aproval_status">Need Aproval</span></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>view/loan_requested_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    </td> 
                            </tr>
                    
                    
                            <tr>
                                    <td class="text-center">4</td>
                                    <td>ANTON PRIADI</td>
                                    <td>Project Manager</td>
                                    <td class="text-center">2 Years 8 Month</td>
                                    <td class="text-center"><span class="need_aproval_status">Need Aproval</span></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>view/loan_requested_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    </td> 
                            </tr> 
                    
                                
                            <tr>
                                    <td class="text-center">5</td>
                                    <td>I KETUT ADI PUJA ASTAWA</td>
                                    <td>SCM Manager</td>
                                    <td class="text-center">3 Years 9 Month</td>
                                    <td class="text-center"><span class="need_aproval_status">Need Aproval</span></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>view/loan_requested_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    </td> 
                            </tr> 
                    
                            <tr>
                                    <td class="text-center">6</td>
                                    <td>SUBARI.M</td>
                                    <td>HRGA Manager</td>
                                    <td class="text-center">2 Years 4 Month</td>
                                    <td class="text-center"><span class="need_aproval_status">Need Aproval</span></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>view/loan_requested_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    </td> 
                            </tr> 

                            <tr>
                                    <td class="text-center">7</td>
                                    <td>SUNARDI</td>
                                    <td>IT Manager</td>
                                    <td class="text-center">1 Years 9 Month</td>
                                    <td class="text-center"><span class="draft_status">Draft</span></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>edit/loan_requested_edit"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                    </td> 
                            </tr> 
                    
                    
                            <tr>
                                    <td class="text-center">8</td>
                                    <td>BAMBANG IRAWAN</td>
                                    <td>F & A Manager</td>
                                    <td class="text-center">4 Years 6 Month</td>
                                    <td class="text-center"><span class="draft_status">Draft</span></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>edit/loan_requested_edit"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                    </td> 
                            </tr> 

                            <tr>
                                    <td class="text-center">9</td>
                                    <td>CHAIRUL AZWAN</td>
                                    <td>Operator Produksi</td>
                                    <td class="text-center">5 Years 2 Month</td>
                                    <td class="text-center"><span class="draft_status">Draft</span></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>edit/loan_requested_edit"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                    </td>
                            </tr> 
                                 
                                 
                            <tr>
                                    <td class="text-center">10</td>
                                    <td>AHMAD SARURI</td>
                                    <td>Assistant Manager</td>
                                    <td class="text-center">4 Years 5 Month</center></td>
                                    <td class="text-center"><span class="draft_status">Draft</span></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>edit/loan_requested_edit"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                    </td>
                            </tr>
                        </table>
                        
                        
                        <!--<div ng-show="false">{{jumlah = (employee | departemen:depart | nama:search | dateRange:dari:to ).length }}</div>-->
                        
                            <div class="col-xs-11 col-sm-12 col-md-12"><br>
                                <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                   <ul class="pagination">
                    <li class="paginate_button previous disabled" id="example1_previous"><a href="#" aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a></li>
                    <li class="paginate_button active"><a href="#" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a></li>
                    <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a></li>
                    <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" tabindex="0">3</a></li>
                    <li class="paginate_button next" id="example1_next"><a href="#" aria-controls="example1" data-dt-idx="7" tabindex="0">Next</a></li>
                   </ul></div>
                            </div>
                    </div>                  
                </div>
            </div>
        </div>

    </section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
      $(".departemen").select2();
    });
    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      $scope.sortType     = 'no'; // set the default sort type
      $scope.sortReverse  = false;  // set the default sort order
      $scope.search   = ''; 
      $scope.limit =25;
      $scope.dari="";
      $scope.to=""; 
      $scope.depart="";  
      $scope.jumlah = 0;
                   
      
      $scope.employee = <?php echo $employee ?>;
      
      
      $scope.departemen = [
        {nama : "Purchasing"},
        {nama : "Research"},
        {nama : "HRD"}
        ];
        
        $scope.reset = function () {
            $scope.depart =  "";     
        } 
        
        
       $scope.currentPage = 1;  
       $scope.totalItems = $scope.employee.length;  
       $scope.numPerPage = $scope.limit;    
       
       $scope.limitPage = function() {
         $scope.numPerPage = $scope.limit;
         if($scope.currentPage * $scope.numPerPage > $scope.employee.length){
            $scope.currentPage = 1;   
         }
      };
       
       $scope.lastPage = function() {      
         $scope.currentPage=$scope.pageCount();
      };
      
      $scope.firstPage = function() {
         $scope.currentPage=1;
      };
       
       $scope.nextPage = function() {
        
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
      };
      
      $scope.jumlahPerpage = function (value) {
        $scope.numPerPage = value;
      } 
      
      $scope.prevPage = function() {
        if ($scope.currentPage > 1) {
          $scope.currentPage--;
        }
      };
      
       $scope.pageCount = function() {
        return Math.ceil($scope.jumlah/$scope.numPerPage);
      };  
      
      $scope.checkAll = function () {
        angular.forEach($scope.employee, function (item) {
            item.Selected = $scope.selectAll;
        });
      };
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    app.filter("dateRange", function() {      
      return function(items, dari, to) {
        if(dari.length==0){
            var dari = +new Date("1980-01-01");
        }else{
            var dari = +new Date(dari);
        }
        
        if(to.length==0){
            var to = +new Date();
        }else{
            var to = +new Date(to);
        }
        var df = dari;
        var dt = to ;
        var arrayToReturn = [];        
        for (var i=0; i<items.length; i++){
            var tf = +new Date(items[i].join);
            if ((tf > df && tf < dt) || (tf==dt) )  {
                arrayToReturn.push(items[i]);
            }
        }
        
        return arrayToReturn;
      };
        
    });
    
    
    app.filter("paging", function() {      
      return function(items, limit, currentPage) {
        if (typeof limit === 'string' || limit instanceof String){
           limit = parseInt(limit);
         }
         
         var begin, end, index;  
         begin = (currentPage - 1) * limit;  
         end = begin + limit;
         var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (begin <= i && i < end )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    
    app.filter("departemen", function() {      
      return function(items, depart) {
      if(depart.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].dept == depart )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    
    app.filter("nama", function() {      
      return function(items, search) {
      if(search.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].name.toUpperCase().indexOf(search.toUpperCase()) != -1 )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    

</script>