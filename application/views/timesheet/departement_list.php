<script>
    $(function() {
        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#">HR</a></li>
            <li class="active">Department</li>
            <li class="active">List</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Department | List</h3>
                        <hr>
                    </div>
                    <div class="box-body"> <!-- BEGIN OF SEARCH -->
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-8 col-md-8">
                                    <label for="searchby" class="col-md-12">Search By :</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-xs-12 col-sm-8 col-md-8">
                                <div class="row form-group">
                                    <div class="col-xs-12 col-sm-5 col-md-5">
                                        <select class="form-control">
                                            <option>-- Select Field Name --</option>
                                            <option>Department Code</option>
                                            <option>Department Name</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-12 col-sm-1 col-md-1">
                                        <label for="code">Value</label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <input type="text" class="form-control" name=""/>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-xs-12 col-sm-5 col-md-5">
                                        <select class="form-control">
                                            <option>-- Select Field Name --</option>
                                            <option>Department Code</option>
                                            <option>Department Name</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-12 col-sm-1 col-md-1">
                                        <label for="code">Value</label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <input type="text" class="form-control" name=""/>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-xs-12 col-sm-5 col-md-5">
                                        <select class="form-control">
                                            <option>-- Select Field Name --</option>
                                            <option>Department Code</option>
                                            <option>Department Name</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-12 col-sm-1 col-md-1">
                                        <label for="code">Value</label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <input type="text" class="form-control" name=""/>
                                    </div>
                                </div>
                                <div class="row form-group">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <label for="operator">Operator</label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <input type="radio" name="operator" class="flat-red" checked="true" value="AND"/> AND
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="operator" class="flat-red" value="OR"/> OR
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div>
                                <div class="col-xs-12 col-sm-8 col-md-4">
                                    <a href="">
                                        <button type="button" class="btn btn-info btn-block"><i class="fa fa-search"></i> Search</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">&nbsp;</div>
                        <!-- END OF SEARCH -->
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <a href="<?php echo base_url('add/departement_register'); ?>">
                                    <button type="button" class="btn btn-success btn-block"> New</button>
                                </a>
                            </div>
                        </div>
                        <!--<div class="col-md-6 col-xs-12 col-sm-12 pull-left" style="margin-top:20px;margin-bottom:20px;">
                                <div class="col-md-4">
                                        <a href="<?php echo base_url(); ?>add/departement_register"><input type="button" class="btn btn-block btn-success" value="New"></a>
                                </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-12 pull-right" style="margin-top:20px;margin-bottom:20px;">
                                <div class="col-md-8">
                                        <div class="col-md-4 col-xs-12 col-sm-12 ">Search</div>
                                        <div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" placeholder="Enter ..." ng-model="search"/></div>
                                </div>
                        </div>-->
                        <div class="box-body table-responsive " style="width:100%">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr class="success"> 
                                    <th ><center>No</center></th> 
                                    <th ><center>Department Code</center></th>
                                    <th ><center>Department Name
                                    <!--<a href="#" ng-click="sortType = 'name'; sortReverse = !sortReverse">
                                    <span ng-show="sortType == 'name' && !sortReverse" class="fa fa-caret-down"></span>
                                    <span ng-show="sortType == 'name' && sortReverse" class="fa fa-caret-up"></span>
                                    </a>--></center>
                                </th>
                                <th ><center>Action</center></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="dat in data| orderBy:sortType:sortReverse ">
                                            <!--<td>{{($index+1)+((currentPage-1)*limit)}}</td>
                                            <td>{{dat.departement_id}}</td>
                                            <td>{{dat.departement_name}}</td>-->
                                            <!--<td class="action">
                                                    <a href="#" class="button" title="Row Detail"><i class="fa fa-search"></i></a>&nbsp;&nbsp;
                                                    <a href="<?php echo base_url(); ?>edit/departement_edit/{{dat.departement_id}}"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                            </td> -->
                                    </tr>
                                    <tr>
                                        <td align="center">1</td>
                                        <td>D_1</td>
                                        <td>Produksi</td>
                                        <td align="center">
                                            <a data-toggle="tooltip" title="View Detail"><i class="fa fa-search" data-toggle="modal" data-target="#myModal1"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal1" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Information Detail</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p><pre>No         : 1</pre></p>
                                                            <p><pre>Dept. ID   : D_1</pre></p>
                                                            <p><pre>Name       : Produksi</pre></p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                    <!--<i class="fa fa-search">-->
                                            &nbsp;
                                            <a href="<?php echo base_url() . 'edit/departement_edit' ?>" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">2</td>
                                        <td>D_2</td>
                                        <td>Pemasaran</td>
                                        <td align="center">
                                            <a data-toggle="tooltip" title="View Detail"><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal2"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal2" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Information Detail</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p><pre>No         : 2</pre></p>
                                                            <p><pre>Dept. ID   : D_2</pre></p>
                                                            <p><pre>Name       : Pemasaran</pre></p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                    <!--<i class="fa fa-search">-->
                                            &nbsp;
                                            <a href="<?php echo base_url() . 'edit/departement_edit' ?>" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">3</td>
                                        <td>D_3</td>
                                        <td>Keuangan</td>
                                        <td align="center">
                                            <a data-toggle="tooltip" title="View Detail"><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal3"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal3" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Information Detail</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p><pre>No         : 3</pre></p>
                                                            <p><pre>Dept. ID   : D_3</pre></p>
                                                            <p><pre>Name       : Keuangan</pre></p>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                    <!--<i class="fa fa-search">-->
                                            &nbsp;
                                            <a href="<?php echo base_url() . 'edit/departement_edit' ?>" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">4</td>
                                        <td>D_4</td>
                                        <td>Layanan Pelanggan</td>
                                        <td align="center">
                                            <a data-toggle="tooltip" title="View Detail"><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal4"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal4" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Information Detail</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p><pre>No         : 4</pre></p>
                                                            <p><pre>Dept. ID   : D_4</pre></p>
                                                            <p><pre>Name       : Layanan Pelanggan</pre></p>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                    <!--<i class="fa fa-search">-->
                                            &nbsp;
                                            <a href="<?php echo base_url() . 'edit/departement_edit' ?>" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">5</td>
                                        <td>D_5</td>
                                        <td>Purchasing</td>
                                        <td align="center">
                                            <a data-toggle="tooltip" title="View Detail"><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal5"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal5" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Information Detail</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p><pre>No         : 5</pre></p>
                                                            <p><pre>Dept. ID   : D_5</pre></p>
                                                            <p><pre>Name       : Purchasing</pre></p>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                    <!--<i class="fa fa-search">-->
                                            &nbsp;
                                            <a href="<?php echo base_url() . 'edit/departement_edit' ?>" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">6</td>
                                        <td>D_6</td>
                                        <td>Logistic</td>
                                        <td align="center">
                                            <a data-toggle="tooltip" title="View Detail"><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal6"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal6" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Information Detail</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p><pre>No         : 6</pre></p>
                                                            <p><pre>Dept. ID   : D_6</pre></p>
                                                            <p><pre>Name       : Logistic</pre></p>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                    <!--<i class="fa fa-search">-->
                                            &nbsp;
                                            <a href="<?php echo base_url() . 'edit/departement_edit' ?>" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">7</td>
                                        <td>D_7</td>
                                        <td>Project</td>
                                        <td align="center">
                                            <a data-toggle="tooltip" title="View Detail"><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal7"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal7" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Information Detail</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p><pre>No         : 7</pre></p>
                                                            <p><pre>Dept. ID   : D_7</pre></p>
                                                            <p><pre>Name       : Project</pre></p>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                    <!--<i class="fa fa-search">-->
                                            &nbsp;
                                            <a href="<?php echo base_url() . 'edit/departement_edit' ?>" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">8</td>
                                        <td>D_8</td>
                                        <td>HRD</td>
                                        <td align="center">
                                            <a data-toggle="tooltip" title="View Detail"><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal8"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal8" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Information Detail</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p><pre>No         : 8</pre></p>
                                                            <p><pre>Dept. ID   : D_8</pre></p>
                                                            <p><pre>Name       : HRD</pre></p>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                    <!--<i class="fa fa-search">-->
                                            &nbsp;
                                            <a href="<?php echo base_url() . 'edit/departement_edit' ?>" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">9</td>
                                        <td>D_9</td>
                                        <td>Supervisor</td>
                                        <td align="center">
                                            <a data-toggle="tooltip" title="View Detail"><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal9"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal9" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Information Detail</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p><pre>No         : 9</pre></p>
                                                            <p><pre>Dept. ID   : D_9</pre></p>
                                                            <p><pre>Name       : Supervisor</pre></p>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                    <!--<i class="fa fa-search">-->
                                            &nbsp;
                                            <a href="<?php echo base_url() . 'edit/departement_edit' ?>" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">10</td>
                                        <td>D_10</td>
                                        <td>Personalia</td>
                                        <td align="center">
                                            <a data-toggle="tooltip" title="View Detail"><i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal10"></i></a>
                                            &nbsp;&nbsp;
                                            <div class="modal fade" id="myModal10" role="dialog">
                                                <div class="modal-dialog modal-sm" align="left">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Information Detail</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p><pre>No         : 10</pre></p>
                                                            <p><pre>Dept. ID   : D_10</pre></p>
                                                            <p><pre>Name       : Personalia</pre></p>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                    <!--<i class="fa fa-search">-->
                                            &nbsp;
                                            <a href="<?php echo base_url() . 'edit/departement_edit' ?>" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></i></a>
                                        </td>
                                    </tr>

                                </tbody> 
                            </table>
                        </div>
                        <br>
                        <div class="col-xs-11 col-sm-12 col-md-12">
                            <ul class="pagination pull-right" >
                                <li class="disabled"><a href="#"><i class="fa fa-arrow-circle-left"></i></a></li>
                                <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#"><i class="fa fa-arrow-circle-right"></i></a></li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("a.button").click(function() {
            $.fancybox(
                    '<div style="height:350px;width:450px;overflow:hidden;"><iframe src="<?php echo base_url(); ?>dashboard/departement_detail" frameborder="0" scrolling="no" style="width:100%;height:800px;margin-top:-200px;"></iframe></div>',
                    {
                        'autoDimensions': false,
                        'width': 'auto',
                        'height': 'auto',
                        'transitionIn': 'none',
                        'transitionOut': 'none'
                    }
            );
        });
    });

    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
        $scope.sortType = 'no'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order
        $scope.search = '';
        $scope.limit = 25;
        $scope.jumlah = 0;


        $scope.data = <?php echo $departement; ?>
        //$scope.data= [{text:'learn angular', done:true}]
        $scope.currentPage = 1;
        $scope.totalItems = $scope.data.length;
        $scope.numPerPage = $scope.limit;

        $scope.limitPage = function() {
            $scope.numPerPage = $scope.limit;
            if ($scope.currentPage * $scope.numPerPage > $scope.data.length) {
                $scope.currentPage = 1;
            }
        };

        $scope.lastPage = function() {
            $scope.currentPage = $scope.pageCount();
        };

        $scope.firstPage = function() {
            $scope.currentPage = 1;
        };

        $scope.nextPage = function() {

            if ($scope.currentPage < $scope.pageCount()) {
                $scope.currentPage++;
            }
        };

        $scope.jumlahPerpage = function(value) {
            $scope.numPerPage = value;
        }

        $scope.prevPage = function() {
            if ($scope.currentPage > 1) {
                $scope.currentPage--;
            }
        };

        $scope.pageCount = function() {
            return Math.ceil($scope.jumlah / $scope.numPerPage);
        };
    });

    app.filter("paging", function() {
        return function(items, limit, currentPage) {
            if (typeof limit === 'string' || limit instanceof String) {
                limit = parseInt(limit);
            }

            var begin, end, index;
            begin = (currentPage - 1) * limit;
            end = begin + limit;
            var arrayToReturn = [];
            for (var i = 0; i < items.length; i++) {
                if (begin <= i && i < end) {
                    arrayToReturn.push(items[i]);
                }
            }
            return arrayToReturn;
        };
    });

    app.filter("nama", function() {
        return function(items, search) {
            if (search.length == 0) {
                return items;
            }
            var arrayToReturn = [];
            for (var i = 0; i < items.length; i++) {
                if (items[i].name.toUpperCase().indexOf(search.toUpperCase()) != -1) {
                    arrayToReturn.push(items[i]);
                }
            }
            return arrayToReturn;
        };
    });

</script>
