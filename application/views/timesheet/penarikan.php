<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#">DEPARTEMENT</a></li>
            <li class="active">Departement List</li>
        </ol>
    </section>
	
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">DEPARTEMENT | Departement List</h3>
                    </div>
					<div class="col-md-6 col-xs-12 col-sm-12 pull-left" style="margin-top:20px;margin-bottom:20px;">
						<div class="col-md-4">
							<a href="<?php echo base_url(); ?>add/departement_register"><input type="button" class="btn btn-block btn-success" value="New"></a>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 col-sm-12 pull-right" style="margin-top:20px;margin-bottom:20px;">
						<div class="col-md-8">
							<div class="col-md-4 col-xs-12 col-sm-12 ">Search</div>
							<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" placeholder="Enter ..." ng-model="search"/></div>
						</div>
					</div>
                    <div class="box-body table-responsive " style="width:100%">
                        <table class="table table-striped table-bordered">
						<a href="<?php echo base_url(); ?>edit/departement_edit/{{dat.departement_id}}"><i class="fa fa-pencil"></i></a>
									</td> 
								 </tr>

					<tr>
									<td>10</td>
									<td>D_10</td>
									<td>Director</td>
									<td class="action">
									
					<a>
                      <i class="fa fa-search" data-toggle="modal" title="Row Detail" data-target="#myModal10"></i></a>
                    &nbsp;&nbsp;
                    <div class="modal fade" id="myModal10" role="dialog">
                      <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Information Detail</h4>
                          </div>
                          <div class="modal-body">
                              <p>No.	     : 10</p>
                              <p>Dept.ID     : D_10</p>
                              <p>Name		 : Director</p>
                            
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
									
							</tbody> 
                        </table>
						
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="container">
  <h2>Information View</h2>
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg" data-toggle="departement_list" data-target="departement_list">Open Small Modal</button>

  <!-- departement -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>No</p>
		  <p>Dept.ID</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
      $("a.button").click(function(){
		$.fancybox(
		'<div style="height:350px;width:450px;overflow:hidden;"><iframe src="<?php echo base_url(); ?>dashboard/departement_detail" frameborder="0" scrolling="no" style="width:100%;height:800px;margin-top:-200px;"></iframe></div>',
			{
				'autoDimensions'	: false,
				'width'         	: 'auto',
				'height'        	: 'auto',
				'transitionIn'		: 'none',
				'transitionOut'		: 'none'
			}
		);
	});
    });
    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      $scope.sortType     = 'no'; // set the default sort type
      $scope.sortReverse  = false;  // set the default sort order
      $scope.search   = ''; 
      $scope.limit =25;
      $scope.jumlah = 0;
                                          
      
      $scope.data = <?php echo $departement; ?>
         
       $scope.currentPage = 1;  
       $scope.totalItems = $scope.data.length;  
       $scope.numPerPage = $scope.limit;    
       
       $scope.limitPage = function() {
         $scope.numPerPage = $scope.limit;
         if($scope.currentPage * $scope.numPerPage > $scope.data.length){
            $scope.currentPage = 1;   
         }
      };
       
       $scope.lastPage = function() {      
         $scope.currentPage=$scope.pageCount();
      };
      
      $scope.firstPage = function() {
         $scope.currentPage=1;
      };
       
       $scope.nextPage = function() {
        
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
      };
      
      $scope.jumlahPerpage = function (value) {
        $scope.numPerPage = value;
      } 
      
      $scope.prevPage = function() {
        if ($scope.currentPage > 1) {
          $scope.currentPage--;
        }
      };
      
       $scope.pageCount = function() {
        return Math.ceil($scope.jumlah/$scope.numPerPage);
      };        
    });
    
    app.filter("paging", function() {      
      return function(items, limit, currentPage) {
        if (typeof limit === 'string' || limit instanceof String){
           limit = parseInt(limit);
         }
         
         var begin, end, index;  
         begin = (currentPage - 1) * limit;  
         end = begin + limit;
         var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (begin <= i && i < end )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
     
    app.filter("nama", function() {      
      return function(items, search) {
      if(search.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].name.toUpperCase().indexOf(search.toUpperCase()) != -1 )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });

</script>
