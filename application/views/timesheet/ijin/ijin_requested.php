<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<link href="../../asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li class="active">Ijin</li>
            <li class="active">Requested</li>
            <li class="active">List</li>
        </ol>
    </section>
	
    <section class="content">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
				        <div class="box-header">
                            <h3 class="box-title">Ijin | Requested | List</h3>
                            <hr>
                        </div>
                        <div class="box-body">
                        
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-8 col-md-8">
                                    <label for="searchby" class="col-md-12">Search By :</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Employee Code</option>
                                        <option>Employee Name</option>
                                        <option>Category</option>
                                        <option>Date</option>
                                        <option>Time</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Employee Code</option>
                                        <option>Employee Name</option>
                                        <option>Category</option>
                                        <option>Date</option>
                                        <option>Time</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Employee Code</option>
                                        <option>Employee Name</option>
                                        <option>Category</option>
                                        <option>Date</option>
                                        <option>Time</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="operator">Operator</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <input type="radio" name="operator" class="flat-red" checked="true" value="AND"/> AND
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="operator" class="flat-red" value="OR"/> OR
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div>
                            <div class="col-xs-12 col-sm-8 col-md-4">
                                <a href="">
                                    <button type="button" class="btn btn-info btn-block"><i class="fa fa-search" data-toggle="tooltip" title="View">&nbsp;</i>Search</button>
                                </a>
                            </div>
                            </div>
                        </div>
                        <!--END OF SEARCH-->
					
                    
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <a href="<?php echo base_url('add/ijin_requested_add'); ?>">
                                    <button type="button" class="btn btn-success btn-block"> New</button>
                                </a>
                            </div>
                        </div>                    
                  <div class="box-body table-responsive " style="width:100%">
                            <table class="table table-striped table-bordered">
                                <tr class="success text-center">
                                    <th rowspan="2" class="text-center" style="vertical-align:middle;">No</th>
                                    <th rowspan="2" class="text-center" style="vertical-align:middle;">Employee Code</th>
                                    <th rowspan="2" class="text-center" style="vertical-align:middle;">Employee Name</th>
                                    <th rowspan="2" class="text-center" style="vertical-align:middle;">Category</th>
                                    <th rowspan="2" class="text-center" style="vertical-align:middle;">Date</th>
                                    <th colspan="2" class="text-center">Time</th>
                                    <th rowspan="2" class="text-center" style="vertical-align:middle;">Status</th>
                                    <th rowspan="2" class="text-center" style="vertical-align:middle;">Action</th>
                                </tr>
                                <tr class="success">
                                    <th class="text-center">Form</th>
                                    <th class="text-center">To</th>
                                </tr>
			      <!--
			      
			      <table id="example1" class="table table-bordered ">
                                <tr>
                                    <th rowspan = 2><center>No</center></th>
									<th rowspan = 2><center>Request Number</center></th>
									<th rowspan = 2><center>Date</center></th>
									<th rowspan = 2><center>Employee ID</center></th>
									<th rowspan = 2><center>Employee Name</center></th>
									<th rowspan = 2><center>Category</center></th>
									<th colspan = 2 ><center>Time</center></th>
									<th rowspan = 2><center>Action</center></th>
									<th rowspan = 2><center>Status</center></th>
                                </tr>-->
								  <!--<tr>
									<th><center>From</center></th>
									<th><center>To</center></th>
								  </tr>-->
								<tr>
									<td align="center">1</td>
									<td >E_1</td>
                                    <td >SUDARMONO</td>
                                    <td >Izin Pulang Cepat</td>
                                    <td align="center">16-Feb-2016</td>
									<td align="center">08.00</td>
									<td align="center">12.00</td>
									<td><center><span class="need_aproval_status">Need Aproval</span></center></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>view/ijin_requested_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">2</td>
                                    <td >E_2</td>
                                    <td >ARBONDES</td>
                                    <td >Izin Terlambat</td>
                                    <td align="center">21-Aug-2016</td>
                                    <td align="center">08.00</td>
                                    <td align="center">09.00</td>
                                    <td><center><span class="draft_status">Draft</span></center></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>edit/ijin_requested_edit"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">3</td>
                                    <td >E_3</td>
                                    <td >ISKANDAR</td>
                                    <td >Izin Keluar Kantor</td>
                                    <td align="center">09-Jan-2016</td>
                                    <td align="center">10.00</td>
                                    <td align="center">13.00</td>
                                    <td><center><span class="need_aproval_status">Need Aproval</span></center></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>view/ijin_requested_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">4</td>
                                    <td >E_4</td>
                                    <td >ANTON PRIADI</td>
                                    <td >Izin Pulang Cepat</td>
                                    <td align="center">01-Apr-2016</td>
                                    <td align="center">08.00</td>
                                    <td align="center">12.00</td>
                                    <td><center><span class="need_aproval_status">Need Aproval</span></center></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>view/ijin_requested_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">5</td>
                                    <td >E_5</td>
                                    <td >I KETUT ADI PUJA ASTAWA</td>
                                    <td >Izin Terlambat</td>
                                    <td align="center">10-May-2016</td>
                                    <td align="center">08.00</td>
                                    <td align="center">12.00</td>
                                    <td><center><span class="need_aproval_status">Need Aproval</span></center></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>view/ijin_requested_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">6</td>
                                    <td >E_6</td>
                                    <td >SUBARI.M</td>
                                    <td >Izin Pulang Cepat</td>
                                    <td align="center">21-Jun-2016</td>
                                    <td align="center">08.00</td>
                                    <td align="center">15.00</td>
                                    <td><center><span class="need_aproval_status">Need Aproval</span></center></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>view/ijin_requested_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    </td>
                                </tr>
								<tr>
                                    <td align="center">7</td>
                                    <td >E_7</td>
                                    <td >SUNARDI</td>
                                    <td >Izin Keluar Kantor</td>
                                    <td align="center">20-Aug-2016</td>
                                    <td align="center">08.00</td>
                                    <td align="center">14.00</td>
                                    <td><center><span class="draft_status">Draft</span></center></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>edit/ijin_requested_edit"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">8</td>
                                    <td >E_8</td>
                                    <td >BAMBANG IRAWAN</td>
                                    <td >Izin Pulang Cepat</td>
                                    <td align="center">15-Oct-2016</td>
                                    <td align="center">08.00</td>
                                    <td align="center">13.00</td>
                                    <td><center><span class="draft_status">Draft</span></center></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>edit/ijin_requested_edit"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">9</td>
                                    <td >E_9</td>
                                    <td >CHAIRUL AZWAN</td>
                                    <td >Izin Keluar Kantor</td>
                                    <td align="center">29-Aug-2016</td>
                                    <td align="center">08.00</td>
                                    <td align="center">10.00</td>
                                    <td><center><span class="draft_status">Draft</span></center></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>edit/ijin_requested_edit"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">10</td>
                                    <td >E_10</td>
                                    <td >AHMAD SARURI</td>
                                    <td >Izin Terlambat</td>
                                    <td align="center">30-Nov-2016</td>
                                    <td align="center">08.00</td>
                                    <td align="center">09.00</td>
                                    <td><center><span class="draft_status">Draft</span></center></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url(); ?>edit/ijin_requested_edit"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                    </td>
                                </tr>
                        </table>
                     <!--   <br>
                        	<ul class="pagination pagination-sm no-margin pull-right">
									<li><a href="#">«</a></li>
									<li><a href="#">1<span class="sr-only">(current)</span></a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
									<li><a href="#">»</a></li>
							</ul>-->

                    </div>
                    <div class="col-xs-11 col-sm-12 col-md-12">
                            <ul class="pagination pull-right" >
                  <li><a href="#">Previous</a></li>
                  <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">Next</a></li>
                </ul>
                        </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
