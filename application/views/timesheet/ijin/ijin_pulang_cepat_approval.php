<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#">LEAVE</a></li>
            <li class="active">Approval</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">LEAVE | Approval</h3>
                    </div>
                    <div class="box-body table-responsive">
                        <table id="example1" class="table  table-striped">
                            <tr>
								<td width=20%>Employee ID</td>
								<td>E_1 </td>
							 </tr>

							<tr>
								<td >Request Number</td>
								<td > 1 </td>
							</tr>

						    <tr>
								<td>Employee Naame</td>
								<td > Dery </td>
							</tr>

							<tr>
								<td >Category</td>
								<td> Pulang Cepat </td>
							</tr>


							<tr>
								<td >From</td>
								<td> 14.00 </td>
							</tr>
							
							<tr>
								<td>To</td>
								<td valign="bottom" >  </td>
							</tr>
						
							<tr>
								<td style=" vertical-align: top;" >Note</td >
								<td ><textarea style="margin-top:10px;" rows="10" cols="50"></textarea></td>
							</tr>
                        </table>
						<div class="col-md-4 col-xs-12 col-sm-12">
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-success" value="Approve"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-danger" value="Reject"></a>
							</div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<a href="#"><input type="button" class="btn btn-block btn-warning" value="Revew Later"></a>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
