<script>
    $(function() {
        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
    $(function() {
        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});
    });
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li><a href="#"></i>Report</a></li>
            <li><a href="#"></i>Employee</a></li>
            <li class="active">List</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">

            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Report | Employee | List</h3>
                        <hr>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12" style="padding-top:5px; padding-bottom:5px;">
                            <label for="searchby" class="col-md-2">Search By :</label>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <?php
                                $department = array("Produksi", "Pemasaran", "Keuangan", "Layanan Pelanggan",
                                    "Purchasing", "Logistic", "Project", "HRD", "Personalia");

                                if ($_SERVER['REQUEST_METHOD'] === "POST") {
                                    if (isset($_POST['name_department'])) {
                                        if (in_array($_POST['name_department'], $department)) {
                                            echo "You selected " . $_POST['name_department'] . "!";
                                            exit;
                                        }
                                    }
                                }
                                ?>
                                <select class="form-control">
                                    <option>-- Select Department --</option>
                                    <?php
                                    foreach ($department as $name_department) {
                                        echo '<option value="' . $name_department . '">' . $name_department . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <?php
                                $position = array("Operation Director", "Finance Director", "Drilling Manager", "Project Manager", "SCM Manager", "
            HRGA Manager", "IT Manager", "F & A Manager", "Operator Produksi", "Assistant Manager");

                                if ($_SERVER['REQUEST_METHOD'] === "POST") {
                                    if (isset($_POST['name_position'])) {
                                        if (in_array($_POST['name_position'], $position)) {
                                            echo "You selected " . $_POST['name_position'] . "!";
                                            exit;
                                        }
                                    }
                                }
                                ?>
                                <select class="form-control">
                                    <option>-- Select Position --</option>
                                    <?php
                                    foreach ($position as $name_position) {
                                        echo '<option value="' . $name_position . '">' . $name_position . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-1">
                                <a href="">
                                    <button type="button" class="btn btn-info btn-block"><i class="fa fa-search">&nbsp;</i>Search</button>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-4 col-xs-12" style="padding-top:5px; padding-bottom:5px;">
                            <div class="col-md-3 col-sm-4 col-xs-12" >
                                <label for="operator">Date From</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" id="datepicker1" class="form-control pull-rigth active"/>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-12">
                                <label for="operator">Date To</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" id="datepicker2" class="form-control pull-rigth active"/>
                                </div>
                            </div>
                            <div class="col-md-2">
                            	<label>&nbsp;</label>
                                <a href="<?php echo base_url(); ?>upload/report/File_employee.xlsx">
                                    <button type="button" class="btn btn-info btn-block"><i class="fa fa-print">&nbsp;</i>Export To Excel</button>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-4 col-xs-12" style="padding-top:5px; padding-bottom:5px;">
                        	<div class="col-md-12">
	                            <div class="row warning_status">
	                            	<a class="col-md-1" style="width:5px;" href="#"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
	                                <label class="col-md-2">Warning Report <span class="badge_status">5</span></label>
	                            </div>
	                            <div class="row leave_status">
	                            	<a class="col-md-1" style="width:5px;" href="#"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
	                                <label class="col-md-2">Leave Request <span class="badge_status">5</span></label>
	                            </div>
	                            <div class="row ijin_status">
	                            	<a class="col-md-1" style="width:5px;" href="#"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
	                                <label class="col-md-2">Ijin <span class="badge_status">5</span></label>
	                            </div>
	                            <div class="row loan_status">
	                            	<a class="col-md-1" style="width:5px;" href="#"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
	                                <label class="col-md-2">Loan <span class="badge_status">5</span></label>
	                            </div>
	                            <div class="row man_status">
	                            	<a class="col-md-1" style="width:5px;" href="#"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
	                                <label class="col-md-2">Man Power Request<span class="badge_status">5</span></label>
	                            </div>
	                        </div>
                        </div>
                    <div class="box-body table-responsive " style="width:100%">
                            <table class="table table-striped table-bordered">
                                <tr class="success">
                                    <th class="text-center">No</th> 
                                    <th class="text-center">Employee Code</th>
                                    <th class="text-center">Employee Name</th>
                                    <th class="text-center">Position</th>
                                    <th class="text-center">Joined Date</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Action</th>

                                </tr>	
                                <tr>
                                    <td class="text-center">1</td>
                                <td>E_1</td>
                                <td>SUDARMONO</td>
                                <td>Operation Director</td>
                                <td class="text-center">1 Januari 2016</td>
                                <td class="text-center"><span class="permanent_status">Permanent</span></td>
                                <td class="text-center">

                                    <a href="<?php echo base_url(); ?>view/report_employee_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    &nbsp;&nbsp;
                                </td> 
                                </tr> 

                                <tr>
                                    <td class="text-center">2</td>
                                <td>E_2</td>
                                <td>ARBONDES</td>
                                <td>Finance Director</td>
                                <td class="text-center">10 Januari 2016</td>
                                <td class="text-center"><span class="permanent_status">Permanent</span></td>
                                <td class="text-center">
                                    <a href="<?php echo base_url(); ?>view/report_employee_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    &nbsp;&nbsp;
                                </td> 
                                </tr> 

                                <tr>
                                    <td class="text-center">3</td>
                                <td>E_3</td>
                                <td>ISKANDAR</td>
                                <td>Drilling Manager</td>
                                <td class="text-center">28 Januari 2016</td>
                                <td class="text-center"><span class="resigned_status">Resigned</span></td>
                                <td class="text-center">
                                    <a href="<?php echo base_url(); ?>view/report_employee_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    &nbsp;&nbsp;
                                </td> 
                                </tr>


                                <tr>
                                    <td class="text-center">4</td>
                                <td>E_4</td>
                                <td>ANTON PRIADI</td>
                                <td>Project Manager</td>
                                <td class="text-center">10 Februari 2016</td>
                                <td class="text-center"><span class="contract_status">Contracted</span></td>
                                <td class="text-center">

                                    <a href="<?php echo base_url(); ?>view/report_employee_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    &nbsp;&nbsp;
                                </td> 
                                </tr> 


                                <tr>
                                    <td class="text-center">5</td>
                                <td>E_5</td>
                                <td>I KETUT ADI PUJA ASTAWA</td>
                                <td>SCM Manager</td>
                                <td class="text-center">13 Februari 2016</td>
                                <td class="text-center"><span class="permanent_status">Permanent</span></td>
                                <td class="text-center">

                                    <a href="<?php echo base_url(); ?>view/report_employee_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    &nbsp;&nbsp;
                                </td> 
                                </tr> 

                                <tr>
                                    <td class="text-center">6</td>
                                <td>E_6</td>
                                <td>SUBARI.M</td>
                                <td>HRGA Manager</td>
                                <td class="text-center">21 Februari 2016</td>
                                <td class="text-center"><span class="resigned_status">Resigned</span></td>
                                <td class="text-center">

                                    <a href="<?php echo base_url(); ?>view/report_employee_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    &nbsp;&nbsp;
                                </td> 
                                </tr> 

                                <tr>
                                    <td class="text-center">7</td>
                                <td>E_7</td>
                                <td>SUNARDI</td>
                                <td>IT Manager</td>
                                <td class="text-center">1 Maret 2016</td>
                                <td class="text-center"><span class="permanent_status">Permanent</span></td>
                                <td class="text-center">

                                    <a href="<?php echo base_url(); ?>view/report_employee_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    &nbsp;&nbsp;
                                    </a>

                                </td> 
                                </tr> 


                                <tr>
                                    <td class="text-center">8</td>
                                <td>E_8</td>
                                <td>BAMBANG IRAWAN</td>
                                <td>F & A Manager</td>
                                <td class="text-center">22 April 2016</td>
                                <td class="text-center"><span class="permanent_status">Permanent</span></td>
                                <td class="text-center">

                                    <a href="<?php echo base_url(); ?>view/report_employee_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    &nbsp;&nbsp;
                                </td> 
                                </tr> 

                                <tr>
                                    <td class="text-center">9</td>
                                <td>E_9</td>
                                <td>CHAIRUL AZWAN</td>
                                <td>Operator Produksi</td>
                                <td class="text-center">25 April 2016</td>
                                <td class="text-center"><span class="contract_status">Contracted</span></td>
                                <td class="text-center">

                                    <a href="<?php echo base_url(); ?>view/report_employee_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    &nbsp;&nbsp;
                                 </td>
                                </tr> 


                                <tr>
                                    <td class="text-center">10</td>
                                <td>E_10</td>
                                <td>AHMAD SARURI</td>
                                <td>Assistant Manager</td>
                                <td class="text-center">16 Mei 2016</td>
                                <td class="text-center"><span class="contract_status">Contracted</span></td>
                                <td class="text-center">

                                    <a href="<?php echo base_url(); ?>view/report_employee_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
                                    &nbsp;&nbsp;
                                 </td>
                                </tr>
                            </table>


                            <!--<div ng-show="false">{{jumlah = (employee | departemen:depart | nama:search | dateRange:dari:to ).length }}</div>-->

                            <div class="col-xs-11 col-sm-12 col-md-12"><br>
                                <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                                    <ul class="pagination">
                                        <li class="paginate_button previous disabled" id="example1_previous"><a href="#" aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a></li>
                                        <li class="paginate_button active"><a href="#" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a></li>
                                        <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a></li>
                                        <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" tabindex="0">3</a></li>
                                        <li class="paginate_button next" id="example1_next"><a href="#" aria-controls="example1" data-dt-idx="7" tabindex="0">Next</a></li>
                                    </ul></div>
                            </div>
                        </div>					
                </div>
            </div>
        </div>

    </section>
</div>
<!--DEPARTMENT-->


<!--POSITION-->


<!--DEPARTMENT-->






<script type="text/javascript">
    $(document).ready(function() {
        $(".departemen").select2();
    });

    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
        $scope.sortType = 'no'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order
        $scope.search = '';
        $scope.limit = 25;
        $scope.dari = "";
        $scope.to = "";
        $scope.depart = "";
        $scope.jumlah = 0;


        $scope.employee = <?php echo $employee ?>;


        $scope.departemen = [
            {nama: "Purchasing"},
            {nama: "Research"},
            {nama: "HRD"}
        ];

        $scope.reset = function() {
            $scope.depart = "";
        }


        $scope.currentPage = 1;
        $scope.totalItems = $scope.employee.length;
        $scope.numPerPage = $scope.limit;

        $scope.limitPage = function() {
            $scope.numPerPage = $scope.limit;
            if ($scope.currentPage * $scope.numPerPage > $scope.employee.length) {
                $scope.currentPage = 1;
            }
        };

        $scope.lastPage = function() {
            $scope.currentPage = $scope.pageCount();
        };

        $scope.firstPage = function() {
            $scope.currentPage = 1;
        };

        $scope.nextPage = function() {

            if ($scope.currentPage < $scope.pageCount()) {
                $scope.currentPage++;
            }
        };

        $scope.jumlahPerpage = function(value) {
            $scope.numPerPage = value;
        }

        $scope.prevPage = function() {
            if ($scope.currentPage > 1) {
                $scope.currentPage--;
            }
        };

        $scope.pageCount = function() {
            return Math.ceil($scope.jumlah / $scope.numPerPage);
        };

        $scope.checkAll = function() {
            angular.forEach($scope.employee, function(item) {
                item.Selected = $scope.selectAll;
            });
        };

    });

    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }


    app.filter("dateRange", function() {
        return function(items, dari, to) {
            if (dari.length == 0) {
                var dari = +new Date("1980-01-01");
            } else {
                var dari = +new Date(dari);
            }

            if (to.length == 0) {
                var to = +new Date();
            } else {
                var to = +new Date(to);
            }
            var df = dari;
            var dt = to;
            var arrayToReturn = [];
            for (var i = 0; i < items.length; i++) {
                var tf = +new Date(items[i].join);
                if ((tf > df && tf < dt) || (tf == dt)) {
                    arrayToReturn.push(items[i]);
                }
            }

            return arrayToReturn;
        };

    });


    app.filter("paging", function() {
        return function(items, limit, currentPage) {
            if (typeof limit === 'string' || limit instanceof String) {
                limit = parseInt(limit);
            }

            var begin, end, index;
            begin = (currentPage - 1) * limit;
            end = begin + limit;
            var arrayToReturn = [];
            for (var i = 0; i < items.length; i++) {
                if (begin <= i && i < end) {
                    arrayToReturn.push(items[i]);
                }
            }
            return arrayToReturn;
        };
    });

    app.filter("departemen", function() {
        return function(items, depart) {
            if (depart.length == 0) {
                return items;
            }
            var arrayToReturn = [];
            for (var i = 0; i < items.length; i++) {
                if (items[i].dept == depart) {
                    arrayToReturn.push(items[i]);
                }
            }
            return arrayToReturn;
        };
    });

    app.filter("nama", function() {
        return function(items, search) {
            if (search.length == 0) {
                return items;
            }
            var arrayToReturn = [];
            for (var i = 0; i < items.length; i++) {
                if (items[i].name.toUpperCase().indexOf(search.toUpperCase()) != -1) {
                    arrayToReturn.push(items[i]);
                }
            }
            return arrayToReturn;
        };
    });


</script>