<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"> EMPLOYEE</a></li>
            <li class="active">List Employee Request</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
					<div class="box-header">
                        <h3 class="box-title">EMPLOYEE | List Employee Request</h3>
                    </div>
					<div class="col-md-12 col-xs-12 col-sm-12" style="margin:20px;0px;20px;0px;">
						<div class="col-md-4">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left">Search</div>
							<div class="col-md-8 col-xs-12 col-sm-12 "><input type="text" class="form-control" placeholder="Enter ..." ng-model="search"/></div>
						</div>
					</div>
                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                                <tr>
                                    <th class="tableNo">No</th>
									<th>No Request</th>
									<th>Date</th>
									<th>Position</th>
									<th>Departement</th>
									<th>Qty</th>
									<th>Details</th>
                                </tr>
								<tr>
									<td class="tableNo">1</td>
									<td>Req_1</td>
									<td>10 Feb 2016</td>
									<td>Manager</td>
									<td>Production</td>
									<td>1</td>
									<td><a href="#"><i class="fa fa-search"></a></td>
                                </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
