<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper" ng-app="sortApp" ng-controller="mainController">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"></i> Leave</a></li>
            <li class="active">List Of Leave Type</li>
            <li class="active">List</li>
        </ol>
    </section>
	
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
					<div class="box-header">
                        <h3 class="box-title">Leave | List Of Leave Type</h3>
                        <hr>
                    </div>
                    <div class="box-body"> <!-- BEGIN OF SEARCH -->
                    	<div class="col-md-12">
                        <div class="row form-group">
                            <label for="searchby" class="col-md-3">Search By :</label>
                        </div>
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Code</option>
                                        <option>Name</option>
                                        <option>Allocation</option>
                                        
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-2 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Code</option>
                                        <option>Name</option>
                                        <option>Allocation</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Code</option>
                                        <option>Name</option>
                                        <option>Allocation</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="operator">Operator</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <input type="radio" name="operator" class="flat-red" checked="true" value="AND"/> AND
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="operator" class="flat-red" value="OR"/> OR
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div>
                            <div class="col-xs-12 col-sm-8 col-md-4">
                                <a href="">
                                    <button type="button" class="btn btn-info btn-block"><i class="fa fa-search"></i>&nbsp;Search</button>
                                </a>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12">&nbsp;</div>
                        <!-- END OF SEARCH -->
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <a href="<?php echo base_url('add/leave_type'); ?>">
                                    <button type="button" class="btn btn-success btn-block"> New</button>
                                </a>
                            </div>
                        </div>
					<!--<div class="col-md-6 col-xs-12 col-sm-12" style="margin-top:20px;margin-bottom:20px;">
						<div class="col-md-4">
							<a href="<?php echo base_url(); ?>add/leave_type"><input type="button" class="btn btn-block btn-success" value="New"></a>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 col-sm-12" style="margin-top:20px;margin-bottom:20px;">
						<div class="col-md-8">
							<div class="col-md-4 col-xs-12 col-sm-12 pull-left"></div>
							<i><div class="col-md-8 col-xs-12 col-sm-12 pull-left"><input type="text" class="form-control" placeholder="Search" ng-model="search"/></div></i>
							<div class="col-md-3">
							<a href="<?php echo base_url(); ?>add/leave_type"><input type="button" class="btn btn-block btn-success" style="background-color:#316CFF;" value="GO"></a>
						</div>
						</div>
					</div>-->
					<div class="col-md-12">&nbsp;</div>
                      <div class="col-md-12">
                        <table class="table table-bordered table-striped">
                            
                                <tr class="success">
                                    <th class="text-center">No</th>
                                    <th class="text-center">Code</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Allocation</th>
                                    <th class="text-center">Action</th>
                                </tr>
								<tbody>
									<tr>
										<td align="center">1</td>
										<td>C_1</td>
										<td>Melahirkan</td>
										<td align="center">90</td>
										<td class="action" align="center">
										   <a href="<?php echo base_url()."edit/leave_type"  ?>"><i class="fa fa-pencil"></i></a>
										</td>
									</tr>
									<tr >
										<td align="center">2</td>
										<td>C_2</td>
										<td>Kelahiran Anak</td>
										<td align="center">2</td>
										<td class="action" align="center">
										   <a href="<?php echo base_url()."edit/leave_type"  ?>"><i class="fa fa-pencil"></i></a>
										</td>
									</tr>
									<tr >
										<td align="center">3</td>
										<td>C_3</td>
										<td>Nikah</td>
										<td align="center">3</td>
										<td class="action" align="center">
										   <a href="<?php echo base_url()."edit/leave_type"  ?>"><i class="fa fa-pencil"></i></a>
										</td>
									</tr>
									<tr >
										<td align="center">4</td>
										<td>C_4</td>
										<td>Nikah Anak</td>
										<td align="center">2</td>
										<td class="action" align="center">
										   <a href="<?php echo base_url()."edit/leave_type"  ?>"><i class="fa fa-pencil"></i></a>
										</td>
									</tr>
									<tr >
										<td align="center">5</td>
										<td>C_5</td>
										<td>Keagamaan</td>
										<td align="center">2</td>
										<td class="action" align="center">
										   <a href="<?php echo base_url()."edit/leave_type"  ?>"><i class="fa fa-pencil"></i></a>
										</td>
									</tr>
									<tr >
										<td align="center">6</td>
										<td>C_6</td>
										<td>Keluarga Meninggal</td>
										<td align="center">2</td>
										<td class="action" align="center">
										   <a href="<?php echo base_url()."edit/leave_type"  ?>"><i class="fa fa-pencil"></i></a>
										</td>
									</tr>
									<tr >
										<td align="center">7</td>
										<td>C_7</td>
										<td>Saudara Kandung Meninggal</td>
										<td align="center">1</td>
										<td class="action" align="center">
										   <a href="<?php echo base_url()."edit/leave_type"  ?>"><i class="fa fa-pencil"></i></a>
										</td>
									</tr>
									<!--<tr >
										<td align="center">8</td>
										<td>C_8</td>
										<td>Meeting dengan client</td>
										<td align="center">3</td>
										<td class="action" align="center">
										   <a href="<?php echo base_url()."edit/leave_type"  ?>"><i class="fa fa-pencil"></i></a>
										</td>
									</tr>
									<tr >
										<td align="center">9</td>
										<td>C_9</td>
										<td>Kegiatan Kuliah</td>
										<td align="center">2</td>
										<td class="action" align="center">
										   <a href="<?php echo base_url()."edit/leave_type"  ?>"><i class="fa fa-pencil"></i></a>
										</td>
									</tr>
									<tr >
										<td align="center">10</td>
										<td>C_2</td>
										<td>Acara Keluarga</td>
										<td align="center">3</td>
										<td class="action" align="center">
										   <a href="<?php echo base_url()."edit/leave_type"  ?>"><i class="fa fa-pencil"></i></a>
										</td>
									</tr>-->
								</tbody>
                        </table>
                    </div>
			                    		<ul class="pagination pull-right" >
			    						  <li><a href="#">Previous</a></li>
			    						  <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
			    						  <li><a href="#">2</a></li>
			    						  <li><a href="#">3</a></li>
			    						  <li><a href="#">Next</a></li>
			    						</ul>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    
    

</script>
