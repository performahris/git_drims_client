<script>
     $(function() {
            $( "#datepicker1" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
        $(function() {
            $( "#datepicker2" ).datepicker({dateFormat:'yy-mm-dd', changeMonth: true, changeYear: true});
        });
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>HR</a></li>
            <li><a href="#"></i>Booking Room</a></li>
            <li><a href="#"></i>Released</a></li>
            <li class="active">List</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
		
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="box box-primary">
		<div class="box-header">
            <h3 class="box-title">Booking Room | Released | List</h3>
			<hr>
		</div>
		<div class="box-body"> 
		<!-- BEGIN OF SEARCH -->
                         <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-8 col-md-8">
                                    <label for="searchby" class="col-md-12">Search By :</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Event</option>
                                        <option>Date From</option>
                                        <option>Date To</option>
                                        <option>Room Name</option>
                                        <option>PIC</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Event</option>
                                        <option>Date From</option>
                                        <option>Date To</option>
                                        <option>Room Name</option>
                                        <option>PIC</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <select class="form-control">
                                        <option>-- Select Field Name --</option>
                                        <option>Event</option>
                                        <option>Date From</option>
                                        <option>Date To</option>
                                        <option>Room Name</option>
                                        <option>PIC</option>
                                        <option>Status</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <label for="code">Value</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" name=""/>
                                </div>
                            </div>
                            <div class="row form-group">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="operator">Operator</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <input type="radio" name="operator" class="flat-red" checked="true" value="AND"/> AND
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="operator" class="flat-red" value="OR"/> OR
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">&nbsp;</div>
                            <div class="col-xs-12 col-sm-8 col-md-4">
                                <a href="">
                                    <button type="button" class="btn btn-info btn-block"><i class="fa fa-search" data-toggle="tooltip" title="View">&nbsp;</i>Search</button>
                                </a>
                            </div>
                            </div>
                        </div>
                        <!--END OF SEARCH-->

						<!--
						<div class="col-md-12 col-xs-12 col-sm-12">
								<select ng-model="depart" class="departemen" >
									<option value= "">List Of Department</option>
									<option value="{{ dept.nama }}" ng-repeat="dept in departemen">{{ dept.nama }}</option>
								</select>
								
								<select ng-model="depart" class="departemen" >
									<option value= "">List Of EmployeeID</option>
									<option value="{{ emp.ID }}" ng-repeat="emp in employee">{{ emp.ID }}</option>
								</select> 
								
								<select ng-model="depart" class="departemen" >
									<option value= "">List Of Joined Date</option>
									<option value="{{ joi.date }}" ng-repeat="joi in joined">{{ joi.date }}</option>
								</select> 
						</div>
						-->
						<div class="box-body table-responsive " style="width:100%">
                            <table class="table table-striped table-bordered">
                                <tr class="success">
                                    <th >No</th> 
									<th >Event</th>
									<th >
										<!--<a href="#" ng-click="sortType = 'name'; sortReverse = !sortReverse">-->
										<center>Date From</center>
										<span ng-show="sortType == 'name' && !sortReverse"></span>
										<span ng-show="sortType == 'name' && sortReverse"></span>
										<!--</a>-->
									</th>
									
									<th >
										<!--<a href="#" ng-click="sortType = 'join'; sortReverse = !sortReverse">-->
										<center>Date To</center>
										<span ng-show="sortType == 'join' && !sortReverse"></span>
										<span ng-show="sortType == 'join' && sortReverse"></span>
										<!--</a>-->
									</th>
									
									<th >
										<!--<a<a href="#" ng-click="sortType = 'join'; sortReverse = !sortReverse">-->
										<center>Room Name</center>
										<span ng-show="sortType == 'join' && !sortReverse"></span>
										<span ng-show="sortType == 'join' && sortReverse"></span>
										<!--</a>-->
									</th>
									
									
									<th >
										<!--<a href="#" ng-click="sortType = 'join'; sortReverse = !sortReverse">-->
										<center>PIC</center>
										<span ng-show="sortType == 'join' && !sortReverse"></span>
										<span ng-show="sortType == 'join' && sortReverse"></span>
										<!--</a>-->
									</th>
									
									<th >
										<!--<a href="#" ng-click="sortType = 'join'; sortReverse = !sortReverse">-->
										<center>Status</center>
										<span ng-show="sortType == 'join' && !sortReverse"></span>
										<span ng-show="sortType == 'join' && sortReverse"></span>
										<!--</a>-->
									</th>
									<th >
										<!--<a href="#" ng-click="sortType = 'join'; sortReverse = !sortReverse">-->
										<center>Action</center>
										<span ng-show="sortType == 'join' && !sortReverse"></span>
										<span ng-show="sortType == 'join' && sortReverse"></span>
										<!--</a>-->
									</th>
									
									<!--<th><input type="checkbox" ng-model="selectAll" ng-click="checkAll()" /></th>-->
                            </tr>
							<tr ng-repeat="em in employee | orderBy:sortType:sortReverse ">
                                    <!--<td>1</td>
									<td>Debi</td>
									<td>Sani</td>
									<td>nnns</td>
									<td>ahdsahd</td>
									
									<td>{{em.employee_date_of_birth |date:'dd MMMM yyyy'}}</td>-->
									
									<!--<td><input type="checkbox" ng-model="em.Selected" /></td>-->
                            </tr>
									
							<tr>
									<td>1</td>
									<td>Meeting HRD</td>
									<td>18 Aug 2014 08:30</td>
									<td>19 Aug 2014 12:00</td>
									<td>Ruangan B Lt.1</td>
									<td>SUDARMONO</td>
									<td><center><span class="approve_status">Approved</span></center></td>
									<td class="text-center">
										<a href="<?php echo base_url(); ?>view/booking_room_released_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
									</td> 
							</tr> 
								 	
							<tr>
									<td>2</td>
									<td>Meeting Drims</td>
									<td>21 May 2016 09:00</td>
									<td>21 May 2016 17:00</td>
									<td>Ruangan A Lt.1</td>
									<td>ARBONDES</td>
									<td><center><span class="reject_status">Rejected</span></center></td>
									<td class="text-center">
										<a href="<?php echo base_url(); ?>view/booking_room_released_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
									</td> 
							</tr> 
					
							<tr>
									<td>3</td>
									<td>Training Vacancy</td>
									<td>17 Aug 2014 08:30</td>
									<td>17 Aug 2014 16:00</td>
									<td>Ruangan C Lt.1</td>
									<td>ISKANDAR</td>
									<td><center><span class="approve_status">Approved</span></center></td>
									<td class="text-center">
										<a href="<?php echo base_url(); ?>view/booking_room_released_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
									</td> 
							</tr>
					
					
							<tr>
									<td>4</td>
									<td>Interview Vacancy</td>
									<td>21 Sep 2014 08:30</td>
									<td>22 Sep 2014 16:00</td>
									<td>Ruangan C Lt.2</td>
									<td>ANTON PRIADI</td>
									<td><center><span class="approve_status">Approved</span></center></td>
									<td class="text-center">
										<a href="<?php echo base_url(); ?>view/booking_room_released_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
									</td> 
							</tr> 
					
								
							<tr>
									<td>5</td>
									<td>Evaluasi Pemasaran</td>
									<td>5 Oct 2015 10:30</td>
									<td>5 Oct 2015 16:00</td>
									<td>Ruangan B Lt.2</td>
									<td>I KETUT ADI PUJA ASTAWA	</td>
									<td><center><span class="approve_status">Approved</span></center></td>
									<td class="text-center">
										<a href="<?php echo base_url(); ?>view/booking_room_released_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
									</td> 
							</tr> 
					
							<tr>
									<td>6</td>
									<td>Interview Vacancy</td>
									<td>21 Sep 2015 08:30</td>
									<td>22 Sep 2015 16:00</td>
									<td>Ruangan C Lt.1</td>
									<td>SUBARI.M</td>
									<td><center><span class="approve_status">Approved</span></center></td>
									<td class="text-center">
										<a href="<?php echo base_url(); ?>view/booking_room_released_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
									</td> 
							</tr> 

							<tr>
									<td>7</td>
									<td>Meeting Drims</td>
									<td>21 Jan 2016 09:00</td>
									<td>21 Jan 2016 17:00</td>
									<td>Ruangan A Lt.1</td>
									<td>SUNARDI </td>
									<td><center><span class="reject_status">Rejected</span></center></td>
									<td class="text-center">
										<a href="<?php echo base_url(); ?>view/booking_room_released_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
									</td> 
							</tr> 
					
					
							<tr>
									<td>8</td>
									<td>Meeting DS</td>
									<td>9 Jan 2016 09:00</td>
									<td>10 Jan 2016 17:00</td>
									<td>Ruangan A Lt.2</td>
									<td>BAMBANG IRAWAN 	</td>
									<td><center><span class="reject_status">Rejected</span></center></td>
									<td class="text-center">
										<a href="<?php echo base_url(); ?>view/booking_room_released_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
									</td> 
							</tr> 

							<tr>
									<td>9</td>
									<td>Meeting Drims</td>
									<td>15 Apr 2016 10:00</td>
									<td>15 Apr 2016 17:00</td>
									<td>Ruangan B Lt.1</td>
									<td>CHAIRUL AZWAN</td>
									<td><center><span class="reject_status">Rejected</span></center></td>
									<td class="text-center">
										<a href="<?php echo base_url(); ?>view/booking_room_released_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
									</td>
							</tr> 
								 
								 
							<tr>
									<td>10</td>
									<td>Meeting HRD</td>
									<td>21 Jan 2016 09:00</td>
									<td>21 Jan 2016 17:00</td>
									<td>Ruangan A Lt.1</td>
									<td>AHMAD SARURI</td>
									<td><center><span class="reject_status">Rejected</span></center></td>
									<td class="text-center">
										<a href="<?php echo base_url(); ?>view/booking_room_released_view"><i class="fa fa-search" data-toggle="tooltip" title="View"></i></a>
									</td>
							</tr>
                        </table>
						
						
						<!--<div ng-show="false">{{jumlah = (employee | departemen:depart | nama:search | dateRange:dari:to ).length }}</div>-->
						
                    		<div class="col-xs-11 col-sm-12 col-md-12"><br>
                            	<div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
				   <ul class="pagination">
					<li class="paginate_button previous disabled" id="example1_previous"><a href="#" aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a></li>
					<li class="paginate_button active"><a href="#" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a></li>
					<li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a></li>
					<li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" tabindex="0">3</a></li>
					<li class="paginate_button next" id="example1_next"><a href="#" aria-controls="example1" data-dt-idx="7" tabindex="0">Next</a></li>
				   </ul></div>
                        	</div>
                    </div>					
                </div>
            </div>
        </div>

    </section>
</div>


<script type="text/javascript">
    $(document).ready(function() {
      $(".departemen").select2();
    });
    
    var app = angular.module('sortApp', ['ui.bootstrap', 'ngResource']);

    app.controller('mainController', function($scope) {
      $scope.sortType     = 'no'; // set the default sort type
      $scope.sortReverse  = false;  // set the default sort order
      $scope.search   = ''; 
      $scope.limit =25;
      $scope.dari="";
      $scope.to=""; 
      $scope.depart="";  
      $scope.jumlah = 0;
                   
      
      $scope.employee = <?php echo $employee ?>;
      
      
      $scope.departemen = [
        {nama : "Purchasing"},
        {nama : "Research"},
        {nama : "HRD"}
        ];
        
        $scope.reset = function () {
            $scope.depart =  "";     
        } 
        
        
       $scope.currentPage = 1;  
       $scope.totalItems = $scope.employee.length;  
       $scope.numPerPage = $scope.limit;    
       
       $scope.limitPage = function() {
         $scope.numPerPage = $scope.limit;
         if($scope.currentPage * $scope.numPerPage > $scope.employee.length){
            $scope.currentPage = 1;   
         }
      };
       
       $scope.lastPage = function() {      
         $scope.currentPage=$scope.pageCount();
      };
      
      $scope.firstPage = function() {
         $scope.currentPage=1;
      };
       
       $scope.nextPage = function() {
        
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
      };
      
      $scope.jumlahPerpage = function (value) {
        $scope.numPerPage = value;
      } 
      
      $scope.prevPage = function() {
        if ($scope.currentPage > 1) {
          $scope.currentPage--;
        }
      };
      
       $scope.pageCount = function() {
        return Math.ceil($scope.jumlah/$scope.numPerPage);
      };  
      
      $scope.checkAll = function () {
        angular.forEach($scope.employee, function (item) {
            item.Selected = $scope.selectAll;
        });
      };
          
    });
    
    function MyCtrl($scope) {
        $scope.dateInput = new Date();
        $scope.definedDateFormats = ['medium', 'short', 'fullDate', 'longDate', 'mediumDate', 'shortDate', 'shortTime'];
    }
    

    app.filter("dateRange", function() {      
      return function(items, dari, to) {
        if(dari.length==0){
            var dari = +new Date("1980-01-01");
        }else{
            var dari = +new Date(dari);
        }
        
        if(to.length==0){
            var to = +new Date();
        }else{
            var to = +new Date(to);
        }
        var df = dari;
        var dt = to ;
        var arrayToReturn = [];        
        for (var i=0; i<items.length; i++){
            var tf = +new Date(items[i].join);
            if ((tf > df && tf < dt) || (tf==dt) )  {
                arrayToReturn.push(items[i]);
            }
        }
        
        return arrayToReturn;
      };
        
    });
    
    
    app.filter("paging", function() {      
      return function(items, limit, currentPage) {
        if (typeof limit === 'string' || limit instanceof String){
           limit = parseInt(limit);
         }
         
         var begin, end, index;  
         begin = (currentPage - 1) * limit;  
         end = begin + limit;
         var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (begin <= i && i < end )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    
    app.filter("departemen", function() {      
      return function(items, depart) {
      if(depart.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].dept == depart )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    
    app.filter("nama", function() {      
      return function(items, search) {
      if(search.length==0){
        return items;
      }
      var arrayToReturn = [];   
         for (var i=0; i<items.length; i++){
            if (items[i].name.toUpperCase().indexOf(search.toUpperCase()) != -1 )  {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
      };   
    });
    

</script>