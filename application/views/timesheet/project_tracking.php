<link href="../../asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<div class="content-wrapper">
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Project</a></li>
            <li class="active">Tracking</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Project | Tracking</h3>
                    </div>
                    <div class="box-body col-md-12 col-xs-12 col-sm-12">
                        <table class="table table-condensed">
                        	<tr>
                			<td style="width:15%"> Project Id</td>
                            <td style="width:1%"> :  </td>
                            <td> P01  </td>
                		</tr>
                          <tr>
                			<td> Customer Name</td>
                			<td> :  </td>
                			<td> Derry  </td>
                        </tr>
                        <tr>
                			<td> Project of Type</td>
		                	<td> :  </td>
        		        	<td> Project Type  </td>
                		</tr>
                	    <tr>
                			<td> Schedule Positioning</td>
		                	<td> :  </td>
        		        	<td>   </td>
                        </tr>
                        <tr> <td colspan="4"></td></tr>
                       </table>
                       </div>
                    	<div class="box-body">
                       <table>
                        	<tr>
                            	<td style="width:7%">&nbsp;</td>
                				<td style="width:4%">Start Date : 01-01-2016</td>
                   				<td width="12%">End Date : 03-03-2016</td>
                                <td>&nbsp;</td>
                    		</tr>
                        	<tr>
                            	<td style="width:7%">&nbsp;</td>
                                <td colspan="2">
                                	<table frame="box" width="600px">
                    					<td width="25%" style="background-color: blue !important;" >&nbsp;</td>
                                <td></td>
                    					<td width="75%" style="background-color: lime !important;" ></td>
                					</table>
                                </td>
                                <td style="width:1%"></td>
                                <td> 30% </td>
                        	</tr>
                        </table>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-striped">
            				<tr align="center" class="success">
                				<td rowspan="3">No</td>
                				<td rowspan="3">Item of Work</td>
                				<td rowspan="3">Start</td>
                				<td rowspan="3">End</td>
                				<td rowspan="3">Duration</td>
                				<td colspan="36">Progress Bar</td>
            				</tr>
            				<tr align="center" class="success">
                				<td colspan="30">Januari</td>
                				<td colspan="6">Februari</td>
            				</tr>
            				<tr align="center" class="success">
                				<td>1</td>
				                <td>2</td>
				                <td>3</td>
				                <td>4</td>
				                <td>5</td>
				                <td>6</td>
				                <td>7</td>
				                <td>8</td>
				                <td>9</td>
				                <td>10</td>
				                <td>11</td>
				                <td>12</td>
				                <td>13</td>
				                <td>14</td>
				                <td>15</td>
				                <td>16</td>
				                <td>17</td>
				                <td>18</td>
				                <td>19</td>
				                <td>20</td>
				                <td>21</td>
				                <td>22</td>
				                <td>23</td>
				                <td>24</td>
				                <td>25</td>
				                <td>26</td>
				                <td>27</td>
				                <td>28</td>
				                <td>29</td>
				                <td>30</td>
				                <td>1</td>
				                <td>2</td>
				                <td>3</td>
				                <td>4</td>
				                <td>5</td>
				                <td>6</td>
				            </tr>
				            <tr align="center">
				                <td>I</td>
				                <td>xxx</td>
				                <td>dd-mm-yy</td>
				                <td>dd-mm-yy</td>
				                <td>xx</td>
				                <td colspan="15" style="background-color: brown;"></td>
				                <td colspan="19"></td>
				                <td colspan="2"></td>
				            </tr>
				            <tr align="center">
			    	            <td>1</td>
				                <td>xxx</td>
				                <td>dd-mm-yy</td>
				                <td>dd-mm-yy</td>
				                <td>xx</td>
				                <td colspan="6" style="background-color: orange;"></td>
				                <td colspan="9"></td>
				                <td colspan="19"></td>
				                <td colspan="2"></td>
				            </tr>
				            <tr align="center">
				                <td>a</td>
				                <td>xxx</td>
				                <td>dd-mm-yy</td>
				                <td>dd-mm-yy</td>
				                <td>xx</td>
				                <td colspan="2" style="background-color: yellow;"></td>
				                <td colspan="13"></td>
				                <td colspan="19"></td>
				                <td colspan="2"></td>
				            </tr>
				            <tr align="center">
				                <td>b</td>
				                <td>xxx</td>
				                <td>dd-mm-yy</td>
				                <td>dd-mm-yy</td>
				                <td>xx</td>
				                <td colspan="2"></td>
				                <td colspan="2" style="background-color: yellow;"></td>
				                <td colspan="11"></td>
				                <td colspan="19"></td>
				                <td colspan="2"></td>
				            </tr>
				            <tr align="center">
				                <td>c</td>
				                <td>xxx</td>
				                <td>dd-mm-yy</td>
				                <td>dd-mm-yy</td>
				                <td>xx</td>
				                <td colspan="4"></td>
				                <td colspan="1" style="background-color: yellow;"></td>
				                <td colspan="10"></td>
				                <td colspan="19"></td>
				                <td colspan="2"></td>
				            </tr>
				            <tr align="center">
				                <td>d</td>
				                <td>xxx</td>
				                <td>dd-mm-yy</td>
				                <td>dd-mm-yy</td>
				                <td>xx</td>
  					            <td colspan="5"></td>
				                <td colspan="1" style="background-color: yellow;"></td>
				                <td colspan="9"></td>
				                <td colspan="19"></td>
				                <td colspan="2"></td>
				            </tr>
            				<tr align="center">
				                <td>2</td>
				                <td>xxx</td>
				                <td>dd-mm-yy</td>
				                <td>dd-mm-yy</td>
				                <td>xx</td>
				                <td colspan="2"></td>
				                <td colspan="9" style="background-color: orange;"></td>
				                <td colspan="4"></td>
				                <td colspan="19"></td>
				                <td colspan="2"></td>
				            </tr>
				            <tr align="center">
				                <td>a</td>
				                <td>xxx</td>
				                <td>dd-mm-yy</td>
				                <td>dd-mm-yy</td>
				                <td>xx</td>
				                <td colspan="2"></td>
				                <td colspan="4" style="background-color: yellow;"></td>
				                <td colspan="9"></td>
				                <td colspan="19"></td>
				                <td colspan="2"></td>
				            </tr>
				            <tr align="center">
				                <td>b</td>
				                <td>xxx</td>
				                <td>dd-mm-yy</td>
				                <td>dd-mm-yy</td>
				                <td>xx</td>
				                <td colspan="6"></td>
				                <td colspan="5" style="background-color: yellow;"></td>
				                <td colspan="4"></td>
				                <td colspan="19"></td>
				                <td colspan="2"></td>
				            </tr>
				            <tr align="center">
				                <td>3</td>
				                <td>xxx</td>
                				<td>dd-mm-yy</td>
				                <td>dd-mm-yy</td>
				                <td>xx</td>
				                <td colspan="11"></td>
				                <td colspan="4" style="background-color: orange;"></td>
				                <td colspan="19"></td>
				                <td colspan="2"></td>
				            </tr>
				            <tr align="center">
				                <td>a</td>
				                <td>xxx</td>
				                <td>dd-mm-yy</td>
				                <td>dd-mm-yy</td>
				                <td>xx</td>
				                <td colspan="11"></td>
				                <td colspan="3" style="background-color: yellow;"></td>
				                <td colspan="1"></td>
				                <td colspan="19"></td>
				                <td colspan="2"></td>
				            </tr>
				            <tr align="center">
				                <td>b</td>
				                <td>xxx</td>
				                <td>dd-mm-yy</td>
				                <td>dd-mm-yy</td>
				                <td>xx</td>
				                <td colspan="13"></td>
				                <td colspan="2" style="background-color: yellow;"></td>
				                <td colspan="2" style="background-color: red;"></td>
				                <td colspan="17"></td>
				                <td colspan="2"></td>
					            </tr>
				            <tr align="center">
				                <td>II</td>
				                <td>xxx</td>
				                <td>dd-mm-yy</td>
				                <td>dd-mm-yy</td>
				                <td>xx</td>
				                <td colspan="15"></td>
				                <td colspan="19" style="background-color: brown;"></td>
				                <td colspan="2" style="background-color: red;"></td>
				            </tr>
				            <tr align="center">
				                <td>1</td>
				                <td>xxx</td>
				                <td>dd-mm-yy</td>
				                <td>dd-mm-yy</td>
				                <td>xx</td>
			    	            <td colspan="15"></td>
				                <td colspan="19" style="background-color: orange;"></td>
				                <td colspan="2" style="background-color: red;"></td>
				            </tr>
				            <tr align="center">
				                <td>a</td>
				                <td>xxx</td>
				                <td>dd-mm-yy</td>
				                <td>dd-mm-yy</td>
				                <td>xx</td>
				                <td colspan="15"></td>
				                <td colspan="14" style="background-color: yellow;"></td>
				                <td colspan="5"></td>
				                <td colspan="2" style="background-color: red;"></td>
				            </tr>
				            <tr align="center">
				                <td>b</td>
				                <td>xxx</td>
					            <td>dd-mm-yy</td>
					            <td>dd-mm-yy</td>
				                <td>xx</td>
				                <td colspan="15"></td>
				                <td colspan="14"></td>
				                <td colspan="5" style="background-color: brown;"></td>
				                <td colspan="2" style="background-color: red;"></td>
				            </tr>
				            </table>
				            </div>
            				<div class="box-body">
				            <table>
				            	<tr>
				            	<td style="width:5%">
				                	<button class="btn btn-block btn-success btn-xs" onclick="">			Back</button></td>                
                                <td>&nbsp;</td>
					           </tr>
				        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>