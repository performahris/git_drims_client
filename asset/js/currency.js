function FormatCurrency(objNum)
{	
    var num = objNum.value
	var ent, dec;
	
	if (num != '' && num != objNum.oldvalue)
	{
		num = MoneyToNumber(num);
		if (isNaN(num))
        {		
			objNum.value = (objNum.oldvalue)?objNum.oldvalue:'';
        } 
		else 
		{
			var ev = (navigator.appName.indexOf('Netscape') != -1)?Event:event;
			if (ev.keyCode == 190 || !isNaN(num.split('.')[1]))
			{		
                objNum.value = AddCommas(num.split('.')[0])+'.'+num.split('.')[1];
            }
			else
			{	
				objNum.value = AddCommas(num.split('.')[0]);
			}
			objNum.oldvalue = objNum.value;
        }

	}
}
	
function MoneyToNumber(num)
{
	return (num.replace(/,/g, ''));
}

function AddCommas(num)
{
	var deciman
   	var numArr = new String(num).split('').reverse();
   for (i=3;i<numArr.length;i+=3)
    {
		numArr[i]+=',';
    }
    return numArr.reverse().join('');
}
function hargaNetto (harga)
{
	var hasil = AddCommas(harga);
	$('#NetPrice').append(hasil);
}// JavaScript Document

function AddDecimalCommas(num)
{
	var decimal	= new String(num).split('.');
   	var numArr = new String(decimal[0]).split('').reverse();
   	for (i=3;i<numArr.length;i+=3)
   	{
		numArr[i]+=',';
	}
	
	var comma	= new String(.00);
	if(decimal[1]) {
		comma	= ".";
		for (i=0;i<decimal[1].length;i++)
		{
			comma	+= decimal[1][i];
			if(i == 1)
				break;
		}
	}
	console.log(comma);
	
	return numArr.reverse().join('') + comma;
}

//by ahmad pujianto
function cleanCommatoInt($x)
 {
   $y = $x.replace(new RegExp(",", 'g'),"");
   $y = parseInt($y);
   if(isNaN($y)){ $y = 0; }
   return $y;
 }
 
 function cleanCommatoFloat($x)
 {
   $y = $x.replace(new RegExp(",", 'g'),"");
   $y = parseFloat($y);
   if(isNaN($y)){ $y = 0; }
   return $y;
 }
 
 function splitCleanCommaToFloat($x)
 { //alert("aaa ==> "+$x);
   $y = new String($x).split('#'); 
   $y =  $y[0];                                                                           
   $y = $x.replace(new RegExp(",", 'g'),"");
   $y = parseFloat($y);
   if(isNaN($y)){ $y = 0; }
   return $y;
 }
 
 function FormatCurrencyMaxOne(objNum)
 {
    var num = objNum.value
	var ent, dec;
	
	if (num != '' && num != objNum.oldvalue)
	{
		num = MoneyToNumber(num);
		if (isNaN(num))
        {		
			objNum.value = (objNum.oldvalue)?objNum.oldvalue:'';
        } 
		else 
		{
			var ev = (navigator.appName.indexOf('Netscape') != -1)?Event:event;
			if (ev.keyCode == 190 || !isNaN(num.split('.')[1]))
			{		
                objNum.value = AddCommas(num.split('.')[0])+'.'+num.split('.')[1];
            }
			else
			{	
				objNum.value = AddCommas(num.split('.')[0]);
			}
			objNum.oldvalue = objNum.value;
        }

	}
	if(num < 0){ objNum.value = 0; }
	if(num > 1){ objNum.value = 1; }
	//alert("aa"+num);
 }
 
 function FormatCurrencyMax24(objNum)
 {
    var num = objNum.value
	var ent, dec;
	
	if (num != '' && num != objNum.oldvalue)
	{
		num = MoneyToNumber(num);
		if (isNaN(num))
        {		
			objNum.value = (objNum.oldvalue)?objNum.oldvalue:'';
        } 
		else 
		{
			var ev = (navigator.appName.indexOf('Netscape') != -1)?Event:event;
			if (ev.keyCode == 190 || !isNaN(num.split('.')[1]))
			{		
                objNum.value = AddCommas(num.split('.')[0])+'.'+num.split('.')[1];
            }
			else
			{	
				objNum.value = AddCommas(num.split('.')[0]);
			}
			objNum.oldvalue = objNum.value;
        }

	}
	if(num < 0){ objNum.value = 0; }
	if(num > 24){ objNum.value = 1; }
	//alert("aa"+num);
 }
 
 function FormatCurrencyMaxMonth(objNum,month)
 {
    var num = objNum.value
	var ent, dec;
	var bln = parseInt(month);
	
	if (num != '' && num != objNum.oldvalue)
	{
		num = MoneyToNumber(num);
		if (isNaN(num))
        {		
			objNum.value = (objNum.oldvalue)?objNum.oldvalue:'';
        } 
		else 
		{
			var ev = (navigator.appName.indexOf('Netscape') != -1)?Event:event;
			if (ev.keyCode == 190 || !isNaN(num.split('.')[1]))
			{		
                objNum.value = parseInt(num.split('.')[0]);//AddCommas(num.split('.')[0])+'.'+num.split('.')[1];
            }
			else
			{	
				objNum.value = parseInt(num.split('.')[0]);//AddCommas(num.split('.')[0]);
			}
			objNum.oldvalue = objNum.value;
        }

	}
	if(num > bln){ objNum.value = month; }
	//alert("aa"+num);
 }
 
 function openModal(f, w, h) {
    if (w == "" || w == null || w == "undefined")
        w = 650;
    if (h == "" || h == null || h == "undefined")
        h = 650;
    lw = window.showModalDialog(f, "", "help:no;status:no;dialogHeight:" + h + "px;dialogWidth:" + w + "px;center:1;");
 }
 function openModalSmall(f) {
    openModal(f, 625, 550);
  }
 function openModalMed(f) {
    openModal(f, 750);
 }
 function openModalBig(f) {
    openModal(f, 990);
  }
 function openPrint(f) {
    openModal(f, 760, 600);
 }
 //eo by ahmad