$(".employeeCombo").live("change", function() {
	var $combo	= $(this);
	if($combo.parent().parent().attr("parent") == 'table1') {
		var table1	= "#table1";
		var table2	= "#table2";
	} else {
		var table1	= "#table2";
		var table2	= "#table1";
	}
	
	var indexLatest = $(table1 + ' #lastest').index($combo.parent().parent());
	// day
	var $list	= $combo.parent().parent().find("input.input");
	$list.each(function (i) {
		var $obj	= $(this);
		$obj.attr("code", $combo.val())
			.attr("name", "input[" +$obj.attr("code")+ "][" +$obj.attr("pricing")+ "][" +$obj.attr("day")+ "]");
	});
	// subtotal
	var $list	= $combo.parent().parent().find("input.subtotal");
	$list.each(function (i) {
		var $obj	= $(this);
		$obj.attr("code", $combo.val())
			.attr("name", "subtotal[" +$obj.attr("code")+ "][" +$obj.attr("pricing")+ "]");
	});
	// total
	var $list	= $combo.parent().parent().find("input.total");
	$list.each(function (i) {
		var $obj	= $(this);
		$obj.attr("code", $combo.val())
			.attr("name", "total[" +$obj.attr("code")+ "][" +$obj.attr("pricing")+ "]");
	});
	
	var $oCombo = $(table2).find("#lastest:eq("+indexLatest+")").find("select[name^='employeeCombo']");
	if($oCombo.val() != $combo.val()) {
		$oCombo.val($combo.val()).trigger("change");
	}
});

$(".pricingCombo").live("change", function() {
	var $combo	= $(this);
	if($combo.parent().parent().attr("parent") == 'table1') {
		var table1	= "#table1";
		var table2	= "#table2";
	} else {
		var table1	= "#table2";
		var table2	= "#table1";
	}
	
	var indexLatest = $(table1 + ' #lastest').index($combo.parent().parent());
	// day
	var $list	= $combo.parent().parent().find("input.input");
	$list.each(function (i) {
		var $obj	= $(this);
		$obj.attr("pricing", $combo.val())
			.attr("name", "input[" +$obj.attr("code")+ "][" +$obj.attr("pricing")+ "][" +$obj.attr("day")+ "]");
	});
	// subtotal
	var $list	= $combo.parent().parent().find("input.subtotal");
	$list.each(function (i) {
		var $obj	= $(this);
		$obj.attr("pricing", $combo.val())
			.attr("name", "subtotal[" +$obj.attr("code")+ "][" +$obj.attr("pricing")+ "]");
	});
	// total
	var $list	= $combo.parent().parent().find("input.total");
	$list.each(function (i) {
		var $obj	= $(this);
		$obj.attr("pricing", $combo.val())
			.attr("name", "total[" +$obj.attr("code")+ "][" +$obj.attr("pricing")+ "]");
	});
	
	var $oCombo = $(table2).find("#lastest:eq("+indexLatest+")").find("select[name^='pricingCombo']");
	if($oCombo.val() != $combo.val()) {
		$oCombo.val($combo.val()).trigger("change");
	}
});

$("input.input").live("keyup", function() {
	countall($(this).parent().parent().attr("row")+''+$(this).attr("day"), 0, $(this).parent().parent().attr("row"));
});

function countall (id,valid,j) {						
	var $a = $("#input"+id).val();
	var $hitung = 0;
	if($a > 1)
	{
		alert("You may only enter the numbers 1 and 0");
		$("#input"+id).val(valid);
	}else{
		for($i=1;$i<=$endOfDate;$i++)
		{
			var $val 	= null;							
			$val 	= parseInt( $("#input"+j+$i).val());
			if(isNaN($val)){
				$val = parseInt(0);
			}
			$hitung	= $hitung + $val;	
			if($i==15)
			{
				$("#subtotal"+j).val($hitung);	
			}
		}
		$("#alltotal"+j).val($hitung);
	}
}

function isInteger(val)
{
	//alert(val.value);
	if(val==null)
	{
		//alert(val);
		return false;
	}
	if (val.length==0)
	{
		//alert(val);
		return false;
	}
	for (var i = 0; i < val.length; i++) 
	{
		var ch = val.charAt(i)
		if (i == 0 && ch == "-")
		{
			continue
		}
		if (ch < "0" || ch > "9")
		{
			return false
		}
	}
	return true
}
