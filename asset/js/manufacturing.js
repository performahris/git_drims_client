// JavaScript Document
function openModal(f, w, h) {
    if (w == "" || w == null || w == "undefined")
        w = 650;
    if (h == "" || h == null || h == "undefined")
        h = 650;
    lw = window.showModalDialog(f, "", "help:no;status:no;dialogHeight:" + h + "px;dialogWidth:" + w + "px;center:1;");
}
function openModalSmall(f) {
    openModal(f, 600, 500);
}
function openModalMed(f) {
    openModal(f, 750);
}
function openModalBig(f) {
    openModal(f, 990);
}
function openPrint(f) {
    openModal(f, 760, 600);
}